<?php

namespace App\Parsers;

use Illuminate\Contracts\Foundation\Application;

/**
 * Class Factory
 * @package App\Parsers\Company
 * @method Parser company(string $name)
 */
class Factory
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * Factory constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param string $type
     * @param string $name
     * @return Parser
     * @throws \Exception
     */
    public function get($type, $name)
    {
        $parser = $this->app->make(implode('\\', [
            __NAMESPACE__,
            studly_case(str_plural($type)),
            studly_case($name),
        ]));

        if (!$parser instanceof Parser) {
            throw new \Exception('Invalid parser');
        }
        return $parser;
    }

    /**
     * @param string $name
     * @param array  $arguments
     * @return Parser
     */
    public function __call($name, $arguments)
    {
        return call_user_func_array([$this, 'get'], array_merge([$name], $arguments));
    }
}