<?php

namespace App\Parsers;

/**
 * Interface Parser
 * @package App\Parsers
 */
interface Parser
{
    /**
     * @param string $url
     * @return array
     */
    public function parse($url);
}