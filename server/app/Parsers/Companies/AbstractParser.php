<?php

namespace App\Parsers\Companies;

use App\Parsers\Parser;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class AbstractParser
 * @package App\Parsers\Companies
 */
abstract class AbstractParser implements Parser
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $cache = [];

    /**
     * AbstractParser constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $url
     * @return array|bool
     */
    public function parse($url)
    {
        $content = $this->content($url);
        if ($content) {
            $dom = new Crawler($content);
            return $this->data($dom);
        } else {
            return false;
        }
    }

    /**
     * @param Crawler $dom
     * @return array
     */
    abstract protected function data(Crawler $dom);

    /**
     * @param string $url
     * @return string
     */
    protected function content($url)
    {
        if (!array_key_exists($url, $this->cache)) {
            try {
                $response = $this->getHttpClient()->request('GET', $url, [
                    'curl' => [
                        CURLOPT_SSL_VERIFYPEER => false,
                        CURLOPT_SSL_VERIFYHOST => false
                    ]
                ]);

                if ($response->getStatusCode() == 200) {
                    $this->cache[$url] = $response->getBody()->getContents();
                }

                return $this->cache[$url];

            } catch (\Exception $exception) {
                echo 'PARSER ERROR' . chr(10);
                echo $url . chr(10);
                return false;
            }
        }
    }

    /**
     * @return Client
     */
    protected function getHttpClient()
    {
        return $this->client;
    }
}