<?php
/**
 * User: Michael Lazarev <mihailaz.90@gmail.com>
 * Date: 19.10.16
 * Time: 18:04
 */

namespace App\Parsers\Companies;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Optlist
 * @package App\Parsers\Companies
 */
class Optlist extends AbstractParser
{
    /**
     * @param Crawler $dom
     * @return array
     */
    protected function data(Crawler $dom)
    {
        //найти блок с категориями

        //выбрать все ссылки категорий

        //пройти по всем категориям и собрать список компаний (контакты)

        //собрать список товаров

        $el    = $dom->filter('.catalog .products_item .price pr');
        $text  = $el->html();
        $price = trim(preg_replace('/<(\w+)(\b)+(.*)?>.*<\/\1>/si', '', $text));

        return [
            'price' => round(str_replace(",", '.', $price)),
        ];
    }
}