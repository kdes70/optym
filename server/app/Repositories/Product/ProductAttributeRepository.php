<?php

namespace App\Repositories\Product;

use App\Core\Eloquent\AbstractModel;
use App\Core\Eloquent\Repository\AbstractRepository;
use App\Domain\Catalog\Category\Attribute;


class ProductAttributeRepository extends AbstractRepository
{
    /**
     * @return AbstractModel
     */
    public function getModel()
    {
        return new Attribute();
    }
}
