<?php

namespace App\Repositories\Product;

use App\Core\Eloquent\Repository\AbstractRepository;
use App\Domain\Product\Product;
use DB;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class ProductRepository
 *
 * @method  Product  getById($id)
 *
 * @package App\Repositories\Product
 */
class ProductRepository extends AbstractRepository
{
    protected $relations = ['unit', 'main_image', 'attributes'];

    protected $fieldsSearchable = ['sku', 'title', 'slug'];

    protected $fieldsSortable = [
        'id',
        'sku',
        'title',
        'slug',
        'weight',
        'cost_price',
        'retail_price',
        'created_at',
        'updated_at',
        'available_at'
    ];


    public function getModel()
    {
        return new Product();
    }

    /**
     * Получение товара по slug.
     *
     * @param  string $slug
     *
     * @return Product
     */
    public function getBySlug(string $slug)
    {
        $found = $this->findOneBy('slug', $slug);

        if (!$found) {
            throw (new ModelNotFoundException)->setModel(get_class($this->getModel()));
        }

        return $found;
    }

    /**
     * @param  int $categoryId
     *
     * @return Collection|Product[]
     */
    public function findByCategoryId(int $categoryId)
    {
        return $this->scopeHasCategory($categoryId)->get();
    }


    private function scopeHasCategory(int $categoryId)
    {
        return $this->getQuery()->whereHas('categories', function ($query) use ($categoryId) {
            /** @var Builder $query */
            $query->where('category_id', '=', $categoryId);
        });
    }
}
