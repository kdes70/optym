<?php

namespace App\Repositories\Categories;


use App\Domain\Catalog\Category\Category;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Cache;

/**
 * Class CategoryRepository
 *
 * @method  Category               getById($id)
 * @method  Category               findOne($id)
 * @method  Category               findOrFail($id)
 * @method  Category               findOneBy($column, $value = null)
 * @method  Collection|Category[]  findManyBy($column, $value = null)
 *
 * @package App\Repositories\User
 */
class CategoryRepository extends BaseRepository
{
    protected $relations = ['companies'];

    public function getModel(): Category
    {
        return new Category();
    }

    /**
     * Получение категории по slug.
     *
     * @param string $slug
     *
     * @return \App\Domain\Catalog\Category\Category
     */
    public function getBySlug(string $slug): Category
    {
        return $this->findOneByOrFail('slug', $slug);
    }

    /**
     *  Получение категории и подкатегорий по path.
     *
     * @param string $path
     * @return Category
     */
    public function getByPath(string $path): Category
    {
        if ($model = $this->getModel()
            ->withDepth()
            ->with('children')
            ->where('path', $path)
            ->enabled()
            ->first()) {

            return $model;
        }

        throw (new ModelNotFoundException)->setModel(get_class($this->getModel()));
    }

    /**
     * // TODO подлежит оптимизаци!
     * Получение текуший категории и ее компании с товарами
     *
     * @param string $path
     * @param int $offset сдвиг для компаний
     * @param int $limit лимит для компаниий
     * @return Category
     */
    public function getByPathCompanyAndItsProducts(string $path, $offset, $limit): Category
    {
        $category = Cache::tags(Category::class)
            ->rememberForever($path . '_category_and_company_page_' . $offset, function () use ($path, $offset, $limit) {
                return $this->getModel()
                    ->with([
                        'children',
                        // выбираем только уникальные компании и его товары
                        'companies' => function ($q) use ($offset, $limit) {
                            $q->with(['products' => function ($query) use ($offset, $limit) {
                                $query->active()->limit(5);
                            }])->distinct('companies')->latest()->offset($offset)->limit($limit); // пагинация по товарам
                        },
                    ])
                    ->where('path', $path)
                    ->first();
            });

        return $category;
    }

    /**
     * Получаем все компаний в категории
     *
     * @param string $path
     * @return Category|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getCompanyForCategory(string $path)
    {
        return $this->getModel()
            ->with([
                // выбираем только уникальные компании
                'companies' => function ($q) {
                    $q->distinct('companies')->latest();
                },
            ])
            ->where('path', $path)
            ->first();
    }

    /**
     *  Получить все категории с подкатегориями
     *
     * @return mixed
     */
    public function getFullTree()
    {
        $roots = Cache::tags(Category::class)
            ->rememberForever('full_category', function () {
                return $this->getModel()->withDepth()->get()->toTree();
            });

        return $roots;
    }


    /**
     *  Получить список подкатегорий
     *
     * @param \App\Domain\Catalog\Category\Category $categories
     * @return mixed
     */
    public function getChildrenNote(Category $categories)
    {
        $children = Cache::tags(Category::class)
            ->rememberForever($categories->getSlug() . '_children', function () use ($categories) {
                return $categories->children->each(function (Category $category) {
                    return $category->children;
                });
            });

        return $children;
    }


    public function getChildrenSelfRoot()
    {
        return $this->getModel()->whereIsRoot()->with('children')->withDepth();
    }

    public function getRootIsNotPage()
    {
        return $this->getModel()
            ->whereIsRoot()
            ->with([
                'children' => function ($q) {
                    return $q->doesntHave('page');
                },
            ])
            ->withDepth()
            ->get();
    }

    /**
     * @param $parent_id
     * @return mixed
     */
    public function getChildrenByParentId($parent_id)
    {
        return $this->getModel()
            ->where('parent_id', '=', $parent_id)
            ->with('children')
            ->get();
    }


    /**
     * Получить категорию по slug с ее подкатегориями
     *
     * @param string $slug
     * @return Category
     */
    public function getChildrenBySlug(string $slug): Category
    {
        $root = $this->getBySlug($slug);

        $result = $this->getModel()->with('ancestors')->descendantsAndSelf($root->getId())->toTree()->first();

        return $result;
    }

    public function getFeatureList(int $category_id)
    {
        return $this->findOrFail($category_id)->allAttributes();
    }


}



