<?php

namespace App\Repositories\User;

use App\Domain\User\User;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use App\Core\Eloquent\Repository\RequestCriterion;

/**
 * Class UserRepository
 *
 * @method  User               getById($id)
 * @method  User               findOne($id)
 * @method  User               findOrFail($id)
 * @method  User               findOneBy($column, $value = null)
 * @method  Collection|User[]  findManyBy($column, $value = null)
 *
 * @package App\Repositories\User
 */
class UserRepository extends BaseRepository
{
    protected $relations = ['roles', 'profile'];

    protected $fieldsSearchable = [
        'username', 'email', 'phone',
    ];

    protected function boot(): void
    {
        $this->pushCriterion(RequestCriterion::class);
    }


    /**
     * @return User
     */
    public function getModel()
    {
        return new User();
    }

    public function findByEmail(string $email): ?User
    {
        return $this->findOneBy('email', $email);
    }
}



