<?php

namespace App\Repositories\User;

use App\Domain\User\Profile;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use App\Core\Eloquent\Repository\RequestCriterion;


/**
 * Class ProfileRepository
 *
 * @method  Profile               getById($id)
 * @method  Profile               findOne($id)
 * @method  Profile               findOneBy($column, $value = null)
 * @method  Collection|Profile[]  findManyBy($column, $value = null)
 *
 * @package App\Repositories\User
 */
class ProfileRepository extends BaseRepository
{
    protected $relations = ['user'];

    protected $fieldsSearchable = [
        'first_name', 'last_name', 'birthday', 'country', 'avatar'
    ];

    protected function boot(): void
    {
        $this->pushCriterion(RequestCriterion::class);
    }

    /**
     * @return Profile
     */
    public function getModel(): Profile
    {
        return new Profile();
    }

}



