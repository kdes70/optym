<?php

namespace App\Repositories\Company;

use App\Domain\Company\Company;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use App\Core\Eloquent\Repository\RequestCriterion;


/**
 * Class CompanyRepository
 *
 * @method  Company               getById($id)
 * @method  Company               findOne($id)
 * @method  Company               findOneBy($column, $value = null)
 * @method  Collection|Company[]  findManyBy($column, $value = null)
 *
 * @package App\Repositories\User
 */
class CompanyRepository extends BaseRepository
{
    protected $fieldsSearchable = [
        'name',
        'tagline',
        'company_type',
        'description',
        'logo',
        'country',
        'region',
        'city',
        'address',
        'zip',
        'phone',
        'email',
        'site_link',
        'skype',
        'other_contacts',
        'legal_other',
        'contact_person',
        'reg_data',
        'tags',
        'meta_description'
    ];

    protected function boot(): void
    {
        $this->pushCriterion(RequestCriterion::class);
    }

    /**
     * @return Company
     */
    public function getModel(): Company
    {
        return new Company();
    }

    /**
     * @param string $slug
     *
     * @return Company
     */
    public function getBySlug(string $slug): Company
    {
        return $this
            ->getModel()
            ->with(['products', 'category'])
            ->where('slug', $slug)->with([
                'users' => function ($query) {
                    $query->select('id', 'username');
                }
            ])->first(['id', 'name', 'slug']);
    }

}



