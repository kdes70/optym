<?php

namespace App\Domain\User;

use App\Core\Domain\Attributes\Traits\GeoAware;
use App\Domain\User\Traits\AvatarAware;
use App\Core\Domain\Attributes\Traits\HasIdentity;
use Carbon\Carbon;
use App\Core\Eloquent\AbstractModel;
use App\Domain\User\Contracts\ProfileContract;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Domain\User\Profile
 *
 * @property int|null $user_id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property int $sex 1 - Мужской, 2 - Женский
 * @property \Illuminate\Support\Carbon|null $birthday
 * @property string|null $avatar
 * @property string|null $position Должность
 * @property string|null $country
 * @property string|null $city
 * @property string|null $notes
 * @property string|null $options
 *
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property-read mixed $avatar_url
 * @property-read User|null $user
 */
class Profile extends AbstractModel implements ProfileContract
{
    use HasIdentity, AvatarAware, GeoAware;

    /** @var string */
    protected $primaryKey = 'user_id';

    /** @var array */
    protected $guarded = ['user_id'];

    /** @var array */
    protected $appends = ['avatar_url'];

    /** @var array */
    protected $dates = ['birthday'];

    /** @var array */
    protected $casts = [
        'birthday' => 'date'
    ];

    /** @var array */
    protected $fillable = [
        'first_name',
        'last_name',
        'birthday',
        'country',
        'avatar'
    ];

    /**
     * A profile belongs to a user.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param string $last_name
     *
     * @return ProfileContract
     */
    public function setLastName(string $last_name): ProfileContract
    {
        $this->setAttribute('last_name', $last_name);

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return (string)$this->getAttributeValue('last_name');
    }

    /**
     * @param string $first_name
     *
     * @return ProfileContract
     */
    public function setFirstName(string $first_name): ProfileContract
    {
        $this->setAttribute('first_name', $first_name);

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return (string)$this->getAttributeValue('first_name');
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        // TODO: Implement getFullName() method.
    }

    /**
     * @return Carbon|null
     */
    public function getBirthday(): ?Carbon
    {
        // TODO: Implement getBirthday() method.
    }

    /**
     * @param $value
     *
     * @return ProfileContract
     */
    public function setBirthday($value): ProfileContract
    {
        // TODO: Implement setBirthday() method.
    }

    /**
     * @param string $sex
     *
     * @return ProfileContract
     */
    public function setSex(string $sex): ProfileContract
    {
        // TODO: Implement setSex() method.
    }

    /**
     * @return string
     */
    public function getSex(): string
    {
        // TODO: Implement getSex() method.
    }

    /**
     * @return string
     */
    public function getAvatarUrlAttribute(): string
    {
        return $this->getAvatarUrl();
    }

    /**
     * @param string $position
     *
     * @return ProfileContract
     */
    public function setPosition(string $position): ProfileContract
    {
        // TODO: Implement setPosition() method.
    }

    /**
     * @return string
     */
    public function getPosition(): string
    {
        // TODO: Implement getPosition() method.
    }

}
