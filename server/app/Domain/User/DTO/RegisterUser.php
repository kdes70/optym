<?php

namespace App\Domain\User\DTO;

use App\Core\DTO\DTO;
use App\Domain\User\Enums\UserStateEnum;
use App\Domain\User\Profile;
use App\Enums\Domain\User\UserRoleEnum;

/**
 * Class RegisterUser
 */
class RegisterUser extends DTO
{
    /**
     * Доступные свойства
     */

    protected $phone;
    protected $email;
    protected $username;
    protected $password;
    protected $type;
    protected $first_name;  //Необязательное
    protected $last_name;   //Необязательное
    protected $verify_token;
    protected $ip_address;

    protected $company_name; //Необязательное
    protected $company_type;
    protected $country;  //Необязательное
    protected $region;  //Необязательное
    protected $city;  //Необязательное

    /**
     * Доступные свойства со значениями по умолчанию
     *
     * @see $state при регистрации статус ожидания письма, при создании из админки статус не потдвержден
     */
    protected $role = UserRoleEnum::USER;
    protected $state = UserStateEnum::WAIT;
    protected $avatar = Profile::NO_AVATAR;


}
