<?php

namespace App\Domain\User\DTO;

use App\Core\DTO\DTO;
use App\Domain\User\Enums\UserStateEnum;
use App\Domain\User\Enums\UserTypeEnum;
use App\Enums\Domain\User\UserRoleEnum;

/**
 * Class CreateUser
 */
class CreateUser extends DTO
{
    /**
     * Доступные свойства
     */
    protected $email;
    protected $first_name;  //Необязательное
    protected $last_name;   //Необязательное
    protected $password;
    protected $phone;       //Необязательное
    protected $username;

    /**
     * Доступные свойства со значениями по умолчанию
     */
    protected $role = UserRoleEnum::USER;
    protected $state = UserStateEnum::UNCONFIRMED;
    protected $type = UserTypeEnum::USER;
}
