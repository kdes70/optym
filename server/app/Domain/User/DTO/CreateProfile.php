<?php

namespace App\Domain\User\DTO;

use App\Core\DTO\DTO;

/**
 * Class CreateProfile
 */
class CreateProfile extends DTO
{
    /**
     * Доступные свойства
     */
    protected $first_name;  //Необязательное
    protected $last_name;   //Необязательное
    protected $birthday;   //Необязательное
    protected $country;   //Необязательное
    protected $avatar;  //Необязательное

    /**
     * Доступные свойства со значениями по умолчанию
     */

}
