<?php

namespace App\Domain\User\DTO;

use App\Core\DTO\UpdateDTO;

/**
 * Class Update
 */
class Update extends UpdateDTO
{
    /**
     * Доступные свойства
     */
    protected $email;
    protected $first_name;  //Необязательное
    protected $last_name;   //Необязательное
    protected $password;
    protected $phone;       //Необязательное
    protected $username;
    protected $country;
    protected $city;
}
