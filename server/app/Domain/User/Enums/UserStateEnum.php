<?php

namespace App\Domain\User\Enums;

use MadWeb\Enum\Enum;

/**
 * class UserStateEnum
 *
 * @method static UserStateEnum WAIT()
 * @method static UserStateEnum ACTIVE()
 * @method static UserStateEnum BANNED()
 * @method static UserStateEnum INACTIVE()
 * @method static UserStateEnum UNCONFIRMED()
 */
final class UserStateEnum extends Enum
{
    const __default = self::WAIT;

    const WAIT = 0;
    const ACTIVE = 1;
    const BANNED = 2;
    const INACTIVE = 3;
    const UNCONFIRMED = 4;
}
