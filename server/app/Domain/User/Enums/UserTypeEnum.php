<?php

namespace App\Domain\User\Enums;

use App\Core\Enums\AbstractIntEnum;
use Illuminate\Support\Collection;

/**
 * class UserTypeEnum
 *
 * @method static UserTypeEnum USER()
 * @method static UserTypeEnum WHOLESALE()
 * @method static UserTypeEnum BUYER()
 */
final class UserTypeEnum extends AbstractIntEnum
{
    const __default = self::USER;

    const USER = 0;
    const WHOLESALE = 1;
    const BUYER = 2;
}
