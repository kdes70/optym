<?php

namespace App\Enums\Domain\User;

use MadWeb\Enum\Enum;

/**
 * Class UserRoleEnum
 *
 * @method static UserRoleEnum USER()
 * @method static UserRoleEnum ADMIN()
 * @method static UserRoleEnum MODERATOR()
 * @method static UserRoleEnum WHOLESALE()
 * @method static UserRoleEnum BUYER()
 */
final class UserRoleEnum extends Enum
{
    const __default = self::USER;

    const USER = 'user';
    const ADMIN = 'admin';
    const MODERATOR = 'moderator';
    const WHOLESALE = 'wholesaler';
    const BUYER = 'buyer';
}
