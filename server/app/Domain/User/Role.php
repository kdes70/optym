<?php

namespace App\Domain\User;

use App\Domain\User\Contracts\RoleContract;
use App\Enums\Domain\User\UserRoleEnum;

/**
 * Class Role
 */
class Role implements RoleContract
{
    /** @var string */
    protected $name;

//    static public function getRoleList()
//    {
//        return [
//            self::USER      => 'Пользовотель',
//            self::ADMIN     => 'Администратор',
//            self::MODERATOR => 'Модератор',
//            self::WHOLESALE => 'Продавец',
//            self::BUYER     => 'Покупатель',
//        ];
//    }


    /**
     * Role constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        if (!in_array($name, UserRoleEnum::toArray(), true)) {
            throw new \InvalidArgumentException("Invalid role name '{$name}'.");
        }

        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

}
