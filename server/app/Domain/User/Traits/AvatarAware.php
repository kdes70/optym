<?php

namespace App\Domain\User\Traits;

/**
 * Trait AvatarAware
 */
trait AvatarAware
{
    /**
     * @param string $avatar
     *
     * @return $this|static
     */
    public function setAvatar(string $avatar)
    {
        $this->setAttribute('avatar', $avatar);

        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        return (string)$this->getAttributeValue('avatar');
    }

    /**
     * @return string
     */
    public function getAvatarUrl(): string
    {
        $avatar = $this->getAvatar() != self::NO_AVATAR ?
            'storage/avatar/' . $this->getAvatar() :
            'img/' . $this->getAvatar();

        return url($avatar);
    }
}
