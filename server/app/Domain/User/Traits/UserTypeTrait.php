<?php

namespace App\Domain\User\Traits;

use App\Domain\User\Contracts\UserContract;

/**
 * Trait UserTypeTrait
 */
trait UserTypeTrait
{
    /**
     * @param int $type
     *
     * @return UserContract|static
     */
    public function setType(int $type): UserContract
    {
        $this->setAttribute('type', $type);

        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return (int)$this->getAttributeValue('type');
    }
}
