<?php

namespace App\Domain\User\Traits;

use App\Domain\User\Contracts\UserContract;
use App\Domain\User\Role;
use DomainException;
use InvalidArgumentException;

/**
 * Trait UserRoleTrait
 */
trait UserRoleTrait
{
    /**
     * @param string $role
     *
     * @throws DomainException
     * @throws InvalidArgumentException
     */
    public function changeRole(string $role): void
    {
        if (!array_key_exists($role, Role::getRoleList())) {
            throw new InvalidArgumentException('Undefined role "' . $role . '"');
        }
        if ($this->getRole() === $role) {
            throw new DomainException('Role is already assigned.');
        }

        $this->update(['role' => $role]);
    }

    /**
     * List roles
     *
     * @return array
     */
    public static function rolesList()
    {
        return Role::getRoleList();
    }

    /**
     * @return Role
     */
    public function getRole(): Role
    {
        return new Role($this->getAttribute('role'));
    }

    /**
     * @param Role $role
     *
     * @return UserContract|static
     */
    public function setRole(Role $role): UserContract
    {
        $this->setAttribute('role', $role->getName());

        return $this;
    }

    /**
     * @return string
     */
    public function getRoleName(): string
    {
        return $this->getRole()->getName();
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->getRoleName() === Role::ADMIN;
    }

    /**
     * @return bool
     */
    public function isUser(): bool
    {
        return $this->getRoleName() === Role::USER;
    }

    /**
     * @return bool
     */
    public function isModerator(): bool
    {
        return $this->getRoleName() === Role::MODERATOR;
    }

    /**
     * @return bool
     */
    public function isWholesale(): bool
    {
        return $this->getRoleName() === Role::WHOLESALE;
    }

}
