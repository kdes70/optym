<?php

namespace App\Domain\User;


use App\Core\Eloquent\AbstractModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Carbon;

/**
 * App\Domain\Activity
 *
 * @property int $id
 * @property int $subject_id
 * @property string $subject_type
 * @property string $name
 * @property int $user_id
 *
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $subject
 * @property-read User $user
 */
class Activity extends AbstractModel
{
    /** @var string */
    protected $table = 'activities';

    /** @var array */
    protected $fillable = [
        'subject_id',
        'subject_type',
        'name',
        'user_id'
    ];

    /**
     * Get the user responsible for the given activity.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the subject of the activity.
     *
     * @return MorphTo
     */
    public function subject(): MorphTo
    {
        return $this->morphTo();
    }
}
