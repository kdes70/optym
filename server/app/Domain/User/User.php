<?php

namespace App\Domain\User;

use App\Core\Domain\Attributes\Traits\HasIdentity;
use App\Core\Domain\Attributes\Traits\PhoneAware;
use App\Core\Domain\Attributes\Traits\EmailAware;
use App\Core\Eloquent\AbstractModel;
use App\Core\Eloquent\Timestampable;
use App\Domain\Company\Company;
use App\Domain\User\Enums\UserStateEnum;
use App\Domain\User\Enums\UserTypeEnum;
use App\Domain\User\Traits\UserRoleTrait;
use App\Domain\User\Traits\UserTypeTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
use App\Domain\User\Contracts\UserContract;
use Illuminate\Support\Carbon;
use MadWeb\Enum\EnumCastable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User Сущность «Пользователь».
 *
 * @property int $id
 * @property string $username
 * @property string|null $phone
 * @property string $email
 * @property string $password
 * @property string|null $api_token
 * @property string|null $ip_address
 * @property string|null $options
 * @property string|null $verify_token
 * @property int $verified
 * @property UserStateEnum $state
 * @property string $role
 * @property string|null $last_login_at
 * @property string|null $last_login_ip
 * @property string|null $remember_token
 * @property UserTypeEnum $type
 *
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 *
 * @property-read Collection|Activity[] $activity
 * @property-read Collection|Company[] $company
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read Profile $profile
 */
class User extends AbstractModel implements UserContract, JWTSubject
{
    use Authenticatable, Authorizable, Notifiable, HasIdentity, PhoneAware, EmailAware, UserRoleTrait, UserTypeTrait,
        Timestampable, EnumCastable;

    /** @var array */
    protected $with = ['profile'];

    /** @var array */
    protected $guarded = ['id'];

    /** @var array */
    protected $fillable = [
        'username',
        'email',
        'phone',
        'password',
        'type',
        'state',
        'role',
        'last_login_at',
        'last_login_ip',
    ];

    /** @var array */
    protected $hidden = [
        'password',
        'remember_token',
        'verify_token'
    ];

    protected $casts = [
        'state' => UserStateEnum::class,
        'type'  => UserTypeEnum::class,
    ];

    /**
     * User Profile Relationships.
     *
     * @return HasOne
     */
    public function profile(): HasOne
    {
        return $this->hasOne(Profile::class, 'user_id');
    }

    /**
     * @return BelongsToMany
     */
    public function company(): BelongsToMany
    {
        return $this->belongsToMany(Company::class, 'companies_user');
    }

    /**
     * Get the activity timeline for the user.
     *
     * @return HasMany
     */
    public function activity(): HasMany
    {
        return $this->hasMany(Activity::class)
            ->with(['user', 'subject'])
            ->latest();
    }

    /**
     * Record new activity for the user.
     *
     * @param string $name
     * @param mixed $related
     *
     * @return void
     *
     * @throws \Exception
     */
    public function recordActivity($name, $related)
    {
        if (!method_exists($related, 'recordActivity')) {
            throw new \Exception('..'); // todo message to recordActivity
        }
        return $related->recordActivity($name);
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->getRelationValue('company')->first();
    }

    /**
     * @return bool
     *
     * @todo вынести отсюда
     */
    public function isWait(): bool
    {
        return UserStateEnum::WAIT()->is($this->getState());
    }

    /**
     * @return bool
     *
     * @todo вынести отсюда
     */
    public function isActive(): bool
    {
        return UserStateEnum::ACTIVE()->is($this->getState());
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->getAttributeValue('username');
    }

    /**
     * @param string $username
     * @param bool $origin
     *
     * @return UserContract|static
     *
     * @throws \Exception
     */
    public function setUsername(string $username, $origin = false): UserContract
    {
        if (!$origin && filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $str = substr($username, 0, strpos($username, "@"));
            $username = $str . '_' . random_int(1000, 999999);
        }

        $this->setAttribute('username', $username);

        return $this;
    }

    /**
     * @param string $password
     *
     * @return UserContract|static
     */
    public function setPassword(string $password): UserContract
    {
        $this->setAttribute('password', bcrypt($password));

        return $this;
    }

    /**
     * @param string $ip_address
     *
     * @return UserContract
     */
    public function setIpAddress(string $ip_address): UserContract
    {
        $this->setAttribute('ip_address', $ip_address);

        return $this;
    }

    /**
     * @return string
     */
    public function getIpAddress(): string
    {
        return (string)$this->getAttributeValue('ip_address');
    }

    /**
     * @param string|null $verify_token
     *
     * @return UserContract|static
     */
    public function setVerifyToken(?string $verify_token): UserContract
    {
        $this->setAttribute('verify_token', $verify_token);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getVerifyToken(): ?string
    {
        return (string)$this->getAttributeValue('verify_token');
    }

    /**
     * @param $verified
     *
     * @return UserContract|static
     */
    public function setVerified($verified): UserContract
    {
        $this->setAttribute('verified', $verified);

        return $this;
    }

    /**
     * @return bool
     */
    public function getVerified(): bool
    {
        return (bool)$this->getAttributeValue('verified');
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return (string)$this->getAttributeValue('state');
    }

    /**
     * @param UserStateEnum $state
     *
     * @return UserContract|static
     */
    public function setState(UserStateEnum $state): UserContract
    {
        $this->setAttribute('state', $state);

        return $this;
    }

    /**
     * @param $name
     *
     * @return bool
     *
     * @todo не используется, для соцсетей
     */
    public function hasProfile($name): bool
    {
//        foreach ($this->profiles as $profile) {
//            if ($profile->name == $name) {
//                return true;
//            }
//        }
//        return false;
    }

    /**
     * @param $profile
     *
     * @return mixed
     *
     * @todo не используется, для соцсетей
     */
    public function assignProfile($profile)
    {
        // return $this->profiles()->attach($profile);
    }

    /**
     * @param $profile
     *
     * @return mixed
     *
     * @todo не используется, для соцсетей
     */
    public function removeProfile($profile)
    {
        //return $this->profiles()->detach($profile);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
