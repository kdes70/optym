<?php

namespace App\Domain;

use App\Core\Eloquent\AbstractModel;
use Illuminate\Support\Carbon;

/**
 * Class Permission
 *
 * @property int $id
 * @property string $name
 * @property string|null $slug
 * @property string|null $description
 *
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class Permission extends AbstractModel
{
    //
}
