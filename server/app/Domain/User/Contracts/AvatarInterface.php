<?php

namespace App\Domain\User\Contracts;

/**
 * Interface AvatarInterface
 */
interface AvatarInterface
{
    /** @var string  */
    const NO_AVATAR = 'no_avatar.jpg';

    /**
     * @param string $avatar
     * @return mixed
     */
    public function setAvatar(string $avatar);

    /**
     * @return string
     */
    public function getAvatar(): string;

    /**
     * @return string
     */
    public function getAvatarUrl(): string;
}
