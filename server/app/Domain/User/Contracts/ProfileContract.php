<?php

namespace App\Domain\User\Contracts;

use App\Core\Domain\Attributes\HasIdentityInterface;
use App\Core\Domain\Attributes\GeoAwareInterface;
use Carbon\Carbon;

/**
 * Interface ProfileContract
 */
interface ProfileContract extends HasIdentityInterface, AvatarInterface, GeoAwareInterface
{
    /**
     * @return string|null
     */
    public function getFirstName(): ?string;

    /**
     * @param string $first_name
     *
     * @return ProfileContract
     */
    public function setFirstName(string $first_name): ProfileContract;

    /**
     * @return string|null
     */
    public function getLastName(): ?string;

    /**
     * @param string $last_name
     *
     * @return ProfileContract
     */
    public function setLastName(string $last_name): ProfileContract;

    /**
     * @return string
     */
    public function getFullName(): string;

    /**
     * @return Carbon|null
     */
    public function getBirthday(): ?Carbon;

    /**
     * @param $value
     *
     * @return ProfileContract
     */
    public function setBirthday($value): ProfileContract;

    /**
     * @param string $sex
     *
     * @return ProfileContract
     */
    public function setSex(string $sex): ProfileContract;

    /**
     * @return string
     */
    public function getSex(): string;

    /**
     * @param string $position
     *
     * @return ProfileContract
     */
    public function setPosition(string $position): ProfileContract;

    /**
     * @return string
     */
    public function getPosition(): string;

}
