<?php

namespace App\Domain\User\Contracts;

use App\Core\Domain\Attributes\EmailAwareInterface;
use App\Core\Domain\Attributes\HasIdentityInterface;
use App\Core\Domain\Attributes\PhoneAwareInterface;
use App\Core\Domain\Attributes\TimestampableInterface;
use App\Domain\User\Enums\UserStateEnum;
use App\Domain\User\Role;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable;

/**
 * class UserContract
 */
interface UserContract extends
    Authenticatable,
    Authorizable,
    HasIdentityInterface,
    PhoneAwareInterface,
    EmailAwareInterface,
    TimestampableInterface
{
    /**
     * @return string
     */
    public function getUsername(): string;

    /**
     * @param string $username
     *
     * @return UserContract|static
     */
    public function setUsername(string $username): UserContract;

    /**
     * @param string $password
     *
     * @return UserContract|static
     */
    public function setPassword(string $password): UserContract;

    /**
     * @param string $ip_address
     *
     * @return UserContract|static
     */
    public function setIpAddress(string $ip_address): UserContract;

    /**
     * @return string
     */
    public function getIpAddress(): string;

    /**
     * @param string|null $verify_token
     *
     * @return UserContract|static
     */
    public function setVerifyToken(?string $verify_token): UserContract;

    /**
     * @return string|null
     */
    public function getVerifyToken(): ?string;

    /**
     * @param $verify_token
     *
     * @return UserContract|static
     */
    public function setVerified($verify_token): UserContract;

    /**
     * @return bool
     */
    public function getVerified(): bool;

    /**
     * @param int $type
     *
     * @return UserContract|static
     */
    public function setType(int $type): UserContract;

    /**
     * @return int
     */
    public function getType(): int;

    /**
     * @return Role
     */
    public function getRole(): Role;

    /**
     * @return string
     */
    public function getRoleName(): string;

    /**
     * @param Role $role
     *
     * @return UserContract|static
     */
    public function setRole(Role $role): UserContract;

    /**
     * @return string
     */
    public function getState(): string;

    /**
     * @param UserStateEnum $state
     *
     * @return UserContract|static
     */
    public function setState(UserStateEnum $state): UserContract;

    /**
     * @return bool
     */
    public function isActive(): bool;
}
