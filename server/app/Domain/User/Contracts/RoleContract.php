<?php

namespace App\Domain\User\Contracts;

/**
 * Interface RoleContract
 */
interface RoleContract
{
    const USER = 'user';
    const ADMIN = 'admin';
    const MODERATOR = 'moderator';
    const WHOLESALE = 'wholesaler';
    const BUYER = 'buyer';

    public function getName(): string;

}
