<?php

namespace App\Domain\Product;

use App\Core\Domain\Attributes\Traits\HasIdentity;
use App\Core\Domain\Attributes\Traits\Priceable;
use App\Core\Domain\Attributes\Traits\SkuAware;
use App\Core\Domain\Attributes\Traits\SlugAware;
use App\Core\Domain\Attributes\Traits\TitleAware;
use App\Core\Domain\Attributes\Traits\UnitAware;
use App\Core\Domain\Attributes\Traits\WeightAware;
use App\Core\Eloquent\AbstractModel;
use App\Core\Eloquent\Timestampable;
use App\Domain\Catalog\Category\AttributeValue;
use App\Domain\Catalog\Category\Category;
use App\Domain\Company\Company;
use App\Domain\Product\Contracts\ProductContract;
use App\Domain\Product\Traits\PhotosAware;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Staudenmeir\EloquentEagerLimit\HasEagerLimit;


/**
 * @property mixed available_at
 * @property mixed id
 */
class Product extends AbstractModel implements ProductContract
{
    use HasIdentity, TitleAware, UnitAware, PhotosAware, Priceable,
         SkuAware, SlugAware, Timestampable, WeightAware, HasEagerLimit;

    protected $table       = 'products';
    protected $fillable    = ['title', 'description', 'weight'];
    protected $dates       = ['available_at', 'archived_at'];

    protected $casts = [
        'price' => 'decimal:2',
        'cost_price' => 'decimal:2',
    ];

    protected $hidden = ['pivot'];

    protected $with = [
        'main_image'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     *  Получение всех категорий товара.
     *
     * @return Category
     */
    public function getCategories(): Category
    {
        return $this->getRelationValue('category');
    }

    public function company()
    {
        return $this->belongsTo(Company::class)->withDefault();
    }

    public function getCompany(): Company
    {
        return $this->getRelationValue('company');
    }

    public function attributes()
    {
        return $this->belongsToMany(
            AttributeValue::class, 'attribute_product', 'product_id', 'value_id'
        );
    }

    public function values()
    {
        return $this->hasMany(Value::class, 'advert_id', 'id');
    }

    /**
     * @return AttributeValue[]|Collection
     */
    public function getProductAttributes(): Collection
    {
        return $this->getRelationValue('attributes');
    }

    /**
     * Получение описания товара.
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->getAttributeValue('description');
    }

    /**
     * Установка описания товара.
     *
     * @param  string $description
     *
     * @return $this
     */
    public function setDescription(string $description)
    {
        $this->setAttribute('description', $description);

        return $this;
    }

    /**
     * Получение статуса товара.
     *
     * @return string
     */
    public function getState(): string
    {
        return (string)$this->getAttributeValue('state');
    }

    /**
     * Установка статуса товара.
     *
     * @param  string $state
     *
     * @return $this
     */
    public function setState(string $state)
    {
        $this->setAttribute('state', $state);

        return $this;
    }

    /**
     * Дата и время переноса товара в архив.
     *
     * @return Carbon|null
     */
    public function getArchivedAt(): ?Carbon
    {
        // TODO: Implement getArchivedAt() method.
    }

    /**
     * Дата и время поступления (первого или очередного) товара в продажу.
     *
     * @return Carbon|null
     */
    public function getAvailableAt(): ?Carbon
    {
        // TODO: Implement getAvailableAt() method.
    }

    /**
     * Товар-черновик?
     *
     * @return bool
     */
    public function isDraft(): bool
    {
        // TODO: Implement isDraft() method.
    }

    /**
     * Товар продаётся?
     *
     * @return bool
     */
    public function isAvailable(): bool
    {
        // TODO: Implement isAvailable() method.
    }

    /**
     * Товар в архиве?
     *
     * @return bool
     */
    public function isArchived(): bool
    {
        // TODO: Implement isArchived() method.
    }


    public function main_image()
    {
        return $this->morphOne(Image::class, 'photo', 'photo_type', 'id', 'main_image_id');
    }

    /**
     * {@inheritdoc}
     */
    public function getMainImage(): ?Image
    {
        if ($this->relationLoaded('images')) {
            return $this->getImages()
                ->where('id', '=', $this->getAttributeValue('photo_id'))
                ->first();
        }

        return $this->getRelationValue('main_image');
    }

    /**
     * Установка заглавной картинки сущности.
     *
     * @param  int $id
     *
     * @return \App\Domain\Product\Product
     */
    public function setMainImage(int $id)
    {
        $this->setAttribute('main_image_id', $id);

        return $this;
    }



    public function scopeActive(Builder $query)
    {
        return $query->where('state', self::STATE_AVAILABLE);
    }

    public function getPhoto(): Collection
    {
        // TODO: Implement getPhoto() method.
    }
}
