<?php

namespace App\Domain\Product;

use App\Core\Domain\Attributes\Traits\HasIdentity;
use App\Core\Domain\Attributes\Traits\TitleAware;
use App\Core\Eloquent\AbstractModel;
use App\Domain\Product\Contracts\UnitInterface;
use App\Domain\Product\Traits\CodeAware;

/**
 * Сущность «Единица измерения».
 *
 * @package App\Domain\Product
 */
class Unit extends AbstractModel implements UnitInterface
{
    use HasIdentity, CodeAware, TitleAware;

    public $timestamps = false;

    protected $table = 'units';

    /**
     * {@inheritdoc}
     */
    public function getAbbreviation(): string
    {
        return $this->getAttributeValue('abbr');
    }

    /**
     * {@inheritdoc}
     */
    public function setAbbreviation(string $abbreviation)
    {
        $this->setAttribute('abbr', $abbreviation);

        return $this;
    }

}
