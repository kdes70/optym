<?php

namespace App\Domain\Product\Contracts;

use App\Domain\Product\Image;
use Carbon\Carbon;
use App\Domain\Catalog\Category\Category;
use App\Domain\Product\Traits\Photos;
use Illuminate\Database\Eloquent\Collection;
use App\Core\Domain\Attributes\HasIdentityInterface;
use App\Core\Domain\Attributes\TitleAwareInterface;
use App\Core\Domain\Attributes\SkuAwareInterface;
use App\Core\Domain\Attributes\PriceableInterface;
use App\Core\Domain\Attributes\SlugAwareInterface;
use App\Core\Domain\Attributes\UnitAwareInterface;
use App\Core\Domain\Attributes\WeightAwareInterface;


interface ProductContract extends
    TitleAwareInterface,
    SkuAwareInterface,
    PriceableInterface,
    SlugAwareInterface,
    UnitAwareInterface,
    PhotosInterface,
    HasIdentityInterface,
    WeightAwareInterface
{
    const STATE_DRAFT = 'draft';
    const STATE_ARCHIVE = 'archive';
    const STATE_AVAILABLE = 'available';

    const BADGE_NEW_IN_MARKET = 'new';
    const BADGE_BACK_ON_MARKET = 'back';

    /**
     * Получение основного изображения товара.
     *
     * @return Image|null
     */
    public function getMainImage(): ?Image;


    /**
     * Получение всех категорий товара.
     *
     * @return Category
     */
    public function getCategories(): Category;

    /**
     * Получение описания товара.
     *
     * @return string
     */
    public function getDescription(): ?string;

    /**
     * Установка описания товара.
     *
     * @param  string $description
     *
     * @return $this
     */
    public function setDescription(string $description);

    /**
     * Получение статуса товара.
     *
     * @return string
     */
    public function getState(): string;

    /**
     * Установка статуса товара.
     *
     * @param  string $state
     *
     * @return $this
     */
    public function setState(string $state);

    /**
     * Дата и время создания товара.
     *
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon;

    /**
     * Дата и время обновления товара.
     *
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon;

    /**
     * Дата и время переноса товара в архив.
     *
     * @return Carbon|null
     */
    public function getArchivedAt(): ?Carbon;

    /**
     * Дата и время поступления (первого или очередного) товара в продажу.
     *
     * @return Carbon|null
     */
    public function getAvailableAt(): ?Carbon;

//    public function getBadges(): Badges;
//
//    public function setBadges(Badges $badges);

    /**
     * Товар-черновик?
     *
     * @return bool
     */
    public function isDraft(): bool;

    /**
     * Товар продаётся?
     *
     * @return bool
     */
    public function isAvailable(): bool;

    /**
     * Товар в архиве?
     *
     * @return bool
     */
    public function isArchived(): bool;
}
