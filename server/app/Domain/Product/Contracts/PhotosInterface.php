<?php

namespace App\Domain\Product\Contracts;


use App\Domain\Product\Traits\Photos;
use Illuminate\Database\Eloquent\Collection;

interface PhotosInterface
{
    public function getImages(): Collection;

//    public function setImages($images);
//
    /**
     * Прикрепление изображений к сущности.
     *
     * @param  Image[]  $image
     *
     * @return Image[]
     */
    public function addImages(array $image);
}
