<?php

namespace App\Domain\Product\Contracts;

use App\Core\Domain\Attributes\HasIdentityInterface;
use App\Core\Domain\Attributes\TitleAwareInterface;
use App\Core\Domain\Attributes\CodeAwareInterface;

/**
 * Интерфейс «Единица измерения».
 *
 * @package App\Domain\Product;
 */
interface UnitInterface extends
    CodeAwareInterface,
    TitleAwareInterface,
    HasIdentityInterface
{
    /**
     * Получение аббревиатуры единицы измерения.
     *
     * @return string
     */
    public function getAbbreviation(): string;

    /**
     * Установка аббревиатуры единицы измерения.
     *
     * @param  string  $abbreviation
     *
     * @return $this
     */
    public function setAbbreviation(string $abbreviation);
}
