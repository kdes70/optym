<?php

namespace App\Domain\Product\Traits;

/**
 * Trait CodeAware
 *
 * @package App\Domain\Product
 */
trait CodeAware
{
    /**
     * {@inheritdoc}
     */
    public function getCode(): string
    {
        return (string) $this->getAttributeValue('code');
    }

    /**
     * {@inheritdoc}
     */
    public function setCode(string $code)
    {
        $this->setAttribute('code', $code);

        return $this;
    }
}
