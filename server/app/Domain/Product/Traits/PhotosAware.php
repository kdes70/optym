<?php

namespace App\Domain\Product\Traits;

use App\Domain\Product\Image;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait PhotosAware
{

    public function images()
    {
        return $this->morphMany(Image::class, 'photo')->orderBy('order');
    }

    public function getImages(): Collection
    {
        return $this->getRelationValue('images');
    }

    /**
     * Прикрепление изображений к сущности.
     *
     * @param  Image[]  $images
     *
     * @return Image[]
     */
    public function addImages(array $images): array
    {
        return $this->images()->saveMany($images);
    }
}
