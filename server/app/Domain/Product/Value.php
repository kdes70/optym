<?php

namespace App\Domain\Product;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $attribute_id
 * @property string $value
 */
class Value extends Model
{
    protected $table = 'attribute_value';

    public $timestamps = false;

    protected $fillable = ['attribute_id', 'value'];
}
