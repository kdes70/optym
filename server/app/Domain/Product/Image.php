<?php

namespace App\Domain\Product;


use App\Core\Domain\Attributes\HasIdentityInterface;
use App\Core\Domain\Attributes\Traits\HasIdentity;
use App\Core\Eloquent\AbstractModel;
use App\Services\ImageManager;

/**
 * @property int $id
 * @property string file
 */
class Image extends AbstractModel implements HasIdentityInterface
{
    use HasIdentity;

    protected $table = 'images';

    public $timestamps = false;

//    protected $fillable = ['product_id', 'image', 'order'];


    public function image_product()
    {
        return $this->morphTo();
    }

    public function getImageableId(): int
    {
        return $this->getAttributeValue('photo_id');
    }

    public function setImageableId($id): Image
    {
        $this->setAttribute('photo_id', $id);

        return $this;
    }

    public function getImageableType(): string
    {
        return $this->getAttributeValue('photo_type');
    }

    public function setImageableType($type): Image
    {
        $this->setAttribute('photo_type', $type);

        return $this;
    }

    public function getPath(): string
    {
        return $this->getAttributeValue('path');
    }

    public function setPath(string $path): Image
    {
        $this->setAttribute('path', $path);

        return $this;
    }

    public function getAlt(): ?string
    {
        return $this->getAttributeValue('alt');
    }

    public function setAlt(?string $alt = null): Image
    {
        $this->setAttribute('alt', $alt);

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->getAttributeValue('title');
    }

    public function setTitle(?string $title = null): Image
    {
        $this->setAttribute('title', $title);

        return $this;
    }

    public function getOrder(): ?int
    {
        return $this->getAttributeValue('order');
    }

    public function setOrder(?int $order = null): Image
    {
        $this->setAttribute('order', $order);

        return $this;
    }

    public function getUrl(?string $style = null): string
    {
        return ImageManager::url($this, $style);
    }
}
