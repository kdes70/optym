<?php

namespace App\Domain\Company\Enums;

use App\Core\Enums\AbstractIntEnum;

/**
 * class CompanyTypeEnum
 *
 * @method static CompanyTypeEnum RETAIL()
 * @method static CompanyTypeEnum WHOLESALE()
 * @method static CompanyTypeEnum PRODUCTION()
 * @method static CompanyTypeEnum SERVICES()
 */
final class CompanyTypeEnum extends AbstractIntEnum
{
    const __default = self::RETAIL;

    const RETAIL = 0;
    const WHOLESALE = 1;
    const PRODUCTION = 2;
    const SERVICES = 3;
}
