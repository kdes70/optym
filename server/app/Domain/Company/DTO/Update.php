<?php

namespace App\Domain\Company\DTO;

use App\Core\DTO\UpdateDTO;
use App\Domain\Company\Company;

class Update extends UpdateDTO
{
    /**
     * Доступные свойства
     */
    protected $name;
    protected $slug;
    protected $tagline;
    protected $company_type;
    protected $description;
    protected $logo;
    protected $country;
    protected $region;
    protected $city;
    protected $address;
    protected $zip;
    protected $phone;
    protected $email;
    protected $site_link;
    protected $skype;
    protected $other_contacts;
    protected $legal_other;
    protected $contact_person;
    protected $reg_data;
    protected $tags;
    protected $min_amount;
    protected $scope_supply;
    protected $payment_options;
    protected $additional_options;
    protected $delivery_options;
    protected $terms_payment_delivery;
    protected $terms_return_guarantee;
    protected $name_legal_entity;
    protected $privacy;


    /**
     * Доступные свойства со значениями по умолчанию
     */

    protected $state = Company::STATE_DRAFT;
    protected $enabled = false;
    protected $verified = false;

}
