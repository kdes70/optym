<?php

namespace App\Domain\Company;

use App\Domain\Company\Contracts\Imorter;
use Illuminate\Database\Eloquent\Model;

class CompanyTemp extends Model implements Imorter
{

    protected $fillable = [
        'vendor',
        'category_id',
        'type_id',
        'name',
        'city',
        'address',
        'phone',
        'email',
    ];

    public function getHead(): array
    {
        return $this->fillable;
    }
}
