<?php

namespace App\Domain\Company;

use App\Core\Domain\Attributes\Traits\GeoAware;
use App\Core\Domain\Attributes\Traits\StateAware;
use App\Core\Domain\Attributes\Traits\EmailAware;
use App\Core\Domain\Attributes\Traits\HasIdentity;
use App\Core\Domain\Attributes\Traits\NameAware;
use App\Core\Domain\Attributes\Traits\PhoneAware;
use App\Core\Domain\Attributes\Traits\SlugAware;
use App\Core\Eloquent\AbstractModel;
use App\Core\Eloquent\Timestampable;
use App\Domain\Catalog\Category\Category;
use App\Domain\Company\Contracts\Company as CompanyContract;
use App\Domain\Company\Enums\CompanyTypeEnum;
use App\Domain\Company\Traits\CompanyTypeTrait;
use App\Domain\Product\Product;
use App\Domain\Region\oldRegion;
use App\Domain\User\User;
use Carbon\Carbon;
use Kalnoy\Nestedset\Collection;
use Staudenmeir\EloquentEagerLimit\HasEagerLimit;


/**
 * App\Domain\Company\Company
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string|null $tagline
 * @property CompanyTypeEnum $company_type Вид деятельности
 * @property string|null $description
 * @property string|null $logo
 * @property string|null $country
 * @property \App\Domain\Region\oldRegion $region
 * @property string|null $city
 * @property string|null $address
 * @property string|null $zip
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $site_link
 * @property string|null $skype
 * @property string|null $other_contacts
 * @property string|null $legal_other
 * @property string|null $contact_person
 * @property string|null $reg_data Дата регистрации компании
 * @property string|null $tags
 * @property string|null $min_amount Минимальная сумма закупки
 * @property int|null $scope_supply Объем поставок
 * @property int|null $payment_options Варианты оплаты
 * @property int|null $additional_options Дополнительные возможности
 * @property int|null $delivery_options Варианты доставки
 * @property string|null $terms_payment_delivery Условия оплаты и доставки
 * @property string|null $terms_return_guarantee Условия возврата и гарантии
 * @property string|null $name_legal_entity Наименование юридического лица
 * @property int $privacy 0 - Публичная анкета, 1 - Скрытая анкета
 * @property int $enabled
 * @property int $status 0 астивен, 1 заблокирован, 2 забанен на время
 * @property int $verified
 *
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @property-read Collection|Category[] $category
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $users
 */
class Company extends AbstractModel implements CompanyContract
{
    use HasIdentity, NameAware, SlugAware, PhoneAware, EmailAware, CompanyTypeTrait, StateAware, Timestampable, HasEagerLimit, GeoAware;

    protected $table = 'companies';

    protected $hidden = ['pivot'];

    // protected $primaryKey

    protected $fillable = [
        'name',
        'slug',
        'tagline',
        'company_type',
        'description',
        'logo',
        'country',
        'region',
        'city',
        'address',
        'zip',
        'phone',
        'email',
        'site_link',
        'skype',
        'other_contacts',
        'legal_other',
        'contact_person',
        'reg_data',
        'tags',
        'meta_description',
        'min_amount',
        'scope_supply',
        'payment_options',
        'additional_options',
        'delivery_options',
        'terms_payment_delivery',
        'terms_return_guarantee',
        'name_legal_entity',
        'privacy',
    ];

    protected $casts = [
        'company_type' => CompanyTypeEnum::class
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public static function getSupplyArray()
    {
        return [
            self::SUPPLY_LARGE => 'Крупный опт',
            self::SUPPLY_SMALL => 'Мелкий опт',
            self::SUPPLY_PIECE => 'Поштучно',
        ];
    }

    public function getSupply()
    {
        return static::getSupplyArray()[$this->scope_supply];
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'companies_user');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'company_id')->latest();
    }

    public function getProducts()
    {
        return $this->getRelationValue('products');
    }

//    public function productsFive()
//    {
//        return $this->hasMany(Product::class, 'company_id')->latest()->take(5);
//    }

    public function category()
    {
        return $this->belongsToMany(Category::class, Product::getTableName());
    }

    public function region()
    {
        return $this->belongsTo(oldRegion::class, 'region_id', 'id');
    }


    /**
     * Получение подзаголовка компании (Краткое описание)
     *
     * @return string
     */
    public function getTagline(): string
    {
        return (string)$this->getAttributeValue('tagline');
    }

    /**
     * Установка подзаголовка компании (Краткое описание)
     *
     * @param string $tagline
     * @return CompanyContract
     */
    public function setTagline(string $tagline): CompanyContract
    {
        $this->setAttribute('tagline', $tagline);

        return $this;
    }

    /**
     * Получение описания компании
     *
     * @return string
     */
    public function getDescription(): string
    {
        return (string)$this->getAttributeValue('description');
    }

    /**
     * Установка описания компании
     *
     * @param string $description
     * @return CompanyContract
     */
    public function setDescription(string $description): CompanyContract
    {
        $this->setAttribute('description', $description);

        return $this;
    }

    /**
     * Получение логотипа компании
     *
     * @return string
     */
    public function getLogo(): string
    {
        return (string)$this->getAttributeValue('logo');
    }

    /**
     * Установка логотипа компании
     *
     * @param string $logo
     * @return CompanyContract
     */
    public function setLogo(string $logo): CompanyContract
    {
        $this->setAttribute('logo', $logo);

        return $this;
    }


    /**
     * Получение адреса компании
     *
     * @return string
     */
    public function getAddress(): string
    {
        return (string)$this->getAttributeValue('address');
    }

    /**
     * Установка адреса компании
     *
     * @param string $address
     * @return CompanyContract
     */
    public function setAddress(string $address): CompanyContract
    {
        $this->setAttribute('address', $address);

        return $this;
    }


    /**
     * Получение сайта компании
     *
     * @return string
     */
    public function getSiteLink(): string
    {
        // TODO: Implement getSiteLink() method.
    }

    /**
     * Установка сайта компании
     *
     * @param string $site_link
     * @return CompanyContract
     */
    public function setSiteLink(string $site_link): CompanyContract
    {
        // TODO: Implement setSiteLink() method.
    }

    /**
     * Получение юридические данные компании
     *
     * @return string
     */
    public function getLegalOther(): string
    {
        // TODO: Implement getLegalOther() method.
    }

    /**
     * Установка юридические данные компании
     *
     * @param string $legal_other
     * @return CompanyContract
     */
    public function setLegalOther(string $legal_other): CompanyContract
    {
        // TODO: Implement setLegalOther() method.
    }

    /**
     * Получение представителя компании
     *
     * @return string
     */
    public function getContactPerson(): string
    {
        // TODO: Implement getContactPerson() method.
    }

    /**
     * Установка представителя компании
     *
     * @param string $contact_person
     * @return CompanyContract
     */
    public function setContactPerson(string $contact_person): CompanyContract
    {
        // TODO: Implement setContactPerson() method.
    }


    /**
     * Получение даты регистрации компании
     *
     * @return Carbon|null
     */
    public function getRegData(): ?Carbon
    {
        // TODO: Implement getRegData() method.
    }

    /**
     * Установка дата регистрации компании
     *
     * @param string $reg_data
     * @return CompanyContract
     */
    public function setRegData(string $reg_data): CompanyContract
    {
        // TODO: Implement setRegData() method.
    }

    /**
     * Получение тегов компании (Ключевые слова / Теги)
     */
    public function getTags(): string
    {
        // TODO: Implement getTags() method.
    }

    /**
     * Установка тегов компании (Ключевые слова / Теги)
     */
    public function setTags(string $tags)
    {
        // TODO: Implement setTags() method.
    }

    public function getMetaDescription()
    {
        // TODO: Implement getMetaDescription() method.
    }

    public function setMetaDescription(string $meta_description)
    {
        // TODO: Implement setMetaDescription() method.
    }

    public function getPrivacy()
    {
        // TODO: Implement getPrivacy() method.
    }

    public function setPrivacy(int $privacy)
    {
        // TODO: Implement setPrivacy() method.
    }


}
