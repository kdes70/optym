<?php

use App\Parsers\Factory;
use App\Parsers\Parser;

class CompanyParserTrait
{
    /**
     * @return Parser
     */
    public function getParser()
    {
        return app(Factory::class)->shop($this->parser);
    }
}