<?php

namespace App\Domain\Company\Traits;

use App\Domain\Company\Contracts\Company;
use App\Domain\Company\Enums\CompanyTypeEnum;


/**
 * Trait CompanyTypeTrait
 *
 * @package App\Domain\Company\Traits
 */
trait CompanyTypeTrait
{

    public static function getBusinessEntity()
    {
        return config('import.types_business');
    }

    /**
     * Получение типа компании (Вид деятельности)
     *
     * @return string
     */
    public function getCompanyType(): string
    {
        return (string)static::companyTypeList()[$this->getAttributeValue('company_type')];
    }

    /**
     * Установка типа компании (Вид деятельности)
     *
     * @param \App\Domain\Company\Enums\CompanyTypeEnum $company_type
     *
     * @return Company
     */
    public function setCompanyType(CompanyTypeEnum $company_type): Company
    {
        $this->setAttribute('company_type', $company_type);

        return $this;
    }


}
