<?php

namespace App\Domain\Company\Contracts;

use App\Core\Domain\Attributes\EmailAwareInterface;
use App\Core\Domain\Attributes\GeoAwareInterface;
use App\Core\Domain\Attributes\HasIdentityInterface;
use App\Core\Domain\Attributes\NameAwareInterface;
use App\Core\Domain\Attributes\PhoneAwareInterface;
use App\Core\Domain\Attributes\SlugAwareInterface;
use App\Core\Domain\Attributes\TimestampableInterface;
use App\Core\Domain\Attributes\StateAwereInterface;
use App\Domain\Company\Enums\CompanyTypeEnum;
use Carbon\Carbon;

interface Company extends
    HasIdentityInterface,
    SlugAwareInterface,
    NameAwareInterface,
    PhoneAwareInterface,
    EmailAwareInterface,
    StateAwereInterface,
    TimestampableInterface,
    GeoAwareInterface
{

//    const TYPE_RETAIL = 1; //Розничная торговля
//    const TYPE_WHOLESALE = 2; //Оптовая торговля
//    const TYPE_PRODUCTION = 3; //Производство товаров
//    const TYPE_SERVICES = 4; // Предоставление услуг

    /**
     * Объем поставок
     */
    const SUPPLY_LARGE = 1; // Крупный опт
    const SUPPLY_SMALL = 2; // Мелкий опт
    const SUPPLY_PIECE = 3; // Поштучно
    /**
     * Варианты оплаты
     */
    const PAYMENT_IN_CASH = 1; // Наличными
    const PAYMENT_BANK_TRANSFER = 2; // Банковский перевод
    const PAYMENT_CREDIT_CARDS = 3; // Кредитные карты
    const PAYMENT_ELECTRONIC_MONEY = 4; // Электронные деньги

    /**
     * Дополнительные возможности
     */
    const OPTION_PAYMENT_BY_INSTALLMENT = 1; // Возможна рассрочка
    const OPTION_CREDIT_AVAILABLE = 2; // Возможен кредит
    const OPTION_POSSIBLE_DEPOSIT = 3; // Возможен депозит
    const OPTION_TRANSFER_IMPLEMENTATION = 4; // Возможна передача на реализацию
    const OPTION_DROP_SHIPPING = 5; // Возможен дропшиппинг
    const OPTION_SHOWROOM = 6; // Есть шоурум

    /**
     * Варианты доставки
     */
    const DELIVERY_PICKUP = 1; // Самовывоз
    const DELIVERY_COMPANY = 2; // Транспортной компаней
    const DELIVERY_CAR = 3; // Автомобилем
    const DELIVERY_AIR = 4; // Авиатранспортом
    const DELIVERY_RAILWEY = 5; // Железной дорогой
    const DELIVERY_COURIER = 6; // Курьером


    /**
     * Получение подзаголовка компании (Краткое описание)
     *
     * @return string
     */
    public function getTagline(): string;

    /**
     * Установка подзаголовка компании (Краткое описание)
     *
     * @param string $tagline
     * @return Company
     */
    public function setTagline(string $tagline): Company;

    /**
     * Получение типа компании (Вид деятельности)
     *
     * @return string
     */
    public function getCompanyType(): string;

    /**
     * Установка типа компании (Вид деятельности)
     *
     * @param CompanyTypeEnum $company_type
     * @return Company
     */
    public function setCompanyType(CompanyTypeEnum $company_type): Company;

    /**
     * Получение описания компании
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * Установка описания компании
     *
     * @param string $description
     * @return Company
     */
    public function setDescription(string $description): Company;


    /**
     * Получение логотипа компании
     *
     * @return string
     */
    public function getLogo(): string;

    /**
     * Установка логотипа компании
     *
     * @param string $logo
     * @return Company
     */
    public function setLogo(string $logo): Company;

    /**
     * Получение адреса компании
     *
     * @return string
     */
    public function getAddress(): string;

    /**
     * Установка адреса компании
     *
     * @param string $address
     * @return Company
     */
    public function setAddress(string $address): Company;

//    /**
//     * @return string
//     */
//    public function getZip(): string;
//
//    public function setZip(string $zip): Company;

    /**
     * Получение сайта компании
     *
     * @return string
     */
    public function getSiteLink(): string;

    /**
     * Установка сайта компании
     *
     * @param string $site_link
     * @return Company
     */
    public function setSiteLink(string $site_link): Company;

//    public function getSkype(): string;
//
//    public function setSkype(string $skype);
//
//    public function getOtherContacts(): string;
//
//    public function setOtherContacts(string $other_contacts);


    /**
     * Получение юридические данные компании
     *
     * @return string
     */
    public function getLegalOther(): string;


    /**
     * Установка юридические данные компании
     *
     * @param string $legal_other
     * @return Company
     */
    public function setLegalOther(string $legal_other): Company;

    /**
     * Получение представителя компании
     *
     * @return string
     */
    public function getContactPerson(): string;

    /**
     * Установка представителя компании
     *
     * @param string $contact_person
     * @return Company
     */
    public function setContactPerson(string $contact_person): Company;

    /**
     * Получение даты регистрации компании
     *
     * @return Carbon|null
     */
    public function getRegData(): ?Carbon;

    /**
     * Установка дата регистрации компании
     *
     * @param string $reg_data
     * @return Company
     */
    public function setRegData(string $reg_data): Company;

    /**
     * Получение тегов компании (Ключевые слова / Теги)
     */
    public function getTags(): string;

    /**
     * Установка тегов компании (Ключевые слова / Теги)
     */
    public function setTags(string $tags);

    public function getMetaDescription();

    public function setMetaDescription(string $meta_description);

//    public function getBadges(): Badges;
//
//    public function setBadges(Badges $badges);

    public function getPrivacy();

    public function setPrivacy(int $privacy);

}
