<?php

namespace App\Domain\Region;


use App\Core\Domain\Attributes\NameAwareInterface;
use App\Core\Domain\Attributes\Traits\HasIdentity;
use App\Core\Domain\Attributes\Traits\NameAware;
use App\Core\Domain\Attributes\Traits\SlugAware;
use App\Core\Eloquent\AbstractModel;
use Illuminate\Database\Eloquent\Builder;
use App\Domain\Region\Contracts\Region as RegionContract;

/**
 * App\Domain\oldRegion\oldRegion
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int|null $parent_id
 * @property oldRegion $parent
 * @property oldRegion[] $children
 * @method Builder roots()
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Region\oldRegion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Region\oldRegion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Region\oldRegion whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Region\oldRegion whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Region\oldRegion whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Region\oldRegion whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class oldRegion extends AbstractModel implements RegionContract
{
    use HasIdentity, SlugAware, NameAware;

    protected $fillable = ['name', 'slug', 'parent_id'];

    public function getPath(): string
    {
        return ($this->parent ? $this->parent->getPath() . '/' : '') . $this->getSlug();
    }

    public function getAddress(): string
    {
        return ($this->parent ? $this->parent->getAddress() . ', ' : '') . $this->name;
    }

    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany(static::class, 'parent_id', 'id');
    }

    public function scopeRoots(Builder $query)
    {
        return $query->where('parent_id', null);
    }
}
