<?php

namespace App\Domain\Region\Contracts;

use App\Core\Domain\Attributes\HasIdentityInterface;
use App\Core\Domain\Attributes\NameAwareInterface;
use App\Core\Domain\Attributes\SlugAwareInterface;

interface Region extends HasIdentityInterface, SlugAwareInterface, NameAwareInterface
{

    public function getPath(): string;

    public function getAddress(): string;


    // TODO переделать по типам
//    public function setCountry(string $country): oldRegion;
//
//    public function getCountry(): string;
//
//    public function setRegion(string $city): oldRegion;
//
//    public function getRegion(): string;
//
//    public function setCity(string $city): oldRegion;
//
//    public function getCity(): string;

}