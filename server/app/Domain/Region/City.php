<?php

namespace App\Domain\Region;


use App\Core\Domain\Attributes\Traits\HasIdentity;
use App\Core\Domain\Attributes\Traits\NameAware;
use App\Core\Eloquent\AbstractModel;


/**
 * App\Domain\Region\City
 */
class City extends AbstractModel
{
    use HasIdentity, NameAware;

//    protected $connection = 'mysql_geo';

    protected $table = 'geo_cities';

    public $timestamps = false;

//    protected $fillable = ['name', 'slug', 'parent_id'];


}
