<?php

namespace App\Domain\Region;

class Location
{
    protected $cookie;

    public static function get()
    {
        return cookie(config('app.location_cookie'));
    }

    public static function set($value)
    {
        return cookie(config('app.location_cookie'), $value);
    }
}