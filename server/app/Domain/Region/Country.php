<?php

namespace App\Domain\Region;

use App\Core\Domain\Attributes\Traits\HasIdentity;
use App\Core\Domain\Attributes\Traits\NameAware;
use App\Core\Eloquent\AbstractModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends AbstractModel
{
    use HasIdentity, SoftDeletes, NameAware;

//    protected $connection = 'mysql_geo';

    protected $primaryKey = 'id';

    protected $table = 'geo_countries';

    public $incrementing = false;

    public $timestamps = false;

    const RUSSIA = 'RU';
    const BELARUS = 'BY';
    const KAZAKHSTAN = 'KZ';
    const UKRAINE = 'UA';


    public function regions()
    {
        return $this->hasMany(Region::class);
    }

}
