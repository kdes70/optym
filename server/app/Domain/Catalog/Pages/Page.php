<?php

namespace App\Domain\Catalog\Pages;

use App\Core\Domain\Attributes\Traits\CategoryIdAware;
use App\Core\Domain\Attributes\Traits\HasIdentity;
use App\Core\Domain\Attributes\Traits\SlugAware;
use App\Core\Domain\Attributes\Traits\SortableAware;
use App\Core\Domain\Attributes\Traits\TitleAware;
use App\Core\Domain\Attributes\Traits\Toggleable;

use App\Domain\Catalog\Category\Category;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

/**
 * App\Domain\Catalog\Pages\Page
 *
 * @property int $id
 * @property string $title
 * @property string $menu_title
 * @property string $slug
 * @property string $content
 * @property string $description
 * @property int|null $parent_id
 * @property int $depth
 * @property Page $parent
 * @property Page[] $children
 * @property mixed enabled
 * @mixin \Eloquent
 */
class Page extends Model implements PagesContract
{
    use HasIdentity, TitleAware, SlugAware, Toggleable, CategoryIdAware, SortableAware;

    protected $table = 'pages';

    protected $fillable = ['title', 'slug', 'category_id', 'banner'];

    protected $guarded = [];


    public function getMenuTitle(): string
    {
        return $this->menu_title ?? $this->title;
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getUrl(): string
    {
        // TODO: Implement getUrl() method.
    }

    public function getContent(): string
    {
        return $this->getAttributeValue('content');
    }

    public function setContent(?string $content)
    {
        $this->setAttribute('content', $content);

        return $this;
    }

    public function getMetaDescription(): string
    {
        return $this->getAttributeValue('meta_description');
    }

    public function setMetaDescription(?string $meta_description)
    {
        $this->setAttribute('meta_description', $meta_description);

        return $this;
    }

    public function getMetaKeywords(): string
    {
        return $this->getAttributeValue('sort');
    }

    public function setMetaKeywords(?string $meta_keywords)
    {
        $this->setAttribute('meta_keywords', $meta_keywords);

        return $this;
    }


    public function getBanner(): string
    {
        return $this->getAttributeValue('banner');
    }

    public function setBanner(?string $banner)
    {
        $this->setAttribute('banner', $banner);

        return $this;
    }

}
