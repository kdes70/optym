<?php

namespace App\Domain\Catalog\Pages;

use App\Core\Domain\Attributes\HasIdentityInterface;
use App\Core\Domain\Attributes\ToggleableInterface;
use App\Core\Domain\Attributes\SlugAwareInterface;
use App\Core\Domain\Attributes\TitleAwareInterface;
use App\Core\Domain\Attributes\CategoryIdAwareInterface;
use App\Core\Domain\Attributes\SortableInterface;

interface PagesContract extends
    HasIdentityInterface,
    SlugAwareInterface,
    TitleAwareInterface,
    ToggleableInterface,
    CategoryIdAwareInterface,
    SortableInterface
{

    public function getUrl(): string;

    public function getContent(): string;

    public function setContent(?string $content);

    public function getMetaDescription(): string;

    public function setMetaDescription(?string $meta_description);

    public function getMetaKeywords(): string;

    public function setMetaKeywords(?string $meta_keywords);

}