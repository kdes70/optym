<?php

namespace App\Domain\Catalog\Category;

use App\Core\Domain\Attributes\Traits\HasIdentity;
use App\Core\Domain\Attributes\Traits\ImageAware;
use App\Core\Domain\Attributes\Traits\NameAware;
use App\Core\Domain\Attributes\Traits\SlugAware;
use App\Core\Domain\Attributes\Traits\Toggleable;
use App\Domain\Company\Company;
use App\Domain\Catalog\Pages\Page;
use App\Domain\Product\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Kalnoy\Nestedset\NodeTrait;
use App\Domain\Catalog\Category\Contracts\Category as CategoryContract;

/**
 * App\Domain\Catalog\Category\Category
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int|null $parent_id
 * @property int $depth
 * @property Category $parent
 * @property Category[] $children
 * @property Attribute[] $attributes
 * @property string|null $path
 * @property string|null $description
 * @property int $_lft
 * @property int $_rgt
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Company\Company[] $companies
 * @property-read mixed $leaf
 * @property-read mixed $link
 */
class Category extends Model implements CategoryContract
{
    use NodeTrait, HasIdentity, NameAware, SlugAware, ImageAware, Toggleable;

    protected $table = 'categories';

    public $timestamps = false;

    protected $fillable = ['name', 'slug', 'parent_id', 'path', 'image', 'enabled'];

    protected $appends = ['link', 'leaf'];

    protected $hidden = ['_lft', '_rgt'];

    protected static function boot()
    {
        parent::boot();

        static::saving(function (self $model) {
            if ($model->isDirty('slug', 'parent_id')) {
                $model->generatePath();
            }
        });

        static::saved(function (self $model) {
            // Данная переменная нужна для того, чтобы потомки не начали вызывать
            // метод, т.к. для них путь также изменится
            static $updating = false;

            if (!$updating && $model->isDirty('path')) {
                $updating = true;

                $model->updateDescendantsPaths();

                $updating = false;
            }
        });
    }


    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }


    /**
     *  Генерация пути
     *
     * @return $this|string
     */
    public function generatePath()
    {
        $slug = $this->slug;

        if ($this->isRoot()) {
            $this->setPath($slug);
        } elseif ($this->parent->isRoot()) {
            $this->setPath($slug);
        } else {
            $this->setPath($this->parent->path . '/' . $slug);
        }

        return $this;
    }

//    public function getPathUrl()
//    {
//        $slug = $this->slug;
//
//        $path = $this->isRoot() ? $slug :
//            implode('/', array_merge($this
//                ->ancestors()
//                ->defaultOrder()
//                ->where('parent_id', '!=', null)
//                ->pluck('slug')
//                ->toArray(),
//                [$slug]));
//
//        return $path;
//
//    }

    /**
     *  Получить всех потомков и сгенерировать для них новый путь
     */
    public function updateDescendantsPaths()
    {
        // Получаем всех потомков в древовидном порядке
        $descendants = $this->descendants()->defaultOrder()->get();

        // Данный метод заполняет отношения parent и children
        $descendants->push($this)->linkNodes()->pop();

        foreach ($descendants as $model) {
            $model->generatePath()->save();
        }
    }

    /**
     *  Получение ссылки
     *
     * @return string
     */
    public function getUrl(): string
    {
        return $this->getPath();
    }


    /**
     * Получение урл категории
     *
     * @return string
     */
    public function getLinkAttribute(): string
    {
        return route('category', $this->getUrl());
    }

    public function getLeafAttribute(): bool
    {
        return false;
    }

    public function parentAttributes(): array
    {
        return $this->parent ? $this->parent->allAttributes() : [];
    }

    /**
     * @return Attribute[]
     */
    public function allAttributes(): array
    {
        return array_merge($this->parentAttributes(), $this->attributes()->getModels());
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class,
            'categories_attribute',
            'value_id',
            'category_id'
        );
    }

//    public function companies()
//    {
//        return $this->hasMany(Company::class)
//            ->using(CategoryCompany::class);
//
//    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function companies()
    {
        return $this->belongsToMany(Company::class, Product::getTableName());
    }

    public function getCompanies()
    {
        return $this->getRelationValue('companies');
    }

    /**
     * Получение всех товаров категории.
     *
     * @return Collection|Category[]
     */
    public function getProducts(): Collection
    {
        return $this->getRelationValue('products');
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }


// TODO для использования смежной таблицы, пока не испльзуем

//    public function products()
//    {
//        return $this->belongsToMany(
//            Product::class, 'category_product', 'product_id', 'category_id'
//        );
//    }

    public function getPath(): string
    {
        return (string)$this->getAttributeValue('path');
    }

    public function setPath(string $path)
    {
        $this->setAttribute('path', $path);

        return $this;
    }

    public function page()
    {
        return $this->hasOne(Page::class);
    }

    public function getPage()
    {
        return $this->getRelationValue('page');
    }

    /**
     * Relation to children.
     *
     * @return HasMany
     */
    public function children()
    {
        return $this->hasMany(get_class($this), $this->getParentIdName())
            ->setModel($this)->enabled()->with('children');
    }

    public function getChildren()
    {
        return $this->getRelationValue('children');
    }
}
