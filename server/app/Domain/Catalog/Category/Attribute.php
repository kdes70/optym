<?php

namespace App\Domain\Catalog\Category;

use App\Core\Domain\Attributes\Traits\HasIdentity;
use App\Core\Domain\Attributes\Traits\SlugAware;
use App\Core\Domain\Attributes\Traits\TitleAware;
use App\Core\Domain\Attributes\Traits\TypeAware;
use App\Core\Eloquent\AbstractModel;
use App\Domain\Catalog\Category\Contracts\Attribute as AttributeContract;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;


/**
 * App\Domain\Catalog\Category\Attribute
 *
 * @property int $id
 * @property int $category_id
 * @property string $title
 * @property string $type
 * @property string $default
 * @property boolean $required
 * @property array $variants
 * @property integer $sort
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Catalog\Category\Attribute whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Catalog\Category\Attribute whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Catalog\Category\Attribute whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Catalog\Category\Attribute whereRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Catalog\Category\Attribute whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Catalog\Category\Attribute whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Catalog\Category\Attribute whereVariants($value)
 * @mixin \Eloquent
 */
class Attribute extends AbstractModel implements AttributeContract
{
    use HasIdentity, SlugAware, TitleAware, TypeAware;

    protected $table = 'attributes';

    public $timestamps = false;

    protected $fillable = ['title', 'slug', 'type'];

//    protected $with = ['values'];

    public function values(): HasMany
    {
        return $this->hasMany(AttributeValue::class);
    }

    /**
     * @return AttributeValue[]|Collection
     */
    public function getValues()
    {
        return $this->getRelationValue('values');
    }

    public function isString(): bool
    {
        return $this->type === self::TYPE_STRING;
    }

    public function isInteger(): bool
    {
        return $this->type === self::TYPE_INTEGER;
    }

    public function isFloat(): bool
    {
        return $this->type === self::TYPE_FLOAT;
    }

    public function isNumber(): bool
    {
        return $this->isInteger() || $this->isFloat();
    }

    public function isSelect(): bool
    {
        return \count($this->variants) > 0;
    }

    /**
     * @return bool
     */
    public function isBoll(): bool
    {
        return $this->type === self::TYPE_BOOL;
    }


    /**
     * Получение списока типов
     *
     * @return array|null
     */
    static public function getListType(): ?array
    {
        return [
            self::TYPE_STRING   => 'String',
            self::TYPE_SELECT   => 'Select',
            self::TYPE_CHECKBOX => 'Checkbox',
            self::TYPE_RADIO    => 'Radio',
            self::TYPE_INTEGER  => 'Integer',
            self::TYPE_FLOAT    => 'Float',
            self::TYPE_BOOL     => 'Bool',
        ];
    }

    /**
     * @return bool
     */
    public function getRequired(): bool
    {
        return $this->getAttributeValue('required');
    }

    /**
     * @param bool $required
     * @return AttributeContract
     */
    public function setRequired(bool $required)
    {
        $this->setAttribute('required', $required);

        return $this;
    }
}
