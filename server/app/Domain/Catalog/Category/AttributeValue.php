<?php

namespace App\Domain\Catalog\Category;


use App\Core\Domain\Attributes\HasIdentityInterface;
use App\Core\Domain\Attributes\SkuAwareInterface;
use App\Core\Domain\Attributes\SlugAwareInterface;
use App\Core\Domain\Attributes\TitleAwareInterface;
use App\Core\Domain\Attributes\Traits\HasIdentity;
use App\Core\Domain\Attributes\Traits\SkuAware;
use App\Core\Domain\Attributes\Traits\SlugAware;
use App\Core\Domain\Attributes\Traits\TitleAware;
use App\Core\Eloquent\AbstractModel;
use App\Domain\Catalog\Category\Attribute;
use App\Domain\Product\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Сущность «Значение атрибута»
 *
 * @property mixed value_id
 * @property mixed title
 * @Slug(field="title")
 *
 * @package App\Domain\Category
 */
class AttributeValue extends AbstractModel implements
    TitleAwareInterface,
    SlugAwareInterface,
    HasIdentityInterface,
    SkuAwareInterface

{
    use HasIdentity, TitleAware, SlugAware, SkuAware;

    protected $table = 'attribute_value';

    protected $primaryKey = 'value_id';

    protected $fillable = ['attribute_id', 'title', 'slug', 'sku'];

    protected $with = ['parent_attribute'];

  //  protected $touches = ['products', 'categoryes'];

   // protected $touches    = ['category'];

    public function parent_attribute(): BelongsTo
    {
        return $this->belongsTo(Attribute::class, 'attribute_id');
    }

    public function getParentAttribute(): Attribute
    {
        return $this->getRelationValue('parent_attribute');
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(
            Product::class, 'attribute_product', 'value_id', 'product_id'
        );
    }

    /**
     * @return Product[]|Collection
     */
    public function getProducts()
    {
        return $this->getRelationValue('products');
    }

    public function categoryes(): BelongsToMany
    {
        return $this->belongsToMany(
            Product::class, 'categories_attribute', 'value_id', 'category_id'
        );
    }

    /**
     * @return Product[]|Collection
     */
    public function getCategoryes()
    {
        return $this->getRelationValue('categoryes');
    }


    public function getAttributeId(): int
    {
        return $this->getAttributeValue('attribute_id');
    }
}
