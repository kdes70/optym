<?php

namespace App\Domain\Catalog\Category\Contracts;

use App\Core\Domain\Attributes\HasIdentityInterface;
use App\Core\Domain\Attributes\SlugAwareInterface;
use App\Core\Domain\Attributes\TitleAwareInterface;
use App\Core\Domain\Attributes\TypeAwareInterface;

interface Attribute extends
    HasIdentityInterface,
    TitleAwareInterface,
    SlugAwareInterface,
    TypeAwareInterface
{

    public const TYPE_STRING = 'string';
    public const TYPE_SELECT = 'select';
    public const TYPE_CHECKBOX = 'checkbox';
    public const TYPE_RADIO = 'radio';
    public const TYPE_INTEGER = 'integer';
    public const TYPE_FLOAT = 'float';
    public const TYPE_BOOL = 'bool';

    /**
     * @return bool
     */
    public function isString(): bool;

    /**
     * @return bool
     */
    public function isInteger(): bool;

    /**
     * @return bool
     */
    public function isFloat(): bool;

    /**
     * @return bool
     */
    public function isNumber(): bool;

    /**
     * @return bool
     */
    public function isSelect(): bool;

    /**
     * @return bool
     */
    public function isBoll(): bool;

    /**
     * @return bool
     */
    public function getRequired(): bool;

    /**
     * @param bool $required
     * @return $this
     */
    public function setRequired(bool $required);


}