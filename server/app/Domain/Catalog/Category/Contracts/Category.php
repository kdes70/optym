<?php

namespace App\Domain\Catalog\Category\Contracts;

use App\Core\Domain\Attributes\HasIdentityInterface;
use App\Core\Domain\Attributes\NameAwareInterface;
use App\Core\Domain\Attributes\SlugAwareInterface;
use App\Core\Domain\Attributes\ImageAwareInterface;
use App\Core\Domain\Attributes\ToggleableInterface;

interface Category extends
    HasIdentityInterface,
    SlugAwareInterface,
    NameAwareInterface,
    ImageAwareInterface,
    ToggleableInterface
{

    public function getUrl(): string;

    public function getPath(): string;

    public function setPath(string $path);

}