<?php

namespace App\Mail\Auth;

use App\Domain\User\User;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class VerifyMail
 */
class VerifyMail extends Mailable
{
    use SerializesModels;

    /** @var User  */
    public $user;

    /**
     * VerifyMail constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return VerifyMail
     */
    public function build()
    {
        return $this
            ->subject('Signup Confirmation')
            ->markdown('email.auth.register.verify')
            ->with('token', $this->user->getVerifyToken());
    }
}
