<?php

namespace App\Http\Resources;

use App\Domain\Product\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var Product $this
         */

//        dd($this->load('company'));

        return [
            'id'           => $this->id,
            'category'     => new CategoryResource($this->getCategories()),
            'company'      => new CompanyResource($this->getCompany()),
            'title'        => $this->getTitle(),
            'slug'         => $this->getSlug(),
            'link'         => route('suppliers.show-product', [$this->company, $this]),
            'cost_price'   => $this->getCostPrice(),
            'price'        => $this->getFormatCostPrice(),
            'retail_price' => $this->getRetailPrice(),
            'main_image'   => ($this->getMainImage()) ? $this->getMainImage()->getPath() : '/img/no_avatar.jpg',
            'description'  => $this->getDescription(),
            'badges'       => $this->badges,
            'state'        => $this->getState()


            //'company_id' =>
            //'category_id' =>
            //'title' =>

            //'slug' =>
            //'cost_price' =>
            //'retail_price' =>
            //'main_image' =>
            //'description' =>
            //'badges' =>
            //'state' =>

        ];
    }
}
