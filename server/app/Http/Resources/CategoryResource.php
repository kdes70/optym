<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'       => $this->id,
            'parent_id' => $this->parent_id,
            'name'     => $this->name,
            'slug'     => $this->slug,
            'link'     => $this->link,
            'depth'    => $this->depth,
            'hovered'  => false,
            'children' => self::collection($this->children)
        ];
    }
}
