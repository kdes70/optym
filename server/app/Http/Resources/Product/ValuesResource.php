<?php

namespace App\Http\Resources\Product;

use App\Domain\Catalog\Category\Attribute;
use App\Domain\Catalog\Category\AttributeValue;
use Illuminate\Http\Resources\Json\JsonResource;

class ValuesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var $this AttributeValue
         */
        return [
            'id'    => $this->getId(),
            'name' => $this->getTitle(),
            'slug' => $this->getSlug(),
        ];
    }
}
