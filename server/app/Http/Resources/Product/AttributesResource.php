<?php

namespace App\Http\Resources\Product;

use App\Domain\Catalog\Category\Attribute;
use Illuminate\Http\Resources\Json\JsonResource;

class AttributesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var $this Attribute
         */
        return [
            'id'     => $this->getId(),
            'title'  => $this->getTitle(),
            'slug'   => $this->getSlug(),
            'type'   => $this->getType(),
            'values' => !empty($this->getValues()) ? ValuesResource::collection($this->getValues()) : '',
        ];
    }
}
