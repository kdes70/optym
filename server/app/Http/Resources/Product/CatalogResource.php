<?php

namespace App\Http\Resources\Product;

use App\Domain\Catalog\Category\Category;
use Illuminate\Http\Resources\Json\JsonResource;

class CatalogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var $this Category
         */
        return [
            'id'       => $this->id,
            'name'     => $this->name,
            'link'     => $this->link,
            'path'     => $this->path,
            'slug'     => $this->slug,
            'children' => !empty($this->children) ? CatalogResource::collection($this->children) : '',
        ];
    }
}
