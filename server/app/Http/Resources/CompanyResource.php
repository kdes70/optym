<?php

namespace App\Http\Resources;

use App\Domain\Company\Company;
use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var Company $this
         */

        return [
            'id'           => $this->id,
            'name'         => $this->getName(),
            'link'         => route('suppliers.show', $this->getSlug()),
            'slug'         => $this->getSlug(),
            'tagline'      => $this->getTagline(),
            'company_type' => $this->getCompanyType(),
            'description'  => $this->getDescription(),
            'logo'         => $this->getLogo(),
            'country'      => $this->getCountry(),
            'region'       => $this->getRegion(),
            'city'         => $this->getCity(),
            'address'      => $this->getAddress(),
            'zip'          => $this->zip,
            //            'phone',
            //            'email',
            //            'site_link',
            //            'skype',
            //            'other_contacts',
            //            'legal_other',
            //            'contact_person',
            //            'reg_data',
            //            'tags',
            //            'meta_description',
            //            'min_amount',
            //            'scope_supply',
            //            'payment_options',
            //            'additional_options',
            //            'delivery_options',
            //            'terms_payment_delivery',
            //            'terms_return_guarantee',
            //            'name_legal_entity',
            //            'privacy',
        ];
    }
}
