<?php

namespace App\Http\Middleware;

use App\Domain\Region\Location;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckCookieLocation
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->hasCookie(config('app.location_cookie'))) {

            /**
             * @see  https://wisdmlabs.com/blog/create-middleware-country-specific-urls-laravel/
             * @see https://pusher.com/tutorials/localized-laravel-part-3
             */
            //проверить есть ли город в базе
                // нету записываем



            //Location::set($request->cookie(config('app.location_cookie')));
         //   dd($request->cookie(config('app.location_cookie')));

            return $next($request);
        }

        return $next($request);
    }
}
