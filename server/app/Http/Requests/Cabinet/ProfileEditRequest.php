<?php

namespace App\Http\Requests\Cabinet;

use App\Core\DTO\MakesDTO;
use App\Core\DTO\Traits\RequestMakesDTO;
use App\Core\Validation\PhoneValidationRule;
use App\Domain\User\DTO\Update;
use App\Domain\User\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Unique;

class ProfileEditRequest extends FormRequest implements MakesDTO
{
    use RequestMakesDTO;

    public function getDTOClass()
    {
        return Update::class;
    }

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'username' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'last_name' => 'string|max:255',
            'first_name' => 'string|max:255',
            'phone'      => 'required|unique:.users,phone,'.$this->user()->id,
            'country' => 'string',
            'city' => 'string'
        ];
    }
}
