<?php

namespace App\Http\Requests\Auth;

use App\Domain\User\Enums\UserTypeEnum;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RegistrationTypeRequest
 *
 * @property int user_type
 */
class RegistrationTypeRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_type' => ['required', 'integer', UserTypeEnum::rule()],
        ];
    }
}
