<?php

namespace App\Http\Requests\Auth;

use App\Core\Validation\PhoneValidationRule;
use App\Domain\Company\Enums\CompanyTypeEnum;
use App\Domain\User\Enums\UserTypeEnum;
use App\Domain\User\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Unique;

/**
 * Class RegistrationRequest
 *
 * @property-read int $type
 * @property-read string $first_name
 * @property-read string $last_name
 * @property-read string $phone
 * @property-read string $email
 * @property-read string $password
 * @property-read string $company_name
 * @property-read int $company_type
 * @property-read string $country
 * @property-read string $region$
 * @property-read string $city$
 */
class RegistrationRequest extends FormRequest
{

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $rule = [
            'type' => [
                'required',
                UserTypeEnum::rule()
            ],
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'phone' => [
                'nullable',
                new PhoneValidationRule(),
                new Unique(User::getTableName(), 'phone')
            ],
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|min:6',
        ];

        $rule_company = [];

        // доп. проверки взависимости от типа формы
        if (UserTypeEnum::WHOLESALE()->is($this->type)) {
            $rule_company = [
                'company_name' => 'required|min:2',
                'company_type' => [
                    'required',
                    CompanyTypeEnum::rule()
                ],
                'country' => 'nullable|min:2',
                'region' => 'nullable|min:2',
                'city' => 'nullable|min:2',
            ];
        }

        $rules = array_merge($rule_company, $rule);

        return $rules;
    }
}
