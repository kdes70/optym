<?php

namespace App\Http\Requests\Admin\Role;


use App\Domain\User\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @property User $user
 */
class UpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string|max:100',
            'slug' => 'required|string|max:100|unique:roles,id,' . $this->roles->id,
            'description' => 'required|string|max:255',
        ];
    }
}
