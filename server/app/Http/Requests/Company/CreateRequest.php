<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;


class CreateRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name'           => 'required',

            // 'tagline',
            'company_type'   => 'required',
            //  'description',
            //   'logo',
            'country'        => 'required',
            'region'         => 'required',
            'city'           => 'required',
            'address'        => 'required',
            // 'zip',
            'phone'          => 'required',
            'email' => 'required|email',
            // 'site_link',
            //  'skype',
            //  'other_contacts',
            //  'legal_other',
            // 'contact_person' => 'required',
            //   'reg_data',
            //   'tags',
            //  'meta_description',
            //  'min_amount',
//            'scope_supply',
//            'payment_options',
//            'additional_options',
//            'delivery_options',
//            'terms_payment_delivery',
//            'terms_return_guarantee',
//            'name_legal_entity',
//            'privacy',
        ];
    }
}
