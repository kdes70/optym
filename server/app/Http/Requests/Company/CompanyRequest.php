<?php

namespace App\Http\Requests\Company;

use App\Core\DTO\MakesDTO;
use App\Core\DTO\Traits\RequestMakesDTO;
use App\Core\Validation\PhoneValidationRule;
use App\Domain\Catalog\Category\Category;
use App\Domain\Company\DTO\Update;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class CompanyRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        // TODO валидация катигории!
        //  $category =  ['category_id' => ['required', Rule::in((new \App\Domain\Catalog\Category\Category)->pluck('id')->toArray())]];

        return [
            'name'           => 'required',
          //  'slug'           => 'required',
            // 'tagline',
            'company_type'   => 'required',
            //  'description',
            //   'logo',
            'country'        => 'required',
            'region'         => 'required',
            'city'           => 'required',
            'address'        => 'required',
           // 'zip',
            'phone'          => 'required',
            'email' => 'required|email',
           // 'site_link',
          //  'skype',
          //  'other_contacts',
          //  'legal_other',
           // 'contact_person' => 'required',
         //   'reg_data',
         //   'tags',
            //  'meta_description',
          //  'min_amount',
//            'scope_supply',
//            'payment_options',
//            'additional_options',
//            'delivery_options',
//            'terms_payment_delivery',
//            'terms_return_guarantee',
//            'name_legal_entity',
//            'privacy',
        ];
    }
}
