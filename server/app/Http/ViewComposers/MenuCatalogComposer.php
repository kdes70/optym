<?php

namespace App\Http\ViewComposers;

use App\Http\Resources\CategoryResource;
use App\Repositories\Categories\CategoryRepository;
use App\Services\CategoryService;
use Illuminate\View\View;

class MenuCatalogComposer
{
    public function compose(View $view): void
    {
        $repository = app()->make(CategoryRepository::class);

        $service = new CategoryService($repository);

        $categories = CategoryResource::collection($service->getFullTree());

        $view->with('categories', $categories->collection);
    }
}
