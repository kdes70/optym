<?php

namespace App\Http\Controllers\Api;

use App\Domain\Catalog\Category\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\Product\SearchRequest;
use App\Http\Resources\ProductResource;
use App\Transformers\ActivityTransformer;
use App\UseCases\Products\SearchService;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * @var SearchService
     */
    private $search;

    /**
     * Create a new controller instance.
     *
     * @param SearchService $search
     */
    public function __construct(SearchService $search)
    {
        $this->search = $search;
    }

    /**
     * Show the application dashboard.
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function search(SearchRequest $request)
    {

       // dd($request->all());

        $error = ['error' => 'No results found, please try with different keywords.'];

       //if($request->has('text') ||  $request->has('price_min') && $request->has('price_max'))
        //{

       // $caterory = Category::where('id',$request->get('category'))->first();

//            dd($caterory);

        $products = $this->search->search(null, $request, 'search/result',20, $request->get('page', 1));

        if($products)
        {
            return response()->json($products);
        }


        //}

        return response()->json($error);

    }
}