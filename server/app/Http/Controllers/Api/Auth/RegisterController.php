<?php

namespace App\Http\Controllers\Api\Auth;

use App\Core\Http\ApiController;
use App\Core\Http\ApiResponse;
use App\Domain\Company\Enums\CompanyTypeEnum;
use App\Domain\User\DTO\RegisterUser;
use App\Domain\User\Enums\UserTypeEnum;
use App\Http\Requests\Auth\RegistrationRequest;
use App\UseCases\Auth\RegisterService;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Http\JsonResponse;


/**
 * Class RegisterController
 */
class RegisterController extends ApiController
{
    /**
     * @var RegisterService
     */
    private $service;
    /**
     * @var Auth
     */
    private $auth;

    /**
     * Create a new controller instance.
     *
     * @param Auth $auth
     * @param RegisterService $service
     */
    public function __construct(Auth $auth, RegisterService $service)
    {
        $this->service = $service;
        $this->auth = $auth;
    }

    /**
     * @return array
     */
    public function form(): array
    {
        $user_types = UserTypeEnum::toArrayLabels([UserTypeEnum::WHOLESALE, UserTypeEnum::BUYER]);
        $company_types = CompanyTypeEnum::toArrayLabels();

        return compact('user_types', 'company_types');
    }

    /**
     * @param RegistrationRequest $request
     *
     * @return JsonResponse
     *
     * @throws \Throwable
     */
    public function register(RegistrationRequest $request): JsonResponse
    {
        $dto = new RegisterUser(array_merge($request->all(), ['ip_address' => $request->getClientIp()]));

        try {
            $user = $this->service->register($dto);

        } catch (\Throwable $e) {

            return self::error(['error' => __('auth.registration.error')]);
        }

        $message = config('optum.registration.verification') ? __('auth.registration.success_body') : __('auth.registration.success_title');

        if ($user) {
            return self::success(['message' => $message]);
        }
    }
}
