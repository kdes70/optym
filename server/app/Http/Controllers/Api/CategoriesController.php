<?php

namespace App\Http\Controllers\Api;

use App\Domain\Catalog\Category\Category;
use App\Http\Controllers\Controller;
use App\Http\Resources\Product\AttributesResource;
use App\Http\Resources\Product\CatalogResource;
use App\Services\CategoryService;
use Illuminate\Http\Request;


class CategoriesController extends Controller
{

    /**
     * @var CategoryService
     */
    private $service;

    public function __construct(CategoryService $service)
    {

        $this->service = $service;
    }

    public function getAll()
    {
        return $category = Category::defaultOrder()->get()->toTree();
    }

    public function catalog(Request $request)
    {
        if($request->has('segment'))
        {
            $categoryes = $this->service->getChildrenByParentId($request->get('segment'));

            return CatalogResource::collection($categoryes);
        }

        return response()->json('error get catalog', 404);
    }

    public function features(Request $request)
    {
        if($request->has('category'))
        {
            $categoryes = $this->service->getFeatureList($request->get('category'));

            $features = AttributesResource::collection(collect($categoryes));

           return $features;
        }

        return response()->json('error get features', 404);
    }
}