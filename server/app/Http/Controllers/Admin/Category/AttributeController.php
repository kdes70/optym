<?php

namespace App\Http\Controllers\Admin\Category;

use App\Domain\Catalog\Category\Attribute;
use App\Domain\Catalog\Category\Category;
use App\Http\Controllers\Controller;
use Breadcrumbs;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AttributeController extends Controller
{
    public function __construct()
    {
        // $this->middleware('can:manage-adverts-categories');
    }

    public function create(Category $category)
    {
        $types = Attribute::getListType();

        Breadcrumbs::addHierarchyOfNeeds($category->getAncestors(), 'admin.categories.show' );
        Breadcrumbs::append($category->getName(), route('admin.categories.show', $category), false);
        Breadcrumbs::append('Attributes Create', null, true);

        return view('admin.categories.attributes.create', compact('category', 'types'));
    }

    public function store(Request $request, Category $category)
    {
        $this->validate($request, [
            'title'    => 'required|string|max:255',
            'slug'     => 'nullable|alpha_dash',
            'type'     => ['required', 'string', 'max:255', Rule::in(array_keys(Attribute::getListType()))],
            'required' => 'nullable|string|max:255',
        ]);

        $attribute = new Attribute();
        $attribute->setTitle($request['title']);
        $attribute->setSlug($request['slug'] ? $request['slug'] : str_slug($request['title']));
        $attribute->setType($request['type']);
        $attribute->setRequired((bool)$request['required']);

        $attribute = $category->attributes()->save($attribute);
        $attribute->fresh();


        return redirect()->route('admin.categories.attributes.show', [$category, $attribute]);
    }

    public function show(Category $category, Attribute $attribute)
    {
        return view('admin.categories.attributes.show', compact('category', 'attribute'));
    }

    public function edit(Category $category, Attribute $attribute)
    {
        $types = Attribute::getListType();

        return view('admin.categories.attributes.edit', compact('category', 'attribute', 'types'));
    }

    public function update(Request $request, Category $category, Attribute $attribute)
    {
        $this->validate($request, [
            'title'    => 'required|string|max:255',
            'slug'     => 'nullable|alpha_dash',
            'type'     => ['required', 'string', 'max:255', Rule::in(array_keys(Attribute::getListType()))],
            'required' => 'nullable|string|max:255',
        ]);

        $category->attributes()->findOrFail($attribute->id)->update([
            'title'    => $request['title'],
            'slug'     => $request['slug'] ? $request['slug'] : str_slug($request['title']),
            'type'     => $request['type'],
            'required' => (bool)$request['required'],
        ]);

        return redirect()->route('admin.categories.show', $category);
    }

    public function destroy(Category $category)
    {
        $category->delete();

        return redirect()->route('admin.categories.show', $category);
    }
}
