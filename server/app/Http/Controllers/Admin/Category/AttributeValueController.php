<?php

namespace App\Http\Controllers\Admin\Category;

use App\Domain\Catalog\Category\Attribute;
use App\Domain\Catalog\Category\AttributeValue;
use App\Domain\Catalog\Category\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AttributeValueController extends Controller
{


    public function __construct()
    {
        // $this->middleware('can:manage-adverts-categories');
    }

    public function create(Category $category, Attribute $attribute)
    {
        return view('admin.categories.attributes.value.create', compact('category', 'attribute'));
    }

    public function store(Request $request, Category $category, Attribute $attribute)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'slug'  => 'nullable|alpha_dash',
        ]);

        $value = new AttributeValue();

        $slug = $request['slug'] ? $request['slug'] : str_slug($request['title']);

        $value->setTitle($request['title']);
        $value->setSlug($slug);
        $value->setSku($attribute->getSlug() . '-' . $slug);

        $attribute->values()->save($value);
        $attribute->fresh();

        return redirect()->route('admin.categories.attributes.show', [$category, $attribute]);
    }

    public function edit(Category $category, Attribute $attribute, AttributeValue $value)
    {
        return view('admin.categories.attributes.value.edit', compact('category', 'attribute', 'value'));
    }

    public function update(Request $request, Category $category, Attribute $attribute, AttributeValue $value)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'slug'  => 'nullable|alpha_dash',
        ]);

        $value = $attribute->getValues()->findOrFail($value->getId());

        $value->fill($request->all())->save();

        return redirect()->route('admin.categories.attributes.show', [$category, $attribute]);
    }

    public function destroy(Category $category, Attribute $attribute, AttributeValue $value)
    {
        $value->delete();

        return redirect()->route('admin.categories.attributes.show', [$category, $attribute]);
    }
}
