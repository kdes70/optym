<?php

namespace App\Http\Controllers\Admin\Category;

use App\Domain\Catalog\Category\Category;
use App\Http\Controllers\Controller;
use Breadcrumbs;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {
      //  $this->middleware('can:manage-adverts-categories');
        Breadcrumbs::append('Categoryes', route('admin.categories.index'));
    }

    public function index()
    {
        $categories = Category::defaultOrder()->withDepth()->get();

        return view('admin.categories.index', compact('categories'));
    }

    public function create()
    {
        $parents = Category::defaultOrder()->withDepth()->get();

        return view('_admin.categories.create', compact('parents'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'slug' => 'alphadash|max:255',
            'parent' => 'nullable|integer|exists:categories,id',
        ]);

        $category = Category::create([
            'name'      => $request['name'],
            'slug'      => str_slug($request['slug']),
            'parent_id' => $request['parent'],
        ]);

        return redirect()->route('admin.categories.show', $category);
    }

    public function show(Category $category)
    {
        $parent_attributes = $category->parentAttributes();
        $attributes = $category->attributes()->get();

        Breadcrumbs::addHierarchyOfNeeds($category->getAncestors(), 'admin.categories.index');
        Breadcrumbs::append($category->getName(), null, true);

        return view('admin.categories.show', compact('category', 'attributes', 'parent_attributes'));
    }

    public function edit(Category $category)
    {
        $parents = Category::defaultOrder()->withDepth()->get();

        return view('admin.categories.edit', compact('category', 'parents'));
    }

    public function update(Request $request, Category $category)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'slug' => 'required|alphadash|max:255',
            'parent' => 'nullable|integer|exists:categories,id',
        ]);

        $category->update([
            'name' => $request['name'],
            'slug' => str_slug($request['slug']),
            'parent_id' => $request['parent'],
        ]);

        return redirect()->route('admin.categories.show', $category);
    }

    public function first(Category $category)
    {
        if ($first = $category->siblings()->defaultOrder()->first()) {
            $category->insertBeforeNode($first);
        }

        return redirect()->route('admin.categories.index');
    }

    public function up(Category $category)
    {
        $category->up();

        return redirect()->route('admin.categories.index');
    }

    public function down(Category $category)
    {
        $category->down();

        return redirect()->route('admin.categories.index');
    }

    public function last(Category $category)
    {
        if ($last = $category->siblings()->defaultOrder('desc')->first()) {
            $category->insertAfterNode($last);
        }

        return redirect()->route('admin.categories.index');
    }

    public function destroy(Category $category)
    {
        $category->delete();

        return redirect()->route('admin.categories.index');
    }
}
