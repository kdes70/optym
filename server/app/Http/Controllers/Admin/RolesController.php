<?php

namespace App\Http\Controllers\Admin;

use App\Domain\User\Role;
use App\Http\Requests\Admin\Role\CreateRequest;
use App\Http\Requests\Admin\Role\UpdateRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RolesController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $query = (new Role)->orderByDesc('id');

        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
        }

        if (!empty($value = $request->get('name'))) {
            $query->where('name', 'like', '%' . $value . '%');
        }

        if (!empty($value = $request->get('slug'))) {
            $query->where('slug', $value);
        }

        $roles = $query->paginate(20);

        return view('admin.roles.index', compact('roles'));
    }

    public function create()
    {
        return view('admin.roles.create');
    }

    public function store(CreateRequest $request)
    {
        $role = Role::new(
            $request['name'],
            $request['slug'],
            $request['description']
        );

        return redirect()->route('admin.roles.show', $role);
    }

    public function show(Role $role)
    {
        return view('admin.roles.show', compact('role'));
    }

    public function edit(Role $role)
    {
        return view('admin.roles.edit', compact('role'));
    }

    public function update(UpdateRequest $request, Role $role)
    {
        $role->update($request->only(['name', 'slug', 'description']));

        return redirect()->route('admin.roles.show', $role);
    }

    public function destroy(Role $role)
    {
        $role->delete();

        return redirect()->route('admin.roles.index');
    }

}
