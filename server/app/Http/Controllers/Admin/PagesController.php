<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Catalog\Pages\Page;
use App\Http\Requests\Admin\Page\CreateRequest;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        $query = (new Page)->orderByDesc('id');
//
//        $pages = $query->paginate(20);

        $order = $request->get('order', 'created_at');
        $dir = $request->get('dir', 'desc');
        $q = $request->get('q');

        $query = (new Page())->newQuery()->defaultOrder()->withDepth();

        if ($order) {
            $query->orderBy($order, $dir ? 'desc' : 'asc');
        }

        if ($q) {
            $query->where(function ($query) use ($q) {
                $query->where('id', $q)
                    ->orWhere('title', 'like', '%' . $q . '%');
            });
        }

        $pages = $query->paginate(10)->appends($request->all());

        return view('admin.pages.index', compact('pages', 'order', 'dir'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @param CategoryService $service
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(CategoryService $service)
    {
        $categories = $service->getRootIsNotPage();

        //  dd($categories);

        return view('admin.pages.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {

      //  dd($request->all());


        $category = new Page();
        $category->setTitle($request['title']);
        $category->setSlug($request['title']);
        $category->setCategoryId((int)$request['category_id'], true);
        $category->setContent($request['content']);
        $category->setMetaDescription($request['meta_description']);
        $category->setMetaKeywords($request['meta_keywords']);
        $category->setEnabled($request['enabled'] ?? false);

        $category->save();

        //  Page::create($request->only(['title', 'slug', 'category_id']));

        return redirect()->route('admin.pages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Domain\Catalog\Pages\Page $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        return view('admin.pages.show', compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Domain\Catalog\Pages\Page $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return view('admin.pages.edit', compact('page', 'parents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param \App\Domain\Catalog\Pages\Page $page
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Page $page)
    {
        $this->validate($request, [
            'title'       => 'required|string|max:255',
            'slug'        => 'alphadash|max:255',
            'category_id' => 'nullable|integer|exists:categories,id',
        ]);

        $page->update([
            'title'       => $request['title'],
            'slug'        => str_slug($request['slug']),
            'category_id' => $request['category_id'],
        ]);

        return redirect()->route('admin.pages.index');
    }

    public function first(Page $page)
    {
        if ($first = $page->siblings()->defaultOrder()->first()) {
            $page->insertBeforeNode($first);
        }

        return redirect()->route('admin.pages.index');
    }

    public function up(Page $page)
    {
        $page->up();

        return redirect()->route('admin.pages.index');
    }

    public function down(Page $page)
    {
        $page->down();

        return redirect()->route('admin.pages.index');
    }

    public function last(Page $page)
    {
        if ($last = $page->siblings()->defaultOrder('desc')->first()) {
            $page->insertAfterNode($last);
        }

        return redirect()->route('admin.pages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
