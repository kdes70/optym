<?php

namespace App\Http\Controllers\Profiles\Company;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function __invoke($username)
    {
        $company = auth()->user()->getCompany();

//        dd($company);

        return view('profiles.company.home', compact('company'));
    }



}
