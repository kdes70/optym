<?php

namespace App\Http\Controllers\Profiles\Company\Product;


use App\Domain\User\User;
use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class CreateController extends Controller
{

    /**
     * @var CategoryService
     */
    private $service;

    public function __construct(CategoryService $service)
    {
        $this->middleware('can:manage-own-product');

        $this->service = $service;

    }

    public function form()
    {
        //$this->checkAccess(auth()->user());

        $segment = $this->service->getChildrenSelfRoot();

        return view('profiles.company.product.index', compact('segment'));
    }

    public function save(Request $request)
    {
        dd($request->all());
    }

//    private function checkAccess(User $user): void
//    {
//        if (!Gate::allows('manage-own-product', $user)) {
//            abort(403);
//        }
//    }

}