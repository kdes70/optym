<?php

namespace App\Http\Controllers\Profiles\Company;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class AvatarController extends Controller
{
    public function __invoke(Request $request, $username)
    {
        $company = auth()->user()->getCompany();

        return view('profiles.company.home', compact('company'));
    }


}
