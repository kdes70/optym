<?php

namespace App\Http\Controllers\Profiles\Settings;

use App\Domain\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Cabinet\ProfileEditRequest;
use App\UseCases\Profile\ProfileService;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    private $service;

    public function __construct(ProfileService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return view('profiles.settings.home');
    }

    /**
     * @param ProfileEditRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProfileEditRequest $request)
    {
        try {
            $this->service->edit(auth()->id(), $request->getDto());

        } catch (\DomainException $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
        return redirect()->route('cabinet.profile.home');
    }

    public function email()
    {
        return view('profiles.settings.email');
    }

    public function password()
    {
        return view('profiles.settings.password');
    }


    /**
     * TODO вынести в отдельный класс
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function avatar(Request $request)
{
    if ($request->hasFile('avatar')) {

        /** @var User $user */
        $user = auth()->user();

        $file = Storage::disk('avatar')->putFile(' / ', $request->file('avatar'));

        if ($user->profile()->update([
            'avatar' => $file,
        ])) {
            return response(['files' => $file]);
        }
        return response('lll');
    }


}
}
