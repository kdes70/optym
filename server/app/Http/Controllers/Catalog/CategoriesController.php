<?php

namespace App\Http\Controllers\Catalog;

use App\Domain\Catalog\Category\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\Product\SearchRequest;
use App\Services\CategoryService;
use App\Services\ProductService;
use App\UseCases\Products\SearchService;
use Breadcrumbs;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class CategoriesController extends Controller
{

    /**
     * @var ProductService
     */
    private $productService;
    /**
     * @var CategoryService
     */
    private $categoryService;
    /**
     * @var SearchService
     */
    private $search;

    /**
     * Create a new controller instance.
     *
     * @param SearchService $search
     * @param CategoryService $categoryService
     * @param ProductService $productService
     */
    public function __construct(SearchService $search, CategoryService $categoryService, ProductService $productService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
        $this->search = $search;
    }


    /**
     * @param SearchRequest $request
     * @param $path
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(SearchRequest $request, $path)
    {

//        dd(count($request->segments()));

//      TODO оптимизировать используя кеширование
        $category = $this->categoryService->getShowByPath($path);

        $result = $this->search->search($category, $request, $category->getPath(), 20, $request->get('page', 1));

        $children = $this->categoryService->getChildren($category);

        $siblings = $category->getSiblings(['name', 'path']);

        $products = $result->products;

        $range_price = $result->range_price;

        Breadcrumbs::addHierarchyOfNeeds($category->getAncestors(), 'category');
        Breadcrumbs::append($category->getName(), null, true);

        return view('catalog.products-list', compact(
            'category',
            'children',
            'siblings',
            'products',
            'range_price'
        ));
    }


    /**
     * // TODO переделать на поиск из elasticsearch
     * Get list company in category
     *
     * @param Request $request
     * @param $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function suppliers(Request $request, $category)
    {

        $perPage = 3;

        $categories = $this->categoryService->getByPathCompanyAndItsProducts($request, $category, $perPage);

        $companies_all_count = $this->categoryService->countCompanyForCategory($category);

        // pagination
        $companies = new LengthAwarePaginator($categories->companies, $companies_all_count, $perPage, $request->get('page', 1), ['path' => '/' . $categories->getPath() . '/suppliers']);

        Breadcrumbs::addHierarchyOfNeeds($categories->getAncestors(), 'category');
        Breadcrumbs::append($categories->getName(), null, true);

        // получаем соседние категории
        $siblings = $categories->getSiblings();

        return view('company.index', compact('categories', 'siblings', 'companies', 'companies_all_count'));
    }

// посадочная страница категории
    public function landingPage(Category $category)
    {
        $children = $this->categoryService->getChildren($category);

        Breadcrumbs::addHierarchyOfNeeds($category->getAncestors(), 'category');
        Breadcrumbs::append($category->getName(), null, true);

        return view('catalog.landing_page_category', compact(
            'category',
            'children'
        ));
    }
}
