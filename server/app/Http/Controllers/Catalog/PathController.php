<?php

namespace App\Http\Controllers\Catalog;


use App\Http\Controllers\Controller;
use App\Http\Requests\Product\SearchRequest;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\ProductResource;
use App\Services\CategoryService;
use App\UseCases\Products\SearchService;


class PathController extends Controller
{

    /**
     * @var CategoryService
     */
    private $categoryService;
    /**
     * @var SearchService
     */
    private $search;

    public function __construct(SearchService $search, CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
        $this->search = $search;
    }

    // TODO SEO !!!
    public function __invoke(SearchRequest $request, string $path)
    {
        if (!$category = $this->categoryService->getShowByPath($path)) {
            abort(404);
        }

        \Breadcrumbs::addHierarchyOfNeeds($category->getAncestors(), 'category');
        \Breadcrumbs::append($category->getName(), null, true);

        // если категория не крайняя и есть потомки показываем стриницу лендинга категории
        // TODO это лучше вынести в отдельный метод а проверку сделать в миделвари
        if (count($request->segments()) < 2 && $category->children->isNotEmpty()) {

            $page = $category->getPage();

            return view('catalog.landing_page_category', compact(
                'category',
                'page'
            ));
        }

        $siblings = $category->getSiblings(['name', 'path']);

        $result = $this->search->search($category, $request, $category->getPath(), 6, $request->get('page', 1));

//        dd($result);

        // TODO максимуи и минимум должны браться товаров со стусом available
        $range_price = $result->range_price;

        $category = new CategoryResource($category);
        $products = $result->products->items();
        $pagination = $result->products;
        $categoriesCounts = $result->categoriesCounts;
//        $categoriesCountsDescendants = $result->categoriesCountsDescendants;
//        $categoriesCountsChildren = $result->categoriesCountsChildren;

        return view('catalog.products-list', compact(
            'category',
            'siblings',
            'products',
            'range_price',
            'pagination',
            'categoriesCounts'
        ));
    }
}
