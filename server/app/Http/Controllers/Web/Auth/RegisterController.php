<?php

namespace App\Http\Controllers\Web\Auth;

use App\Domain\Company\Enums\CompanyTypeEnum;
use App\Domain\User\Enums\UserTypeEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegistrationRequest;
use App\UseCases\Auth\RegisterService;
use Auth;
use Illuminate\Http\RedirectResponse;

/**
 * Class RegisterController
 */
class RegisterController extends Controller
{
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /** @var string */
    private $form_session = 'register_form';

    /** @var RegisterService */
    private $service;

    /**
     * Create a new controller instance.
     *
     * @param RegisterService $service
     */
    public function __construct(RegisterService $service)
    {
        $this->service = $service;
    }

    /**
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register_form()
    {
        $user_types = UserTypeEnum::toArrayLabels([UserTypeEnum::WHOLESALE, UserTypeEnum::BUYER]);
        $company_type = CompanyTypeEnum::toArrayLabels();

        return view('auth.register', compact('user_types', 'company_type'));
    }

    /**
     * @param RegistrationRequest $request
     *
     * @return void
     */
    public function register_ajax(RegistrationRequest $request)
    {
        dd($request->all());
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
//        dd(session()->has($this->form_session));
//
//        // prevent access without filling out step1
//        if (!session()->has($this->form_session)) {
//            return redirect(route('register.account_type'));
//        }
//
//        // get forms session data
//        $data = session()->get($this->form_session);
//
//        dd($data);
//
//        if ($data['user_type'] == UserTypeEnum::WHOLESALE) {
//
//            $company_types = Company::companyTypeList();
//
//            return view('auth.form.wholesaler')
//                ->with('input', $data)
//                ->with('company_types', $company_types);
//        }
//
//        if ($data['user_type'] == UserTypeEnum::BUYER) {
//
//            return view('auth.form.wholesale_buyer')->with('input', $data);
//        }
//
//        // retun the confirm view w/ session data as input
//        return view('auth.form.account_type')->with('input', $data);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param RegistrationRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function register(RegistrationRequest $request)
    {
//        //TODO при пустом user_type нужно отловить!!!
//        // set the form input to the session
//        session()->put($this->form_session, $request->all());
//        // get forms session data
//
//        $data = session()->get($this->form_session);
//
//        $user = $this->service->register($request);
//
//        return $this->registered($request, $user) ?: redirect($this->redirectPath());
    }


    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param RegistrationRequest $request
     * @param mixed $user
     * @return mixed
     */
    protected function registered(RegistrationRequest $request, $user)
    {
        // clear forms session data
//        session()->forget($this->form_session);
//
//        return redirect()->route('login')
//            ->with('success', 'Check your email and click on the link to verify.');
    }

    /**
     * @param $token
     * @return RedirectResponse
     */
    public function verify($token)
    {
//        if (!$user = (new User)->where('verify_token', $token)->first()) {
////            return redirect()->route('login')
////                ->with('error', 'Sorry your link cannot be identified.');
////        }
////
////        try {
////            $this->service->verify($user->id);
////            return redirect()->route('login')->with('success', 'Your e-mail is verified. You can now login.');
////        } catch (\DomainException $e) {
////            return redirect()->route('login')->with('error', $e->getMessage());
////        }
    }


    /**
     * @param RegistrationRequest $request
     * @return array
     * @throws \Exception
     */
    private function buildUserAttributes(RegistrationRequest $request): array
    {
//        $loginField = $request->session()->get('login_type');
//
//        return [
//            'username'  => Str::slug($request['username']) . '_' . random_int(1000, 999999),
//            $loginField => $loginField === 'phone' ? clear_phone($request['login']) : $request['login']
//        ];
    }


}
