<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\UseCases\Auth\AuthenticatesUsers;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Foundation\Auth\ThrottlesLogins;


class LoginController extends Controller
{
    use AuthenticatesUsers;

    /** @var StatefulGuard */
    protected $guard;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';


    /**
     * LoginController constructor.
     *
     * @param Guard $guard
     * @return void
     */
    public function __construct(Guard $guard)
    {
        $this->guard = $guard;

        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * The user has been authenticated.
     *
     * @param LoginRequest $request
     * @param  mixed $user
     * @return mixed
     */
    public function authenticated(LoginRequest $request, $user)
    {
        $user->update([
            'last_login_at' => Carbon::now()->toDateTimeString(),
            'last_login_ip' => getRealUserIp(),
        ]);

        if ($request->ajax()) {

            return response()->json([
                'auth'     => auth()->check(),
                'user'     => $user,
                'intended' => $this->redirectPath(),
            ]);
        }
    }

    public function username()
    {
        return 'login';
    }

}
