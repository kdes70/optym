<?php

namespace App\Http\Controllers\Web;

use App\Domain\Catalog\Category\Category;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

/**
 * Class WelcomeController
 */
class WelcomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return View
     */
    public function index(): View
    {
        return view('welcome');
    }


    public function test()
    {
        // получить категории только последних узлов
        $categories = (new Category)->withDepth()->having('depth', '=', 3)->inRandomOrder()->limit(3)->get();

        //dd($categories);
    }
}
