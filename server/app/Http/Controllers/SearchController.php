<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\SearchRequest;
use App\Http\Resources\ProductResource;
use App\UseCases\Products\SearchService;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * @var SearchService
     */
    private $search;

    /**
     * Create a new controller instance.
     *
     * @param SearchService $search
     */
    public function __construct(SearchService $search)
    {
        $this->search = $search;
    }

    /**
     * Show the application dashboard.
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(SearchRequest $request)
    {

//        dd($request->all());

        $result = $this->search->search(null, $request, 'search/result',20, $request->get('page', 1));



        return response()->json($result);
    }
}
