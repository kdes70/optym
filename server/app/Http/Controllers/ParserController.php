<?php

namespace App\Http\Controllers;

use App\UseCases\Parsers\Parser;
use App\UseCases\Parsers\ParserManager;
use Illuminate\Http\Request;


class ParserController extends Controller
{
    /**
     * @var Parser
     */
    private $parser;
    /**
     * @var ParserManager
     */
    private $manager;

    /**
     * Create a new controller instance.
     * @param ParserManager $manager
     */
    public function __construct(ParserManager $manager)
    {
        $this->middleware('auth');

        $this->manager = $manager;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$this->parser = new Parser('https://optlist.ru/company/optovyi-internet-mag-3--zhenskoe-nizhnee-bele-optom');
        $this->manager->driver('optlist')->parse();
    }
}
