<?php

namespace App\Http\Controllers\Company;

use App\Domain\Company\Company;
use App\Domain\Product\Product;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class SuppliersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function index()
    {
        $companies = Company::with(['products' => function ($query) {
            $query->take(5);
        }])->paginate(15);

        dd($companies);

//        foreach($companies as $company)
//        {
//            foreach($company->products as $product)
//            {
//                dump($product->getMainImage()->getPath());
//            }
//        }

        return view('company.suppliers.index', compact('companies'));
    }

    public function show($slug)
    {
        $company = Company::with('products')
            ->with(['category' => function ($q) {
                $q->distinct('category');
            }])->with([
                'users' => function ($query) {
                    $query->select('id', 'username');
                }
            ])->where('slug', $slug)->first(['id', 'name', 'slug']);


        if (!$company) {
            throw new ModelNotFoundException('Company not found by slug ' . $slug);
        }

        return view('company.suppliers.show', compact('company'));
    }

    public function category($slug, $category)
    {
        $company = Company::with(['products' => function ($q) use ($category) {
            return $q->whereHas('category', function ($q) use ($category) {
                return $q->where('slug', $category);
            })->distinct('category');
        }])->where('slug', $slug)
            ->first();

//        dd($company->category);
        //   $products_categories    = $company->getProducts();

        return view('company.suppliers.show', compact('company'));
    }

    public function showGoods(Company $company, Product $product)
    {

//        dd($company, $product);

//        $company = Company::with(['products' => function($q) use($goods){
//            $q->where('id', $goods)->first();
//        }])->where('slug', $slug)->first();


//        $goods = Product::with('company')
//            ->where('slug', $goods)
//            ->first();

//       if (!$goods->company) {
//            throw new ModelNotFoundException('Company not found by company ' . $company);
//        }

        return view('company.show-goods', compact('company', 'product'));
    }

    public function contact(Company $company)
    {
//        dd($company);

        return view('company.suppliers.contact', compact('company'));
    }

}
