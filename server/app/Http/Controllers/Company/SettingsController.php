<?php

namespace App\Http\Controllers\Company;

use App\Domain\Company\Company;
use App\Http\Controllers\Controller;
use App\Services\CompanyService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * @var CompanyService
     */
    private $service;

    public function __construct(CompanyService $service)
    {

        $this->service = $service;
    }

    public function index()
    {
        return view('profiles.company.index')->with('company', auth()->user()->company()->first());
    }

//    public function show($id)
//    {
//        $user = $this->service->user->getById($id);
//
//        return view('cabinet.profile.show', compact('user'));
//    }
//
//
//


    public function edit()
    {
        $list_type = Company::companyTypeList();

        // dd($category);

        return view('profiles.company.edit')->with([
            'company'   => auth()->user()->company()->first(),
            'list_type' => $list_type,
        ]);
    }

    public function update(CompanyRequest $request)
    {
        dd($request->all());

        $this->service->updare($request->all());

        return view('profiles.company.edit')->with('company', auth()->user()->company()->first());
    }

}
