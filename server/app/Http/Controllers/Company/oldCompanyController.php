<?php

namespace App\Http\Controllers\Company;

use App\Domain\Company\Company;
use App\Domain\Product\Product;
use App\Http\Controllers\Controller;
use App\Services\CompanyService;
use Breadcrumbs;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class oldCompanyController extends Controller
{
    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * Create a new controller instance.
     *
     * @param CompanyService $companyService
     */
    public function __construct(CompanyService $companyService)
    {
        //$this->middleware('auth');
        $this->companyService = $companyService;
    }

    public function index()
    {
        $companies = Company::with('products')->get(['id', 'slug', 'name', 'description']);

        return view('company.suppliers.index', compact('companies'));
    }

    public function show($slug)
    {
        $company = $this->companyService->getBySlug($slug);

        if (!$company) {

            throw new ModelNotFoundException('Company not found by slug ' . $slug);
        }

        return view('company.suppliers.show', compact('company'));
    }


    public function showGoods(Company $company, Product $product)
    {
//        $product = Product::with(['company' => function ($q) use ($slug) {
//            $q->where('slug', $slug);
//        }, 'category'])
//            ->where('id', $product->getId())
//            ->first();

       // dd($product);

        Breadcrumbs::addHierarchyOfNeeds($product->getCategories()->getAncestors()->push($product->getCategories()), 'catalog');
        Breadcrumbs::append($product->getTitle(), null, true);

        return view('company.suppliers.show-goods', compact('company', 'product'));
    }


    public function category($slug, $category)
    {
        $company = Company::with(['products' => function ($q) use ($category) {
            return $q->whereHas('category', function ($q) use ($category) {
                return $q->where('slug', $category);
            });
        }])->where('slug', $slug)
            ->first();

        $products_categories = $company->category;

        return view('company.suppliers.show', compact('company', 'products_categories'));
    }

    public function contact($slug)
    {
        dd($slug);
    }
}
