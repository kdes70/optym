<?php

namespace App\Console\Commands\Installer;


use Illuminate\Console\Command;
use Schema;

class Database extends Command
{
    protected $signature = 'app:install:database';

    public function handle()
    {
        $this->info('Создаём структуру БД...');

        $bar = $this->output->createProgressBar(2);

       // $this->dropAllTables();

        $this->callSilent('migrate:fresh');
        $this->callSilent('db:seed');
        $this->callSilent('search:delete');
        $this->callSilent('search:init');
        $this->callSilent('search:reindex');

        $bar->advance();
        $bar->advance(); // второй раз – для красоты

        $bar->finish();
    }

    private function dropAllTables()
    {

        $this->callSilent('migrate:fresh');
//        foreach (\DB::select('SHOW TABLES') as $table) {
//            $table_array = get_object_vars($table);
//
////                dd($table_array);
//
//             \DB::statement("DROP TABLE {$table_array[key($table_array)]} CASCADE");
////            Schema::disableForeignKeyConstraints();
////            Schema::drop($table_array[key($table_array)]);
////            Schema::enableForeignKeyConstraints();
//        }
    }

}