<?php

namespace App\Console\Commands\Installer;


use App\UseCases\Import\CsvReader;
use Illuminate\Console\Command;
use Illuminate\Database\Connection;
use Illuminate\Filesystem\Filesystem;

class Import extends Command
{
    protected $signature = 'app:install:import';

    /** @var Filesystem */
    protected $filesystem;

    /** @var Connection */
    protected $connection;

    protected $config = [];

    public function __construct(Filesystem $filesystem, Connection $connection)
    {
        parent::__construct();

        $this->config = config('import');

        $this->filesystem = $filesystem;
        $this->connection = $connection;
    }


    public function handle()
    {
        $this->info('Импортируем данные...');



        $sections = array_keys($this->config['sections']['company_temps']);

//        dd($sections);

        foreach ($sections as $section) {

            $csvFile = $this->normalizePath(sprintf('storage/app/imports/%s.csv', $section));


            if (!$this->filesystem->exists($csvFile)) {
                $this->error(sprintf('Файл "%s" не найден.', $csvFile));
            }

            $this->output->write(sprintf("<info>Раздел \"%s\"...\n</info>", $section));

            dd($csvFile, $section);

            $this->importFile($csvFile, $section);
        }
    }


    private function importFile($csvFile, $section)
    {
        $csv = new CsvReader($csvFile,  $this->config['sections'][$section]);

        $datas = $csv->parse();


        $data = (new CsvReader($data, $this->config['sections'][$section]))->parse();


        $class = new WinnerImport($this->connection, $this->output);

        $class = sprintf('\App\Services\Import\Importer\%sImporter', ucfirst($section));

        /** @var \Hm\Services\Import\Importer\AbstractImporter $class */
        $class = new $class($this->connection, $this->output, $this->config);

        $class->import($data);
    }

    private function normalizePath($path)
    {
        if (DIRECTORY_SEPARATOR === '\\') {
            $path = str_replace('/', DIRECTORY_SEPARATOR, $path);
        }

        return base_path($path);
    }
}