<?php

namespace App\Console\Commands\Installer;

use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;

class Installer extends Command
{
    use ConfirmableTrait;

    private $commands = [
        Database::class,
        Setup::class,
      //  Import::class
    ];

    protected $signature   = 'app:install {--force}';
    protected $description = 'Установка движка.';


    public function handle()
    {
        if (! $this->confirmToProceed()) {
            return;
        }

        $this->output->write('<info>Устанавливаем ...</info>');

        foreach ($this->commands as $step => $command) {
            $name = strtolower(class_basename($command));
            $this->getApplication()->add($this->laravel->make($command));

            $this->output->newLine(2);
            $this->output->write(
                sprintf('<comment>Шаг %d из %d.</comment> ', $step + 1, count($this->commands))
            );

            $this->call("app:install:{$name}");
        }

        $this->info('Сервис готов к работе!');

        $this->showStats();
    }

    private function showStats()
    {
        $memory = memory_get_peak_usage(true);

        $this->line(str_repeat('-', 40));
        $this->info(sprintf('Выполнено за: %0.2f sec.', round(microtime(true) - LARAVEL_START, 3)));
        $this->info(sprintf('Максимум памяти: %0.2f MiB', $memory / (1024 * 1024)));
    }
}