<?php

namespace App\Console\Commands\Search;


use App\Domain\Product\Product;
use App\Services\Search\ProductIndexer;
use Illuminate\Console\Command;

class ReindexCommand extends Command
{
    protected $signature = 'search:reindex';
    /**
     * @var ProductIndexer
     */
    private $products;


    public function __construct(ProductIndexer $products)
    {
        parent::__construct();
        $this->products = $products;
    }

    public function handle(): bool
    {
        // TODO переиндексировать при добавлении компании и товара, раз в час к примеру

        $this->products->clear();

        foreach (Product::active()->with('category')->orderBy('id')->cursor() as $product) {
            $this->products->index($product);
        }

        return true;
    }
}
