<?php

namespace App\Console\Commands\Search;

use Elasticsearch\Common\Exceptions\Missing404Exception;
use Illuminate\Console\Command;
use Elasticsearch\Client;

class InitCommand extends Command
{
    protected $signature = 'search:init';

    private $client;

    public function __construct(Client $client)
    {
        parent::__construct();
        $this->client = $client;
    }

    public function handle(): bool
    {
        $this->initProduction();

        return true;
    }

    private function initProduction(): void
    {
        try {

            $this->client->indices()->delete([
                'index' => 'production'
            ]);

        } catch (Missing404Exception $e) {
        }

        $this->client->indices()->create([
            'index' => 'production',
            'body' => [
                'mappings' => [
                    '_doc' => [
//                        '_source' => [
//                            'enabled' => true,
//                        ],
                        'properties' => [
                            'id' => [
                                'type' => 'integer',
                            ],
                            'available_at' => [
                                'type' => 'date',
                            ],
                            'title' => [
                                'type' => 'text',
                            ],
                            'description' => [
                                'type' => 'text',
                            ],
                            'cost_price' => [
                                'type' => 'integer',
                            ],
                            'state' => [
                                'type' => 'keyword',
                            ],
                            'categories' => [
                                'type' => 'integer',
                            ],
//                            'values' => [
//                                'type' => 'nested',
//                                'properties' => [
//                                    'attribute' => [
//                                        'type' => 'integer'
//                                    ],
//                                    'value_string' => [
//                                        'type' => 'keyword',
//                                    ],
//                                    'value_int' => [
//                                        'type' => 'integer',
//                                    ],
//                                ],
//                            ],
                        ],
                    ],
                ],
                'settings' => [
                    'analysis' => [
                        'char_filter' => [
                            'replace' => [
                                'type' => 'mapping',
                                'mappings' => [
                                    '&=> and '
                                ],
                            ],
                        ],
                        'filter' => [
                            'word_delimiter' => [
                                'type' => 'word_delimiter',
                                'split_on_numerics' => false,
                                'split_on_case_change' => true,
                                'generate_word_parts' => true,
                                'generate_number_parts' => true,
                                'catenate_all' => true,
                                'preserve_original' => true,
                                'catenate_numbers' => true,
                            ],
                            'trigrams' => [
                                'type' => 'ngram',
                                'min_gram' => 4,
                                'max_gram' => 5,
                            ],
//                            'range' => [
//                                'price_price' => [
//                                    'gte'   => 0,
//                                    'lte'   => 20,
//                                    'boost' => 2.0,
//                                ],
//                            ],
                        ],
                        'analyzer' => [
                            'default' => [
                                'type' => 'custom',
                                'char_filter' => [
                                    'html_strip',
                                    'replace',
                                ],
                                'tokenizer' => 'whitespace',
                                'filter' => [
                                    'lowercase',
                                    'word_delimiter',
                                    'trigrams',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }


}
