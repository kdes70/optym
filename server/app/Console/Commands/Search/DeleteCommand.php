<?php

namespace App\Console\Commands\Search;

use Elasticsearch\Common\Exceptions\Missing404Exception;
use Illuminate\Console\Command;
use Elasticsearch\Client;

class DeleteCommand extends Command
{
    protected $signature = 'search:delete';

    private $client;

    public function __construct(Client $client)
    {
        parent::__construct();
        $this->client = $client;
    }
    
    public function handle(): bool
    {
        $this->deleteAllIndex();

        return true;
    }

    private function deleteAllIndex(): void
    {
        try {

            $this->client->indices()->delete(['index' => '_all']);

        } catch (Missing404Exception $e) {
        }
    }


}
