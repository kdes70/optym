<?php

namespace App\Transformers;
;

use App\Domain\Activity;
use League\Fractal\TransformerAbstract;

class ActivityTransformer extends TransformerAbstract
{
    public function transform(Activity $activity)
    {
        return [
            "description" => call_user_func_array([$this, $activity->name], [$activity]),
            "lapse" => $activity->created_at->diffForHumans(),
            "user" => $activity->user,
        ];
    }

    protected function updated_user(Activity $activity)
    {
        return $activity->user->username . " update a user, " . $activity->subject->name;
    }

    protected function created_user(Activity $activity)
    {
        return $activity->user->username . " created a user, " . $activity->subject->name;
    }

}