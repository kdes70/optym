<?php

namespace App\UseCases\Products;

use Illuminate\Contracts\Pagination\Paginator;

class SearchResult
{
    public $products;
    public $categoriesCounts;
    public $categoriesCountsChildren;
    public $categoriesCountsDescendants;
    public $range_price;
    public $total;

    /* array $categoriesCountsDescendants,
            array $categoriesCountsChildren,*/
    /**
     * SearchResult constructor.
     * @param Paginator $products
     * @param array $categoriesCounts
     * @param array $categoriesCountsDescendants
     * @param array $categoriesCountsChildren
     * @param int $total
     * @param PriceRangeResult $range_price
     */
    public function __construct(
        Paginator $products,
        array $categoriesCounts,
        int $total,
        PriceRangeResult $range_price
    ) {
        $this->products = $products;
        $this->categoriesCounts = $categoriesCounts;
        $this->range_price = $range_price;
        $this->total = $total;
    }

    /*  $this->categoriesCountsDescendants = $categoriesCountsDescendants;
       $this->categoriesCountsChildren = $categoriesCountsChildren;*/
}
