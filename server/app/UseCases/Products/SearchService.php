<?php

namespace App\UseCases\Products;

use App\Domain\Catalog\Category\Category;
use App\Domain\Product\Product;
use App\Http\Requests\Product\SearchRequest;
use App\Http\Resources\ProductResource;
use Elasticsearch\Client;
use Illuminate\Database\Query\Expression;
use Illuminate\Pagination\LengthAwarePaginator;

class SearchService
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function search(?Category $category, SearchRequest $request, string $path, int $perPage, int $page): SearchResult
    {
        // атрибуты для сортировки
        $values = array_filter((array)$request->input('attrs'), function ($value) {
            return !empty($value['equals']) || !empty($value['from']) || !empty($value['to']);
        });

//        dd($category->getChildren());

        $params = [
            'index' => 'production',
            'type' => 'product',
            'body' => [
                '_source' => ['id'],
                'from' => ($page - 1) * $perPage,
                'size' => $perPage,
                'sort' => empty($request['text']) ? [
                    ['available_at' => ['order' => 'desc']],
                ] : [],
                'aggs' => [
                    'group_by_category' => [
                        'terms' => [
                            'field' => 'categories',
                        ],
                    ],
//                    'group_by_category_descendants' => [
//                        'terms' => [
//                            'field' => 'categories_descendants',
//                        ],
//                    ],
//                    'group_by_category_children' => [
//                        'terms' => [
//                            'field' => 'categories_children',
//                        ],
//                    ],
                    'min_price' => [
                        'min' => [
                            'field' => 'cost_price',
                        ],
                    ],
                    'max_price' => [
                        'max' => [
                            'field' => 'cost_price',
                        ],
                    ],
                ],
                'query' => [
                    'bool' => [
                        'must' => array_merge(
                            [
                                ['term' => ['state' => Product::STATE_AVAILABLE]],
                            ],
                            array_filter([
                                // если есть дочернии категории то выбираем и поним тоже, иначе только по одной категории
                                $category ? [$category->getChildren()->isNotEmpty() ? 'terms' : 'term' => [
                                    'categories' => $category->getChildren()->isNotEmpty() ? $category->getChildren()->pluck('id')->toArray() : $category->id]
                                ] : false,

//                                $category ? [
//                                    'term' => ['categories' =>  $category->id],
//                                ] : false,
//                                $category ? [
//                                    'term' => ['categories_descendants' =>  $category->id],
//                                ] : false,
//                                $category ? [
//                                    'term' => ['categories_children' =>  $category->id],
//                                ] : false,
                                !empty($request['text']) ?
                                    [
                                        'multi_match' => [
                                            'query' => $request['text'],
                                            'fields' => ['title^3', 'description']
                                        ]

                                    ] : false,
                                !empty($request['min_price']) && !empty($request['max_price']) ?
                                    [
                                        'range' => [
                                            'cost_price' => [
                                                'gte' => (integer)$request['min_price'],
                                                'lte' => (integer)$request['max_price']
                                            ],
                                        ]

                                    ] : false,
                            ]),
                            array_map(function ($value, $id) {
                                return [
                                    'nested' => [
                                        'path' => 'values',
                                        'query' => [
                                            'bool' => [
                                                'must' => array_values(array_filter([
                                                    ['match' => ['values.attribute' => $id]],
                                                    !empty($value['equals']) ? ['match' => ['values.value_string' => $value['equals']]] : false,
                                                    !empty($value['from']) ? ['range' => ['values.value_int' => ['gte' => $value['from']]]] : false,
                                                    !empty($value['to']) ? ['range' => ['values.value_int' => ['lte' => $value['to']]]] : false,
                                                ])),
                                            ],
                                        ],
                                    ],
                                ];
                            }, $values, array_keys($values))
                        )
                    ],
                ],
            ],
        ];

        $response = $this->client->search($params);

//        dd($response);

        $ids = array_column($response['hits']['hits'], '_id');

        if ($ids) {
            $items = Product::active()
                ->with(['category', 'company'])
                ->whereIn('id', $ids)
                ->orderBy(new Expression('FIELD(id,' . implode(',', $ids) . ')'))
                ->get();

            $products = ProductResource::collection($items);

            $pagination = new LengthAwarePaginator($products, $response['hits']['total'], $perPage, $page, ['path' => '/' . $path]);
        } else {
            $pagination = new LengthAwarePaginator([], 0, $perPage, $page);
        }

        /*array_column($response['aggregations']['group_by_category_descendants']['buckets'], 'doc_count', 'key'),
            array_column($response['aggregations']['group_by_category_children']['buckets'], 'doc_count', 'key'),*/
        return new SearchResult(
            $pagination,
            array_column($response['aggregations']['group_by_category']['buckets'], 'doc_count', 'key'),
            $pagination->total(),
            new PriceRangeResult(
                $response['aggregations']['max_price']['value'],
                $response['aggregations']['min_price']['value']
            )
        );
    }
}
