<?php

namespace App\UseCases\Products;


class PriceRangeResult
{

    public $min_price;
    public $max_price;

    public function __construct(?float $maxPrice = null, ?float $minPrice = null)
    {
        $this->min_price = $minPrice;
        $this->max_price = $maxPrice;
    }
}
