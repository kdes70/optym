<?php

namespace App\UseCases\Import;


use Illuminate\Support\Collection;

class CsvResolve
{
    private $items;
    private $header;

    public function __construct($items, $header)
    {
        $this->items = $items;
        $this->header = $header;
    }

    public function getItems(): Collection
    {
        return collect($this->items);
    }

    public function getHeaders(): array
    {
        return $this->header;
    }
}
