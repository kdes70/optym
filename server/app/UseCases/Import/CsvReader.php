<?php

namespace App\UseCases\Import;

use League\Csv\Reader;

class CsvReader
{
    protected $data;

    /**
     * CsvReader constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function parse()
    {
        $reader = Reader::createFromPath($this->data);
        $reader->setDelimiter(';');

        $reader->addStreamFilter('convert.iconv.windows-1251/utf-8');

        $headers = $reader->fetchOne();

        $col_name = (new Winner())->getHeaders();

        $records = (new Statement())->offset(1)->process($reader);

        $data = [];

        foreach ($records as $key => $record) {

            $item = array_filter(
                array_map('trim', array_values($record)),
                function ($value) {
                    return $value !== false;
                }
            );

            try{
                $data[] = array_combine(array_values($col_name), array_values($item));
            }catch (\Exception $e)
            {
                logs('winners')->error('Ошибка паринга - '. $e->getMessage(), $item);
            }
        }

        return new CsvResolve($data, $headers);
    }
}
