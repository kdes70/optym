<?php

namespace App\UseCases\Import\Importer;

use Illuminate\Console\OutputStyle;
use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Model;

abstract class AbstractImport
{
    /** @var Connection */
    protected $db;

    /** @var OutputStyle */
    protected $cli;


    protected $chunk_size = 100;

    public function __construct(Connection $db, OutputStyle $cli)
    {
        $this->db = $db;
        $this->cli = $cli;
    }

    abstract public function import($data);

    protected function addRecords($records, $message = null)
    {
        $total = count($records);

        if ($message) {
            $this->cli->writeln("<info>{$message}</info> ({$total} записей)");
        }

        $chunks = $records->chunk($this->chunk_size);

        $bar = $this->cli->createProgressBar($total);

        foreach ($chunks as $chunk) {

            if ($bar->getProgress() + $this->chunk_size > $total) {
                $bar->advance($total - $bar->getProgress());
            } else {
                $bar->advance($this->chunk_size);
            }

            $this->db->transaction(function () use ($chunk) {
                foreach ($chunk as $row) {
                    try {
                        /** @var Model $row */
                        $row->save();
                    } catch (\Exception $e) {
                        logs('winners')->error('Ошибка записи - ' . $e->getMessage(), ['item' => $row]);
                    }
                }
            });
        }

        $bar->finish();
        $this->cli->newLine(2);
    }

}
