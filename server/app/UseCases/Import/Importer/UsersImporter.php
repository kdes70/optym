<?php

namespace Hm\Services\Import\Importer;

use Hm\Domain\User\User;
use Illuminate\Support\Collection;

class UsersImporter extends AbstractImporter
{
    private $phonePatterns = [
        'UA' => '~^380\d{9}~',
        'RU' => '~^[78][3489]\d{9}~',
        'KZ' => '~^77(00|01|02|05|07|12|13|17|18|21|25|27|47|71|75|76|77|78)\d{7}~',
    ];

    private $roleMapping = [
        'customer'      => 'customer',
        'administrator' => 'administrator',
        'shop_manager'  => 'manager',
        'subscriber'    => 'customer'
    ];

    public function import($data)
    {
        $data = $this->convert(collect($data));

        $users = [];
        foreach ($data as $user) {
            $users[] = array_only($user, [
                'id', 'username', 'password', 'email', 'phone', 'first_name', 'last_name',
                /*'bio', */'additionals', 'state', 'role', 'created_at', 'updated_at'
            ]);
        }

        $this->addRecords(User::class, $users, 'Пользователи');
        $this->updateAutoincrement(new User());
    }

    public function convert(Collection $users)
    {
        $this->cli->progressStart($users->count());

        $users = $users->transform(function ($user) {
            $this->cli->progressAdvance();

            $user['phone']       = $this->sanitizePhoneWithCountry($user['phone'], $user['shipping_country']);
            $user['username']    = $this->getUsername($user['username'], $user['email']);
            $user['first_name']  = empty($user['first_name']) ? null : $user['first_name'];
            $user['last_name']   = empty($user['last_name']) ? null : $user['last_name'];
            $user['password']    = bcrypt($user['username'], ['rounds' => 4]);
            $user['role']        = $this->transformRole($user['role']);
            $user['state']       = User::STATE_UNCONFIRMED;
            $user['additionals'] = '{}';

            return $this->addTimestamps($user, $user['created_at']);
        })->groupBy('phone');

        $this->cli->progressFinish();

        // все юзеры без телефона
        $realWithoutPhones = $users->pull('');

        // фильтруем телефоны, на которые есть более одного юзера
        $duplicates = $users->filter(function ($value) {
            return count($value) > 1;
        });

        // сортируем дублей по id от большего к меньшему и берём первого
        $duplicates->transform(function (Collection $item) {
            $item = $item->sortByDesc('id');

            return $item->first();
        });

        // все юзеры с телефонами, но без дублей
        $realWithPhones = $users->diffKeys($duplicates)->values()->collapse();

        $real = collect()
            ->merge($realWithPhones)
            ->merge($realWithoutPhones)
            ->merge($duplicates->values());

        return $real->toArray();
    }

    private function getUsername($username, $email)
    {
        $username = trim($username, " \0");

        if (empty($username)) {
            [$username,] = explode('@', $email);
        }

        return $username;
    }

    private function sanitizePhoneWithCountry($phone, $country)
    {
        $phone = clear_phone($phone);

        if (
            isset($this->phonePatterns[$country])
            && preg_match($this->phonePatterns[$country], $phone)
        ) {
            return $phone;
        }

        return null;
    }

    private function transformRole($role)
    {
        static $oldRoleNames;

        if (null === $oldRoleNames) {
            $oldRoleNames = array_keys($this->roleMapping);
        }

        if (in_array($role, $oldRoleNames)) {
            return $this->roleMapping[$role];
        }

        return 'customer';
    }
}
