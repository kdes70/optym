<?php

namespace Hm\Services\Import\Importer;

use Illuminate\Support\Collection;

class MenuImporter extends AbstractImporter
{
    public function import($data)
    {
        $groups = $this->convert(collect($data))->groupBy('slug');

        /** @var Collection $items */
        foreach ($groups as $type => $items) {
            $this->cli->writeln(sprintf('<info>Меню "%s" </info> (%d записей)', $type, count($items)));

            $this->addRecords('hm_menu', $items->toArray());
        }
    }

    protected function convert(Collection $items): Collection
    {
        $this->cli->progressStart($items->count());

        $menu = $items->map(function ($item) {
            $this->cli->progressAdvance();

            $parentId = (int) $item['parent_id'];
            $parentId = $parentId ?: null;

            return [
                'id'          => (int) $item['id'],
                'type'        => $item['type'],
                'slug'        => $item['slug'],
                'title'       => $item['title'],
                'order'       => $item['order'],
                'url'         => $item['url'],
                'parent_id'   => $parentId,
                'category_id' => (int) $item['category_id']
            ];
        });

        $this->cli->progressFinish();

        return $menu;
    }
}
