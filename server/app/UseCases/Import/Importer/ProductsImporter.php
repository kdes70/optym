<?php

namespace Hm\Services\Import\Importer;

use Hm\Domain\User\User;
use Hm\Core\Eloquent\Model;
use Illuminate\Support\Str;
use Hm\Domain\Product\Product;
use Hm\Domain\Core\Image\Image;
use Hm\Domain\Taxonomy\Category;
use Hm\Domain\Taxonomy\Attribute;
use Illuminate\Console\OutputStyle;
use Illuminate\Database\Connection;
use Hm\Domain\Product\ProductPacking;
use Hm\Domain\Taxonomy\AttributeValue;
use Hm\Domain\Product\ProductInterface;
use Hm\Repositories\Measurement\UnitRepository;
use Hm\Repositories\Currency\CurrencyRepository;
use Hm\Domain\Inventory\Document\ReceiptNoteItem;
use Hm\Domain\Inventory\Document\ReceiptNoteService;
use Hm\Domain\Inventory\Document\Events\ReceiptNoteAccepted;

class ProductsImporter extends AbstractImporter
{
    private $units             = [];
    private $skuList           = [];
    private $categories        = [];
    private $attributes        = [];
    private $productAttributes = [];

    /** @var CurrencyRepository */
    private $currencies;

    public function __construct(Connection $connection, OutputStyle $cli, array $config)
    {
        parent::__construct($connection, $cli, $config);

        $this->currencies = app(CurrencyRepository::class);

        $this->units = app(UnitRepository::class)
            ->all()
            ->keyBy('abbr')
            ->flatMap(function (Model $item, $abbr) {
                $abbr = trim(Str::lower($abbr), '.');

                return [$abbr => $item];
            })->toArray();
    }

    public function import($data)
    {
        // товары в файле идут в обратном порядке
        $data = array_reverse($data);

        // конвертируем товары
        $data = $this->convert($data);

        // категории
        $this->cli->writeln('<info>Категории</info> ('.count($this->categories).' записей)');

        Model::unguard();
        $this->db->transaction(function () {
            $bar = $this->cli->createProgressBar(count($this->categories));

            foreach ($this->categories as $title => $id) {
                if (str_contains($title, '|')) {
                    [$pid, $title] = explode('|', $title);

                    $slug = str_slug($title);
                    $parent = Category::find($pid);

                    Category::create(compact('id', 'slug', 'title'), $parent);
                } else {
                    $slug = str_slug($title);
                    $category = new Category(compact('id', 'slug', 'title'));
                    $category->save();
                }

                $bar->advance();
            }

            $bar->finish();
            $this->cli->newLine(2);
        });
        Model::reguard();


        // товары, картинки, связка "товар-категория" и фасовки
        $products = $images = $relations = $packings = [];
        foreach ($data as $item) {
            // товар
            $products[] = array_only($item, [
                'id', 'title', 'sku', 'slug', 'cost_price', 'retail_price', 'exchange_rate',
                'unit_id', 'weight', 'on_hand', 'on_hold', 'reserve', 'sold',
                'description', 'state', 'created_at', 'updated_at',
            ]);

            // картинки
            $cnt = 0;
            foreach ($item['images'] as $image) {
                $images[] = $this->addTimestamps([
                    'path'           => $image,
                    'order'          => $cnt++,
                    'imageable_id'   => $item['id'],
                    'imageable_type' => 'product',
                ]);
            }

            // связка "товар-категория"
            foreach ($item['categories'] as $chain) {
                array_walk($chain, function ($category) use (&$relations, $item) {
                    $relations["{$item['id']}-{$category}"] = [
                        'category_id' => $category,
                        'product_id'  => $item['id'],
                    ];
                });
            }

            $unitAbbr = strtoupper($this->units[$item['unit_abbr']]['code']);

            // фасовка
            $packings[] = $this->addTimestamps([
                'product_id' => $item['id'],
                'sku'        => $item['sku'] . '-' . $item['@packing'] . $unitAbbr,
                'price'      => $item['retail_price'] * $item['@packing'],
                'raw_price'  => $item['retail_price'] * $item['@packing'],
                'unit_id'    => $item['unit_id'],
                'quantity'   => $item['@packing'],
                'weight'     => $item['weight'] * $item['@packing'],
                'discount'   => 0,
            ]);
        }

        // неуникальные sku
        // отсутствует вес

        $this->addRecords(Product::class, $products, 'Товары');
        $this->addRecords(ProductPacking::class, $packings, 'Фасовки товаров');

        $this->addRecords('hm_taxonomies_category_product', array_values($relations), 'Товар <-> Категория');
        $this->addRecords(Image::getTableName(), $images, 'Картинки товаров');

        // устанавливаем главное изображение для товаров
        $this->cli->writeln('<info>Товар <-> Главное изображение</info> (' . count($data) . ' записей)');
        $images = $this->db->table(Image::getTableName())->select(['id AS mid', 'path'])->get();
        $images = $images->keyBy('path');

        $this->db->transaction(function () use ($data, $images) {
            $bar = $this->cli->createProgressBar(count($data));

            foreach ($data as $item) {
                if (! $item['main_image']) {
                    continue;
                }

                $imageId = $images[$item['main_image']]->mid;

                $this->db
                    ->table(Product::getTableName())
                    ->where('id', '=', $item['id'])
                    ->update(['main_image_id' => $imageId]);

                $bar->advance();
            }

            $bar->finish();
            $this->cli->newLine(2);
        });

        // атрибуты и их значения, кроме фасовок
        $count = 0;
        $attributes = $attributeValues = [];

        foreach ($this->attributes as $name => $values) {
            if ($name === '@packing') {
                continue;
            }
            $count++;

            $name = substr($name, 1);

            $attributes[] = $this->addTimestamps([
                'id'    => $count,
                'title' => $name,
                'slug'  => $name,
            ]);

            foreach ($values as $value => $id) {
                $attributeValues[$id] = $this->addTimestamps([
                    'value_id'     => $id,
                    'attribute_id' => $count,
                    'title'        => $value,
                    'slug'         => str_slug($value),
                ]);
            }
        }

        $productAttributes = array_flatten($this->productAttributes, 1);

        $this->addRecords(Attribute::class, $attributes, 'Атрибуты');
        $this->addRecords(AttributeValue::class, $attributeValues, 'Варианты атрибутов');
        $this->addRecords('hm_taxonomies_attribute_product', $productAttributes, 'Атрибуты товаров');

        $this->updateAutoincrement(new Category());
        $this->updateAutoincrement(new Product());
        $this->updateAutoincrement(new Attribute());

        $this->createReceiptNote();
    }

    private function convert($products)
    {
        $rate = $this->currencies->findByCode('USD')->getExchangeRate();

        $this->cli->progressStart(count($products));
        $records = array_map(function ($product) use ($rate) {
            $this->cli->progressAdvance();

            $product['sku'] = $this->getUniqueSKU(
                $product['title'], $product['id'], $product['sku']
            );

            if (empty($product['slug'])) {
                $product['slug'] = str_slug($product['title']);
            }

            $product['slug'] = str_replace('%', '-', $product['slug']);

            $product['images'] = [];

            $product['exchange_rate'] = $rate;

            $product['categories'] = $this->parseCategories($product['categories']);

            if (empty($product['description'])) {
                $product['description'] = null;
            }

            if (! empty($product['main_image'])) {
                $product['images'][] = $product['main_image'];
            }

            foreach ($product as $key => $value) {
                if (starts_with($key, '@')) {
                    if ($key === '@packing') {
                        [$unit, $qty] = $this->parsePacking($value);

                        $product['title'] = html_entity_decode($product['title']);

                        $product['title'] = $this->removePackingFromTitle(
                            $product['title'], $qty, $unit
                        );

                        $unit = trim(Str::lower($unit), '.');

                        $product['unit_id'] = $this->units[$unit]['id'];
                        $product['unit_abbr'] = $unit;
                        $product[$key] = $qty;

                        $product['state'] = ProductInterface::STATE_AVAILABLE;
                        $product['weight'] = (int) $product['weight'];
                        $product['on_hand'] = abs((int) $product['on_hand']);
                        $product['on_hold'] = 0;
                        $product['reserve'] = 0;
                        $product['sold'] = 0;
                        $product['cost_price'] = (float) $product['cost_price'];
                        $product['retail_price'] = (float) $product['retail_price'];

                        if (empty($product['cost_price'])) {
                            $product['cost_price'] = $product['retail_price'] / 2;
                        }

                        // переводим закупочную цену в доллары США
                        $product['cost_price'] /= $rate;

                        if ($qty > 1) {
                            $product['weight'] = round($product['weight'] / $qty);
                            $product['cost_price'] /= $qty;
                            $product['retail_price'] /= $qty;
                        }

                        $product['created_at'] = $this->normalizeDate($product['created_at']);
                        $product['updated_at'] = $this->normalizeDate($product['updated_at']);
                    } else {
                        $product[$key] = $this->parseAttributes($key, $value);

                        foreach ($product[$key] as $item) {
                            if (! isset($this->productAttributes[$key])){
                                $this->productAttributes[$key] = [];
                            }

                            $this->productAttributes[$key][] = [
                                'product_id' => $product['id'],
                                'value_id' => $item
                            ];
                        }
                    }
                }
            }

            return $product;
        }, $products);
        $this->cli->progressFinish();

        return $records;
    }

    private function addAttribute($type, $name)
    {
        static $counter = 0;

        if (!isset($this->attributes[$type][$name])) {
            $this->attributes[$type][$name] = ++$counter;
        }

        return $this->attributes[$type][$name];
    }

    private function parseCategories($categories)
    {
        if (empty($categories)) {
            return [];
        }

        if (is_string($categories)) {
            $categories = [$categories];
        }

        $data = array_map(function ($chain) {
            $ids = [];
            foreach (explode('>', $chain) as $name) {
                $ids[] = $this->addCategory($name, count($ids) ? end($ids) : null);
            }

            return $ids;
        }, $categories);

        return $data;
    }

    private function addCategory($name, $parentId = null)
    {
        static $counter = 0;

        $hash = ($parentId === null) ? $name : "{$parentId}|$name";

        if (!isset($this->categories[$hash])) {
            $this->categories[$hash] = ++$counter;
        }

        return $this->categories[$hash];
    }

    private function parseAttributes($type, $attributes)
    {
        if (empty($attributes)) {
            return [];
        }

        $data = array_map(function ($chain) use ($type) {
            $ids = [];
            foreach (explode('|', $chain) as $name) {
                $ids[] = $this->addAttribute($type, $name);
            }

            return $ids;
        }, (array) $attributes);

        return array_unique(array_flatten($data));
    }

    /**
     * @param $value
     *
     * @return array ['шт', 1]
     */
    private function parsePacking($value)
    {
        $matches = [];

        if (preg_match('~(\d+)\s*(.*)~u', $value, $matches)) {
            return [$matches[2], $matches[1]];
        }

        return [empty($value) ? 'шт' : $value, 1];
    }

    private function getUniqueSKU($title, $id, $sku)
    {
        if (empty($sku)) {
            $sku = "HM{$id}";

            if (preg_match('~\(([\da-z\-\.]+)\)$~iu', $title, $matches)) {
                $sku = $matches[1];
            }
        }

        if (isset($this->skuList[$sku])) {
            $suffix = strtoupper(str_random(4));

            $sku = "{$sku}-{$suffix}";
        }

        $this->skuList[$sku] = true;

        return $sku;
    }

    private function removePackingFromTitle($title, $qty, $unit)
    {
        $unit = trim($unit, '.');

        // 5 шт
        $title = preg_replace("~,\\s{$qty}\\s?{$unit}[\\,\\.]*~u", '', $title);

        // (~80 шт), ( ~80 шт), (10 шт), (~50-100 шт), (50-55 шт)
        $title = preg_replace('/\s\(\s?~?\d*-?\d{1,}\s?шт\)/u', '', $title);

        return $title;
    }

    private function createReceiptNote()
    {
        $this->cli->write('Создаём первичную приходную накладную... ');

        $service = app(ReceiptNoteService::class);

        $note = $service->create(User::first(), 'Начальное состояние склада');
        $rate = $this->currencies->findByCode('USD')->getExchangeRate();

        $note
            ->setExchangeRate($rate)
            ->accept(User::first());

        if (! $note->exists) {
            $this->cli->writeln(sprintf('<error>Накладная не создана.</error>'));

            return;
        }

        $items = [];
        $docAmountCost = $docAmountRetail = 0;

        /** @var ProductInterface $product */
        foreach (Product::cursor() as $product) {
            if (0 === $product->getOnHand()) {
                continue;
            }

            $items[] = [
                'document_id'  => $note->getId(),
                'subject_id'   => $product->getId(),
                'quantity'     => $product->getOnHand(),
                'cost_price'   => $product->getCostPrice(),
                'retail_price' => $product->getRetailPrice(),
            ];

            $docAmountCost += $product->getOnHand() * $product->getCostPrice();
            $docAmountRetail += $product->getOnHand() * $product->getRetailPrice();
        }

        $note
            ->setAttribute('total_cost', $docAmountCost)
            ->setAttribute('total_retail', $docAmountRetail)
            ->save();

        $this->cli->writeln('готово.');
        $this->cli->newLine();

        $this->addRecords(ReceiptNoteItem::getTableName(), $items, 'Добавляем товары приходной накладной');

        config(['queue.default' => 'sync']);

        event(new ReceiptNoteAccepted($note));
    }
}
