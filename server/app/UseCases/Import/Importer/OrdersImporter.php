<?php

namespace Hm\Services\Import\Importer;

use Hm\Domain\State\State;
use Hm\Domain\Order\Order;
use Hm\Domain\Product\Product;
use Hm\Domain\Order\OrderItem;
use Hm\Domain\Addressing\Address;
use Hm\Domain\Addressing\Country;
use Hm\Domain\Product\ProductPacking;
use Hm\Domain\Order\OrderPaymentGraph;
use Hm\Domain\Order\OrderProcessGraph;
use Illuminate\Console\OutputStyle;
use Illuminate\Database\Connection;
use Hm\Domain\Payment\PaymentMethod;
use Hm\Domain\Shipping\ShippingMethod;

class OrdersImporter extends AbstractImporter
{
    private $countries = [];
    private $addresses = [];
    private $shippings = [];
    private $payments  = [];
    private $packings  = [];
    private $sold      = [];

    private $statesMap = [
        'Pay'        => [OrderPaymentGraph::STATE_PAID, OrderPaymentGraph::class],
        'On-hold'    => [OrderProcessGraph::STATE_RESERVED, OrderProcessGraph::class],
        'Pending'    => [OrderPaymentGraph::STATE_PENDING, OrderPaymentGraph::class],
        'Shipping'   => [OrderProcessGraph::STATE_SHIPPED, OrderProcessGraph::class],
        'Refunded'   => [OrderPaymentGraph::STATE_REFUNDED, OrderPaymentGraph::class],
        'Processing' => [OrderProcessGraph::STATE_ASSEMBLING, OrderProcessGraph::class],
        'Completed'  => [OrderProcessGraph::STATE_DELIVERED, OrderProcessGraph::class],
        //'Failed'     => '...',
        'Cancelled'  => [OrderProcessGraph::STATE_CANCELLED, OrderProcessGraph::class]
    ];

    public function __construct(Connection $db, OutputStyle $cli, array $config)
    {
        parent::__construct($db, $cli, $config);

        $this->shippings = collect(array_replace_recursive(
            ShippingMethod::all()->keyBy('title')->toArray(),
            collect($this->config['maps']['shipping_methods'])->keyBy('title')->toArray())
        )->keyBy('code')->toArray();

        $this->payments = collect(array_replace_recursive(
            PaymentMethod::all()->keyBy('title')->toArray(),
            collect($this->config['maps']['payments_methods'])->keyBy('title')->toArray())
        )->keyBy('code')->toArray();

        $this->packings = ProductPacking::all()->keyBy('product_id');
    }

    public function import($data)
    {
        $data = $this->convert($data);

        $orders = $items = $states = $payments = $shippings = $countries = [];

        // страны
        $countries = collect($this->countries)->keys()->map(function ($code) {
            return ['code' => $code, 'enabled' => true];
        });
        $this->addRecords(Country::getTableName(), $countries->all(), 'Страны');

        // адреса
        $addresses = collect($this->addresses)->values()->map(function ($items) {
            return array_values($items);
        });
        $this->addRecords(Address::getTableName(), $addresses->collapse()->all(), 'Адреса');

        // заказы, позиции заказов и статусы заказов
        foreach ($data as $order) {
            $orders[] = array_only($order, [
                'id', 'customer_id', 'number', 'additionals',
                'payment_method_id', 'shipping_method_id',
                'overall_total', 'items_total', 'weight', 'shipping_cost',
                'shipping_address', 'created_at', 'updated_at'
            ]);

            foreach ($order['items'] as $item) {
                $items[] = array_merge($item, [
                    'created_at' => $order['created_at'],
                    'updated_at' => $order['updated_at']
                ]);
            }

            $states[] = [
                'stateful_id'   => $order['id'],
                'stateful_type' => 'order',
                'creator_id'    => $order['customer_id'],
                'type'          => $this->statesMap[$order['state']][0],
                'graph'         => $this->statesMap[$order['state']][1]::getName(),
                'ttl'           => 600,
                'data'          => '{}',
                'created_at'    => $order['created_at'],
                'expired_at'    => $order['updated_at']
            ];
        }

        $this->addRecords(Order::getTableName(), $orders, 'Заказы');
        $this->addRecords(OrderItem::getTableName(), $items, 'Позиции заказов');
        $this->addRecords(State::getTableName(), $states, 'Статусы заказов');

        $this->writeItemsSold();

        $this->updateAutoincrement(new Order());
    }

    private function convert($orders)
    {
        $this->cli->progressStart(count($orders));

        $records = array_map(function ($order) {
            $this->cli->progressAdvance();

            if (! isset($order['@ids'][0]) || empty($order['@ids'][0])) {
                return false;
            }

            $additionals = new \stdClass();

            if ((int) $order['discount']) {
                $additionals->{'old_discount_amount'} = abs((int) $order['discount']);
            }

            if (is_array($order['Coupon Code'])) {
                $additionals->{'old_coupon_codes'} = $order['Coupon Code'];
            }

            $order['additionals'] = json_encode($additionals);

            [$order['items_total'], $order['weight'], $order['items']] = $this->getItemsData($order);

            $order['payment_method_id'] = $this->getPaymentId($order['Payment Gateway ID']);
            $order['shipping_method_id'] = $this->getShippingId($order['Shipping Method ID']);

            if (empty($order['customer_id'])) {
                $order['customer_id'] = null;
            }

            $address = $this->getShippingInfo($order);
            $customerId = (int) $order['customer_id'];
            $hash = $this->getAddressHash($address);

            if ($customerId > 0 && count(array_filter($address)) > 1) {
                $this->addresses[$customerId][$hash] = array_merge($address, ['customer_id' => $customerId]);
            }

            $order['shipping_address'] = json_encode((object) $address, JSON_UNESCAPED_UNICODE);

            $this->addCountry($address['country_code']);

            $orderDate = str_replace('/', '-', $order['Order Date']);

            $order['number'] = $order['id'];

            return $this->addTimestamps($order, $orderDate.' '.$order['Order Time']);
        }, $orders);
        $this->cli->progressFinish();

        $records = array_filter($records);

        return $records;
    }

    private function getItemsData($order): array
    {
        $items = [];
        $itemsTotal = $weightTotal = 0;

        $ids  = is_string($order['@ids']) ? [$order['@ids']] : $order['@ids'];
        $qtys = is_string($order['@qtys']) ? [$order['@qtys']] : $order['@qtys'];
        $weights = is_string($order['@weights']) ? [$order['@weights']] : $order['@weights'];
        $cost_prices = is_string($order['@item_costs']) ? [$order['@item_costs']] : $order['@item_costs'];
        $retail_prices = is_string($order['@subtotals']) ? [$order['@subtotals']] : $order['@subtotals'];

        for ($i = 0, $iMax = count($ids); $i < $iMax; $i++) {
            $qty = (int) $qtys[$i];
            $subjectId = empty($ids[$i]) ? null : $ids[$i];

            if (0 === $qty || null === $subjectId || !isset($this->packings[$subjectId])) {
                continue;
            }

            $weightTotal += (int) $weights[$i];
            $itemsTotal += (float) $retail_prices[$i];

            $retailPrice = (float) $retail_prices[$i] / $qty;
            $costPrice = (float) $cost_prices[$i];
            $costPrice = $costPrice > 0 ? $costPrice : $retailPrice / 2;

            $this->sold[$subjectId] = isset($this->sold[$subjectId])
                ? $this->sold[$subjectId] + $qty
                : $qty;

            $items[] = [
                'order_id'         => $order['id'],
                'purchasable_id'   => $this->packings[$subjectId]->getId(),
                'purchasable_type' => 'packing',
                'quantity'         => $qty,
                'cost_price'       => $costPrice,
                'retail_price'     => $retailPrice,
            ];
        }

        return [$itemsTotal, $weightTotal, $items];
    }

    private function addCountry($code)
    {
        if ($code && !isset($this->countries[$code])) {
            $this->countries[$code] = true;
        }
    }

    private function getPaymentId($code)
    {
        if (empty($code)) {
            return null;
        }

        return isset($this->payments[$code])
            ? $this->payments[$code]['id']
            : null;
    }

    private function getShippingId($code)
    {
        if (empty($code)) {
            return null;
        }

        return isset($this->shippings[$code])
            ? $this->shippings[$code]['id']
            : null;
    }

    private function getShippingInfo(array $order): array
    {
        if (is_array($order['Billing: Full Name'])) {
            $order['Billing: Full Name'] = array_filter($order['Billing: Full Name']);
            $order['Billing: Full Name'] = reset($order['Billing: Full Name']);
        }

        if ($order['Billing: Country (prefix)'] === 'Россия') {
            $order['Billing: Country (prefix)'] = 'RU';
        }

        $order['Billing: City'] = preg_replace('~г\.\s*~iu', '', $order['Billing: City']);

        $order['Billing: Street Address (Full)'] = preg_replace(
            [
                '~\s*\\\\+\s*~', '~^(улица|ул\.|ул,)\s*(?=\w)~iu',
                '~(копрус|корпус|корп|кор)(\.|,)*\s*(?=\d)~iu',
                '~(квартира|кв)(\.|,)*\s*(?=\d)~iu',
                '~д(\.|,)*\s*(?=\d)~iu', '~дом\s*(?=\d)~iu'
            ],
            ['', 'ул. ', 'корп. ', 'кв. ', 'д. ', 'д. '],
            trim($order['Billing: Street Address (Full)'], '.,')
        );

        $order['Billing: Street Address (Full)'] = preg_replace(
            ['~,*\s(д\.|корп\.|кв\.)~', '~,\s*(д.|корп\.|кв\.)~'], ', $1', $order['Billing: Street Address (Full)']
        );

        return [
            'full_name'    => $order['Billing: Full Name'],
            'country_code' => $order['Billing: Country (prefix)'],
            'post_code'    => str_limit($order['Billing: ZIP Code'], 10, ''),
            'province'     => $order['Billing: State'],
            'locality'     => $order['Billing: City'],
            'street'       => $order['Billing: Street Address (Full)'],
            'email'        => strtolower($order['Billing: E-mail Address']),
            'phone'        => clear_phone($order['Billing: Phone Number']),
        ];
    }

    private function getAddressHash(array $address): string
    {
        $address = array_except($address, [
            'full_name', 'country_code', 'province', 'phone'
        ]);

        return md5(vsprintf('%s%s%s%s', array_map('Illuminate\Support\Str::lower', $address)));
    }

    private function writeItemsSold()
    {
        $this->cli->writeln('Записываем в товары количество продаж...');

        $this->db->transaction(function () {
            $table = Product::getTableName();

            $bar = $this->cli->createProgressBar(count($this->sold));

            foreach ($this->sold as $productId => $sold) {
                $bar->advance();

                $this->db->table($table)->where('id', $productId)->update(compact('sold'));
            }

            $bar->finish();
        });

        $this->cli->newLine(2);
    }
}
