<?php

namespace App\UseCases\Import\Importer;

use Illuminate\Support\Collection;

class CompanyTempImporter extends AbstractImport
{
    public function import($data)
    {
        $records = $this->convert($data);

        $this->addRecords($records, 'Победители');
    }

    public function convert(Collection $data)
    {
        $winners = $data->transform(function ($item) {

            $winner = new Winner();

            $winner->num_card = (string)$item['num_card'];
            $winner->purchase_at = $this->addDataTimeFormat($item['purchase_at']);
            $winner->amount = (string)$item['amount'];
            $winner->phone = $this->clear_phone($item['phone']);
            $winner->email = (string)$item['email'];
            $winner->prize = (string)$item['prize'];
            $winner->draw_at = $this->addDataFormat($item['draw_at']);

            return $winner;

        });

        return $winners;
    }


    private function addDataTimeFormat($datetime): ?string
    {
        try {
            $datetime = $datetime ? new \DateTime($datetime) : new \DateTime();
            return $datetime->format('Y-m-d H:i:s');
        } catch (\Exception $e) {
            logs('winners')->error('Ошибка формата - ' . $e->getMessage(), ['datetime' => $datetime]);
        }

        return null;
    }

    private function addDataFormat($date): ?string
    {
        try {
            $date = $date ? new \DateTime($date) : new \DateTime();
            return $date->format('Y-m-d');
        } catch (\Exception $e) {
            logs('winners')->error('Ошибка формата - ' . $e->getMessage(), ['date' => $date]);
        }

        return null;
    }

    private function clear_phone($phone)
    {
        return preg_replace('~[\D+]~', '', $phone);
    }
}
