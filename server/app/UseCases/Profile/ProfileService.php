<?php

namespace App\UseCases\Profile;


use App\Domain\User\User;
use App\Http\Requests\Cabinet\ProfileEditRequest;
use App\Repositories\User\UserRepository;

class ProfileService
{


    /**
     * @var UserRepository
     */
    public $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }


    public function edit($id, ProfileEditRequest $request): void
    {
        /** @var User $user */
        $user = $this->user->findOrFail($id);
       // $oldPhone = $user->phone;
        $user->update($request->only('username', 'email', 'last_name', 'first_name', 'phone'));
//        if ($user->phone !== $oldPhone) {
//            $user->unverifyPhone();
//        }
    }
}
