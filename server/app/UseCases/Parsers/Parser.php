<?php
namespace App\UseCases\Parsers;

use Goutte\Client;

class Parser
{
    private $client;

    public function __construct($url)
    {



        $this->client = new Client();

        $crawler = $this->client->request('GET', $url);

        $crawler->filter('body > div:nth-child(8) > div > div.layout-main__col-content.container-btm-space > div:nth-child(4) > div.card__content > div > div:nth-child(1) > div:nth-child(4) > div.named-list__content')->each(function ($node) {
            print $node->text()."\n";
        });

    }



}