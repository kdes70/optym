<?php

namespace App\UseCases\Parsers\Drivers;

use App\UseCases\Parsers\ConfigTrait;
use App\UseCases\Parsers\Contracts\Provider;
use Goutte\Client;
use Illuminate\Http\Request;

abstract class AbstractDriver implements Provider
{
    use ConfigTrait;

    /**
     * The client driver ID.
     *
     * @var string
     */
    protected $clientId;

    /**
     * The client Url.
     *
     * @var string
     */
    protected $clientUrl;


    /**
     * The HTTP request instance.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * The HTTP Client instance.
     *
     * @var \Goutte\Client
     */
    protected $httpClient;

    /**
     * The custom parameters to be sent with the request.
     *
     * @var array
     */
    protected $parameters = [];


    /**
     * The scopes being requested.
     *
     * @var array
     */
    protected $scopes = [];


    /**
     * The separating character for the requested scopes.
     *
     * @var string
     */
    protected $scopeSeparator = ',';


    /**
     * The type of the encoding in the query.
     *
     * @var int Can be either PHP_QUERY_RFC3986 or PHP_QUERY_RFC1738.
     */
    protected $encodingType = PHP_QUERY_RFC1738;


    public function __construct(Request $request, $clientId, $clientUrl)
    {
        $this->request = $request;
        $this->clientId = $clientId;
        $this->clientUrl = $clientUrl;
    }

    /**
     * Set the scopes of the requested access.
     *
     * @param  array $scopes
     * @return $this
     */
    public function scopes(array $scopes)
    {
        $this->scopes = array_unique(array_merge($this->scopes, $scopes));

        return $this;
    }

    /**
     * Get the current scopes.
     *
     * @return array
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * Format the given scopes.
     *
     * @param  array $scopes
     * @param  string $scopeSeparator
     * @return string
     */
    protected function formatScopes(array $scopes, $scopeSeparator)
    {
        return implode($scopeSeparator, $scopes);
    }


    /**
     * Get a instance of the Guzzle HTTP client.
     *
     * @return \Goutte\Client
     */
    protected function getHttpClient()
    {
        if (is_null($this->httpClient)) {
            $this->httpClient = new Client();
        }

        return $this->httpClient;
    }


    /**
     * Set the Guzzle HTTP client instance.
     *
     * @param  \Goutte\Client $client
     * @return $this
     */
    public function setHttpClient(Client $client)
    {
        $this->httpClient = $client;

        return $this;
    }

    /**
     * Set the request instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get the GET parameters for the code request.
     *
     * @return array
     */
    protected function getCodeFields()
    {
        $fields = [
            'client_id'     => $this->clientId,
            'redirect_uri'  => $this->clientUrl,
            'scope'         => $this->formatScopes($this->getScopes(), $this->scopeSeparator),
            'response_type' => 'code',
        ];

        return array_merge($fields, $this->parameters);
    }

    /**
     * Set the custom parameters of the request.
     *
     * @param  array $parameters
     * @return $this
     */
    public function with(array $parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * Set the fields to request.
     *
     * @param array $fields
     *
     * @return $this
     */
    public function fields(array $fields)
    {
        $this->fields = $fields;

        return $this;
    }

    public function link()
    {
        // TODO: Implement link() method.
    }

    public function url()
    {
        // TODO: Implement url() method.
    }

    public function aboutCompany()
    {
        // TODO: Implement aboutCompany() method.
    }

    public function contactCompany()
    {
        // TODO: Implement contactCompany() method.
    }
}