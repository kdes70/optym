<?php

namespace App\UseCases\Parsers;

use App\UseCases\Parsers\Contracts\Factory;
use App\UseCases\Parsers\Drivers\AbstractDriver;
use Illuminate\Support\Arr;
use InvalidArgumentException;
use Illuminate\Support\Manager;


class ParserManager extends Manager implements Factory
{

    /** @var array */
    private $config;

    /** @var array */
    protected $availableDrivers;

    protected $currentDriverName;


    public function __construct($app, array $config)
    {
        parent::__construct($app);

        $this->config = $config;
    }

    /**
     * Get a driver instance.
     *
     * @param  string  $driver
     * @return mixed
     */
    public function with($driver)
    {
        return $this->driver($driver);
    }


    public function driver($driver = null)
    {
        $this->currentDriverName = $driver;

        $object = parent::driver($driver);

        if ($object instanceof AbstractDriver) {
            $scopes = config("parser.{$driver}.scopes");
            $fields = config("parser.{$driver}.fields");

            if (! empty($scopes)) {
                $object->scopes($scopes);
            }

            if (! empty($fields)) {
                $object->fields($fields);
            }
        }

        return $object;
    }



    public function getAvailableDrivers(): array
    {
        if (null === $this->availableDrivers) {
            $keys = array_keys($this->config['drivers']);
            $values = Arr::pluck($this->config['drivers'], 'title');

            $this->availableDrivers = array_combine($keys, $values);
        }

        return $this->availableDrivers;
    }

    public function isDriverExists(string $driver): bool
    {
        return isset($this->config['drivers'][$driver]);
    }


    /**
     * Get the default driver name.
     *
     * @throws \InvalidArgumentException
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        throw new InvalidArgumentException('No Socialite driver was specified.');
    }

    public function aboutCompany()
    {
        // TODO: Implement aboutCompany() method.
    }

    public function contactCompany()
    {
        // TODO: Implement contactCompany() method.
    }
}
