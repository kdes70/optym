<?php

namespace App\UseCases\Parsers\Contracts;

interface Factory
{
    /**
     * Get an Parser provider implementation.
     *
     * @param  string $driver
     * @return \App\UseCases\Parsers\Contracts\Provider
     */
    public function driver($driver = null);

}
