<?php

namespace App\UseCases\Parsers\Contracts;

use  App\UseCases\Parsers\Contracts\ConfigInterface as Config;

interface Provider
{

    public function link();

    public function url();

    public function aboutCompany();

    public function contactCompany();

    /**
     * @param Config $config
     *
     * @return $this
     */
    public function setConfig(Config $config);




}
