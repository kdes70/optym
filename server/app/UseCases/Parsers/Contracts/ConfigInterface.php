<?php
namespace App\UseCases\Parsers\Contracts;

interface ConfigInterface
{
    /**
     * @return array
     */
    public function get();
}
