<?php

namespace App\UseCases\Auth;

use App\Domain\User\User;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

trait AuthenticatesUsers
{
    use RedirectsUsers, ThrottlesLogins;

    public function show()
    {
        return view('auth.login');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed $user
     * @return mixed
     */
    public function authenticated(Request $request, $user)
    {

    }

    /**
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws ValidationException
     */
    public function login(LoginRequest $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            $this->sendLockoutResponse($request);
        }

        // TODO: проверка забаненных и экспортированных

        dd( $this->getCredentials($request));


        $authenticate = $this->guard->attempt(
            $this->getCredentials($request),
            $request->has('remember') ?? true
        );

        if ($authenticate) {

            $request->session()->regenerate();
            $this->clearLoginAttempts($request);

            /** @var User $user */
            $user = auth()->user();

            if ($user->isWait()) {
                auth()->logout();
                return back()->with('error', 'You need to confirm your account. Please check your email.');
            }

            return $this->authenticated($request, $this->guard->user())
                ?: redirect()->intended(route('home'));
        }

        $this->incrementLoginAttempts($request);

        throw ValidationException::withMessages(['login' => [trans('auth.failed')]]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws ValidationException
     */
    public function verify(Request $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            $this->sendLockoutResponse($request);
        }

        $this->validate($request, [
            'token' => 'required|string',
        ]);

        if (!$session = $request->session()->get('auth')) {
            throw new BadRequestHttpException('Missing token info.');
        }

        /** @var User $user */
        $user = User::findOrFail($session['id']);

        if ($request['token'] === $session['token']) {
            $request->session()->flush();
            $this->clearLoginAttempts($request);
            auth()->login($user, $session['remember']);
            return redirect()->intended(route('profiles.home', ['username' => $user->getUsername()]));
        }

        $this->incrementLoginAttempts($request);

        throw ValidationException::withMessages(['token' => ['Invalid auth token.']]);
    }

    public function logout()
    {
        $this->guard->logout();
        request()->session()->invalidate();

        return redirect()->route('home');
    }

    private function getCredentials(LoginRequest $request): array
    {
        /** @var string $field */
        $field = $this->username();

        if (Str::startsWith($request['login'], '+')) {
            $field = 'phone';
            // remove "+" sign
            $request->merge(['login' => Str::substr($request['login'], 1)]);
        } elseif (filter_var($request['login'], FILTER_VALIDATE_EMAIL)) {
            $field = 'email';
        } else {
            throw new BadRequestHttpException('login must be telephone or email');
        }

        $request->merge([$field => $request['login']]);

        return $request->only([$field, 'password']);
    }

    protected function username()
    {
        return 'login';
    }
}
