<?php

namespace App\UseCases\Auth;

use App\Domain\Company\Company;
use App\Domain\User\DTO\RegisterUser;
use App\Domain\User\Enums\UserStateEnum;
use App\Domain\User\Enums\UserTypeEnum;
use App\Domain\User\Role;
use App\Services\CompanyService;
use App\Domain\User\Profile;
use App\Domain\User\User;
use App\Services\ProfileService;
use App\Services\UserService;
use App\Mail\Auth\VerifyMail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Mail\Mailer;

/**
 * Class RegisterService
 */
class RegisterService
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var \App\Services\UserService
     */
    private $userService;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var ProfileService
     */
    private $profileService;

    /**
     * RegisterService constructor.
     *
     * @param Mailer $mailer
     * @param UserService $userService
     * @param ProfileService $profileService
     * @param \App\Services\CompanyService $companyService
     */
    public function __construct(
        Mailer $mailer,
        UserService $userService,
        ProfileService $profileService,
        CompanyService $companyService
    )
    {
        $this->mailer = $mailer;
        $this->userService = $userService;
        $this->companyService = $companyService;
        $this->profileService = $profileService;
    }

    /**
     * @param RegisterUser $dto
     * @param string $ip
     *
     * @return User
     *
     * @throws \Throwable
     */
    public function register(RegisterUser $dto): User
    {
        $user = $this->userService->register($dto);
        $profile = $this->profileService->registerFields($dto);

        if ($user->type->is(UserTypeEnum::BUYER)) {
            $user = $this->createBuyer($user, $profile);
        }

        if ($user->type->is(UserTypeEnum::WHOLESALE)) {
            $company = $this->companyService->registerFields($dto);
            $user = $this->createWholesale($user, $profile, $company);
        }

        if (config('optum.registration.verification')) {
            $this->mailer->to($user->getEmail())->send(new VerifyMail($user));
        }

        event(new Registered($user));

        return $user;
    }


    /**
     * @param $id
     */
    public function verify($id): void
    {
        /** @var User $user */
        $user = User::findOrFail($id);

        // TODO усилить проверку на подтверждения по токину
        if (!$user->isWait()) {
            throw new \DomainException('User is already verified.');
        }

        $user->setState(UserStateEnum::ACTIVE());
        $user->setVerifyToken(null);
        $user->setVerified(true);
        $user->save();
    }

    /**
     * Создание покупателя
     *
     * @param User $user
     * @param Profile $profile
     * @return User
     * @throws \Exception
     * @throws \Throwable
     */
    private function createBuyer(User $user, Profile $profile): User
    {
        $user->setRole(new Role(Role::BUYER));

        $user = $this->createAccount($user, $profile);

        if (!$user) {
            throw new \DomainException('Не удалось создать аккаунт!');
        }
        return $user;
    }

    /**
     * Аккаунт продавца
     *
     * @param User $user
     * @param Profile $profile
     * @param Company $company
     * @return User
     * @throws \Throwable
     */
    private function createWholesale(User $user, Profile $profile, Company $company): User
    {
        $user->setRole(new Role(Role::WHOLESALE));

        $user = $this->createAccountCompany($user, $profile, $company);

        if (!$user) {
            throw new \DomainException('Не удалось создать аккаунт продавца!');
        }

        return $user;
    }


    /**
     * Создание аккаунта
     *
     * @param User $user
     * @param Profile $profile
     * @return User
     * @throws \Exception
     * @throws \Throwable
     */
    private function createAccount(User $user, Profile $profile): User
    {
        \DB::transaction(function () use ($user, $profile) {
            $user->save();
            $user->profile()->save($profile);
        });

        return $user->fresh();

    }

    /**
     * @param User $user
     * @param Profile $profile
     * @param Company $company
     *
     * @return User
     *
     * @throws \Exception
     * @throws \Throwable
     */
    private function createAccountCompany(User $user, Profile $profile, Company $company): User
    {
        return \DB::transaction(function () use ($user, $profile, $company) {
            $user->save();
            $user->profile()->save($profile);
            $user->company()->save($company);

            return $user;
        });
    }

}
