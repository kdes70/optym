<?php
namespace App\Core\DTO\Traits;

use App\Core\DTO\DTO;

trait RequestMakesDTO
{
    /**
     * Возвращает DTO объект, заполенный значениями из FormRequest
     *
     * @return DTO
     */
    public function getDto()
    {
        /**
         * TODO:
         * проверка на наследование от FormRequest и имплементирование MakesDTO
         */
        $class = $this->getDTOClass();

        $availableFields = DTO::getVars($class);

        return new $class($this->only($availableFields));
    }
}
