<?php
namespace App\Core\DTO;

class UpdateDTO extends DTO
{
    /**
     * {@inheritdoc}
     */
    protected $serializeWithNullVars = false;
}