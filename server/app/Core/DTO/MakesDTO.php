<?php
namespace App\Core\DTO;

interface MakesDTO
{
    /**
     * @return string DTO class
     */
    public function getDTOClass();

    /**
     * Возвращает DTO объект, заполенный значениями
     *
     * @return DTO
     */
    public function getDto();
}