<?php
namespace App\Core\DTO;

use DomainException;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Collection;

class DTO implements Arrayable, Jsonable
{
    /**
     * @var $serializeWithNullVars bool
     *
     * Переменная контролирует результат функции toArray.
     * Управляет фильтром переменных, которым не было присвоено значение ( далее 'пустые').
     *
     * true - вернутся все объявленные в классе переменные, пустые будут равны null.
     * false - вернутся только те переменные, которым было присвоено значение (не null)
     *
     */
    protected $serializeWithNullVars = true;

    public function __construct(array $data = [])
    {
        if (!is_array($data) || empty($data)) {
            return;
        }

        foreach ($data as $key => $value) {
            /**
             * Не даем перезаписать значения по умолчанию в null
             */
            if (null === $this->$key && null !== $value) {
                $this->$key = $value;
            }
        }
    }

    /**
     * Получаем массив доступных полей
     *
     * @return array
     */
    public static function getVars($class)
    {
        $vars = static::filterVars(get_class_vars($class));

        return array_keys($vars);
    }

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        $vars = static::filterVars(get_object_vars($this));

        if ($this->serializeWithNullVars) {
            return $vars;
        } else {
            return array_where($vars, function ($item) {
                return null !== $item;
            });
        }
    }

    /**
     * {@inheritdoc}
     */
    public function toJson($options = 271)
    {
        return json_encode($this->toArray(), $options);
    }

    /**
     * {@inheritdoc}
     */
    public function __set($name, $value)
    {
        throw new DomainException("Параметр '{$name}' не существует.");
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return collect($this->toArray());
    }

    /**
     * Убираем сервисные переменные
     *
     * @param array $vars
     *
     * @return array
     */
    protected static function filterVars(array $vars)
    {
        unset($vars['serializeWithNullVars']);

        return $vars;
    }
}
