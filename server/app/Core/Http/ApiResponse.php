<?php

namespace App\Core\Http;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Http\JsonResponse as IlluminateJsonResponse;

/**
 * Class ApiResponse
 */
class ApiResponse extends IlluminateJsonResponse
{
    const HTTP_RETRY_WITH = 449;

    /** @var mixed */
    public $original;

    /** @var array */
    protected static $meta = [];

    /** @var array */
    private $includes;

    public static function create($data = null, $status = 200, $headers = [], $options = 271)
    {
        return new static($data, $status, $headers, $options);
    }

    public function __construct($data = null, $status = 200, $headers = [], $options = 271)
    {
        static::$statusTexts[self::HTTP_RETRY_WITH] = 'Retry With';

        parent::__construct(null, $status, $headers, $options);

        if ($data instanceof LengthAwarePaginator) {
            $this->addMeta('pagination', [
                'total_items' => $data->total(),
                'total_pages' => $data->lastPage(),
                'page'        => $data->currentPage(),
                'per_page'    => $data->perPage(),
            ]);

            $data = $data->items();
        }

        $this->includes = [];

        $this->original = $data;
    }

    public function getMeta(): array
    {
        return self::$meta;
    }

    public function pullMeta(): array
    {
        $meta = self::$meta;

        self::$meta = [];

        return $meta;
    }

    public function addMeta(string $key, $value): ApiResponse
    {
        if (isset(self::$meta[$key])) {
            $value = array_merge(self::$meta[$key], $value);
        }

        self::$meta[$key] = $value;

        return $this;
    }

    public function getIncludes(): array
    {
        return $this->includes;
    }

    public function withIncludes(...$includes)
    {
        $this->includes = $includes;

        return $this;
    }

    public static function created($data): ApiResponse
    {
        return new static($data, static::HTTP_CREATED);
    }

    public static function ok($data): ApiResponse
    {
        return new static($data, static::HTTP_OK);
    }

    public static function noContent(): IlluminateResponse
    {
        return new IlluminateResponse(null, static::HTTP_NO_CONTENT);
    }

    public static function retryWith($message): ApiResponse
    {
        return new static(compact('message'), static::HTTP_RETRY_WITH);
    }

    public static function unprocessable($message): ApiResponse
    {
        return new static(compact('message'), static::HTTP_UNPROCESSABLE_ENTITY);
    }

    public static function failedDependency($message = null): ApiResponse
    {
        return new static(compact('message'), static::HTTP_FAILED_DEPENDENCY);
    }

    public static function redirect(string $url): ApiResponse
    {
        return static::ok(['redirect_url' => $url]);
    }
}
