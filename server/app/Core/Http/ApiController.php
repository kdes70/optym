<?php

namespace App\Core\Http;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as IlluminateController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiController
 */
abstract class ApiController extends IlluminateController
{
    use DispatchesJobs, AuthorizesRequests;

    /**
     * @param null $data
     * @param array $headers
     * @param int $options
     *
     * @return JsonResponse
     */
    protected function success($data = null, array $headers = [], $options = 271): JsonResponse
    {
        return response()->json($data, Response::HTTP_OK, $headers, $options);
    }

    /**
     * @param null $data
     * @param array $headers
     * @param int $options
     *
     * @return JsonResponse
     */
    protected function error($data = null, array $headers = [], $options = 271): JsonResponse
    {
        return response()->json($data, Response::HTTP_FORBIDDEN, $headers, $options);
    }

    /**
     * @param  mixed  $errors
     * @param  array  $headers
     * @param  int    $options
     *
     * @return ApiResponse
     */
    protected function validation($errors, array $headers = [], $options = 271)
    {
        if (is_string($errors)) {
            $errors = ['message' => $errors];
        }

        return ApiResponse::create($errors, ApiResponse::HTTP_UNPROCESSABLE_ENTITY, $headers, $options);
    }

    protected function deleted()
    {
        return ApiResponse::create(null, ApiResponse::HTTP_NO_CONTENT);
    }
}
