<?php

namespace App\Core\Eloquent\Repository;

use App\Core\Eloquent\AbstractModel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;
use App\Core\Eloquent\Repository\Filter\FilterParser;
use App\Core\Eloquent\Repository\Contracts\Criterion as CriterionContract;
use App\Core\Eloquent\Repository\Contracts\Repository as RepositoryContract;

class RequestCriterion implements CriterionContract
{
    /** @var Request */
    private $request;

    private $operators = [
        'eq'  => '=',
        'ne'  => '<>',
        'lt'  => '<',
        'gt'  => '>',
        'gte' => '>=',
        'lte' => '<=',
    ];

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param  AbstractModel|Builder       $model
     * @param  RepositoryContract  $repository
     *
     * @return AbstractModel
     */
    public function apply($model, RepositoryContract $repository)
    {
        $this->setWith($repository);

        $model = $this->setSort($model, $repository);

        $model = $this->setSearch($model, $repository);

        $model = $this->setFilter($model);

        $repository->setPerPage($this->request->get('per_page', 10));

        return $model;
    }

    protected function setWith(RepositoryContract $repository): void
    {
        $relations = $this->request->get('with', []);

        if (! empty($relations) && is_string($relations)) {
            $relations = explode(',', $relations);
        }

        $repository->with($relations);
    }

    protected function setSort($model, RepositoryContract $repository)
    {
        $column = $this->request->get('sort', 'created_at');

        if (in_array($column, $repository->getSortableFields(), true)) {
            $direction = $this->request->get('direction', 'desc');

            /** @var Builder $model */
            $sortingMethod = 'sortBy'.Str::studly($column);

            if (method_exists($repository, $sortingMethod)) {
                return $repository->{$sortingMethod}($model, $direction);
            }

            return $model->orderBy($column, $direction);
        }

        return $model;
    }

    protected function setSearch($model, RepositoryContract $repository)
    {
        $query = Str::lower($this->request->get('search'));

        if (empty($query)) {
            return $model;
        }

        /** @var Builder $model */
        $model = $model->where(function (Builder $model) use ($repository, $query) {
            $id = (int)$query;

            if ($id > 0) {
                $model = $model->orWhere($model->getModel()->getKeyName(), '=', $id);
            }

            foreach ($repository->getSearchableFields() as $field) {
                $model = $model->orWhere("{$field}", 'ilike', "%{$query}%");
            }
        });

        return $model;
    }

    public function setFilter($model)
    {
        $clauses = (new FilterParser())->parse($this->request->get('filter'));

        if (! $clauses) {
            return $model;
        }

        foreach ($clauses as $clause) {
            if (0 === key($clause)) {
                /** @var AbstractModel|Builder $model */
                $model = $model->where(function ($query) use ($clause) {
                    foreach ($clause as $subClause) {
                        //sd($subClause);
                        $query = $this->addClause($query, $subClause['relation'], $subClause['field'],
                            $subClause['operator'], $subClause['value']);
                    }

                    return $query;
                });
            } else {
                $model = $this->addClause($model, $clause['relation'], $clause['field'], $clause['operator'],
                    $clause['value']);
            }
        }

        return $model;
    }

    /**
     * @param  AbstractModel|Builder  $model
     * @param  string|null    $relation
     * @param  string         $field
     * @param  string         $operator
     * @param  mixed          $value
     * @param  bool           $nested
     *
     * @return AbstractModel|Builder
     */
    private function addClause($model, ?string $relation, string $field, string $operator, $value, bool $nested = false)
    {
        $type = $nested ? 'or' : 'and';

        if ($relation) {
            $model = $model->has($relation, '>=', 1, $type, function (Builder $query) use ($field, $operator, $value) {
                return $this->addClause($query, null, $field, $operator, $value);
            });
        } else {
            if (array_key_exists($operator, $this->operators)) {
                $model = $model->where($field, $this->operators[$operator], $value, $type);
            } elseif ('in' === $operator) {
                $model = $model->whereIn($field, $value, $type);
            }
        }

        return $model;
    }
}
