<?php

namespace App\Core\Eloquent\Repository;

use App\Core\Eloquent\AbstractModel;
use App\Repositories\Traits\Sortable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection as SupportCollection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Core\Eloquent\Repository\Contracts\Criterion as CriterionContract;
use App\Core\Eloquent\Repository\Contracts\Repository as RepositoryContract;

abstract class AbstractRepository implements RepositoryContract
{
    use Sortable;

    /** @var int */
    protected $perPage = 10;

    /** @var SupportCollection */
    protected $criteria;

    /** @var array */
    protected $relations = [];

    /** @var bool */
    protected $skipCriteria = false;

    /** @var array */
    protected $fieldsSortable = [];

    /** @var array */
    protected $fieldsSearchable = [];

    public function __construct()
    {
        $this->resetCriteria();

        $this->boot();
    }

    protected function boot(): void
    {
        //
    }

    /**
     * {@inheritdoc}
     */
    abstract public function getModel();

    /**
     * {@inheritdoc}
     */
    public function getQuery(): Builder
    {
        return $this->applyCriteria($this->getModel())->with($this->relations);
    }

    /**
     * {@inheritdoc}
     */
    public function pushCriterion($criterion)
    {
        if (is_string($criterion)) {
            $criterion = app($criterion);
        }

        $class = get_class($criterion);

        if (! $criterion instanceof CriterionContract) {
            throw new RepositoryException(sprintf('Class \'%s\' must be an instance of %s', $class, CriterionContract::class));
        }

        $this->criteria->put($class, $criterion);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function pullCriterion($criteria)
    {
        if (is_object($criteria)) {
            $criteria = get_class($criteria);
        }

        $this->criteria->pull($criteria);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * {@inheritdoc}
     */
    public function skipCriteria(bool $skip = true)
    {
        $this->skipCriteria = $skip;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function resetCriteria()
    {
        $this->criteria = new \Illuminate\Support\Collection();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSortableFields(): array
    {
        return $this->fieldsSortable;
    }

    /**
     * {@inheritdoc}
     */
    public function getSearchableFields(): array
    {
        return $this->fieldsSearchable;
    }

    /**
     * {@inheritdoc}
     */
    public function setPerPage(int $value = 10)
    {
        $this->perPage = $value;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function with($relations)
    {
        if (is_string($relations)) {
            $relations = func_get_args();
        }

        if (is_array($relations)) {
            $this->relations = array_unique(array_merge($this->relations, $relations));
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return $this->getQuery()->get();
    }

    /**
     * {@inheritdoc}
     */
    public function findOne($id)
    {
        return $this->getModel()->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function findOrFail($id)
    {
        return $this->getModel()->findOrFail($id);
    }

    /**
     * {@inheritdoc}
     */
    public function getById($id)
    {
        $model = $this->getModel();

        $found = $this->getModel()->where($model->getQualifiedKeyName(), '=', $id)->first();

        if (! $found) {
            throw (new ModelNotFoundException)->setModel(get_class($model));
        }

        return $found;
    }

    /**
     * {@inheritdoc}
     */
    public function findMany(array $ids)
    {
        return $this->getModel()->findMany($ids);
    }

    /**
     * {@inheritdoc}
     */
    public function findOneBy($column, $value = null)
    {
        if (empty($column)) {
            return null;
        }

        $query = $this->getModel();

        if (is_string($column)) {
            return $query->where($column, $value)->first();
        }

        if (! is_array($column)) {
            return null;
        }

        foreach ($column as $k => $v) {
            $query = $query->where($k, '=', $v);
        }

        return $query->first();
    }

    public function getOneBy($column, $value = null): AbstractModel
    {
        $model = $this->getModel();

        $found = $this->findOneBy($column, $value);

        if (! $found) {
            throw (new ModelNotFoundException)->setModel(get_class($model));
        }

        return $found;
    }

    /**
     * {@inheritdoc}
     */
    public function findManyBy($column, $values = null)
    {
        $query = $this->getModel();

        if (empty($column)) {
            return $query->newCollection();
        }

        if (is_string($column)) {
            $method = is_array($values) ? 'whereIn' : 'where';

            return $query->{$method}($column, $values)->get();
        }

        foreach ($column as $k => $v) {
            $query = $query->where($k, '=', $v);
        }

        return $query->get();
    }

    /**
     * {@inheritdoc}
     */
    public function paginate($query = null): LengthAwarePaginator
    {
        $query = $query ?? $this->getQuery();

        return $query->paginate($this->perPage);
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByOrFail($column, $value = null)
    {
        if ($found = $this->findOneBy($column, $value)) {
            return $found;
        }

        throw (new ModelNotFoundException)->setModel(get_class($this->getModel()));
    }

    /**
     * Применение критериев.
     *
     * @param  AbstractModel  $model
     *
     * @return AbstractModel
     */
    protected function applyCriteria($model)
    {
        if ($this->skipCriteria) {
            return $model;
        }

        foreach ($this->getCriteria() as $criterion) {
            $model = $criterion->apply($model, $this);
        }

        return $model;
    }
}
