<?php

namespace App\Core\Eloquent\Repository\Contracts;

use App\Core\Eloquent\AbstractModel;

interface Criterion
{
    public function apply(/*AbstractModel*/ $model, Repository $repository);
}
