<?php

namespace App\Core\Eloquent\Repository\Contracts;

use App\Core\Eloquent\AbstractModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection as SupportCollection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

interface Repository
{
    /**
     * «Чистая» модель.
     *
     * @return AbstractModel
     */
    public function getModel();

    /**
     * Запрос с критериями, связями, сортировкой, фильтрами и прочим.
     *
     * @return Builder
     */
    public function getQuery(): Builder;

    /**
     * @return array
     */
    public function getSortableFields(): array;

    /**
     * @return array
     */
    public function getSearchableFields(): array;

    /**
     * Получение списка критериев.
     *
     * @return SupportCollection|Criterion[]
     */
    public function getCriteria();

    /**
     * Включить/выключить применение критериев к запросу.
     *
     * @param  bool  $skip
     *
     * @return $this
     */
    public function skipCriteria(bool $skip = true);

    /**
     * Сброс всех критериев.
     *
     * @return $this
     */
    public function resetCriteria();

    /**
     * Добавление критерия.
     *
     * @param  Criterion|string  $criterion
     *
     * @return $this
     */
    public function pushCriterion($criterion);

    /**
     * Удаление критерия.
     *
     * @param  Criterion|string  $criterion
     *
     * @return $this
     */
    public function pullCriterion($criterion);

    /**
     * @param  int  $value
     *
     * @return $this
     */
    public function setPerPage(int $value = 10);

    /**
     * @param  array|string  $relations
     *
     * @return $this
     */
    public function with($relations);

    /**
     * Поиск сущности по первичному ключу.
     *
     * @param  int|string  $id
     *
     * @return AbstractModel|null
     */
    public function findOne($id);

    /**
     * Получение сущности по первичному ключу.
     *
     * @param  int|string  $id
     *
     * @return AbstractModel
     *@throws ModelNotFoundException
     *
     */
    public function getById($id);

    /**
     * Поиск сущностей по первичным ключам.
     *
     * @param  array  $ids  массив первичных ключей
     *
     * @return EloquentCollection
     */
    public function findMany(array $ids);

    /**
     * Поиск одной сущности по любым полям.
     *
     * @param  string|array  $column  название поля или массив [поле => значение]
     * @param  mixed         $value   может быть null, если $column является массивом
     *
     * @return AbstractModel|null
     */
    public function findOneBy($column, $value = null);

    /**
     * Получение одной сущности по любым полям.
     *
     * @param  string|array  $column  название поля или массив [поле => значение]
     * @param  mixed         $value   может быть null, если $column является массивом
     *
     * @return AbstractModel
     *@throws ModelNotFoundException
     *
     */
    public function getOneBy($column, $value = null): AbstractModel;

    /**
     * Поиск сущностей по любым полям.
     *
     * @param  string|array  $column  название поля или массив [поле => значение]
     * @param  mixed         $values  может быть null, если $column является массивом
     *
     * @return EloquentCollection
     */
    public function findManyBy($column, $values = null);

    /**
     * Коллекция всех сущностей.
     *
     * @return EloquentCollection
     */
    public function all();

    /**
     * Пагинация запроса.
     *
     * @param  AbstractModel|Builder|null  $query
     *
     * @return LengthAwarePaginator
     */
    public function paginate($query = null): LengthAwarePaginator;

    /**
     * @param  string|array  $column
     * @param  null          $value
     *
     * @return AbstractModel|null
     */
    public function findOneByOrFail($column, $value = null);
}
