<?php

namespace App\Core\Eloquent\Repository\Filter;

use Carbon\Carbon;

class FilterParser
{
    protected const TYPE_DELIMITER   = ':';
    protected const TOKEN_DELIMITER  = '|';
    protected const CLAUSE_DELIMITER = ';';

    public function parse(?string $query): ?array
    {
        if (empty($query)) {
            return [];
        }

        $index = 0;
        $stack = [];
        $nestable = false;

        foreach ($this->getClauses($query) as $clause) {
            $tokenizedClause = $this->tokenize(trim($clause, '()'));

            if ('(' === $clause{0}) {
                $nestable = true;

                $stack[$index] = [$tokenizedClause];
            } elseif (')' === $clause{-1}) {
                $nestable = false;

                $stack[$index++][] = $tokenizedClause;
            } else {
                if ($nestable) {
                    $stack[$index][] = $tokenizedClause;
                } else {
                    $stack[$index++] = $tokenizedClause;
                }
            }
        }

        return $stack;
    }

    protected function getClauses(string $string): array
    {
        $clauses = explode(static::CLAUSE_DELIMITER, $string);

        return array_map('trim', $clauses);
    }

    protected function tokenize(string $clause): array
    {
        $relation = null;

        [$field, $operator, $value] = explode(static::TOKEN_DELIMITER, $clause);

        if (strpos($field, '.') > 0) {
            [$relation, $field] = explode('.', $field);
        }

        $value = $this->convertValue($value);

        if (is_array($value)) {
            $value = array_map([$this, 'convertValue'], $value);
        }

        return compact('relation', 'field', 'operator', 'value');
    }

    /**
     * @param  mixed  $value
     *
     * @return mixed
     */
    protected function convertValue($value)
    {
        if (false === strpos($value, self::TYPE_DELIMITER)) {
            if ('[' === $value{0} && ']' === $value{-1}) {
                $value = explode(',', trim($value, '[]'));
            }

            return $value;
        }

        [$type, $value] = explode(self::TYPE_DELIMITER, $value);

        switch ($type) {
            case 'date':
                $value = Carbon::parse($value);
        }

        return $value;
    }
}
