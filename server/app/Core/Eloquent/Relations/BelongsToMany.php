<?php

namespace App\Core\Eloquent\Relations;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany as IlluminateBelongsToMany;

class BelongsToMany extends IlluminateBelongsToMany
{
    /**
     * Attach a model to the parent.
     *
     * @param  EloquentCollection|array  $id
     * @param  array                     $attributes
     * @param  bool                      $touch
     */
    public function attach($id, array $attributes = [], $touch = true)
    {
        parent::attach($id, $attributes, $touch);

        $ids = $this->parseIds($id);

        $this->fireParentEvent("attached_{$this->relationName}", $ids, false);
    }

    /**
     * Detach models from the relationship.
     *
     * @param  mixed  $ids
     * @param  bool   $touch
     *
     * @return int
     */
    public function detach($ids = null, $touch = true)
    {
        $results = parent::detach($ids, $touch);

        $ids = $this->parseIds($ids);

        $this->fireParentEvent("detached_{$this->relationName}", $ids, false);

        return $results;
    }

    protected function fireParentEvent($event, $records, $halt = true)
    {
        if (! $dispatcher = $this->getParent()->getEventDispatcher()) {
            return true;
        }

        $method = $halt ? 'until' : 'fire';

        $event = "eloquent.{$event}: ".get_class($this->getParent());

        return $dispatcher->$method($event, [$this->getParent(), $records]);
    }
}

