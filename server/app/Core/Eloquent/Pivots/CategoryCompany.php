<?php

namespace App\Core\Eloquent\Pivots;

use App\Domain\Catalog\Category\Category;
use App\Domain\Company\Company;
use App\Domain\Product\Product;
use Illuminate\Database\Eloquent\Relations\Pivot;

class CategoryCompany extends Pivot {

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function categoryCompany()
    {
        return $this->hasManyThrough(Product::class, Company::class);
    }

}