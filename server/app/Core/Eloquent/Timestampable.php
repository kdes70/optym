<?php

namespace App\Core\Eloquent;

use Carbon\Carbon;

/**
 * Trait Timestampable
 */
trait Timestampable
{
    /**
     * Дата и время создания.
     *
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->getAttribute('created_at');
    }

    /**
     * Дата и время обновления.
     *
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->getAttribute('updated_at');
    }
}
