<?php

namespace App\Core\Eloquent;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Core\Eloquent\AbstractModel
 *
 * @mixin Eloquent
 */
abstract class AbstractModel extends Model
{
    /**
     * @return string
     */
    public static function getTableName(): string
    {
        return (new static)->getTable();
    }
}
