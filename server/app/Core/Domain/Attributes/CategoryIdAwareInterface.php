<?php

namespace App\Core\Domain\Attributes;

/**
 * Интерфейс CategoryIdAwareInterface
 *
 * @package App\Core\Domain\Attributes;
 */
interface CategoryIdAwareInterface
{
    /**
     * Получение id категории сущности.
     *
     * @return int
     */
    public function getCategoryId(): int;

    /**
     * Установка id категории сущности.
     *
     * @param  int $category_id
     *
     * @param bool $required
     * @return $this
     */
    public function setCategoryId(int $category_id, bool $required = false);
}
