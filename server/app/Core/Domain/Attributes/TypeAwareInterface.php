<?php

namespace App\Core\Domain\Attributes;

/**
 * Interface TypeAwareInterface
 *
 * @package App\Core\Domain\Attributes;
 */
interface TypeAwareInterface
{
    /**
     * Получение типа сущности.
     *
     * @return string|null
     */
    public function getType(): ?string;

    /**
     * Установка типа сущности.
     *
     * @param  string  $name
     *
     * @return $this
     */
    public function setType(string $name);


    /**
     * Получение списока типов
     *
     * @return array|null
     */
    static public function getListType(): ?array;
}
