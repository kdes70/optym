<?php

namespace App\Core\Domain\Attributes;

use App\Domain\Product\Contracts\UnitInterface;

/**
 * Интерфейс UnitAwareInterface
 *
 * @package App\Core\Domain\Attributes;
 */
interface UnitAwareInterface
{
    /**
     * Получение единицы измерения сущности.
     *
     * @return UnitInterface
     */
    public function getUnit(): UnitInterface;

    /**
     * Установка единицы измерения сущности.
     *
     * @param  UnitInterface|null  $unit
     *
     * @return $this
     */
    public function setUnit(?UnitInterface $unit);
}
