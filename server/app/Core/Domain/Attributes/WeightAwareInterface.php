<?php

namespace App\Core\Domain\Attributes;

/**
 * Интерфейс WeightAwareInterface
 *
 * @package App\Core\Domain\Attributes;
 */
interface WeightAwareInterface
{
    /**
     * Получение веса сущности.
     *
     * @return int
     */
    public function getWeight(): int;

    /**
     * Установка веса сущности.
     *
     * @param  int  $weight
     *
     * @return $this
     */
    public function setWeight(int $weight);
}
