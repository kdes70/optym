<?php

namespace App\Core\Domain\Attributes;

/**
 * Interface EmailAwareInterface
 */
interface EmailAwareInterface
{
    /**
     * Получение email сущности.
     *
     * @return string|null
     */
    public function getEmail(): ?string;

    /**
     * Установка email сущности.
     *
     * @param string $email
     *
     * @return $this|static
     */
    public function setEmail(string $email): EmailAwareInterface;
}
