<?php

namespace App\Core\Domain\Attributes;

/**
 * Interface TitleAwareInterface
 *
 * @package App\Core\Domain\Attributes
 */
interface TitleAwareInterface
{
    /**
     * Получение наименования сущности.
     *
     * @return string|null
     */
    public function getTitle(): ?string;

    /**
     * Установка наименования сущности.
     *
     * @param  string  $title
     *
     * @return $this
     */
    public function setTitle(string $title);
}
