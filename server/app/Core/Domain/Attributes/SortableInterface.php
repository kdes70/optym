<?php

namespace App\Core\Domain\Attributes;

interface SortableInterface
{

    /**
     * Получить сортировку сущьности
     *
     * @return mixed
     */
    public function getSort(): int ;

    /**
     * Задать сортировку сущьности
     *
     * @param int $sort
     * @return $this
     */
    public function setSort(int $sort);


//    /**
//     * Переместите сушьность на заданное количество позиций.
//     *
//     * @param int $amount
//     * @return $this
//     */
//    public function up(int $amount = 1);
//
//    /**
//     * Переместите сушьность вниз на заданное количество позиций.
//     *
//     * @param int $amount
//     * @return $this
//     */
//    public function down(int $amount = 1);
}
