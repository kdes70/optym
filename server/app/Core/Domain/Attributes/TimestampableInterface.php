<?php

namespace App\Core\Domain\Attributes;

use Carbon\Carbon;

/**
 * Interface TimestampableInterface
 */
interface TimestampableInterface
{
    /**
     * Дата и время создания.
     *
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon;

    /**
     * Дата и время обновления компании в сервисе.
     *
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon;
}
