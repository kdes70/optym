<?php

namespace App\Core\Domain\Attributes;

/**
 * Interface ImageAwareInterface
 *
 * @package App\Core\Domain\Attributes;
 */
interface ImageAwareInterface
{
    /**
     * Получение картинки сущности.
     *
     * @return string|null
     */
    public function getImage(): ?string;

    /**
     * Установка картинки сущности.
     *
     * @param string $image
     * @return $this
     */
    public function setImage(string $image);


    /**
     * Получение url картинки.
     *
     * @return string|null
     */
    public function getImagePath(): ?string;
}
