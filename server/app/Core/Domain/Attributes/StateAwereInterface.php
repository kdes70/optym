<?php

namespace App\Core\Domain\Attributes;

interface StateAwereInterface
{

    const STATE_DRAFT = 'draft';
    const STATE_ARCHIVE = 'archive';
    const STATE_AVAILABLE = 'available';


    /**
     * Получение статуса сущности.
     *
     * @return string
     */
    public function getState(): string;

    /**
     * Установка статуса сущности
     *
     * @param $state
     * @return mixed
     */
    public function setState(string $state);


    /**
     * Сущность черновик ?
     *
     * @return bool
     */
    public function isDraft(): bool;

    /**
     * Сущность доступна ?
     *
     * @return bool
     */
    public function isAvailable(): bool;


    /**
     * Сущность в архиве ?
     *
     * @return bool
     */
    public function isArchived(): bool;


}
