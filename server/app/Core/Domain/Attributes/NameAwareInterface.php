<?php

namespace App\Core\Domain\Attributes;

/**
 * Interface NameAwareInterface
 *
 * @package App\Core\Domain\Attributes;
 */
interface NameAwareInterface
{
    /**
     * Получение наименования сущности.
     *
     * @return string|null
     */
    public function getName(): ?string;

    /**
     * Установка наименования сущности.
     *
     * @param  string  $name
     *
     * @return $this
     */
    public function setName(string $name);
}
