<?php

namespace App\Core\Domain\Attributes;

/**
 * Interface HasIdentityInterface
 */
interface HasIdentityInterface
{
    /**
     * Получение идентификатора сущности.
     *
     * @return int
     */
    public function getId(): int;
}
