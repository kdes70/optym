<?php

namespace App\Core\Domain\Attributes;

/**
 * Interface PhoneAwareInterface
 */
interface PhoneAwareInterface
{
    /**
     * Получение телефона сущности.
     *
     * @return string|null
     */
    public function getPhone(): ?string;

    /**
     * Установка телефона сущности.
     *
     * @param  string  $phone
     *
     * @return $this|static
     */
    public function setPhone(string $phone);
}
