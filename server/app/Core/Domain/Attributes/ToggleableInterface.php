<?php

namespace App\Core\Domain\Attributes;

interface ToggleableInterface
{
    /**
     * Сделать сущность доступной для использования.
     *
     * @param  bool|int  $enabled
     *
     * @return $this
     */
    public function setEnabled($enabled);

    /**
     * Доступна ли сущность для использования?
     *
     * @return bool
     */
    public function isEnabled(): bool;

    /**
     * Сделать сущность доступной для использования.
     *
     * @return $this
     */
    public function enable();

    /**
     * Сделать сущность недоступной для использования.
     *
     * @return $this
     */
    public function disable();
}
