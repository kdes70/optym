<?php

namespace App\Core\Domain\Attributes;

/**
 * Interface SlugAwareInterface
 *
 * @package App\Core\Domain\Attributes
 */
interface SlugAwareInterface
{
    /**
     * Получение slug сущности.
     *
     * @return string|null
     */
    public function getSlug(): ?string;

    /**
     * Установка slug сущности.
     *
     * @param  string  $slug
     *
     * @return $this
     */
    public function setSlug(string $slug);
}
