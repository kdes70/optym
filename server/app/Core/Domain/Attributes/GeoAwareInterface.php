<?php

namespace App\Core\Domain\Attributes;

/**
 * Интерфейс GeoAwareInterface
 *
 * @package App\Core\Domain\Attributes\Attributes;
 */
interface GeoAwareInterface
{
    /**
     * Получение страны
     *
     * @return string
     */
    public function getCountry(): string;


    /**
     * @param string|null $country
     *
     * @return GeoAwareInterface
     */
    public function setCountry(?string $country): GeoAwareInterface;

    /**
     * Установка региона
     *
     * @param string|null $region
     *
     * @return GeoAwareInterface
     */
    public function setRegion(?string $region): GeoAwareInterface;

    /**
     * Получение региона
     *
     * @return string
     */
    public function getRegion(): string;

    /**
     * Установка города
     *
     * @param string|null $city
     *
     * @return GeoAwareInterface
     */
    public function setCity(?string $city): GeoAwareInterface;

    /**
     * Получение города
     *
     * @return string
     */
    public function getCity(): string;
}
