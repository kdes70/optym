<?php

namespace App\Core\Domain\Attributes\Traits;

use App\Core\Domain\Attributes\GeoAwareInterface;

/**
 * Trait GeoAware
 *
 * @package App\Core\Domain\Traits;
 */
trait GeoAware
{
    /**
     * Получение страны
     *
     * @return string
     */
    public function getCountry(): string
    {
        return (string)$this->getAttributeValue('country');
    }

    /**
     * Установка страны
     *
     * @param string|null $country
     *
     * @return GeoAwareInterface
     */
    public function setCountry(?string $country): GeoAwareInterface
    {
        $this->setAttribute('country', $country);

        return $this;
    }

    /**
     * Получение региона
     *
     * @return string
     */
    public function getRegion(): string
    {
        return (string)$this->getAttributeValue('region');
    }

    /**
     * Установка региона
     *
     * @param string $region
     *
     * @return GeoAwareInterface
     */
    public function setRegion(?string $region): GeoAwareInterface
    {
        $this->setAttribute('region', $region);

        return $this;
    }

    /**
     * Получение города
     *
     * @return string
     */
    public function getCity(): string
    {
        return (string)$this->getAttributeValue('city');
    }

    /**
     * Установка города
     *
     * @param string|null $city
     *
     * @return GeoAwareInterface
     */
    public function setCity(?string $city): GeoAwareInterface
    {
        $this->setAttribute('city', $city);

        return $this;
    }
}
