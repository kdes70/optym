<?php

namespace App\Core\Domain\Attributes\Traits;

/**
 * Trait PhoneAware
 */
trait PhoneAware
{
    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return (string)$this->getAttributeValue('phone');
    }

    /**
     * @param string|null $phone
     *
     * @return $this|static
     */
    public function setPhone(?string $phone)
    {
        if ($phone) {
            $this->setAttribute('phone', clear_phone($phone));
        }

        $this->setAttribute('phone', null);

        return $this;
    }
}
