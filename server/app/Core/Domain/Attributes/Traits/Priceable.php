<?php

namespace App\Core\Domain\Attributes\Traits;


use App\Exceptions\InvalidAmountException;

/**
 * Трейт Priceable
 *
 * @package  App\Core\Domain\Attributes\Traits;
 */
trait Priceable
{
    /**
     * {@inheritdoc}
     */
    public function getCostPrice(): float
    {
        return (float) $this->getAttributeValue('cost_price');
    }

    /**
     * {@inheritdoc}
     */
    public function setCostPrice(float $costPrice)
    {
        if (0 >= $costPrice) {
            throw new InvalidAmountException('Закупочная цена должна быть больше 0.');
        }

        $this->setAttribute('cost_price', $costPrice);

        return $this;
    }


    public function getFormatCostPrice()
    {
        return $this->getCostPrice(). ' ₽';
    }

    /**
     * {@inheritdoc}
     */
    public function getRetailPrice(): float
    {
        return (float) $this->getAttributeValue('retail_price');
    }

    /**
     * {@inheritdoc}
     */
    public function setRetailPrice(float $retailPrice)
    {
        if (0 >= $retailPrice) {
            throw new InvalidAmountException('Розничная цена должна быть больше 0.');
        }

        $this->setAttribute('retail_price', $retailPrice);

        return $this;
    }
}
