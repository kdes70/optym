<?php

namespace App\Core\Domain\Attributes\Traits;



use App\Exceptions\InvalidAmountException;

/**
 * Trait WeightAware
 *
 * @package App\Core\Domain\Attributes\Traits;
 */
trait WeightAware
{
    /**
     * Получение веса в граммах.
     *
     * @return int
     */
    public function getWeight(): int
    {
        return (int) $this->getAttributeValue('weight');
    }

    /**
     * Установка веса в граммах.
     *
     * @param $weight
     *
     * @return $this
     */
    public function setWeight(int $weight)
    {
        if (0 >= $weight) {
            throw new InvalidAmountException('Вес должен быть больше 0.');
        }

        $this->setAttribute('weight', $weight);

        return $this;
    }
}
