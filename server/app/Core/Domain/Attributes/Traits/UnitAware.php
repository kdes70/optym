<?php

namespace App\Core\Domain\Attributes\Traits;

use App\Core\Eloquent\AbstractModel;
use App\Domain\Product\Contracts\UnitInterface;
use App\Domain\Product\Unit;

/**
 * Trait UnitAware Ленивая загрузка.
 *
 * @package App\Core\Domain\Attributes\Traits;
 */
trait UnitAware
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    /**
     * Получение единицы измерения сущности.
     *
     * @return UnitInterface
     */
    public function getUnit(): UnitInterface
    {
        return $this->getRelationValue('unit');
    }

    /**
     * Установка единицы измерения сущности.
     *
     * @param  UnitInterface|null $unit
     *
     * @return $this
     */
    public function setUnit(?UnitInterface $unit)
    {
        if (null === $unit) {
            $this->unit()->dissociate();

            return $this;
        }

        /** @var AbstractModel $unit */
        $this->unit()->associate($unit);

        return $this;
    }
}
