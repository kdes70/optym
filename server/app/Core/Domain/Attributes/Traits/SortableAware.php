<?php

namespace App\Core\Domain\Attributes\Traits;

/**
 * Trait TypeAware
 *
 * @package App\Core\Domain\Traits;
 */
trait SortableAware
{

    /**
     * Получить сортировку сущьности
     *
     * @return mixed
     */
    public function getSort(): int
    {
        return $this->getAttributeValue('sort');
    }

    /**
     * Задать сортировку сущьности
     *
     * @param int $sort
     * @return $this
     */
    public function setSort(int $sort)
    {
        $this->setAttribute('sort', $sort);

        return $this;
    }

}
