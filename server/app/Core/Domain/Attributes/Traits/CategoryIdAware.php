<?php

namespace App\Core\Domain\Attributes\Traits;

use App\Exceptions\InvalidAmountException;

/**
 * Trait CategoryIdAware
 *
 * @package App\Core\Domain\Attributes\Traits;
 */
trait CategoryIdAware
{


    /**
     * Получение id категории сущности.
     *
     * @return int
     */
    public function getCategoryId(): int
    {
        return (int)$this->getAttributeValue('category_id');
    }

    /**
     * Установка id категории сущности.
     *
     * @param  int $category_id
     *
     * @param bool $required
     * @return $this
     */
    public function setCategoryId(int $category_id, bool $required = false)
    {

        if ($required === true && 0 >= $category_id) {
            throw new InvalidAmountException('Категория не выбрана!');
        }

        $this->setAttribute('category_id', $category_id);

        return $this;
    }


}
