<?php

namespace App\Core\Domain\Attributes\Traits;

/**
 * Trait TypeAware
 *
 * @package App\Core\Domain\Traits;
 */
trait TypeAware
{

    /**
     * {@inheritdoc}
     */
    public function getType(): ?string
    {
        return $this->getAttributeValue('type');
    }

    /**
     * {@inheritdoc}
     */
    public function setType($type)
    {
        $this->setAttribute('type', $type);

        return $this;
    }


}
