<?php

namespace App\Core\Domain\Attributes\Traits;

/**
 * Trait SlugAware
 *
 * @package App\Core\Domain\Traits;
 */
trait SlugAware
{
    /**
     * {@inheritdoc}
     */
    public function getSlug(): ?string
    {
        return $this->getAttributeValue('slug');
    }

    /**
     * {@inheritdoc}
     */
    public function setSlug(string $slug)
    {
        $this->setAttribute('slug', str_slug($slug));

        return $this;
    }
}
