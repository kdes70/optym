<?php

namespace App\Core\Domain\Attributes\Traits;

/**
 * Trait HasIdentity
 */
trait HasIdentity
{
    /**
     * Получение идентификатора сущности.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->getKey();
    }
}
