<?php

namespace App\Core\Domain\Attributes\Traits;

/**
 * Trait TitleAware
 *
 * @package App\Core\Domain\Traits;
 */
trait TitleAware
{
    /**
     * {@inheritdoc}
     */
    public function getTitle(): ?string
    {
        return $this->getAttributeValue('title');
    }

    /**
     * {@inheritdoc}
     */
    public function setTitle(string $title)
    {
        $this->setAttribute('title', trim($title));

        return $this;
    }
}
