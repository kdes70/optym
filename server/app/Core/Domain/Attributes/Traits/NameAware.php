<?php

namespace App\Core\Domain\Attributes\Traits;

/**
 * Trait NameAware
 *
 * @package App\Core\Domain\Traits;
 */
trait NameAware
{
    /**
     * {@inheritdoc}
     */
    public function getName(): ?string
    {
        return $this->getAttributeValue('name');
    }

    /**
     * {@inheritdoc}
     */
    public function setName(string $name)
    {
        $this->setAttribute('name', $name);

        return $this;
    }
}
