<?php

namespace App\Core\Domain\Attributes\Traits;

/**
 * Trait StateAware
 *
 * @package App\Core\Domain\Traits;
 */
trait StateAware
{

    /**
     * Получение статуса сущности.
     *
     * @return string
     */
    public function getState(): string
    {
        return $this->getAttributeValue('state');
    }

    /**
     * Установка статуса сущности
     *
     * @param $state
     * @return mixed
     */
    public function setState(string $state)
    {
        $this->setAttribute('state', $state);

        return $this;
    }


    /**
     * Сущность черновик ?
     *
     * @return bool
     */
    public function isDraft(): bool
    {
        return $this->getState() === self::STATE_DRAFT;
    }

    /**
     * Сущность доступна ?
     *
     * @return bool
     */
    public function isAvailable(): bool
    {
        return $this->getState() === self::STATE_AVAILABLE;
    }


    /**
     * Сущность в архиве ?
     *
     * @return bool
     */
    public function isArchived(): bool
    {
        return $this->getState() === self::STATE_ARCHIVE;
    }


}
