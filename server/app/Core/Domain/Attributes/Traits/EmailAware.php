<?php

namespace App\Core\Domain\Attributes\Traits;

use App\Core\Domain\Attributes\EmailAwareInterface;

/**
 * Trait EmailAware
 *
 * @package App\Core\Domain\Traits;
 */
trait EmailAware
{
    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return (string)$this->getAttributeValue('email');
    }

    /**
     * @param string $email
     *
     * @return EmailAwareInterface|static
     */
    public function setEmail(string $email): EmailAwareInterface
    {
        $this->setAttribute('email', $email);

        return $this;
    }
}
