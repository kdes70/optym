<?php

namespace App\Core\Domain\Attributes\Traits;

/**
 * Trait ImageAware
 *
 * @package App\Core\Domain\Traits;
 */
trait ImageAware
{
    /**
     * Получение картинки сущности.
     *
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->getAttributeValue('image');
    }

    /**
     * Установка картинки сущности.
     *
     * @param string $image
     * @return $this
     */
    public function setImage(string $image)
    {
        $this->setAttribute('image', $image);

        return $this;
    }


    /**
     * Получение url картинки.
     *
     * @return string|null
     */
    public function getImagePath(): ?string
    {
        return $this->getImage();
    }
}
