<?php

namespace App\Core\Domain\Attributes\Traits;

use Illuminate\Database\Eloquent\Builder;

/**
 * Trait Toggleable
 *
 * @method  Builder  enabled()
 * @method  Builder  disabled()
 *
 * @package Hm\Domain\Core
 */
trait Toggleable
{
    /**
     * {@inheritdoc}
     */
    public function isEnabled(): bool
    {
        return $this->getAttributeValue('enabled');
    }

    /**
     * {@inheritdoc}
     */
    public function setEnabled($enabled)
    {
        $this->setAttribute('enabled', (bool) $enabled);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function enable()
    {
        return $this->setAttribute('enabled', true);
    }

    /**
     * {@inheritdoc}
     */
    public function disable()
    {
        return $this->setAttribute('enabled', false);
    }

    public function scopeEnabled(Builder $query)
    {
        return $query->where('enabled', true);
    }

    public function scopeDisabled(Builder $query)
    {
        return $query->where('enabled', false);
    }
}
