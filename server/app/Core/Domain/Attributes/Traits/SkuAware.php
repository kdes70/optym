<?php

namespace App\Core\Domain\Attributes\Traits;

/**
 * Trait SkuAware
 *
 * @package App\Core\Domain\Attributes\Traits;
 */
trait SkuAware
{
    /**
     * {@inheritdoc}
     */
    public function getSku(): ?string
    {
        return $this->getAttributeValue('sku');
    }

    /**
     * {@inheritdoc}
     */
    public function setSku(string $sku = null)
    {
        if (! empty($sku) && $sku !== $this->getSku()) {
            // TODO: проверяем SKU на уникальность
            $this->setAttribute('sku', strtoupper($sku));
        }

        return $this;
    }
}
