<?php

namespace App\Core\Domain\Attributes;

interface CodeAwareInterface
{
    /**
     * Получение кода сущности.
     *
     * @return string
     */
    public function getCode(): string;

    /**
     * Установка кода сущности.
     *
     * @param string $code
     */
    public function setCode(string $code);
}
