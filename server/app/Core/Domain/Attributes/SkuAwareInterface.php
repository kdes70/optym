<?php

namespace App\Core\Domain\Attributes;

interface SkuAwareInterface
{
    /**
     * Получение SKU сущности.
     *
     * @return string|null
     */
    public function getSku(): ?string;

    /**
     * Установка SKU сущности.
     *
     * @param  string  $sku
     *
     * @return $this
     */
    public function setSku(string $sku = null);
}
