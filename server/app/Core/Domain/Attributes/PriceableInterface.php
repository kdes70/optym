<?php

namespace App\Core\Domain\Attributes;

/**
 * Интерфейс PriceableInterface
 *
 * @package  App\Core\Domain\Attributes;
 */
interface PriceableInterface
{
    /**
     * Получение закупочной цены.
     *
     * @return float
     */
    public function getCostPrice(): float ;

    /**
     * Установка закупочной цены.
     *
     * @param  float  $costPrice
     *
     * @return $this
     */
    public function setCostPrice(float $costPrice);

    /**
     * Получение розничной цены в рублях.
     *
     * @return float
     */
    public function getRetailPrice(): float;

    /**
     * Установка розничной цены в рублях.
     *
     * @param  float  $retailPrice
     *
     * @return $this
     */
    public function setRetailPrice(float $retailPrice);
}
