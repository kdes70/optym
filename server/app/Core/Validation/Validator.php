<?php

namespace App\Core\Validation;

use Illuminate\Routing\Route;
use Illuminate\Database\Query\Builder;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Validation\Validator as IlluminateValidator;
use Symfony\Component\Routing\Route as SymfonyRoute;

class Validator extends IlluminateValidator
{
    const PHONE_REGEX = '~^\+?([87](?!95[4-79]|99[08]|907|94[^0]|336)([348]\d|9[0-6789]|7[0247])\d{8}|[1246]\d{9,13}|68\d{7}|5[1-46-9]\d{8,12}|55[1-9]\d{9}|55[12]19\d{8}|500[56]\d{4}|5016\d{6}|5068\d{7}|502[45]\d{7}|5037\d{7}|50[4567]\d{8}|50855\d{4}|509[34]\d{7}|376\d{6}|855\d{8}|856\d{10}|85[0-4789]\d{8,10}|8[68]\d{10,11}|8[14]\d{10}|82\d{9,10}|852\d{8}|90\d{10}|96(0[79]|17[01]|13)\d{6}|96[23]\d{9}|964\d{10}|96(5[69]|89)\d{7}|96(65|77)\d{8}|92[023]\d{9}|91[1879]\d{9}|9[34]7\d{8}|959\d{7}|989\d{9}|97\d{8,12}|99[^4568]\d{7,11}|994\d{9}|9955\d{8}|996[57]\d{8}|9989\d{8}|380[3-79]\d{8}|381\d{9}|385\d{8,9}|375[234]\d{8}|372\d{7,8}|37[0-4]\d{8}|37[6-9]\d{7,11}|30[69]\d{9}|34[67]\d{8}|3[12359]\d{8,12}|36\d{9}|38[1679]\d{8}|382\d{8,9}|46719\d{10})$~';

    protected $routeCollection;

    public function __construct(Translator $translator, $data, $rules, $messages, $customAttributes)
    {
        $messages = array_merge($messages, [
            'kd_price'    => 'Цена должна быть больше 0.',
            'kd_exists'   => 'Недопустимое сочетание.',
            'kd_username' => 'Допустимы только буквы латинского алфавита, цифры, символ подчеркивания, тире и точка.',
            'kd_phone'    => 'Поле :attribute должно быть телефоном.',
            'kd_url'      => trans('validation.active_url'),
        ]);

        parent::__construct($translator, $data, $rules, $messages, $customAttributes);
    }

    /**
     * Имя пользователя из латинских букв, цифр, подчёркивания, дефиса и точки.
     *
     * 'username' => 'required|username'
     *
     * @param  string  $attribute
     * @param  string  $value
     *
     * @return bool
     */
    public function validateUsername($attribute, $value)
    {
        return preg_match('~^[a-z\d\-\_\.]*$~i', $value);
    }

    /**
     * Цена, целое/дробное с точкой: 5, 10, 7.00, 4.36.
     *
     * 'price' => 'required|price'
     *
     * @param  string  $attribute
     * @param  string  $value
     *
     * @return bool
     */
    public function validateHmPrice($attribute, $value)
    {
        return preg_match('~^\d+(?:\.\d{1,2})?$~', $value);// && (float) $value > 0;
    }

    /**
     * Проверка сочетаний.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @param  array $params
     *
     * @return bool
     */
    public function validateHmExists($attribute, $value, $params)
    {
        /** @var Builder $query */
        $query = \DB::table(array_shift($params));

        foreach ($params as $param) {
            $param = explode('@', $param);

            [$sourceField, $dbField] = $param;

            $current = $value[$sourceField];

            $query->where($dbField, $current);
        }

        return $query->exists();
    }

    protected function validateHmRoute($attribute, $value)
    {
        $value = parse_url($value, PHP_URL_PATH);

        return !empty(array_first($this->getRouteCollection(), function (Route $route) use ($value) {
            $uri = preg_replace('~\{(\w+?)\??\}~', '{$1}', $route->uri());

            preg_match_all('~\{(\w+?)\?\}~', $route->uri(), $matches);

            $optionals = isset($matches[1]) ? array_fill_keys($matches[1], null) : [];

            $compiled = (new SymfonyRoute($uri, $optionals))->compile();

            return preg_match($compiled->getRegex(), $value);
        }));
    }

    protected function validateHmUrlAnchor($attribute, $value)
    {
        return preg_match('~^#[\w\-]+$~', $value);
    }

    protected function validateHmUrl($attribute, $value)
    {
        /** TODO: поместить в соответствии с частотой использования */
        return $this->validateHmUrlAnchor($attribute, $value) ||
            $this->validateUrl($attribute, $value) ||
            $this->validateHmRoute($attribute, $value);
    }

    protected function validateHmTrackCode($attribute, $value)
    {
        // TODO: выпилить этот костыль
        $russianPostInternational = '~^[RCEVL]{1}[A-Z]{1}\d{9}[A-Z]{2}$~';
        $russianPostDomestic = '~^\d{14}$~';
        $cdek = '~^\d{7}$~';

        return
            preg_match($russianPostDomestic, strtoupper($value)) ||
            preg_match($russianPostInternational, $value) ||
            preg_match($cdek, $value);
    }

    protected function validateHmPhone($attribute, $value)
    {
        $value = clear_phone($value);

        return strlen($value) <= 32 && preg_match(self::PHONE_REGEX, $value);
    }

    protected function getRouteCollection(): array
    {
        if (! $this->routeCollection) {
            $this->routeCollection = app('router')->getRoutes()->get('GET');
        }

        return $this->routeCollection;
    }
}
