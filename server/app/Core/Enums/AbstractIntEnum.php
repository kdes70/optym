<?php

namespace App\Core\Enums;

/**
 * Class AbstractIntEnum
 */
abstract class AbstractIntEnum extends AbstractEnums
{
    /**
     * AbstractIntEnum constructor.
     *
     * @param $value
     */
    public function __construct($value)
    {
        if (!empty($value) && !is_object($value) && is_numeric($value)) {
            $value = intval($value);
        }

        parent::__construct($value);
    }

    /**
     * @param $value
     *
     * @return bool
     */
    public static function isValid($value): bool
    {
        if (!empty($value) && !is_object($value) && is_numeric($value)) {
            $value = intval($value);
        }

        return parent::isValid($value);
    }
}
