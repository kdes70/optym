<?php

namespace App\Core\Enums;

use Illuminate\Support\Collection;
use MadWeb\Enum\Enum;

/**
 * Class AbstractEnums
 */
abstract class AbstractEnums extends Enum
{
    /**
     * @param array|null $only
     *
     * @return array
     */
    public static function toArrayLabels(?array $only = null): array
    {
        $arrays = (is_null($only)) ? static::toArray() : static::only($only);

        $item = [];

        foreach ($arrays as $value) {
            $value = new static($value);
            $item[(int)$value->getValue()] = $value->label();
        }

        return $item;
    }

    /**
     * @param array $keys
     *
     * @return Collection
     */
    public static function only(array $keys): Collection
    {
        $item = collect();
        foreach ($keys as $value) {
            if ($res = static::search($value)) {
                $item->push($value);
            }
        }
        return $item;
    }
}
