<?php

namespace App\Providers;

use App\Http\ViewComposers\MenuCatalogComposer;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

//        View::composer(
//            'cabinet.partials._nav-block', 'App\Http\ViewComposers\Cabinet\CompanyNavComposer'
//        );


        if (config('optum.frontend_type') === 'web') {
            View::composer(['profiles.*'], function ($view) {
                $view->with('user', auth()->user());
            });

            //TODO подключить каталог !
            View::composer(['layouts.app', 'welcome'], MenuCatalogComposer::class);

            View::composer('layouts.partials.breadcrumbs', function ($view) {

                $breadcrumbs = app()->make('app.breadcrumbs');
                $breadcrumbs->prepend('Home', route('home'));
                $view->with('breadcrumbs', $breadcrumbs->all());
            });

            View::composer('layouts.partials.admin.breadcrumbs', function ($view) {

                $breadcrumbs_admin = app()->make('app.breadcrumbs');
                $breadcrumbs_admin->prepend('Dashboards', route('admin.dashboard'));
                $view->with('breadcrumbs_admin', $breadcrumbs_admin->all());
            });

        }


//        View::composer('cabinet.partials._nav-block', function ($view) {
//            $view->with('company', auth()->user()->company()->first());
//        });


        //$breadcrumbs->prepend(trans('main.main_page'), '/');


    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
