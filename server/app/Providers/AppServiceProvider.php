<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @param UrlGenerator $url
     * @return void
     */
    public function boot(UrlGenerator $url)
    {
        if (env('REDIRECT_HTTPS')) {
            $url->formatScheme('https');
        }

        $url->forceScheme('https');

        Blade::if('frontend', function ($type) {
            return config('optum.frontend_type') === $type;
        });

//        $this->app->singleton(Factory::class, function ( $app) {
//
//            return new ParserManager($app, config('parser.parsers'));
//        });


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    public function provides()
    {
        // return [Factory::class];
    }
}
