<?php

namespace App\Services;

use App\Domain\User\DTO\CreateUser;
use App\Domain\User\DTO\RegisterUser;
use App\Domain\User\DTO\Update;
use App\Domain\User\Enums\UserStateEnum;
use App\Domain\User\Role;
use App\Domain\User\User;
use App\Repositories\User\ProfileRepository;
use App\Repositories\User\UserRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * Class UserService
 */
class UserService
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ProfileRepository
     */
    private $profileRepository;

    /**
     * UserService constructor.
     * @param UserRepository $userRepository
     * @param ProfileRepository $profileRepository
     */
    public function __construct(UserRepository $userRepository, ProfileRepository $profileRepository)
    {
        $this->userRepository = $userRepository;
        $this->profileRepository = $profileRepository;
    }

    /**
     * @param RegisterUser $dto
     *
     * @return User
     *
     * @throws \Exception
     */
    public function register(RegisterUser $dto): User
    {
        $user = $this->userRepository->getModel();

        $user->fill($dto->collection()->all());

        $this->registerFields($user, $dto->collection());

        return $user;
    }


    public function create(CreateUser $dto)
    {
        $user = $this->userRepository->getModel();

        $this->fill($user, $dto->collection());

        $user->save();

        return $user->fresh();
    }

    /**
     * @param $username
     * @param $email
     * @return User
     * @throws \Exception
     */
    public function new($username, $email): User
    {
        $user = $this->userRepository->getModel();

        $user->setUsername($username, true);
        $user->setEmail($email);
        $user->setPassword(Str::random());
        $user->setRole(new Role(Role::USER));
        $user->setState(User::STATE_ACTIVE);
        $user->setType(User::TYPE_USER);

        $user->save();

        return $user->fresh();
    }

    public function update(User $user, Update $dto)
    {
        $this->fill($user, $dto->collection());

        $user->save();

        return $user->fresh();
    }


//    /**
//     * @param User $user
//     * @param Collection $parameters
//     * @return User
//     * @throws \Exception
//     */
//    protected function fill(User $user, Collection $parameters)
//    {
//        $user->fill($parameters->all());
//
//        $this->fillSpecialFields($user, $parameters);
//
//        return $user;
//    }


    /**
     * @param User $user
     * @param Collection $parameters
     *
     * @return $this
     *
     * @throws \Exception
     */
    public function registerFields(User $user, Collection $parameters): self
    {
        $this
            ->setUsername($user, $parameters)
            ->setState($user, $parameters)
            ->setPassword($user, $parameters)
            ->setPhone($user, $parameters)
            ->setType($user, $parameters)
            ->setRole($user, $parameters)
            ->setIpAddress($user, $parameters)
            ->setVerifyToken($user);

        return $this;
    }

    /**
     * @param User $user
     * @param Collection $parameters
     * @return $this
     * @throws \Exception
     */
    protected function fillSpecialFields(User $user, Collection $parameters)
    {
        $this
            ->setState($user, $parameters)
            ->setPassword($user, $parameters)
            ->setPhone($user, $parameters)
            ->setUsername($user, $parameters)
            ->setType($user, $parameters)
            ->setRole($user, $parameters)
            ->setVerifyToken($user, $parameters);

//         if ($parameters->has('options')) {
//             $this->setOptions($user, $parameters->get('options'));
//         }

        return $this;
    }

    /**
     * @param User $user
     * @param Collection $parameters
     * @return $this
     */
    protected function setType(User $user, Collection $parameters)
    {
        if ($parameters->has('type')) {
            $user->setType(intval($parameters->get('type')));
        }

        return $this;
    }

    /**
     * @param User $user
     * @param Collection $parameters
     * @return $this
     */
    protected function setIpAddress(User $user, Collection $parameters)
    {
        if ($parameters->has('ip_address')) {
            $user->setIpAddress($parameters->get('ip_address'));
        }

        return $this;
    }


    /**
     * @param User $user
     * @return $this
     */
    protected function setVerifyToken(User $user)
    {
        $user->setVerifyToken(Str::uuid());

        return $this;
    }

    /**
     * @param User $user
     * @param Collection $parameters
     * @return $this
     */
    protected function setState(User $user, Collection $parameters)
    {
        if ($parameters->has('state')) {
            $user->setState(new UserStateEnum($parameters->get('state')));
        }

        return $this;
    }

    /**
     * @param User $user
     * @param Collection $parameters
     * @return $this
     */
    protected function setPassword(User $user, Collection $parameters)
    {
        if ($parameters->has('password')) {
            $user->setPassword($parameters->get('password'));
        }

        return $this;
    }

    /**
     * @param User $user
     * @param Collection $parameters
     *
     * @return $this
     */
    protected function setPhone(User $user, Collection $parameters)
    {
        if ($parameters->has('phone')) {
            $user->setPhone($parameters->get('phone'));
        }

        return $this;
    }

    /**
     * @param User $user
     * @param Collection $parameters
     *
     * @return $this
     *
     * @throws \Exception
     */
    protected function setUsername(User $user, Collection $parameters)
    {
        if ($parameters->has('username') && $parameters->get('email')) {
            $user->setUsername($parameters->get('email'));
        }

        return $this;
    }

    /**
     * @param User $user
     * @param Collection $parameters
     * @return $this
     */
    protected function setRole(User $user, Collection $parameters)
    {
        if ($parameters->has('role')) {
            $user->setRole(new Role($parameters->get('role')));
        }

        return $this;
    }
}
