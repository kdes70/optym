<?php

namespace App\Services\Search;

use App\Domain\Product\Product;
use Elasticsearch\Client;

class ProductIndexer
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function clear(): void
    {
        $this->client->deleteByQuery([
            'index' => 'production',
            'type' => 'product',
            'body' => [
                'query' => [
                    'match_all' => new \stdClass(),
                ],
            ],
        ]);
    }

    public function index(Product $product): void
    {

//        dd($product->category->getId());


        $this->client->index([
            'index' => 'production',
            'type' => 'product',
            'id' => $product->getId(),
            'body' => [
                'id' => $product->getId(),
                'available_at' => $product->available_at ? $product->available_at->format(DATE_ATOM) : null,
                'title' => $product->getTitle(),
                'description' => $product->getDescription(),
                'cost_price' => $product->getCostPrice(),
                'state' => $product->getState(),
                'categories' => array_merge(
                    [$product->category->id],
                    $product->category->ancestors()->pluck('id')->toArray()
                ),
//                'categories_children' => array_merge(
//                    [$product->category->id],
//                    $product->category->getChildren()->pluck('id')->toArray()
//                ),
//                'categories_descendants' => array_merge(
//                    [$product->category->id],
//                    $product->category->descendants()->pluck('id')->toArray()
//                ),
               // 'regions' => $regions ?: [0],
//                'values' => array_map(function (Value $value) {
//                    return [
//                        'attribute' => $value->attribute_id,
//                        'value_string' => (string)$value->value,
//                        'value_int' => (int)$value->value,
//                    ];
//                }, $product->values()->getModels()),
            ],
        ]);
    }

    public function remove(Product $product): void
    {
        $this->client->delete([
            'index' => 'production',
            'type' => 'product',
            'id' => $product->getId(),
        ]);
    }
}