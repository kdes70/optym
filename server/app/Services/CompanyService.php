<?php

namespace App\Services;

use App\Domain\Company\Company;
use App\Domain\User\DTO\RegisterUser;
use App\Repositories\Company\CompanyRepository;
use Illuminate\Support\Collection;

/**
 * Class CompanyService
 */
class CompanyService
{
    /**
     * @var CompanyRepository
     */
    private $repository;

    /**
     * CompanyService constructor.
     *
     * @param CompanyRepository $repository
     */
    public function __construct(CompanyRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $slug
     *
     * @return Company
     */
    public function getBySlug(string $slug): Company
    {
        return $this->repository->getBySlug($slug);
    }

    /**
     * @param RegisterUser $dto
     *
     * @return Company $company
     */
    public function registerFields(RegisterUser $dto)
    {
        $company = $this->repository->getModel();

        $this->fill($company, $dto->collection());

        return $company;
    }


    /**
     * @param Company $company
     * @param Collection $parameters
     * @return Company
     */
    protected function fill(Company $company, Collection $parameters): Company
    {
        $company->fill($parameters->all());

        $this->fillSpecialFields($company, $parameters);

        return $company;
    }


    /**
     * @param Company $company
     * @param Collection $parameters
     *
     * @return $this
     */
    protected function fillSpecialFields(Company $company, Collection $parameters)
    {
        $this
            ->setCountry($company, $parameters);

//         if ($parameters->has('options')) {
//             $this->setOptions($user, $parameters->get('options'));
//         }

        return $this;
    }

    /**
     * @param Company $company
     * @param Collection $parameters
     *
     * @return $this
     */
    protected function setCountry(Company $company, Collection $parameters)
    {
        if ($parameters->has('country')) {
            $company->setCountry($parameters->get('country'));
        }

        return $this;
    }

}
