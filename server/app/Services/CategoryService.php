<?php

namespace App\Services;

use App\Repositories\Categories\CategoryRepository;
use Illuminate\Http\Request;

class CategoryService
{
    /**
     * @var CategoryRepository
     */
    private $repository;

    /**
     * CategoryService constructor.
     * @param CategoryRepository $repository
     */
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }


    public function getFullTree()
    {
        return $this->repository->getFullTree();
    }

    public function getChildrenSelfRoot()
    {
        $roots = $this->repository->getChildrenSelfRoot()->get();

        return $roots;
    }


    public function getChildrenByParentId($parent_id)
    {
        return $this->repository->getChildrenByParentId($parent_id);
    }


    public function getChildrenBySlug($slug)
    {
        return $this->repository->getChildrenBySlug($slug);
    }


    public function getShowByPath($path)
    {
        return $this->repository->getByPath($path);
    }

    public function getByPathCompanyAndItsProducts(Request $request, $path, $perPage)
    {
        $page_start = $request->get('page', 1);
        $offset    = ($page_start * $perPage) - $perPage;

        return $this->repository->getByPathCompanyAndItsProducts($path, $offset, $perPage);
    }


    public function countCompanyForCategory($path)
    {
        $category = $this->repository->getCompanyForCategory($path);

        return $category->companies->count();
    }

    public function getChildren($categories)
    {
        return $this->repository->getChildrenNote($categories);
    }


    public function getRootIsNotPage()
    {
        $roots = $this->repository->getRootIsNotPage();

        //dd($roots);

        return $roots;
    }

    public function getFeatureList($category_id)
    {
       return $this->repository->getFeatureList($category_id);
    }

}