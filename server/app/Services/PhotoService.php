<?php

namespace App\Services;

use App\Exceptions\ImageuploadException;
use Exception;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PhotoService
{

    private $original_path = '/public/uploads/';

    public function save(UploadedFile $files)
    {
        if ($files instanceof UploadedFile) {
            $files = [$files];
        }

        $this->validate($files);

        $items = array_map(function (UploadedFile $file) {

            $image = \Image::make($file);

            $file_name = time() . $file->getClientOriginalName();

            $dir = pathinfo($this->original_path, PATHINFO_DIRNAME);

            $path = $dir . $file_name;

            $this->uploadOriginal($image, $path);

            return [$file->getClientOriginalName() => $path];
        }, $files);

        return array_collapse($items);
    }


    public function uploadThumbnail($file, $path, int $w, ?int $h = null): bool
    {
        $file->widen($w);

        return Storage::put($path, $file->encode());
    }

    public function uploadOriginal($file, $path): bool
    {
        return Storage::put($path, $file->encode());
    }


    public function createThumbnails()
    {

    }

    /**
     * @param  UploadedFile[] $files
     */
    private function validate($files)
    {
        foreach ($files as $file) {
            if (!$file->isValid()) {
                throw new FileException($file->getErrorMessage());
            }
        }
    }


    /**
     * TODO на запас
     * Check and create directory if not exists.
     *
     * @access private
     * @param $absolute_path
     * @return bool
     */
    private function createDirectoryIfNotExists($absolute_path)
    {
        if (File::isDirectory($absolute_path) && File::isWritable($absolute_path)) {
            return true;
        }
        try {
            @File::makeDirectory($absolute_path, 0777, true);
            return true;
        } catch (Exception $e) {
            throw new FileException($e->getMessage());
        }
    }


}