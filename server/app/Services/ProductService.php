<?php

namespace App\Services;

use App\Repositories\Product\ProductRepository;

class ProductService
{
    /**
     * @var ProductRepository
     */
    private $repository;

    /**
     * CategoryService constructor.
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function findByCategoryId(int $category_id)
    {
        return $this->repository->findByCategoryId($category_id);
    }


}