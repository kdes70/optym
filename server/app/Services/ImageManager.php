<?php

namespace App\Services;

use App\Domain\Product\Contracts\PhotosInterface;
use App\Domain\Product\Image;
use Illuminate\Http\UploadedFile;
use Symfony\Component\HttpFoundation\ParameterBag;

class ImageManager
{
    public static function url(Image $image): string
    {
        return url($image->getPath());
    }

    /**
     * Прикрепление изображений к сущности.
     *
     * @param PhotosInterface $entity
     * @param  UploadedFile[] $files
     *
     * @return Image[]
     */
    public static function attach(PhotosInterface $entity, $files): array
    {
        return $entity->addPhoto(self::save($files));
    }

    /**
     * Сохранение изображений.
     *
     * @param  UploadedFile|UploadedFile[] $files
     *
     * @return Image[]
     */
    public static function save($files): array
    {
        $photo = new PhotoService();

        if ($files instanceof UploadedFile) {
            $files = [$files];
        }

        $paths = collect($photo->save($files));

        /** @var UploadedFile[] $files */
        $files = collect($files)->keyBy(function (UploadedFile $file) {
            return $file->getClientOriginalName();
        })->all();

        return $paths->map(function ($realPath, $clientName) use ($files) {

            $meta = isset($files[$clientName]) ? $files[$clientName]->getMeta() : new ParameterBag();

            return (new Image())
                ->setPath($realPath)
                ->setOrder($meta->get('order'));
        })->values()->all();
    }




//    public static function update(Image $image, array $attrs): Image
//    {
//        $attrs = collect($attrs);
//
//        $image
//            ->setAlt($attrs->get('alt'))
//            ->setTitle($attrs->get('title'))
//            ->setOrder($attrs->get('order'))
//            ->save();
//
//        return $image;
//    }

//    public static function delete(Image $image): ?bool
//    {
//        $imagenator = app('appwilio.imagenator');
//
//        $imagenator->delete($image->getPath(), $image->getImageableType());
//
//        return $image->delete();
//    }
}
