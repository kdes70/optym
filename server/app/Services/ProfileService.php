<?php

namespace App\Services;

use App\Domain\User\DTO\RegisterUser;
use App\Domain\User\Profile;
use App\Repositories\User\ProfileRepository;
use Illuminate\Support\Collection;


class ProfileService
{
    /**
     * @var ProfileRepository
     */
    private $repository;

    public function __construct(ProfileRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param RegisterUser $dto
     * @return Profile
     */
    public function registerFields(RegisterUser $dto)
    {
        $profile = $this->repository->getModel();

        $this->fill($profile, $dto->collection());

        return $profile;
    }


    /**
     * @param Profile $profile
     * @param Collection $parameters
     * @return Profile
     */
    protected function fill(Profile $profile, Collection $parameters): Profile
    {
        $profile->fill($parameters->all());

        $this->fillSpecialFields($profile, $parameters);

        return $profile;
    }

    /**
     * @param Profile $profile
     * @param Collection $parameters
     * @return $this
     */
    protected function fillSpecialFields(Profile $profile, Collection $parameters)
    {
        $this->setLastName($profile, $parameters)
            ->setFirstName($profile, $parameters)
            ->setBirthday($profile, $parameters)
            ->setAvatar($profile, $parameters)
            ->setCountry($profile, $parameters)
            ->setCity($profile, $parameters);

//         if ($parameters->has('options')) {
//             $this->setOptions($user, $parameters->get('options'));
//         }

        return $this;
    }

    /**
     * @param Profile $profile
     * @param Collection $parameters
     * @return $this
     */
    protected function setLastName(Profile $profile, Collection $parameters)
    {
        if ($parameters->has('last_name')) {
            $profile->setLastName($parameters->get('last_name'));
        }

        return $this;
    }

    /**
     * @param Profile $profile
     * @param Collection $parameters
     * @return $this
     */
    protected function setFirstName(Profile $profile, Collection $parameters)
    {
        if ($parameters->has('first_name')) {
            $profile->setFirstName($parameters->get('first_name'));
        }

        return $this;
    }

    /**
     * @param Profile $profile
     * @param Collection $parameters
     * @return $this
     */
    protected function setBirthday(Profile $profile, Collection $parameters)
    {
        if ($parameters->has('birthday')) {
            $profile->setBirthday($parameters->get('birthday'));
        }

        return $this;
    }

    /**
     * @param Profile $profile
     * @param Collection $parameters
     * @return $this
     */
    protected function setAvatar(Profile $profile, Collection $parameters)
    {
        if ($parameters->has('avatar')) {
            $profile->setAvatar($parameters->get('avatar'));
        }

        return $this;
    }

    /**
     * @param Profile $profile
     * @param Collection $parameters
     * @return $this
     */
    protected function setCountry(Profile $profile, Collection $parameters)
    {
        if ($parameters->has('country')) {
            $profile->setCountry($parameters->get('country'));
        }

        return $this;
    }

    /**
     * @param Profile $profile
     * @param Collection $parameters
     * @return $this
     */
    protected function setCity(Profile $profile, Collection $parameters)
    {
        if ($parameters->has('city')) {
            $profile->setCity($parameters->get('city'));
        }

        return $this;
    }
}
