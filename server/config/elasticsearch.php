<?php

return [
    'hosts' => explode(',', env('ELASTICSEARCH_HOSTS')),
    'retries' => 1,
//    'include_type_name' => true,
];
