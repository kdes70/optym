<?php

namespace Tests\Feature;

use App\Domain\Catalog\Category\Category;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\DB;
use Tests\CreatesApplication;

abstract class TestWebCase extends BaseTestCase
{
    use CreatesApplication;
    use DatabaseMigrations;

//    protected function disableExceptionHandling()
//    {
//        $this->app->instance(ExceptionHandler::class, new class extends Handler {
//            public function __construct() {}
//
//            public function report(Exception $e)
//            {
//                // no-op
//            }
//
//            public function render($request, Exception $e) {
//                throw $e;
//            }
//        });
//    }

    public function faker()
    {
        return Factory::create('ru_RU');
    }


    /**
     *
     */
    public function createCategorySeed()
    {
//        /** @var array $data */
//        $data = include  storage_path('test/data/categories.php');
//
//        DB::table(Category::getTableName())->insert($this->treeFormat($data));

        $this->artisan('db:seed', ['--class' => 'CategoryTableSeeder', '--database' => 'testing']);

    }

}
