<?php

namespace Tests\Feature\Web\Admin;


use App\Domain\User\User;
use Tests\TestCase;

class TestAdminCase extends TestCase
{

    protected function createAdmin(): User
    {
        return factory(User::class)->states(['admin'])->create();
    }

}
