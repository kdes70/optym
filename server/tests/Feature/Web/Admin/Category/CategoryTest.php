<?php

namespace Tests\Feature\Admin\Category;

use App\Domain\User\User;
use Tests\Feature\Web\Admin\TestAdminCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestAdminCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_can_see_categoryes_list()
    {
        $this
            ->actingAs($this->createAdmin())
            ->get(route('admin.categories.index'))
            ->assertStatus(200);
    }

    public function test_cannot_see_categoryes_list_if_unauthenticated()
    {
        $this
            ->get(route('admin.categories.index'))
            ->assertStatus(302)
            ->assertRedirect(route('login'));
    }

    public function test_cannot_see_categoryes_list_if_non_priveleged_type_user()
    {
        $res = $this
            ->actingAs(factory(User::class)->states(['type_user'])->create())
            ->get(route('admin.categories.index'))
            ->assertStatus(403);
         //   ->assertRedirect(route('login')); //TODO сделать редирект на форму фхода для админа


    }

    public function test_cannot_see_categoryes_list_if_non_priveleged_type_wholesale()
    {
        $this
            ->actingAs(factory(User::class)->states(['wholesale'])->create())
            ->get(route('admin.categories.index'))
            ->assertStatus(403);
            //->assertRedirect(route('login'));
    }

    public function test_cannot_see_categoryes_list_if_non_priveleged_type_buyer()
    {
        $this
            ->actingAs(factory(User::class)->states(['buyer'])->create())
            ->get(route('admin.categories.index'))
            ->assertStatus(403);
//            ->assertRedirect(route('login'));
    }

//    public function test_cannot_see_categoryes_list_if_non_state_user_active()
//    {
//      $res =  $this
//            ->actingAs(factory(User::class)->states(['active'])->create())
//            ->get(route('admin.categories.index'))
//
////          $res->dump();
//
//            ->assertStatus(403);
////            ->assertRedirect(route('login'));
//    }
}
