<?php

namespace Tests\Feature\Web\Auth;

use App\Domain\Company\Company;
use App\Domain\User\Profile;
use App\Domain\User\Role;
use App\Domain\User\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    //use RefreshDatabase;

    public function test_form(): void
    {
        $response = $this->get('/login');

        $response
            ->assertStatus(200)
            ->assertSee('Login');
    }

    public function test_errors(): void
    {
        $response = $this->post('/login', [
            'login' => '',
            'password' => '',
        ]);

      //  dd(session()->all());

        $response
            ->assertStatus(302)
            ->assertSessionHasErrors(['login', 'password']);
    }

    public function test_wait(): void
    {
        $user = factory(User::class)->create(['state' => User::STATE_WAIT]);

        $response = $this->post('/login', [
            'login' => $user->email,
            'password' => 'secret',
        ]);

        $response
            ->assertStatus(302)
            ->assertRedirect('/')
            ->assertSessionHas('error', 'You need to confirm your account. Please check your email.');
    }

//    public function test_wholesale(): void
//    {
//        $user = factory(User::class)->states(['wholesale', 'active'])->create();
//
//        $response = $this->post('/login', [
//            'login' => $user->email,
//            'password' => 'secret',
//        ]);
//
//        $response
//            ->assertStatus(302)
//            ->assertRedirect(route('cabinet.home'));
//            //->assertTrue($user->is);
//
//    }

//    public function test_buyer(): void
//    {
//        $user = factory(User::class)->states(['buyer', 'active'])->create();
//
//        $response = $this->post('/login', [
//            'login' => $user->email,
//            'password' => 'secret',
//        ]);
//
//        $response
//            ->assertStatus(302)
//            ->assertRedirect(route('cabinet.home'));
//        //->assertTrue($user->is);
//
//    }

}