<?php

namespace Tests\Feature\Web\Auth;

use App\Domain\User\Enums\UserStateEnum;
use App\Domain\User\Enums\UserTypeEnum;
use App\Domain\User\Profile;
use App\Domain\User\User;
use Illuminate\Support\Str;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    /** @var string */
    private $form_session = 'register_form';

    /** @var User */
    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    public function test_redirect_to_auth()
    {
        $this->be($this->user);
        $response = $this->get(route('register.form'));

        $response->assertStatus(302)
            ->assertRedirect(route('home'));
    }

    public function test_get_see_registration_form()
    {
        $response = $this->get(route('register.form'));

        $response->assertStatus(200)
            ->assertSee('registration_form');
    }

    public function test_registration_account_type_wholesale()
    {
        //$this->withoutExceptionHandling();

        $this->withoutMiddleware();

        $user_wholesale = factory(User::class)->state('wholesale')->make()->toArray();

        $response = $this
            ->postJson(route('register.send'), $user_wholesale);
//            ->assertStatus(201)
//            ->assertJson($user_wholesale);

        $response->dump();

       // $this->assertDatabaseHas(User::getTableName(), $user_wholesale);
    }

    public function test_form_wholesale()
    {
        $this->postTypeUser(UserTypeEnum::WHOLESALE);

        $response = $this->get(route('register'));

        $response
            ->assertStatus(200)
            ->assertSee('Регистрация оптового продавца');
    }

    public function test_create_wholesale_error()
    {
        $this->test_fotm_wholesale();

        $response = $this->post('/register',
            [
                'user_type'    => UserTypeEnum::WHOLESALE,
                'email'        => '',
                'phone'        => '',
                'password'     => '',
                'first_name'   => '',
                'last_name'    => '',
                'company_name' => '',
                'company_type' => '',
                'city'         => ''

            ]
        );
        $response->assertStatus(302)
            ->assertSessionHas($this->form_session)
            ->assertSessionHasErrors([
                    'email',
                    'phone',
                    'password',
                    'first_name',
                    'last_name',
                    'company_name',
                    'company_type',
                    'city'
                ]
            );
    }

//    public function test_create_wholesale_success()
//    {
//        //$this->disableExceptionHandling();
//
//        $response = $this->postTypeUser(User::TYPE_WHOLESALE);
//
//        $response2 = $this->get(route('register'));
//        $response2->assertStatus(200)
//            ->assertSeeText('Регистрация оптового продавца');
//
//        /** @var User $user */
//        $user = factory(User::class)->states('wholesale')->make();
//
//        /** @var Profile $profile */
//        $profile = factory(Profile::class)->make();
//
//        /** @var Company $company */
//        $company = factory(Company::class)->make();
//
//        $company_user = [
//            'email'        => $user->getEmail(),
//            'phone'        => $user->getPhone(),
//            'password'     => 'secret',
//            'user_type'    => $user->getType(),
//            'first_name'   => $profile->getFirstName(),
//            'last_name'    => $profile->getLastName(),
//            'company_name' => $company->getName(),
//            'company_type' => $company->getCompanyType(),
//            'city'         => $company->getCity()
//        ];
//
//        $response3 = $this->post('/register', $company_user);
//
//        $response3->assertStatus(302)
//            ->assertRedirect('/login')
//            ->assertSessionHas('success', 'Check your email and click on the link to verify.');
//
//          //$this->assertDatabaseHas(User::getTableName(), $this->verify_token);
//        //$this->assertEquals($authUser->getRoleName(), Role::CUSTOMER)
//    }

    public function test_create_wholesale_buyer()
    {
        $response = $this->postTypeUser(UserTypeEnum::BUYER);

        $response = $this->get(route('register'));
        $response->assertStatus(200)
            ->assertSeeText('Регистрация оптового покупателя');

        /** @var User $user */
        $user = factory(User::class)->states('buyer')->make();

        /** @var Profile $profile */
        $profile = factory(Profile::class)->make();

        $buyer_user = [
            'email'      => $user->getEmail(),
            'phone'      => $user->getPhone(),
            'password'   => 'secret',
            'user_type'  => $user->getType(),
            'first_name' => $profile->getFirstName(),
            'last_name'  => $profile->getLastName()
        ];

        $response3 = $this->post('/register', $buyer_user);

        $response3->assertStatus(302)
            ->assertRedirect('/login')
            ->assertSessionHas('success', 'Check your email and click on the link to verify.');
    }

    public function testVerifyIncorrect(): void
    {
        $response = $this->get('/verify/' . Str::uuid());

        $response
            ->assertStatus(302)
            ->assertRedirect('/login')
            ->assertSessionHas('error', 'Sorry your link cannot be identified.');
    }

    public function testVerify(): void
    {
        $user = factory(User::class)->create([
            'state'        => UserStateEnum::WAIT,
            'verify_token' => Str::uuid(),
        ]);

        $response = $this->get('/verify/' . $user->verify_token);

        $response
            ->assertStatus(302)
            ->assertRedirect('/login')
            ->assertSessionHas('success', 'Your e-mail is verified. You can now login.');
    }

    /**
     * @param $type
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    private function postTypeUser($type)
    {
        $response = $this->post(route('register.send'), ['user_type' => $type]);

        $response->assertStatus(302)
            ->assertSessionHas($this->form_session)
            ->assertRedirect('/register');

        return $response;
    }


}
