<?php

namespace Tests\Feature\Web\Catalog;

use App\Domain\Catalog\Category\Category;
use App\Domain\Catalog\Pages\Page;
use App\Domain\Company\Company;
use App\Domain\Product\Product;
use App\Repositories\Categories\CategoryRepository;
use App\Services\CategoryService;
use Illuminate\Support\Str;
use Tests\Feature\TestWebCase;


class CategoryCompanyTest extends TestWebCase
{

    private $CategoryService;

    private $landingPageUrl = 'stroitelstvo-i-remont';
    private $secondLevelUrl = '/stroymaterialy';

    protected function setUp(): void
    {
        parent::setUp();

        $this->createCategorySeed();

        $repository = $this->app->make(CategoryRepository::class);
        $this->CategoryService = new CategoryService($repository);
    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_landing_page_category(): void
    {
        /** @var Category $category */
        $category = $this->CategoryService->getShowByPath($this->landingPageUrl);

        factory(Page::class)->create([
            'category_id' => $category->getId(),
            'title' => $category->getName(),
            'menu_title' => $category->getName(),
            'slug' => Str::slug($category->getName()),
            'enabled' => true,
        ]);


        // проверить что категория уровня depth = 1
        self::assertEquals(1, $category->depth);

        // проверить что у Page тже имя что и у категории
        self::assertEquals($category->getPage()->getTitle(), $category->getName());

        // проверить количество дочарних категории совподает
        self::assertNotEmpty($category->getChildren());

        // проверить количество потдочарних у дочарних категории совподает
        self::assertNotEmpty($category->getChildren()->first()->getChildren());

    }

    /**
     * проверить список продуктов категории второго уровня
     */
    public function test_second_level_category_product_list(): void
    {
        /** @var Category $category */
        $category = $this->CategoryService->getShowByPath($this->landingPageUrl . $this->secondLevelUrl);

        factory(Product::class)->create([
            'category_id' => $category->getId(),
        ]);

        // создаем товары у дочерних категорий
        foreach ($category->getChildren() as $child) {

            $companis = factory(Company::class, 2)->create()->each(function (Company $company) use ($child) {
                $products = factory(Product::class, 3)->create([
                    'company_id' => $company->getId(),
                    'category_id' => $child->getId(),
                ]);
            });
        }

        // TODO заменяет запрос в elasticsearch
        $cat_ids = $category->getChildren()->pluck('id')->toArray();

        $items = Product::active()
            ->whereHas('category', function ($q) use ($cat_ids) {
                $q->whereIn('id', $cat_ids);
                // ->orderBy(new Expression('FIELD(categories.id,' . implode(',', $cat_ids) . ')'));
            })
            ->with('company')
            ->get();

        // проверить что категория уровня depth = 2
        self::assertEquals(2, $category->depth);

        // проверить что у Page null
        self::assertNull($category->getPage());

        // проверить количество дочарних категории совподает
        self::assertNotEmpty($category->getChildren());

        // проверить что есть соседнии категории
        self::assertNotEmpty($category->getSiblings());

        // проверим что у первой дочерней категории есть компании
        self::assertNotEmpty($category->getChildren()->first()->getCompanies());

        // количество компаний совподает
        self::assertEquals(6, $category->getChildren()->first()->getCompanies()->count());

        // проверим что у первой дочерней категории есть компании с товарами
        self::assertNotEmpty($category->getChildren()->first()->getCompanies()->first()->getProducts());

        // проверить количество товаров во всех доерних категорий
        self::assertEquals($category->getChildren()->count() * 2 *3, $items->count());
    }
}
