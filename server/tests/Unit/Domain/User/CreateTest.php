<?php

namespace Tests\Unit\Domain\User;

use App\Domain\User\User;
use App\Services\UserService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateTest extends TestCase
{
    //  use RefreshDatabase;

    private $userService;

    /**
     * CreateTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->userService = app()->make('App\Services\UserService');
    }


    public function test_user()
    {
        $username = $this->faker()->userName;
        $email = $this->faker()->email;

        $user = $this->userService->new($username, $email);

        self::assertNotEmpty($user);
        self::assertNotEmpty($user->getUsername());
        self::assertEquals($email, $user->getEmail());
        self::assertNotEmpty($user->password);
        self::assertTrue($user->isActive());
        self::assertFalse($user->isAdmin());
        self::assertTrue($user->isUser());
        self::assertEquals($user->getType(), User::TYPE_USER);
    }
}
