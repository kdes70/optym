<?php

namespace Tests\Unit\Domain\User;

use App\Domain\User\Role;
use App\Domain\User\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use DatabaseTransactions;

    private $email = 'test@user.ru';
    private $phone = '+79130003333';
    private $password = 'secret';

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testNew()
    {
        $user = new User();
        $user->setUsername($this->email);
        $user->setEmail($this->email);
        $user->setPassword($this->password);
        $user->setState(User::STATE_ACTIVE);
        $user->setType(User::TYPE_USER);
        $user->setRole(new Role(Role::USER));
        $user->save();

        self::assertNotEmpty($user);
        self::assertNotEmpty($user->getUsername());
        self::assertEquals($this->email, $user->getEmail());
        self::assertNotEmpty($user->password);
        self::assertTrue($user->isActive());
        self::assertFalse($user->isAdmin());
        self::assertEquals($user->getType(), User::TYPE_USER);
    }
}
