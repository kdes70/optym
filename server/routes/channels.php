<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel(
    $this->app['config']->get('chatter.channel.chat_room') . '-{conversation_id}',
    function ($user, $conversation_id) {
        if ($this->app['conversation.repository']->canJoinConversation($user, $conversation_id)) {
            return $user;
        }
    }
);


Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});
