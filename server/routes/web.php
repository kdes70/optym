<?php


use App\Http\Controllers\Web\Auth\LoginController;
use App\Http\Controllers\Web\Auth\RegisterController;
use App\Http\Controllers\Web\WelcomeController;

Route::namespace('Web')->group(function () {

    // TODO удалит
    Route::get('/test', [WelcomeController::class, 'test']);
    Route::get('/', [WelcomeController::class, 'index'])->name('home');

    # Auth
    Route::namespace('Auth')->group(function () {

        # Вход todo вынести в api
        Route::get('login', [LoginController::class, 'show'])->name('login');
        Route::post('login', [LoginController::class, 'login'])->name('login');

        # Выход
        Route::post('logout', [LoginController::class, 'logout'])->name('logout');

        # По токену
        //  get('/verify/{token}', 'RegisterController@verify')->name('register.verify');

        # Сбросить пароль
        Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('password/reset', 'ResetPasswordController@reset');

        # Регистрация
        Route::prefix('register')->name('register.')->group(function () {
            Route::get('/', [RegisterController::class, 'register_form'])->name('form');
            Route::post('/', [RegisterController::class, 'register_ajax'])->name('send');
//                get('account_type', [RegisterController::class, 'getRegisterAccountType'])->name('account_type.form');
//                post('account_type', [RegisterController::class, 'postRegisterAccountType'])->name('account_type');
        });
    });
});


//Route::get('/home', 'HomeController@index')->name('home');

// TODO PARSER
//Route::get('/parser', 'ParserController@index')->name('parser');

# По токену (мыло + срок жизни)
//get('auth/token/{token}', 'AuthenticationController@token')->name('token');

# Profiles
Route::group([
    'prefix'     => 'profiles',
    'as'         => 'profiles.',
    'namespace'  => 'Profiles',
    'middleware' => ['auth:web'],
], function () {

    Route::group([
        'prefix'     => 'settings',
        'as'         => 'settings.',
        'namespace'  => 'Settings',
        'middleware' => ['auth:web'],
    ], function () {
        Route::get('/', 'SettingsController@index')->name('home');
        Route::put('/update', 'SettingsController@update')->name('update');
        Route::get('/email', 'SettingsController@email')->name('email');
        Route::get('/password', 'SettingsController@password')->name('password');
    });


    Route::group([
        'namespace' => 'Company',
        'prefix'    => 'company',
        'as'        => 'company.',
    ], function () {

        # Форма редактирование данных компании
        Route::get('/{username}', 'HomeController')
            ->name('home')
            ->where('username', '[a-zA-Z0-9_-]+');

        # редактирование данных компании
        Route::post('/edit/{username}', 'EditController')
            ->name('edit')
            ->where('username', '[a-zA-Z0-9_-]+');

        Route::post('/avatar/{id}', 'AvatarController')
            ->name('avatar');


        Route::group([
            'namespace' => 'Product',
            'prefix'    => 'product',
            'as'        => 'product.',
        ], function () {
            Route::get('/add', 'CreateController@form')->name('add');
            Route::post('/save', 'CreateController@save')->name('save');
        });

    });


//    group([
//        'namespace' => 'Company',
//        'prefix'    => 'company',
//        'as'        => 'company.',
//    ], function () {
//        get('/', 'ProfileController@index')->name('home');
//        // get('/{id}', 'ProfileController@show')->name('show');
////        get('/edit', 'ProfileController@edit')->name('edit');
//        put('/update', 'ProfileController@update')->name('update');
//
//    });

});


# ADMIN
Route::group(
    [
        'prefix'     => 'admin',
        'as'         => 'admin.',
        'namespace'  => 'Admin',
        'middleware' => ['auth', 'can:admin-panel'],
    ],
    function () {

        Route::get('/', 'HomeController@index')->name('dashboard');

        Route::get('/activities', 'ActivityController@index')->name('activities');
        // user
        Route::resource('users', 'UsersController');
        Route::resource('roles', 'RolesController');

        Route::resource('pages', 'PagesController');

        Route::group(['prefix' => 'pages/{page}', 'as' => 'pages.'], function () {
            Route::post('/first', 'PagesController@first')->name('first');
            Route::post('/up', 'PagesController@up')->name('up');
            Route::post('/down', 'PagesController@down')->name('down');
            Route::post('/last', 'PagesController@last')->name('last');
        });

        Route::post('/users/{user}/verify', 'UsersController@verify')->name('users.verify');


        Route::resource('regions', 'RegionController');

        Route::group(['namespace' => 'Category',], function () {

            Route::resource('categories', 'CategoryController');

            Route::group(['prefix' => 'categories/{category}', 'as' => 'categories.'], function () {
                Route::post('/first', 'CategoryController@first')->name('first');
                Route::post('/up', 'CategoryController@up')->name('up');
                Route::post('/down', 'CategoryController@down')->name('down');
                Route::post('/last', 'CategoryController@last')->name('last');

                Route::resource('attributes', 'AttributeController');
                Route::resource('attributes.value', 'AttributeValueController');

            });
        });

    }
);


Route::group(['namespace' => 'Company'], function () {

    Route::group(['prefix' => 'suppliers', 'as' => 'suppliers.'], function () {

        # компании
        Route::get('/', 'SuppliersController@index')->name('list');
        Route::get('/{slug}', 'SuppliersController@show')->name('show');

        Route::get('/{company}/contact', 'SuppliersController@contact')->name('contact');
        Route::get('/{company}/product/{product}', 'SuppliersController@showGoods')->name('show-product');
        Route::get('/{company}/{category}', 'SuppliersController@category')->name('category');


    });

});

//TODO categoty праверка по slug родительских !!!

Route::group(['namespace' => 'Catalog'], function () {

    // product list
//    get('catalog/{category}', 'CategoriesController@show')
//        ->name('catalog')
//        ->where('category', '[a-zA-Z0-9/_-]+');

    // company list
    Route::get('/{category}/suppliers', 'CategoriesController@suppliers')
        ->name('catalog.suppliers')
        ->where('category', '[a-zA-Z0-9/_-]+');

//    get('/{category}/producer', 'CategoriesController@producer')
//        ->name('catalog.suppliers')
//        ->where('category', '[a-zA-Z0-9/_-]+');


});

#СООБЩИТЕ ПОСТАВЩИКАМ, ЧТО ВАМ НУЖНО
Route::group([
    'prefix' => 'tenders',
    'as'     => 'tenders.',
], function () {

});


Route::get('/search/result', 'SearchController@index');

//
Route::get('/{path}', 'Catalog\PathController')
    ->name('category')
    ->where('path', '[a-zA-Z0-9/_-]+');


