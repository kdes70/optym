<?php
//
//
//use App\Domain\Activity;
//use App\Domain\User\User;
//use DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator as Crumbs;
//
//Breadcrumbs::register('home', function (Crumbs $crumbs) {
//    $crumbs->push('Home', route('home'));
//});
//
//Breadcrumbs::register('login', function (Crumbs $crumbs) {
//    $crumbs->parent('home');
//    $crumbs->push('Login', route('login'));
//});
//
//Breadcrumbs::register('login.phone', function (Crumbs $crumbs) {
//    $crumbs->parent('home');
//    $crumbs->push('Login', route('login.phone'));
//});
//
//Breadcrumbs::register('register', function (Crumbs $crumbs) {
//    $crumbs->parent('home');
//    $crumbs->push('Register', route('register'));
//});
//
//Breadcrumbs::register('password.request', function (Crumbs $crumbs) {
//    $crumbs->parent('login');
//    $crumbs->push('Reset Password', route('password.request'));
//});
//
//Breadcrumbs::register('password.reset', function (Crumbs $crumbs) {
//    $crumbs->parent('password.request');
//    $crumbs->push('Change', route('password.reset'));
//});
//
//
//// Admin
//
//Breadcrumbs::register('admin.home', function (Crumbs $crumbs) {
//    $crumbs->parent('home');
//    $crumbs->push('Admin', route('admin.home'));
//});
//
//// Activity
//Breadcrumbs::register('admin.activities', function (Crumbs $crumbs, Activity $activity) {
//    $crumbs->parent('admin.home');
//    $crumbs->push('Activities', route('admin.activities', $activity));
//});
//
//// Users
//
//Breadcrumbs::register('admin.users.index', function (Crumbs $crumbs) {
//    $crumbs->parent('admin.home');
//    $crumbs->push('Users', route('admin.users.index'));
//});
//
//Breadcrumbs::register('admin.users.create', function (Crumbs $crumbs) {
//    $crumbs->parent('admin.users.index');
//    $crumbs->push('Create', route('admin.users.create'));
//});
//
//Breadcrumbs::register('admin.users.show', function (Crumbs $crumbs, User $user) {
//    $crumbs->parent('admin.users.index');
//    $crumbs->push($user->name, route('admin.users.show', $user));
//});
//
//Breadcrumbs::register('admin.users.edit', function (Crumbs $crumbs, User $user) {
//    $crumbs->parent('admin.users.show', $user);
//    $crumbs->push('Edit', route('admin.users.edit', $user));
//});
