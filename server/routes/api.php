<?php

use App\Http\Controllers\Api\Auth\RegisterController;
use Illuminate\Http\Request;

//Route::namespace('Api')->group(function () {
#Auth
Route::prefix('auth')->namespace('Auth')->group(function () {

#Registration
    Route::get('register', [RegisterController::class, 'form'])->name('register.form');
    Route::post('register', [RegisterController::class, 'register'])->name('register.register');
});
//});

Route::get('activities', 'Api\ActivityController@index');

// upload avatar
Route::post('profile/avatar/', 'Api\ProfileController@avatar');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => ['auth:api']], function () {
    Route::get('activities', 'Api\ActivityController@index');

});

Route::get('/search', [
    'as' => 'api.search',
    'uses' => 'Api\SearchController@search'
]);

Route::get('/category/getAll', 'Api\CategoriesController@getAll');

Route::post('/catalog', 'Api\CategoriesController@catalog');
Route::post('/catalog/features', 'Api\CategoriesController@features');
