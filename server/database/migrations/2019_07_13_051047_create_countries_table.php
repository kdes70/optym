<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        if (Schema::connection('mysql_geo')->hasTable('countries')) {
////            Schema::connection('mysql_geo')->table('countries', function (Blueprint $table) {
////                $table->dropForeign('cities_country_id_foreign');
////            });
//
//            Schema::connection('mysql_geo')->dropIfExists('countries');
//        }


        Schema::create('geo_countries', function (Blueprint $table) {
            $table->string('id', 2)->primary()->index();
            $table->string('id3', 3)->index()->unique();
            $table->string('name', 150)->index()->unique();
            $table->string('full_name', 150)->index()->unique();
            $table->string('name_en', 150)->index()->unique();
            $table->string('code', 3)->index();
            $table->string('phone_code', 8)->index();
            $table->string('domain', 2)->index();
            $table->smallInteger('order')->default(0)->index();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_countries');
    }
}
