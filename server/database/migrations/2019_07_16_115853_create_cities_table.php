<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

//        if (Schema::connection('mysql_geo')->hasTable('cities'))
//        {
//            Schema::connection('mysql_geo')->table('regions', function (Blueprint $table) {
//                $table->dropForeign('	cities_country_id_foreign');
//                $table->dropForeign('cities_region_id_foreign');
//            });
//            Schema::connection('mysql_geo')->dropIfExists('cities');
//        }


        Schema::create('geo_cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('country_id', 2);
            $table->unsignedInteger('region_id');
            $table->string('name')->index();
            $table->string('slug')->index();
            $table->smallInteger('order')->default(0);
            $table->string('latitude', 100)->nullable();
            $table->string('longitude', 100)->nullable();
            $table->nullableTimestamps();

            $table->foreign('country_id')->references('id')->on('geo_countries');
            $table->foreign('region_id')->references('id')->on('geo_regions');

            $table->unique(['name', 'country_id', 'region_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_cities');
    }
}
