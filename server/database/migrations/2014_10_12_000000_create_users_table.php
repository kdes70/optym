<?php

use App\Domain\User\Enums\UserStateEnum;
use App\Domain\User\Role;
use App\Domain\User\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/** @noinspection PhpUnused */

/**
 * Class CreateUsersTable
 */
class CreateUsersTable extends Migration
{
    /** @var string */
    private $table;

    /**
     * CreateUsersTable constructor.
     */
    public function __construct()
    {
        $this->table = User::getTableName();
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('username', 64)->unique();
            $table->string('phone', 32)->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('api_token', 100)->nullable();
            $table->ipAddress('ip_address')->nullable();
            $table->string('options')->nullable();
            $table->string('verify_token')->nullable()->unique();
            $table->boolean('verified')->default(false);
            $table->string('state', 16)->default(UserStateEnum::WAIT);
            $table->string('role', 30)->default(Role::USER);
            $table->datetime('last_login_at')->nullable();
            $table->string('last_login_ip')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
