<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            // TODO разобраться со связями, нужно что бы при удалении юзера удалялась связующая таблица компаний, и сома компания
            $table->bigIncrements('id');
            $table->string('name', 60);
            $table->string('slug', 60)->unique();
            $table->string('tagline', 60)->nullable();
            $table->mediumText('description')->nullable();
            $table->text('text')->nullable();
            $table->tinyInteger('company_type')->unsigned()->nullable()->comment('Вид деятельности');
            $table->string('types_business')->nullable()->comment('Тип бизнеса');
            $table->string('logo')->nullable();
            $table->string('country')->nullable();
            $table->string('region')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('zip', 20)->nullable();
            $table->string('phone', 32)->nullable();
            $table->string('email')->nullable();
            $table->string('site_link')->nullable();
            $table->string('skype')->nullable();
            $table->string('other_contacts')->nullable();
            $table->string('legal_other')->nullable();
            $table->string('contact_person')->nullable();
            $table->date('reg_data')->nullable()->comment('Дата регистрации компании');
            $table->string('tags')->nullable();

            $table->string('min_amount')->nullable()->comment('Минимальная сумма закупки');
            $table->tinyInteger('scope_supply')->nullable()->unsigned()->comment('Объем поставок'); // Крупный опт, Мелкий опт, Поштучно
            $table->tinyInteger('payment_options')->nullable()->unsigned()->comment('Варианты оплаты');
            $table->tinyInteger('additional_options')->nullable()->unsigned()->comment('Дополнительные возможности');
            $table->tinyInteger('delivery_options')->nullable()->unsigned()->comment('Варианты доставки');

            $table->text('terms_payment_delivery')->nullable()->comment('Условия оплаты и доставки');
            $table->text('terms_return_guarantee')->nullable()->comment('Условия возврата и гарантии');
            $table->string('name_legal_entity')->nullable()->comment('Наименование юридического лица ');

            $table->boolean('privacy')->default(0)->comment('0 - Публичная анкета, 1 - Скрытая анкета');
            $table->boolean('enabled')->default(false);
            $table->boolean('status')->default(0)->comment('0 астивен, 1 заблокирован, 2 забанен на время');
            $table->boolean('verified')->default(false);
            $table->timestamps();

           // $table->foreign('id')->references('company_id')->on('companies_user')->onDelete('CASCADE');
        });

        // Create table for associating companies to user (Many-to-Many)
        Schema::create('companies_user', function (Blueprint $table) {
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('user_id');

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('CASCADE');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');

            $table->primary(['company_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('companies', function (Blueprint $table) {
//            $table->dropForeign(['id']);
//        });

        Schema::table('companies_user', function (Blueprint $table) {
            $table->dropForeign(['company_id']);
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('companies_user');
        Schema::dropIfExists('companies');
    }
}
