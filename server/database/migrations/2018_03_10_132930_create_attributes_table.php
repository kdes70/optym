<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributesTable extends Migration
{
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->unique();
            $table->string('slug')->unique();
            $table->string('type');
//          $table->integer('sort')->nullable();
            $table->boolean('required')->default(0);

            $table->nullableTimestamps();
        });

        Schema::create('attribute_value', function (Blueprint $table) {
            $table->increments('value_id');
            $table->unsignedInteger('attribute_id');
            $table->string('title');
            $table->string('slug');
            $table->string('sku', 32)->unique();
            $table->nullableTimestamps();

            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('CASCADE');
        });

        Schema::create('attribute_product', function (Blueprint $table) {
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('value_id');

            $table->unique(['product_id', 'value_id'], 'pv_unique');
        });

        Schema::create('categories_attribute', function (Blueprint $table) {
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('value_id');

            $table->unique(['category_id', 'value_id'], 'pv_cat_unique');
        });


    }

    public function down()
    {
        Schema::table('attribute_value', function (Blueprint $table) {
            $table->dropForeign(['attribute_id']);
        });

        Schema::dropIfExists('attributes');
        Schema::dropIfExists('attribute_value');
        Schema::dropIfExists('attribute_product');
        Schema::dropIfExists('categories_attribute');
    }
}
