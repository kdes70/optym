<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id')->index()->nullable();
            $table->unsignedInteger('category_id')->index();
            $table->unsignedSmallInteger('unit_id')->nullable();
            $table->string('title');
            $table->string('sku', 32)->unique();
            $table->string('slug')->unique();
            $table->decimal('cost_price', 12, 2)->unsigned();
            $table->decimal('retail_price', 12, 2)->unsigned();
            $table->integer('discount')->nullable();

            $table->unsignedInteger('main_image_id')->nullable();

            $table->unsignedSmallInteger('weight');
            $table->text('description')->nullable();
            $table->json('badges');
            $table->string('state', 16);

            $table->nullableTimestamps();

            $table->timestamp('available_at')->nullable();
            $table->timestamp('archived_at')->nullable();

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });

// TODO пока не используем смежную таблицу, проблемы со связями!

//        Schema::create('category_product', function (Blueprint $table) {
//            $table->unsignedInteger('product_id');
//            $table->unsignedInteger('category_id');
//
//            $table->primary(['product_id', 'category_id']);
//        });

//        Schema::create('product_images', function (Blueprint $table) {
//            $table->bigIncrements('id');
//            $table->bigInteger('product_id')->unsigned();
//            $table->string('image');
//            $table->boolean('main')->default(0);
//            $table->unsignedTinyInteger('order')->nullable();
//            $table->nullableTimestamps();
//
//            $table->foreign('product_id')->references('id')->on('products')->onDelete('CASCADE');
//        });

//        `position_id` int(11) NOT NULL,
//`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
//`product_id` int(11) NOT NULL,
//`image_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
//`title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
//`alt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
//`created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
//`updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',




//        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
//`parent_category_id` int(255) DEFAULT '0',
//`category_id` int(11) DEFAULT '0',
//`child_category_id` int(255) DEFAULT '0',
//`on_sale` tinyint(1) NOT NULL DEFAULT '1',
//`quantity` int(11) NOT NULL DEFAULT '0',
//`price` float(8,2) DEFAULT NULL,
//`price_wholesale` float(8,2) DEFAULT '0.00',
//`show_price` tinyint(1) NOT NULL DEFAULT '0',
//`manufacturer_id` int(11) DEFAULT NULL,
//`product_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
//`reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
//`out_of_stock` tinyint(1) NOT NULL DEFAULT '0',
//`enabled` tinyint(1) NOT NULL DEFAULT '1',
//`condition` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'new',
//`type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
//`name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'standard',
//`short_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
//`description` text COLLATE utf8_unicode_ci,
//`slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
//`meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
//`meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
//`meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
//`created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
//`updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
//`view_count` int(5) NOT NULL DEFAULT '0',
//`installation_charges` tinyint(1) DEFAULT '1',
//`product_position` int(20) NOT NULL,

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
//        Schema::dropIfExists('product_images');
//        Schema::dropIfExists('category_product');
    }
}
