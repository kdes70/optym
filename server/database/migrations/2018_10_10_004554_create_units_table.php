<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitsTable extends Migration
{
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 8)->unique();
            $table->string('abbr', 32);
            $table->string('title', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('units');
    }
}
