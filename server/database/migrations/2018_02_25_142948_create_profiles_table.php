<?php

use App\Domain\User\Profile;
use App\Domain\User\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateProfilesTable
 */
class CreateProfilesTable extends Migration
{
    /** @var string */
    private $table;

    /**
     * CreateUsersTable constructor.
     */
    public function __construct()
    {
        $this->table = Profile::getTableName();
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->bigInteger('user_id')->unsigned()->nullable()->index()->primary();
            $table->string('first_name', 30)->nullable();
            $table->string('last_name', 30)->nullable();
            //$table->string('patronymic')->nullable();
            $table->boolean('sex')->default(0)->comment('1 - Мужской, 2 - Женский');
            $table->date('birthday')->nullable();
            $table->string('avatar')->nullable();
            $table->string('position')->nullable()->comment('Должность');
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->mediumText('notes')->nullable();
            $table->string('options')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on(User::getTableName())
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
    }
}
