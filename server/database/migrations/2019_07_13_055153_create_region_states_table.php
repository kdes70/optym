<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        if (Schema::connection('mysql_geo')->hasTable('regions')) {
//            Schema::connection('mysql_geo')->table('regions', function (Blueprint $table) {
//                $table->dropForeign('regions_country_id_foreign');
//            });
//
//            Schema::connection('mysql_geo')->dropIfExists('regions');
//        }


        Schema::create('geo_regions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('country_id', 2);
            $table->string('name', 200);
            $table->smallInteger('time_zone')->default(0);
            $table->smallInteger('order')->default(0);
            $table->nullableTimestamps();

            $table->foreign('country_id')->references('id')->on('geo_countries');

            $table->unique(['name', 'country_id']);
            $table->index(['name', 'country_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_regions');
    }
}
