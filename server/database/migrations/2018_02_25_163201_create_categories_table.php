<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('slug')->index();
            $table->string('path')->index()->nullable();
            $table->text('description')->nullable();
            $table->text('meta_description')->nullable();
            $table->string('image')->nullable();
            $table->boolean('enabled')->default(false);
//            $table->string('meta_json')->default('{}');
            NestedSet::columns($table);
            $table->nullableTimestamps();

        });

        //
//        Schema::create('category_companies', function (Blueprint $table) {
//            $table->unsignedInteger('company_id')->references('id')->on('companies')->onUpdate('cascade')->onDelete('CASCADE');;
//            $table->unsignedInteger('category_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('CASCADE');
//            $table->primary(['company_id', 'category_id']);
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
        // Schema::dropIfExists('category_companies');
    }
}
