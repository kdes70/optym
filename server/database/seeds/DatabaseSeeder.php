<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(AdminUserTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(UnitSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(RegionSeeder::class);
        $this->call(CompanySeeder::class);

        if (config('app.env') === 'local') {
            $this->call(UserTableSeeder::class);
//            $this->call(RegionsTableSeeder::class);
            $this->call(PageTableSeeder::class);
            $this->call(ProductSeeder::class);
        }


        //  $this->call(ChatMessagesTableSeeder::class);
    }
}
