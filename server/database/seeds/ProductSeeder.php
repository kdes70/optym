<?php

use App\Domain\Company\Company;
use App\Domain\Product\Image;
use App\Domain\Product\Product;
use App\Domain\Product\Unit;
use Illuminate\Database\Seeder;
use \App\Domain\Catalog\Category\Category;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $product_count_all = 0;

        for ($i = 0; $i < 10; $i++) {

            // получить категории только последних узлов
            $categoryes[$i] = (new Category)
                ->withDepth()
                ->having('depth', '=', 3)
                ->inRandomOrder()
                ->limit(rand(2, 4))
                ->get();

            $company = Company::inRandomOrder()->first();

            $cat_product_count = 0;

            foreach ($categoryes[$i] as $category) {

                if ($category->getId() && $company->getId()) {

                    $product_count = 0;

                    for ($p = 0; $p < rand(10, 40); $p++) {

                        $product[$p] = factory(Product::class)->create([
                            'unit_id' => Unit::inRandomOrder()->first()->getId(),
                            'company_id' => $company->getId(),
                            'category_id' => $category->getId(),
                        ]);

                        $main = factory(Image::class)->create();
                        $product[$p]->addImages([$main]);

                        $product[$p]->setMainImage($main->getId());
                        $product[$p]->save();

                        $images = factory(Image::class, rand(2, 5))->create();

                        $arr = [];
                        foreach ($images as $item) {
                            $arr[] = $item;
                        }

                        $product[$p]->addImages($arr);

                        $product_count = $product[$p]->getId() ? $p : null;
                    }

                    $cat_product_count += $product_count;
                }
            }

            $this->command->info($i . " - Созданно:" . " кат - " . $categoryes[$i]->count() . " прод - " . $cat_product_count);

            $product_count_all =+ $cat_product_count;
        }

        $this->command->info('Всего товаров: '. $product_count_all);

//        $this->command->info('Индексируем');

//        \Artisan::call('search:init');
    }
}
