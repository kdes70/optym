<?php

use App\Domain\Catalog\Category\Category;
use App\Domain\Catalog\Pages\Page;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Page::truncate();

        $categoryes = (new Category)
            ->withDepth()
//            ->groupBy('depth')
            ->having('depth', '=', 1)
            ->inRandomOrder()
            ->get();

//        dd($categoryes->count());

        foreach ($categoryes as $category) {
            factory(Page::class)->create([
                'category_id' => $category->getId(),
                'title'       => $category->getName(),
                'menu_title'  => $category->getName(),
                'slug'        => Str::slug($category->getName()),
                'enabled'     => true,
            ]);
        }


    }


}
