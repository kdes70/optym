<?php


use App\Domain\Company\Company;
use App\Domain\Product\Product;
use App\Domain\Product\Unit;
use App\Domain\User\Profile;
use App\Domain\User\User;
use Illuminate\Database\Seeder;
use App\Domain\Catalog\Category\Category;

// TODO удалить при релизе
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create('ru_RU');

        $is_company = $faker->boolean;

        factory(User::class, 10)
            ->state($is_company ? 'wholesale' : 'buyer')
            ->create()
            ->each(function (User $user) use ($faker, $is_company) {
                /** @var User $user */
                $user->profile()->save(factory(Profile::class)->create([
                    'user_id' => $user->getId(),
                    'avatar'  => 'no_avatar.jpg'
                ]));

                if ($is_company) {

                    $company = factory(Company::class)->create();
                    $user->company()->save($company);

                    // категору
                    $categoryes = (new Category)
                        ->withDepth()
                        ->having('depth', '=', 3)
                        ->inRandomOrder()
                        ->first()
                        ->getId();

                    $company->products()->saveMany(factory(Product::class, 3)->create([
                        'category_id' => $categoryes,
                        'unit_id'     => Unit::inRandomOrder()->first()->getId(),
                    ]));
                }
            });

    }
}
