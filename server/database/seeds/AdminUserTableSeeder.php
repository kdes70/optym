<?php

use App\Domain\Company\Company;
use App\Domain\User\Enums\UserStateEnum;
use App\Domain\User\Role;
use App\Domain\User\User;
use Illuminate\Database\Seeder;

// TODO удалить при релизе
class AdminUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = factory(User::class)->create([
            'username' => 'admin',
            'email'    => 'admin@admin.ru',
            'password' => bcrypt('111111'),
            'role'     => Role::ADMIN,
            'state'    => UserStateEnum::ACTIVE,
        ]);

        $admin->profile()->create([
            'user_id'    => $admin->id,
            'first_name' => 'Super',
            'last_name'  => 'Admin',
            'avatar'     => 'no_avatar.jpg',
        ]);

        $user = factory(User::class)->create([
            'username' => 'user_company',
            'email'    => 'company@admin.ru',
            'password' => bcrypt('111111'),
            'role'     => Role::WHOLESALE,
            'state'    => UserStateEnum::ACTIVE,
        ]);

        $user->profile()->create([
            'user_id'    => $user->id,
            'first_name' => 'User',
            'last_name'  => 'Company',
            'avatar'     => 'no_avatar.jpg',
        ]);

        $user->company()->save(factory(Company::class)->make());

    }
}
