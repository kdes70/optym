<?php


use App\Domain\Product\Unit;
use Illuminate\Database\Seeder;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $units = config('import.units');

        foreach ($units as $unit) {
            factory(Unit::class)->create([
                'code'  => $unit['code'],
                'title' => $unit['title'],
                'abbr'  => $unit['abbr'],
            ]);
        }
    }
}
