<?php

use App\Domain\Catalog\Category\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attributes = config('import.categories');

        Category::rebuildTree($this->array_values_recursive($attributes), true);
    }

    public function array_values_recursive($array)
    {
        $res = array();

        foreach ($array as $key => $value) {

            if (is_array($value)) {
                $res[] = [
                    'name'     => $key,
                    'slug'     => str_slug($key),
                    'image'    => 'https://picsum.photos/300/300',
                    'enabled'  => true,
                    'children' => $this->array_values_recursive($value),
                ];
            } else {
                $res[] = [
                    'name'    => $value,
                    'slug'    => str_slug($value),
                    'image'   => 'https://picsum.photos/300/300',
                    'enabled' => true,
                ];
            }
        }

        return $res;
    }


}
