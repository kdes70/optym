<?php

use Illuminate\Database\Seeder;
use Kdes70\Chatter\Models\Message;

// TODO удалить при релизе
class ChatMessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create('ru_RU');

        factory(Message::class, 5)->create();

    }
}
