<?php


use App\Domain\Region\oldRegion;
use Illuminate\Database\Seeder;

class oldRegionsTableSeeder extends Seeder
{
    public function run(): void
    {
        factory(oldRegion::class, 5)->create()->each(function(oldRegion $region) {
            $region->children()->saveMany(factory(oldRegion::class, random_int(3, 5))->create()->each(function(oldRegion $region) {
                $region->children()->saveMany(factory(oldRegion::class, random_int(3, 5))->make());
            }));
        });
    }
}
