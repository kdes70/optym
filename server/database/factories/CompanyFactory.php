<?php


use App\Domain\Company\Company;
use App\Domain\Company\Enums\CompanyTypeEnum;
use Illuminate\Support\Str;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Company::class, function (Faker\Generator $faker) {

    $company_name = "Компания {$faker->unique()->company}";

    // TODO оптимизировать структуру, вынести привязки к странам - городам в отдельную таблицу
    $location = [
        ['country' => 'Россия', 'region' => 'Москва', 'city' => 'Москва'],
        ['country' => 'Россия', 'region' => 'Томская область', 'city' => 'Томск'],
        ['country' => 'Россия', 'region' => 'Новосибирская область', 'city' => 'Новосибирск'],
        ['country' => 'Беларусь', 'region' => 'Минск', 'city' => 'Минск'],
        ['country' => 'Беларусь', 'region' => 'Брестская область', 'city' => 'Брест'],
    ];

    $company_location = $faker->randomElement($location);

    return [
        'name'         => $company_name,
        'slug'         => Str::slug($company_name),
        'tagline'      => $faker->companySuffix,
        'company_type' => function () {
            return CompanyTypeEnum::randomValue();
        },
        'description'  => $faker->text(250),
        'logo'         => 'https://picsum.photos/300/300',
        'country'      => $company_location['country'],
        'region'       => $company_location['region'],
        'city'         => $company_location['city'],
        'address'      => $faker->address,
        'zip'          => $faker->postcode,
        'phone'        => $faker->unique()->phoneNumber,
        'email'        => $faker->unique()->companyEmail,
        'site_link'    => $faker->domainName,
        'skype'        => $faker->userName,
        'scope_supply' => $faker->randomElement(array_keys(Company::getSupplyArray())),
        'min_amount'   => $faker->randomFloat(2, 10000, 100000),
        //        'other_contacts',
        //        'legal_other',
        //        'contact_person',
        'reg_data'     => $faker->dateTimeAd(),
        //        'tags' => ,
        //        'meta_description'
    ];
});
