<?php


use App\Domain\Region\oldRegion;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(oldRegion::class, function (Faker $faker) {

    $city = $faker->unique()->city;

    return [
        'name' => $city,
        'slug' => str_slug($city),
        'parent_id' => null,
    ];
});
