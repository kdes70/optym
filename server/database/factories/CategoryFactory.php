<?php

use App\Domain\Catalog\Category\Category;

$factory->define(Category::class, function (Faker\Generator $faker) {
    return [
        'name'      => $faker->unique()->name,
        'slug'      => $faker->unique()->slug(2),
        'parent_id' => null,
        'image'     => 'https://picsum.photos/300/300',
        'enabled'   => false,
    ];
});