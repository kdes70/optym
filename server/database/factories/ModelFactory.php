<?php

use App\Domain\Product\Image;
use App\Domain\Product\Product;
use App\Domain\Product\Unit;
use Kdes70\Chatter\Models\Message;


/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Message::class, function (Faker\Generator $faker) {

    $user_ids = $faker->randomElements([1, 22], 2);

    $conversation = DB::table('messages_conversation')
        ->select('id')
        ->where('user_one', $user_ids[0])
        ->orWhere('user_two', $user_ids[0])->get();

    if (empty($conversation->toArray())) {

        $conversation_id = DB::table('messages_conversation')->insert([
            'user_one' => $user_ids[0],
            'user_two' => $user_ids[1],
        ]);

    } else {
        $conversation_id = $conversation->toArray()[0]->id;
    }

    return [
        'sender_user_id'    => $user_ids[0],
        'recipient_user_id' => $user_ids[1],
        'conversation_id'   => $conversation_id,
        'message'           => $faker->sentence(4),
        'status'            => 1,
    ];

});


$factory->define(Image::class, function (Faker\Generator $faker) {
    return [
        'photo_id'   => function () {
            return factory(Product::class)->create()->getId();
        },
        'photo_type' => 'product',
        'path'           => 'https://picsum.photos/300/250',
        'title'          => $faker->word,
        'alt'            => $faker->word,
        'order'          => $faker->randomDigitNotNull,
    ];
});


$factory->define(Unit::class, function () {
    return [
        'code'  => $code = str_random(3),
        'title' => 'Unit ' . $code,
        'abbr'  => $code,
    ];
});

// product
