<?php

use App\Domain\User\Profile;
use App\Domain\User\User;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Profile::class, function (Faker\Generator $faker) {

//    $user_id = factory(User::class)->create()->id;

    return [
        //        'user_id'    => $user_id,
        'first_name' => $faker->firstName,
        'last_name'  => $faker->lastName,
        'avatar'     => 'no_avatar.jpg',
    ];
});

$factory->defineAs(Profile::class, 'profile_user', function (Faker\Generator $faker) {

    $user_id = factory(User::class)->create()->id;

    return [
        'user_id'    => (integer)$user_id,
        'first_name' => $faker->word,
        'last_name'  => $faker->word,
        'birthday'   => $faker->date(),
    ];
});