<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domain\User\Enums\UserStateEnum;
use App\Domain\User\Enums\UserTypeEnum;
use App\Domain\User\Role;
use App\Domain\User\User;
use Illuminate\Support\Str;

$factory->define(User::class, function (Faker\Generator $faker) {

    $faker = Faker\Factory::create('ru_RU');

    $active = $faker->boolean;

    return [
        'username'       => $faker->userName,
        'password'       => bcrypt('secret'),
        'email'          => $faker->unique()->safeEmail,
        'phone'          => clear_phone($faker->phoneNumber),
        'verify_token'   => $active ? null : Str::uuid(),
        'api_token'      => Str::random(100),
        'remember_token' => Str::random(100),
        'role'           => function () use ($faker) {
            return $faker->randomElement([
                Role::USER,
                Role::WHOLESALE,
                Role::BUYER,
                Role::MODERATOR,
                Role::ADMIN
            ]);
        },
        'state'          => $active ? UserStateEnum::ACTIVE : UserStateEnum::WAIT,
    ];
});

$factory->state(User::class, 'type_user', function () {
    return [
        'type' => UserTypeEnum::USER,
    ];
});

$factory->state(User::class, 'wholesale', function () {
    return [
        'type' => function () {
            return new UserTypeEnum(UserTypeEnum::WHOLESALE);
        },
        'role' => Role::WHOLESALE,
    ];
});

$factory->state(User::class, 'buyer', function () {
    return [
        'type' => UserTypeEnum::BUYER,
        'role' => Role::BUYER,
    ];
});

$factory->state(User::class, 'admin', function () {
    return [
        'role' => Role::ADMIN,
    ];
});

$factory->state(User::class, 'active', function () {
    return [
        'state' => UserStateEnum::ACTIVE,
    ];
});
