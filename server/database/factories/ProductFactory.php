<?php

use App\Domain\Catalog\Category\Category;
use App\Domain\Company\Company;
use App\Domain\Product\Image;
use App\Domain\Product\Product;
use App\Domain\Product\Unit;
use Carbon\Carbon;
use Faker\Generator;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Product::class, function (Faker\Generator $faker) {
    return [
        'company_id' => Company::inRandomOrder()->first() ? Company::inRandomOrder()->first()->getId() : factory(Company::class)->create()->getId(),
        'category_id' => Category::inRandomOrder()->first() ? Category::inRandomOrder()->first()->getId() : factory(Category::class)->create()->getId(),
        //        'unit_id'      => function () {
        //            return factory(Unit::class)->create()->getId();
        //        },
        'title' => $title = "Товар {$faker->unique()->sentence}",
        'sku' => substr($faker->uuid, 24),
        'slug' => str_slug($title),
        'cost_price' => $faker->randomFloat(2, 0, 10000),
        'retail_price' => $faker->randomFloat(2, 0, 100000),
        'weight' => $faker->randomNumber(2),
        'state' => Product::STATE_AVAILABLE,
        'available_at' => Carbon::now(),
        'description' => $faker->paragraph,
        'badges' => '{}',
    ];
});

$factory->state(Product::class, 'draft', function () {
    return [
        'exchange_rate' => 0,
        'available_at' => null,
        'state' => Product::STATE_DRAFT,
    ];
});
