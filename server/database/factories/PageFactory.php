<?php

use App\Domain\Catalog\Pages\Page;
use Illuminate\Support\Str;

$factory->define(Page::class, function (Faker\Generator $faker) {

    return [
        //  'category_id' => $category->getId() ?? factory(Category::class)->create()->getId(),
        'title'            => $title = $faker->unique()->name,
        'slug'             => Str::slug($title),
        'menu_title'       => $title,
        'image'            => 'https://picsum.photos/300/300',
        'banner'           => 'https://picsum.photos/1200/300',
        'meta_description' => $faker->text(100),
        'meta_keywords'    => $faker->text(10),
        'content'          => $faker->text(200),
        'enabled'          => false,
    ];
});