<?php

if (!function_exists('clear_phone')) {
    /**
     * Clear the phone number.
     *
     * @param string $phone
     *
     * @return string
     */
    function clear_phone($phone)
    {
        return preg_replace('~[\D+]~', '', $phone);
    }
}

if (!function_exists('sd')) {
    /**
     * Non-dying version of dd().
     */
    function sd()
    {
        array_map(function ($x) {
            (new \Illuminate\Support\Debug\Dumper)->dump($x);
        }, func_get_args());
    }
}

if (!function_exists('is_using_trait')) {
    function is_using_trait($object, $trait)
    {
        $traits = trait_uses_recursive($object);

        return in_array($trait, $traits, true);
    }
}

if (!function_exists('kd_timestamp')) {
    function kd_timestamp($datetime)
    {
        if ($datetime instanceof DateTime) {
            $datetime = \Carbon\Carbon::instance($datetime);
        }

        return $datetime instanceof \Carbon\Carbon ? $datetime->getTimestamp() : null;
    }
}

if (!function_exists('paginated')) {
    /**
     * Register a new paginate routes with the router.
     *
     * @param string $uri
     * @param \Closure|array|string $action
     *
     * @return \Illuminate\Routing\Route
     */
    function paginated($uri, $action)
    {
        return app('router')->paginated($uri, $action);
    }
}

if (!function_exists('kd_array_key_by')) {
    /**
     * New associative array with keys by a field
     *
     * @param array $array
     * @param string $key
     *
     * @return array
     */
    function kd_array_key_by(array $array, string $key)
    {
        return array_combine(array_pluck($array, $key), $array);
    }
}

if (!function_exists('getRealUserIp')) {
    function getRealUserIp()
    {
        switch (true) {
            case (!empty($_SERVER['HTTP_X_REAL_IP'])) :
                return $_SERVER['HTTP_X_REAL_IP'];
            case (!empty($_SERVER['HTTP_CLIENT_IP'])) :
                return $_SERVER['HTTP_CLIENT_IP'];
            case (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) :
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
            default :
                return $_SERVER['REMOTE_ADDR'];
        }
    }
}

if (!function_exists('assetImg')) {
    /**
     * Generate an asset path for the application.
     *
     * @param string $path
     * @param bool $secure
     * @return string
     */
    function assetImg($path, $secure = null)
    {
        return app('url')->asset('img/thems/' . config('optum.admin.theme') . '/' . $path, $secure);
    }
}

if (!function_exists('asset_admin')) {
    /**
     * Generate an asset path for the application.
     *
     * @param string $path
     * @param bool $secure
     * @return string
     */
    function asset_admin($path, $secure = null)
    {
        return app('url')->asset('admin-theme/assets/' . $path, $secure);
    }
}
