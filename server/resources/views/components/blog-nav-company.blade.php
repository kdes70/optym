<!-- /resources/views/components/blog-nav-company.blade.php -->
<div class="card list-group">
    <div class="list-group-item list-group-item-gray">
        <div class="media btm-space-zero">
            <div class="media-body">
                <div class="dropdown">
                    <a href="javascript:void(0)"
                       class="dropdown-toggle"
                       data-toggle="dropdown"
                       aria-haspopup="true"
                       aria-expanded="false">
                        <b>{{ $company->name }}
                            &nbsp;
                            <span class="caret"></span></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            {{--                                                                    <a href="{{ route('cabinet.company.edit') }}" class="">Редактировать информацию</a>--}}
                        </li>
                        <li>
                            <a href="javascript:void(0)">Выбрать другую компанию</a>
                        </li>
                        <li>
                            <a href="#/company/delete/id">Удалить компанию</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <a class="list-group-item active" href="{{ route('profiles.home', ['username' => $user->getUsername()]) }}">Моя анкета</a>
    <a class="list-group-item" href="{{route('messages')}}">Сообщения</a>
    <a class="list-group-item" href="#/order">Заказы</a>
    <a class="list-group-item ng-hide" href="#/product">Товары</a>

</div>