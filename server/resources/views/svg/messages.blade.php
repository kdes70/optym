<svg height="28" version="1.1" width="28" xmlns="http://www.w3.org/2000/svg"
     xmlns:xlink="http://www.w3.org/1999/xlink"
     style="overflow: hidden; position: relative; left: -0.515625px;"
     id="canvas-for-livicon-1">
    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël
        2.1.2
    </desc>
    <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
    <path fill="#42aaca" stroke="none"
          d="M24,16.931V27.059C24,27.578,23.578,28,23.059,28H2.941C2.422,28,2,27.578,2,27.059V15.035L6,18.32L2.9,27.1L7.577999999999999,19.672L11.517,22.516000000000002C12.334999999999999,23.161,13.663,23.161,14.482,22.516000000000002L18.421,19.672L23.218,27.125L20,18.32L22.225,16.493000000000002C22.79,16.704,23.383,16.853,24,16.931ZM14.482,20.818L20.936999999999998,15.880000000000003C19.438,14.991,19,14,19,12H2.941C2.422,12,2,12.422,2,12.941V13.537L11.518,20.818C12.336,21.459,13.664,21.459,14.482,20.818Z"
          stroke-width="0" transform="matrix(0.875,0,0,0.875,0,0)"
          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
    <path fill="#42aaca" stroke="none"
          d="M24,14H22V4H24V14ZM30,12C26,12,28,10,24,10C24.688,8,24.75,6.125,24,4C28,4,26,6,30,6C30,8,30,10,30,12Z"
          stroke-width="0" transform="matrix(0.875,0,0,0.875,0,0)"
          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
</svg>