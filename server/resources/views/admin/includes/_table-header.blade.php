<div class="page-header light-fg row no-gutters align-items-center justify-content-between">

    <!-- APP TITLE -->
    <div class="col-12 col-sm">

        <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
            <div class="logo-icon mr-3 mt-1">
                <i class="icon-cube-outline s-6"></i>
            </div>
            <div class="logo-text">
                <div class="h4">{{$title}}</div>
                <div class="">Total: {{$total}}</div>
            </div>
        </div>

    </div>
    <!-- / APP TITLE -->

    <!-- SEARCH -->
    <div class="col search-wrapper px-2">
        <div class="input-group">
            <span class="input-group-btn">
                <button class="btn btn-icon">
                    <i class="icon icon-magnify"></i>
                </button>
            </span>
            <input name="q" value="{{ request()->query('q') }}" id="products-search-input" type="text" class="form-control" placeholder="Search" aria-label="Search" />
        </div>
    </div>
    <!-- / SEARCH -->

    <div class="col-auto">
        <a href="{{$add_url}}" class="btn btn-info btn-fab fuse-ripple-ready">
            <i class="icon-plus"></i>
        </a>
    </div>

</div>
