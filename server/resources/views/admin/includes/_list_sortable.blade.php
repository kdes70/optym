@if(!$loop->first)
    <form method="POST" action="{{ route('admin.'.$route.'.first', $model) }}" class="mr-1">
        @csrf
        <button class="btn btn-icon btn-sm fuse-ripple-ready" aria-label="Product details">
            <i class="icon icon-chevron-double-up s-4"></i>
        </button>
    </form>
@endif
<form method="POST" action="{{ route('admin.'.$route.'.up', $model) }}" class="mr-1">
    @csrf
    <button class="btn btn-icon btn-sm fuse-ripple-ready" aria-label="Product details">
        <i class="icon icon-chevron-up s-4"></i>
    </button>
</form>
<form method="POST" action="{{ route('admin.'.$route.'.down', $model) }}" class="mr-1">
    @csrf
    <button class="btn btn-icon btn-sm fuse-ripple-ready" aria-label="Product details">
        <i class="icon icon-chevron-down s-4"></i>
    </button>
</form>
@if (!$loop->last)
    <form method="POST" action="{{ route('admin.'.$route.'.last', $model) }}" class="mr-1">
        @csrf
        <button class="btn btn-icon btn-sm fuse-ripple-ready" aria-label="Product details">
            <i class="icon icon-chevron-double-down s-4"></i>
        </button>
    </form>
@endif