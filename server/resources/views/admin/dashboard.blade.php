@extends('layouts.admin')

@section('content')

<div class="content custom-scrollbar">

    <div id="project-dashboard" class="page-layout simple right-sidebar">

        <div class="page-content-wrapper custom-scrollbar">

            <!-- HEADER -->
            <div class="page-header bg-primary text-auto d-flex flex-column justify-content-between px-6 pt-4 pb-0">

                <div class="row no-gutters align-items-start justify-content-between flex-nowrap">

                    <div>
                        <span class="h2">{{__('main.admin.welcome')}}, {{auth()->user()->name}}!</span>
                    </div>

                    <button type="button" class="sidebar-toggle-button btn btn-icon d-block d-xl-none" data-fuse-bar-toggle="dashboard-project-sidebar" aria-label="Toggle sidebar">
                        <i class="icon icon-menu"></i>
                    </button>
                </div>

                <div class="row no-gutters align-items-center project-selection">

                    <div class="selected-project h6 px-4 py-2">ACME Corp. Backend App</div>

                    <div class="project-selector">
                        <button type="button" class="btn btn-icon">
                            <i class="icon icon-dots-horizontal"></i>
                        </button>
                    </div>

                </div>

            </div>
            <!-- / HEADER -->

            <!-- CONTENT -->
            <div class="page-content">

                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link btn active" id="home-tab" data-toggle="tab" href="#home-tab-pane" role="tab" aria-controls="home-tab-pane" aria-expanded="true">Home</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade show active p-3" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab">

                        <!-- WIDGET GROUP -->
                        <div class="widget-group row no-gutters">

                            <!-- WIDGET 1 -->
                            <div class="col-12 col-sm-6 col-xl-3 p-3">

                                <div class="widget widget1 card">

                                    <div class="widget-header pl-4 pr-2 row no-gutters align-items-center justify-content-between">

                                        <div class="col">

                                            <select class="h6 custom-select">
                                                <option selected="" value="today">Today</option>
                                                <option value="yesterday">Yesterday</option>
                                                <option value="tomorrow">Tomorrow</option>
                                            </select>

                                        </div>

                                        <button type="button" class="btn btn-icon">
                                            <i class="icon icon-dots-vertical"></i>
                                        </button>

                                    </div>

                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">

                                        <div class="title text-secondary">25</div>

                                        <div class="sub-title h6 text-muted">DUE TASKS</div>

                                    </div>

                                    <div class="widget-footer p-4 bg-light row no-gutters align-items-center">

                                        <span class="text-muted">Completed:</span>

                                        <span class="ml-2">7</span>

                                    </div>
                                </div>
                            </div>
                            <!-- / WIDGET 1 -->

                            <!-- WIDGET 2 -->
                            <div class="col-12 col-sm-6 col-xl-3 p-3">

                                <div class="widget widget2 card">

                                    <div class="widget-header pl-4 pr-2 row no-gutters align-items-center justify-content-between">

                                        <div class="col">
                                            <span class="h6">Overdue</span>
                                        </div>

                                        <button type="button" class="btn btn-icon">
                                            <i class="icon icon-dots-vertical"></i>
                                        </button>
                                    </div>

                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">
                                        <div class="title text-danger">4</div>
                                        <div class="sub-title h6 text-muted">TASKS</div>
                                    </div>

                                    <div class="widget-footer p-4 bg-light row no-gutters align-items-center">
                                        <span class="text-muted">Yesterday's:</span>
                                        <span class="ml-2">2</span>
                                    </div>
                                </div>
                            </div>
                            <!-- / WIDGET 2 -->

                            <!-- WIDGET 3 -->
                            <div class="col-12 col-sm-6 col-xl-3 p-3">

                                <div class="widget widget3 card">

                                    <div class="widget-header pl-4 pr-2 row no-gutters align-items-center justify-content-between">

                                        <div class="col">
                                            <span class="h6">Issues</span>
                                        </div>

                                        <button type="button" class="btn btn-icon">
                                            <i class="icon icon-dots-vertical"></i>
                                        </button>

                                    </div>

                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">
                                        <div class="title text-warning">32</div>
                                        <div class="sub-title h6 text-muted">OPEN</div>
                                    </div>

                                    <div class="widget-footer p-4 bg-light row no-gutters align-items-center">
                                        <span class="text-muted">Closed today:</span>
                                        <span class="ml-2">0</span>
                                    </div>
                                </div>
                            </div>
                            <!-- / WIDGET 3 -->

                            <!-- WIDGET 4 -->
                            <div class="col-12 col-sm-6 col-xl-3 p-3">

                                <div class="widget widget4 card">

                                    <div class="widget-header pl-4 pr-2 row no-gutters align-items-center justify-content-between">

                                        <div class="col">
                                            <span class="h6">Features</span>
                                        </div>

                                        <button type="button" class="btn btn-icon">
                                            <i class="icon icon-dots-vertical"></i>
                                        </button>

                                    </div>

                                    <div class="widget-content pt-2 pb-8 d-flex flex-column align-items-center justify-content-center">
                                        <div class="title text-info">42</div>
                                        <div class="sub-title h6 text-muted">PROPOSALS</div>
                                    </div>

                                    <div class="widget-footer p-4 bg-light row no-gutters align-items-center">
                                        <span class="text-muted">Implemented:</span>
                                        <span class="ml-2">8</span>
                                    </div>
                                </div>
                            </div>
                            <!-- / WIDGET 4 -->

                           {{-- <!-- WIDGET 5 -->
                            <div class="col-12 p-3">

                                <div class="widget widget5 card">

                                    <div class="widget-header px-4 row no-gutters align-items-center justify-content-between">

                                        <div class="col">
                                            <span class="h6">Github Issues</span>
                                        </div>

                                        <div>

                                            <button type="button" class="widget5-option-change-btn btn btn-link" data-interval="TW">
                                                This Week
                                            </button>

                                            <button type="button" class="widget5-option-change-btn btn btn-link" data-interval="LW">
                                                Last Week
                                            </button>

                                            <button type="button" class="widget5-option-change-btn btn btn-link" data-interval="2W">
                                                2 WEEKS AGO
                                            </button>

                                        </div>
                                    </div>

                                    <div class="widget-content p-4">

                                        <div class="row">

                                            <div class="col-12 col-lg-6">

                                                <div id="widget5-main-chart">
                                                    <svg></svg>
                                                </div>

                                            </div>

                                            <div class="col-12 col-lg-6">

                                                <div id="widget5-supporting-charts" class="row">

                                                    <div class="col-6 p-4">

                                                        <div id="widget5-created-chart">
                                                            <div class="h6 text-muted">CREATED</div>
                                                            <div class="h2 count">48</div>
                                                            <div class="chart-wrapper">
                                                                <svg></svg>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-6 p-4">

                                                        <div id="widget5-closed-chart">
                                                            <div class="h6 text-muted">CLOSED</div>
                                                            <div class="h2 count">26</div>
                                                            <div class="chart-wrapper">
                                                                <svg></svg>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-6 p-4">

                                                        <div id="widget5-re-opened-chart">
                                                            <div class="h6 text-muted">RE-OPENED</div>
                                                            <div class="h2 count">2</div>
                                                            <div class="chart-wrapper">
                                                                <svg></svg>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-6 p-4">

                                                        <div id="widget5-wont-fix-chart">
                                                            <div class="h6 text-muted">WON'T FIX</div>
                                                            <div class="h2 count">4</div>
                                                            <div class="chart-wrapper">
                                                                <svg></svg>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-6 p-4">

                                                        <div id="widget5-needs-test-chart">
                                                            <div class="h6 text-muted">NEED'S TEST</div>
                                                            <div class="h2 count">8</div>
                                                            <div class="chart-wrapper">
                                                                <svg></svg>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-6 p-4">

                                                        <div id="widget5-fixed-chart">
                                                            <div class="h6 text-muted">FIXED</div>
                                                            <div class="h2 count">14</div>
                                                            <div class="chart-wrapper">
                                                                <svg></svg>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- / WIDGET 5 -->

                            <!-- WIDGET 6 -->
                            <div class="col-12 col-lg-6 p-3">

                                <div class="widget widget6 card">

                                    <div class="widget-header px-4 row no-gutters align-items-center justify-content-between">

                                        <div class="col">
                                            <span class="h6">Task Distrubition</span>
                                        </div>

                                        <div class="">
                                            <select id="widget6-option-select" class="h6 custom-select">
                                                <option selected="" value="TW">This Week</option>
                                                <option value="LW">Last Week</option>
                                                <option value="2W">2 Weeks Ago</option>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="widget-content">

                                        <div class="row no-gutters">

                                            <div class="col-12">

                                                <div id="widget6-main-chart" class="p-4">
                                                    <svg></svg>
                                                </div>

                                            </div>

                                            <div class="divider col-12"></div>

                                            <div id="added-tasks" class="col-6 d-flex flex-column align-items-center justify-content-center py-4">

                                                <div class="count h2">594</div>

                                                <div class="count-title">Tasks Added</div>

                                            </div>

                                            <div id="completed-tasks" class="col-6 d-flex flex-column align-items-center justify-content-center py-4">

                                                <div class="count h2">287</div>

                                                <div class="count-title">Tasks Completed</div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- / WIDGET 6 -->

                            <!-- WIDGET 7 -->
                            <div class="col-12 col-lg-6 p-3">

                                <div class="widget widget-7 card">

                                    <div class="widget-header px-4 row no-gutters align-items-center justify-content-between">

                                        <div class="col">
                                            <span class="h6">Schedule</span>
                                        </div>

                                        <div class="">
                                            <select class="h6 custom-select">
                                                <option selected="" value="today">Today</option>
                                                <option value="yesterday">Yesterday</option>
                                                <option value="tomorrow">Tomorrow</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="widget-content p-4">

                                        <div class="py-4 row no-gutters align-items-center justify-content-between">

                                            <div class="col">

                                                <div class="h6">Group Meeting</div>

                                                <div>
                                                    <span class="text-muted">In 32 minutes</span>

                                                    <span>, Room 1B</span>

                                                </div>

                                            </div>

                                            <div class="col-auto">
                                                <button type="button" class="btn btn-icon">
                                                    <i class="icon icon-dots-vertical"></i>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="py-4 row no-gutters align-items-center justify-content-between">

                                            <div class="col">

                                                <div class="h6">Coffee Break</div>

                                                <div>
                                                    <span class="text-muted">10:30 AM</span>

                                                </div>

                                            </div>

                                            <div class="col-auto">
                                                <button type="button" class="btn btn-icon">
                                                    <i class="icon icon-dots-vertical"></i>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="py-4 row no-gutters align-items-center justify-content-between">

                                            <div class="col">

                                                <div class="h6">Public Beta Release</div>

                                                <div>
                                                    <span class="text-muted">11:00 AM</span>

                                                </div>

                                            </div>

                                            <div class="col-auto">
                                                <button type="button" class="btn btn-icon">
                                                    <i class="icon icon-dots-vertical"></i>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="py-4 row no-gutters align-items-center justify-content-between">

                                            <div class="col">

                                                <div class="h6">Lunch</div>

                                                <div>
                                                    <span class="text-muted">12:10 PM</span>

                                                </div>

                                            </div>

                                            <div class="col-auto">
                                                <button type="button" class="btn btn-icon">
                                                    <i class="icon icon-dots-vertical"></i>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="py-4 row no-gutters align-items-center justify-content-between">

                                            <div class="col">

                                                <div class="h6">Dinner with David</div>

                                                <div>
                                                    <span class="text-muted">17:30 PM</span>

                                                </div>

                                            </div>

                                            <div class="col-auto">
                                                <button type="button" class="btn btn-icon">
                                                    <i class="icon icon-dots-vertical"></i>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="py-4 row no-gutters align-items-center justify-content-between">

                                            <div class="col">

                                                <div class="h6">Jane&#39;s Birthday Party</div>

                                                <div>
                                                    <span class="text-muted">19:30 PM</span>

                                                </div>

                                            </div>

                                            <div class="col-auto">
                                                <button type="button" class="btn btn-icon">
                                                    <i class="icon icon-dots-vertical"></i>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="py-4 row no-gutters align-items-center justify-content-between">

                                            <div class="col">

                                                <div class="h6">Overseer&#39;s Retirement Party</div>

                                                <div>
                                                    <span class="text-muted">21:30 PM</span>

                                                </div>

                                            </div>

                                            <div class="col-auto">
                                                <button type="button" class="btn btn-icon">
                                                    <i class="icon icon-dots-vertical"></i>
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- / WIDGET 7 -->--}}
                        </div>
                        <!-- / WIDGET GROUP -->
                    </div>
                </div>

            </div>
            <!-- / CONTENT -->

        </div>

    </div>

    {{--<script type="text/javascript" src="{{asset_admin('js/apps/dashboard/project.js')}}"></script>--}}

</div>

@endsection
