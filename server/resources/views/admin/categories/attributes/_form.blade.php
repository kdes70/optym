<div class="row">
    <div class="col">
        <div class="form-group">
            <label for="title" class="col-form-label">Title</label>
            <input id="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                   name="title"
                   value="{{ isset($attribute->title) ?  old('title', $attribute->title) :  old('title') }}"
                   required>
            @if ($errors->has('title'))
                <span class="invalid-feedback"><strong>{{ $errors->first('title') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="slug" class="col-form-label">Slug</label>
            <input id="slug" class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}"
                   name="slug"
                   value="{{ isset($attribute->slug) ?  old('slug', $attribute->slug) :  old('slug') }}"
                   required>
            @if ($errors->has('slug'))
                <span class="invalid-feedback"><strong>{{ $errors->first('slug') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="type" class="col-form-label">Type</label>
            <select id="type"
                    class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}"
                    name="type">
                @foreach ($types as $type => $label)
                    <option value="{{ $type }}"{{ $type == old('type') ? ' selected' : '' }}>{{ $label }}</option>
                @endforeach;
            </select>
            @if ($errors->has('type'))
                <span class="invalid-feedback"><strong>{{ $errors->first('type') }}</strong></span>
            @endif
        </div>
    </div>
</div>

<div class="form-check">
    <label class="form-check-label">
        <input class="form-check-input" type="checkbox" value="1" name="required" {{ old('required') ? 'checked' : '' }}>
        <span class="checkbox-icon"></span>
        <span class="form-check-description">Is required</span>
    </label>
    @if ($errors->has('required'))
        <span class="invalid-feedback"><strong>{{ $errors->first('required') }}</strong></span>
    @endif
</div>






