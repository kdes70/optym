@extends('layouts.admin')

@section('content')
    <div id="pages" class="page-layout simple tabbed">

        <!-- HEADER -->
    @include('admin.includes._page-header' , [
        'title' => 'Attibute '. $attribute->title,
        'back_link' => route('admin.categories.show', $category),
        'logo' => null,
      ])
    <!-- / HEADER -->

        <!-- CONTENT -->
        <div class="page-content">

            <div class="card p-6">
                <div class="d-flex flex-row mb-3">
                    <a href="{{ route('admin.categories.attributes.edit', [$category, $attribute]) }}"
                       class="btn btn-primary mr-1">Edit</a>
                    <form method="POST"
                          action="{{ route('admin.categories.attributes.destroy', [$category, $attribute]) }}"
                          class="mr-1">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger">Delete</button>
                    </form>
                </div>

                <table class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <th>ID</th>
                        <td>{{ $attribute->id }}</td>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <td>{{ $attribute->title }}</td>
                    </tr>
                    <tr>
                        <th>Slug</th>
                        <td>{{ $attribute->slug }}</td>
                    </tr>
                    <tbody>
                    </tbody>
                </table>

                <div class="py-3">
                    <p><a href="{{ route('admin.categories.attributes.value.create', [$category, $attribute]) }}"
                          class="btn btn-success">Add Value</a>
                    </p>
                </div>
            </div>

            @if($attribute->values)
                @include('admin.categories.attributes.value._list')
            @endif

        </div>
        <!-- / CONTENT -->
    </div>

@endsection

@section('scripts')
@endsection