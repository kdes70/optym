<table class="table table-bordered">
    <thead>
    <tr>
        {{--<th>Sort</th>--}}
        <th>Name</th>
        <th>Slug</th>
        <th>Required</th>
    </tr>
    </thead>
    <tbody>

    <tr><th colspan="4">Parent attributes</th></tr>

    @forelse ($parent_attributes as $attribute)
        <tr>
            {{--<td>{{ $attribute->sort }}</td>--}}
            <td>
                <a href="{{ route('admin.categories.attributes.show', [$category, $attribute]) }}">{{ $attribute->title }}</a>
            </td>
            <td>{{ $attribute->type }}</td>
            <td>{{ $attribute->required ? 'Yes' : '' }}</td>
        </tr>
    @empty
        <tr><td colspan="4">None</td></tr>
    @endforelse

    <tr><th colspan="4">Own attributes</th></tr>

    @forelse ($attributes as $attribute)
        <tr>
            {{--<td>{{ $attribute->sort }}</td>--}}
            <td>
                <a href="{{ route('admin.categories.attributes.show', [$category, $attribute]) }}">{{ $attribute->title }}</a>
            </td>
            <td>{{ $attribute->type }}</td>
            <td>{{ $attribute->required ? 'Yes' : '' }}</td>
        </tr>
    @empty
        <tr><td colspan="4">None</td></tr>
    @endforelse

    </tbody>
</table>