@extends('layouts.admin')

@section('content')
    <div id="pages" class="page-layout simple tabbed">

        <!-- HEADER -->
    @include('admin.includes._page-header' , [
        'title' => 'New Attribute',
        'back_link' => route('admin.categories.show', $category),
        'logo' => null,
      ])
    <!-- / HEADER -->

        <!-- CONTENT -->
        <div class="page-content">

            <div class="card p-6">
                <form id="form_save" method="POST" action="{{ route('admin.categories.attributes.store', $category) }}">
                    @csrf
                    @include('admin.categories.attributes._form')
                </form>
            </div>
        </div>
        <!-- / CONTENT -->
    </div>

@endsection

@section('scripts')
@endsection