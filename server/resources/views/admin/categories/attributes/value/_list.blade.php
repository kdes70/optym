<div class="card p-6">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Value</th>
            <th>Slug</th>
            <th>Sku</th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        <tr>
            <th colspan="4">Parent attributes</th>
        </tr>

        @forelse ($attribute->values as $value)
            <tr>
                <td>{{ $value->value_id }}</td>
                <td> {{ $value->getTitle() }}</td>
                <td>{{ $value->getSlug() }}</td>
                <td>{{ $value->getSku() }}</td>
                <td>
                    <div class="d-flex flex-row">
                        <a href="{{ route('admin.categories.attributes.value.edit', [$category, $attribute, $value]) }}"
                           class="btn btn-primary mr-1">Edit</a>
                        <form method="POST"
                              action="{{ route('admin.categories.attributes.value.destroy', [$category, $attribute, $value]) }}"
                              class="mr-1">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">Delete</button>
                        </form>
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="4">None</td>
            </tr>
        @endforelse

        </tbody>
    </table>
</div>