@extends('layouts.admin')

@section('content')
    <div id="pages" class="page-layout simple tabbed">

        <!-- HEADER -->
    @include('admin.includes._page-header' , [
        'title' => 'Edit Attibute Value',
        'back_link' => route('admin.categories.attributes.show', [$category, $attribute]),
        'logo' => null,
      ])
    <!-- / HEADER -->

        <!-- CONTENT -->
        <div class="page-content">

            <div class="card p-6">
                <form id="form_save"
                      method="POST"
                      action="{{ route('admin.categories.attributes.value.update', [$category, $attribute, $value]) }}">
                    @csrf
                    @method('PUT')

                    @include('admin.categories.attributes.value._form')
                </form>
            </div>
        </div>
        <!-- / CONTENT -->
    </div>

@endsection

@section('scripts')
@endsection