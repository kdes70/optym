@extends('layouts.admin')

@section('content')
    <div id="pages" class="page-layout simple tabbed">

        <!-- HEADER -->
    @include('admin.includes._page-header' , [
        'title' => 'Category '. $category->name,
        'back_link' => route('admin.categories.index'),
        'logo' => null,
      ])
    <!-- / HEADER -->

        <!-- CONTENT -->
        <div class="page-content">

            <div class="card p-6">
                <div class="d-flex flex-row mb-3">
                    <a href="{{ route('admin.categories.create') }}" class="btn btn-success">Add Category</a>
                    <a href="{{ route('admin.categories.edit', $category) }}" class="btn btn-primary mr-1">Edit</a>
                    <form method="POST" action="{{ route('admin.categories.destroy', $category) }}" class="mr-1">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger">Delete</button>
                    </form>
                </div>

                <table class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <th>ID</th>
                        <td>{{ $category->id }}</td>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <td>{{ $category->name }}</td>
                    </tr>
                    <tr>
                        <th>Slug</th>
                        <td>{{ $category->slug }}</td>
                    </tr>
                    <tbody>
                    </tbody>
                </table>

                <p><a href="{{ route('admin.categories.attributes.create', $category) }}" class="btn btn-success">Add
                        Attribute</a></p>

                @include('admin.categories.attributes._list', [$parent_attributes, $attributes])

            </div>
        </div>
        <!-- / CONTENT -->
    </div>

@endsection

@section('scripts')
@endsection