@extends('layouts.admin')

@section('content')

    <div class="page-layout carded full-width">

        <div class="top-bg bg-primary text-auto"></div>

        <!-- CONTENT -->
        <div class="page-content-wrapper">

            <form>
                <!-- HEADER -->
            @include('admin.includes._table-header', [
                'title' => __('Сатегории'),
                'total' => $categories->count(),
                'add_url' => route('admin.categories.index')
            ])
            <!-- / HEADER -->
            </form>

            <div class="page-content-card">

                <div class="toolbar p-6">Content Toolbar</div>

                <div class="page-content custom-scrollbar">

                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Slug</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($categories as $category)
                            <tr>
                                <td>
                                    @for ($i = 0; $i < $category->depth; $i++) &mdash; @endfor
                                    <a href="{{ route('admin.categories.show', $category) }}">{{ $category->name }}</a>
                                </td>
                                <td>{{ $category->slug }}</td>
                                <td>
                                    <div class="d-flex flex-row">

                                        @include('admin.includes._list_sortable', ['route' => 'categories', 'model' => $category])

                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>


            </div>

           {{-- <nav aria-label="pagination" class="my-8">
                {{$categories->links()}}
            </nav>--}}
        </div>
        <!-- / CONTENT -->
    </div>

@endsection
