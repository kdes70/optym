@extends('layouts.admin')

@section('content')


    <div class="page-layout carded full-width">

        <div class="top-bg bg-primary text-auto"></div>

        <!-- CONTENT -->
        <div class="page-content-wrapper">

            <form>
                <!-- HEADER -->
            @include('admin.includes._table-header', [
                'title' => __('Users'),
                'total' => $users->total(),
                'add_url' => route('admin.users.create')
            ])
{{--                <p><a href="{{ route('admin.users.create') }}" class="btn btn-success">Add User</a></p>--}}

                <!-- / HEADER -->
            </form>

            <div class="card mb-3">
                <div class="card-header">Filter</div>
                <div class="card-body">
                    <form action="?" method="GET">
                        <div class="row">
                            <div class="col-sm-1">
                                <div class="form-group">
                                    <label for="id" class="col-form-label">ID</label>
                                    <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="username" class="col-form-label">Username</label>
                                    <input id="username" class="form-control" name="username" value="{{ request('username') }}">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="email" class="col-form-label">Email</label>
                                    <input id="email" class="form-control" name="email" value="{{ request('email') }}">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="state" class="col-form-label">Status</label>
                                    <select id="state" class="form-control" name="state">
                                        <option value=""></option>
                                        @foreach ($statuses as $value => $label)
                                            <option value="{{ $value }}"{{ $value === request('state') ? ' selected' : '' }}>{{ $label }}</option>
                                        @endforeach;
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="role" class="col-form-label">Role</label>
                                    <select id="role" class="form-control" name="role">
                                        <option value=""></option>
                                        @foreach ($roles as $value => $label)
                                            <option value="{{ $value }}"{{ $value === request('role') ? ' selected' : '' }}>{{ $label }}</option>
                                        @endforeach;
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="col-form-label">&nbsp;</label><br />
                                    <button type="submit" class="btn btn-primary">Search</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="page-content-card">

                <div class="toolbar p-6">Content Toolbar</div>

                <div class="page-content custom-scrollbar">

                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Role</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td><a href="{{ route('admin.users.show', $user) }}">{{ $user->username }}</a></td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    @if ($user->isWait())
                                        <span class="badge badge-secondary">Waiting</span>
                                    @endif
                                    @if ($user->isActive())
                                        <span class="badge badge-primary">Active</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($user->isAdmin())
                                        <span class="badge badge-danger">Admin</span>

                                    @elseif($user->isModerator())
                                        <span class="badge badge-warning">Moderator</span>
                                    @else
                                        @foreach($user->getRole() as $role)
                                            @if($role->getName())
                                                <span class="badge badge-secondary">{{$role->getName()}}</span>
                                            @endif
                                        @endforeach
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>


                </div>
                <nav aria-label="pagination" class="my-8">
                    {{ $users->links() }}
                </nav>
            </div>


        </div>
        <!-- / CONTENT -->
    </div>




@endsection