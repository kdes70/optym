@extends('layouts.admin')

@section('content')
    <div id="pages" class="page-layout simple tabbed">

        <!-- HEADER -->
    @include('admin.includes._page-header' , [
        'title' => 'New Page',
        'back_link' => route('admin.pages.index'),
        'logo' => null,
      ])
    <!-- / HEADER -->

        <!-- CONTENT -->
        <div class="page-content">

            <ul class="nav nav-tabs" id="myTab" role="tablist">

                <li class="nav-item">
                    <a class="nav-link btn active"
                       id="basic-info-tab"
                       data-toggle="tab"
                       href="#basic-info-tab-pane"
                       role="tab"
                       aria-controls="basic-info-tab-pane"
                       aria-expanded="true">Базовая информация</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link btn"
                       id="product-images-tab"
                       data-toggle="tab"
                       href="#product-images-tab-pane"
                       role="tab"
                       aria-controls="product-images-tab-pane">Скриншоты</a>
                </li>

            </ul>

            <div class="tab-content">

                <div class="tab-pane fade show active" id="basic-info-tab-pane" role="tabpanel"
                     aria-labelledby="basic-info-tab">
                    <div class="card p-6">
                        <form id="form_save" method="POST" action="{{route('admin.pages.store')}}">
                            @csrf
                            @include('admin.pages._form')
                        </form>
                    </div>
                </div>

                <div class="tab-pane fade" id="product-images-tab-pane" role="tabpanel"
                     aria-labelledby="product-images-tab">

                    <div class="card p-6">
                        <div class="row">
                            <div class="alert alert-success" role="alert">
                                <h4 class="alert-heading">Сохраните базовую информацию клиента!</h4>
                                <p>Чтобы загрузить логотип нужно сначала сохранить клиента</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- / CONTENT -->
    </div>

@endsection

@section('scripts')
@endsection