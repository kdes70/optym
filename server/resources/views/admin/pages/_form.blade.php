<div class="form-row">
    <div class="form-group col-md-6">
        <label for="category_id" class="col-form-label">Category</label>
        <select @change="changeTitlePage"
                id="category_id"
                class="form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }}"
                name="category_id">
            <option value=""></option>
            @foreach ($categories as $category)
                <optgroup label="{{ $category->name }}">
                    @foreach($category->children as $item)
                        @if(isset($page->category_id))
                            <option value="{{ $item->id }}"{{ $item->id == old('category_id', $page->category_id) ? ' selected' : '' }}>
                                {{ $item->name }}
                            </option>
                        @else
                            <option value="{{ $item->id }}"{{ $item->id == old('category_id') ? ' selected' : '' }}>
                                {{ $item->name }}
                            </option>
                        @endif
                    @endforeach
                </optgroup>
            @endforeach;
        </select>
        @if ($errors->has('category_id'))
            <span class="invalid-feedback"><strong>{{ $errors->first('category_id') }}</strong></span>
        @endif
    </div>

    <div class="form-group col-md-6">
        <input id="title"
               type="text"
               class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
               name="title" :value="titleSelected"
               value="{{ isset($page->title) ?  old('title', $page->title) : old('title') }}">
        @if ($errors->has('title'))
            <span class="invalid-feedback"><strong>{{ $errors->first('title') }}</strong></span>
        @endif
        <label for="title" class="col-form-label">Title</label>
    </div>

</div>

<div class="form-group">
    <textarea id="content" rows="4" name="content"
              class="form-control">{{ isset($page->content) ? old('content', $page->content) : old('content')}}</textarea>
    @if ($errors->has('content'))
        <span class="invalid-feedback"><strong>{{ $errors->first('content') }}</strong></span>
    @endif
    <label for="content" class="col-form-label">Текст страницы</label>
</div>


<hr>

<fieldset class="form-group border-success">
    <legend class="text-success" align="center">SEO</legend>

    <hr>

    <div class="form-row">
        <div class="col-md-6">
            <div class="form-group">
                <textarea id="meta_description" rows="4" name="meta_description" class="form-control">
                    {{ isset($page->meta_description) ? old('meta_description', $page->meta_description) : old('meta_description')}}
                </textarea>
                @if ($errors->has('meta_description'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('meta_description') }}</strong></span>
                @endif
                <label for="meta_description" class="col-form-label">Описание</label>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <textarea id="meta_keywords" :value="titleSelected" rows="4" name="meta_keywords" class="form-control">
                    {{ isset($page->meta_keywords)? old('meta_keywords', $page->meta_keywords) : old('meta_keywords')}}
                </textarea>
                @if ($errors->has('meta_keywords'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('meta_keywords') }}</strong></span>
                @endif
                <label for="meta_keywords" class="col-form-label">Ключевые слова</label>
            </div>
        </div>
    </div>

    <div class="form-row">

        @if(isset($page->enabled))
            <input type="checkbox" ref="cb" name="enabled" class="js-switch"
                   @if( old('enabled', $page->enabled)) checked @endif />
        @else
            <input type="checkbox" ref="cb" :checked="pages.enabled" @change="pages.enabled = $event.target.checked"
                   name="enabled" class="js-switch" @if( old('enabled')) checked @endif />
        @endif

    </div>

</fieldset>

