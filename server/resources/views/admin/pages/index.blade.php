@extends('layouts.admin')

@section('content')

    <div class="page-layout carded full-width">

        <div class="top-bg bg-primary text-auto"></div>

        <!-- CONTENT -->
        <div class="page-content-wrapper">

            <form>
                <!-- HEADER -->
            @include('admin.includes._table-header', [
                'title' => __('Страницы'),
                'total' => $pages->total(),
                'add_url' => route('admin.pages.create')
            ])
            <!-- / HEADER -->
            </form>

            <div class="page-content-card">

                <div class="toolbar p-6">Content Toolbar</div>

                <div class="page-content custom-scrollbar">

                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <?php $columns = [
                                'id'   => 'ID',
                                'category_id' => 'Категория',
                                'name' => 'Название',
                                'path' => 'Ссылка',
                                'sort' => 'Сорт',
                                'enabled' => 'Статус'
                            ]; ?>

                            @foreach($columns as $k => $v)
                                <th class="d-md-table-cell">
                                    <a class="fuse-ripple-ready" style="text-decoration: none" href="{{route('admin.pages.index',  http_build_query([
                                        'order'  => $k,
                                        'dir'    => $order == $k ? !$dir : $dir
                                    ]))}}">
                                        {{$v}}

                                        @if ($order != $k)
                                            <i class="icon icon-sort font-seze-10"></i>
                                        @elseif ($dir)
                                            <i class="icon icon-sort-descending"></i>
                                        @else
                                            <i class="icon icon-sort-ascending"></i>
                                        @endif

                                    </a>
                                </th>
                            @endforeach

                            <th class="d-md-table-cell">
                                Порядок
                            </th>
                            <th class="d-md-table-cell">
                                <span class="fuse-ripple-ready">Управление</span>
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        @if($pages)

                            @foreach($pages as $page)
                            <tr>
                                {{--<th scope="row">{{$loop->iteration}}</th>--}}
                                <th scope="row">{{$page->id}}</th>
                                <td>
                                    @if($page->category)
                                    <a href="{{route('admin.categories.show', $page->category)}}">{{$page->category->name}}</a>
                                    @endif
                                </td>
                                {{--<td><img src="{{ $page->getLogo()}}" alt="" width="40" class="img-thumbnail img-fluid"></td>--}}
                                <td>{{$page->title}}</td>
                                <td>
                                    @if($page->path)
                                        <a href="{{$page->path}}" target="_blank">{{$page->path}}</a>
                                    @endif
                                </td>
                                <td>{{$page->sort}}</td>
                                <td>
                                    @if(!$page->isEnabled())
                                        <i class="icon-clock-fast text-muted" title="черновик"></i>
                                    @else
                                        <i class="icon-checkbox-marked-circle text-success" title="активен"></i>
                                    @endif
                                </td>
                                <td>
                                    <div class="d-flex flex-row">

                                      @include('admin.includes._list_sortable', ['route' => 'pages', 'model' => $page])
                                    </div>
                                </td>
                                <td>

                                    <a href="{{route('admin.pages.edit', $page)}}" class="btn btn-icon fuse-ripple-ready"
                                       aria-label="Customer details">
                                        <i class="icon icon-pencil s-4"></i>
                                    </a>

                                    {{--<a @click.prevent="confirmDelete({text:'Вы уверены что хотите удалить портфолио: {{$page->name}}?', url:'{{ route("admin.pages.destroy", $page) }}'})"--}}
                                       {{--class="btn btn-icon fuse-ripple-ready"--}}
                                       {{--aria-label="Customer delete"><i class="icon s-4 icon-delete"></i>--}}
                                    {{--</a>--}}

                                </td>
                            </tr>
                        @endforeach

                        @endif
                        </tbody>
                    </table>

                </div>


            </div>

            <nav aria-label="pagination" class="my-8">
                {{$pages->links()}}
            </nav>
        </div>
        <!-- / CONTENT -->
    </div>

@endsection
