@extends('layouts.app')


{{--@section('title', $category->getName() . ' оптом')--}}

@yield('includes.nav_bar_top')

@section('content')

    @include('layouts.partials.catalog_top', ['category' => $categories])

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h1>{{$categories->getName()}} - {{trans('pages.pages.wholesale.title')}}</h1>
            </div>
        </div>
    </div>

    <small>Всего:{{$companies_all_count}} / {{count($companies)}}</small>

    <div class="container wholesale">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-3">
                @if($categories->children->isNotEmpty())
                    <div class="left_catalog">
                        <p class="popover_open_link js-left_catalog-open">Уточнить категорию</p>
                        <div id="left_catalog_wrapper" class="hidden-xs">
                            <p class="left_catalog__item">{{$categories->getName()}}</p>
                            <div class="left_catalog__level">
                                <ul>
                                    @foreach($categories->children as $children)
                                        <li>
                                            <a href="{{route('catalog.suppliers', ['category' => $children->getUrl()])}}"
                                               class="left_catalog__item">
                                                {{$children->getName()}}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-xs-12 col-sm-8 col-md-9 suppliersList">
               @each('company.suppliers.item', $companies, 'company')

                <div class="row pagination">
                    {!! $companies->render() !!}
                </div>
            </div>


        </div>
    </div>
@endsection
