<div class="section-goods_catalog py-3">

    <div class="container">

        <div class="row">
            <div class="col-md-3">
                <div id="left_catalog_wrapper" class="hidden-xs">
                  Всего:  {{$pagination->total()}}
                </div>
            </div>
        </div>

        <product-list :categories="{{$category->resource}}"
                      :categories_counts="{{json_encode($categoriesCounts)}}"
                      :products="{{json_encode($products)}}"
                      :range_price="{{ json_encode($range_price)}}"/>

    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-9">
                {{$pagination}}
            </div>
        </div>
    </div>
</div>