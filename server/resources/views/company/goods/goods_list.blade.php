<div class="section-goods_catalog py-3">

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @if($category->children->isNotEmpty())
                    <div class="left_catalog">
                        <p class="popover_open_link js-left_catalog-open">Уточнить категорию</p>
                        <div id="left_catalog_wrapper" class="hidden-xs">
                            <p class="left_catalog__item">{{$category->getName()}}</p>
                            <div class="left_catalog__level">
                                <ul>
                                    @foreach($category->children as $category)
                                        <li>
                                            <a href="{{route('category', ['path' => $category->getUrl()])}}"
                                               class="left_catalog__item">
                                                {{$category->getName()}}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

{{--                @if($products->isNotEmpty())--}}
{{--                    <div class="filters-catalog-wrapper">--}}
{{--                        <div class="filters__body">--}}
{{--                            <div class="filters--price">--}}
{{--                                <p class="filters__title">Цена</p>--}}
{{--                                <range-cost-price :category="{{$category}}"--}}
{{--                                                  :range_price="{{ json_encode($range_price)}}"></range-cost-price>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endif--}}


            </div>
            <div class="col-md-9">
                @if($products->isNotEmpty())
                    <div class="goods_list-wrapper">
                        <div class="row goods_list clearfix">
                            @foreach($products as $product)
                                <div class="col-md-4 py-4">
                                    <div class="goods_item-wrapper leaf__item">
                                        <div class="goods_item">
                                            <a class="goods_link"
                                               href="{{route('suppliers.show-product',[$company, $product])}}">
                                        <span>
                                            <div class="goods-image consumer">
                                                <span class="imgWrapper">
                                                    <img src="{{(!is_null($product->getMainImage())) ? $product->getMainImage()->getPath() : '/img/no_avatar.jpg'}}"
                                                         alt="{{$product->getTitle()}}">
                                                </span>
                                            </div>
                                        </span>
                                                <span class="goods-name">{{$product->getTitle()}}</span>
                                            </a>
                                            <a href="{{route('suppliers.show', $company->getSlug())}}"
                                               class="goods-company_name"
                                               target="_blank">{{$company->getName()}}</a>
                                            <div>
                                                <span class="goods-price">{{$product->getFormatCostPrice()}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

</div>