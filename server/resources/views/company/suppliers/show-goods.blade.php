@extends('layouts.app')
@section('title', $product->getTitle())
@section('content')

    @include('layouts.partials.breadcrumbs')
    <div class="company">
        <div class="company__header">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="company_name">
                            <a href="{{route('suppliers.show', $company->getSlug())}}">{{$company->getName()}}</a>
                        </h1>
                        <div class="company_links">

                            <span class="active company_links__item">
                                    Каталог
                            </span>
                            <a href="{{ route('suppliers.contact', $company->getSlug())}}"
                               class="company_links__item">Контакты</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container py-4">

            <div class="section-company_catalog">
                <div class="col-xs-12 col-sm-4">
                    <div class="row">
                        <img src="https://media.novosib.ru/CACHE/images/goods/4f/cc/valenki-unty-048626/bc403aaac0afeb58f75bcafd282b28b7.jpg"
                             alt="Валенки, унты">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <h1 class="goods__name h2">
                        {{$product->getTitle()}}
                    </h1>
                    <div class="goods__price">
                        <span class="goods-price">  {{$product->getFormatCostPrice()}}</span>
                    </div>
                </div>
            </div>

        </div>
@endsection
