<div class="product_item_wrapper">
    <div class="product_item">
        <a href="{{route('suppliers.show-product', [$company, $product])}}" class="product_link">
            <span class="product_img">
                <img src="{{(!is_null($product->getMainImage())) ? $product->getMainImage()->getPath() : '/img/no_avatar.jpg'}}" alt="{{$product->getTitle()}} артикул: 91-031" class="">
            </span>
            <span class="product_name">{{$product->getTitle()}}</span>
            <span class="product_price">{{$product->getFormatCostPrice()}}</span>
        </a>
    </div>
</div>