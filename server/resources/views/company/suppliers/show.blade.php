@extends('layouts.app')
@section('title', $company->getName())
@section('content')
    <div class="company">

        @if($company)
            <div class="company__header">

                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">


                            <h1 class="company_name">
                                <a href="{{route('suppliers.show', ['slug' => $company->slug])}}">{{$company->name}}</a>
                            </h1>

                            <div class="company_links">
{{--                                TODO не работает получения категории в компании--}}
                                @if($company->category->isNotEmpty())
                                    <a class="company_links__item popover_open_link"
                                       @click="popover_open = !popover_open"
                                       :class="{active:popover_open}">
                                        Каталог
                                    </a>
                                    <transition name="fade">
                                        <div v-cloak v-show="popover_open" class="popover-list__wrapper">
                                            <div class="popover-list__content">
                                                <ul class="popover-list">
                                                    @foreach($company->category as $category)
                                                        <li>
                                                            <a href="{{route('suppliers.category', [$company->getSlug(), $category])}}"
                                                               class="active popover-list__item">
                                                                {{$category->getName()}}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </transition>
                                @endif
                                <a href="{{ route('suppliers.contact', $company)}}"
                                   class="producer_links__item">Контакты!</a>
                            </div>

                            {{--TODO вернуть когда будет чат между пользователями--}}
                            {{-- @if(isset($company->users[0]->id))
                                 <a href="{{route('new_conversation', ['recipient_id' => $company->users[0]->id])}}"
                                    class="btn btn-warning -align-right js-send_feedback" target="_blank">Отправить
                                     запрос</a>
                             @endif--}}


                        </div>
                    </div>

                    @if($company->category->isNotEmpty())
                        @include('company.suppliers._partition')
                    @endif
                </div>
            </div>

            @include('company.goods.goods_list', ['company' => $company, 'products' => $company->getProducts()])

        @endif
    </div>
@endsection
