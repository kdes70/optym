<div class="partition-list__wrapper" style="max-height: 56px;">

    <div class="container partition-list">

        @foreach($company->category->unique() as $sibling)

            <a class="partition-list__item @if(Request::segment(3) === $sibling->getSlug())) active @endif"
               href="{{route('suppliers.category', ['company' => $company->getSlug(),'category' => $sibling->getSlug()])}}">
                <span>{{$sibling->getName()}}</span>
            </a>

        @endforeach

    </div>
</div>