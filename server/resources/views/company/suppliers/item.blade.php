<div class="suppliers_item">
    <div class="row">
        <div class="col-xs-12 col-sm-3 col-md-3">
            <div class="suppliers_short_description">

                <a href="/producer/stile-n" target="_blank">
                    <div class="suppliers_main_photo imgWrapper">
                        <span class="img-slider">
                            <img src="{{$company->logo}}"
                                 width="200" height="200"
                                 alt="{{$company->name}} - {{$company->tagline}}">
                        </span>
                    </div>
                </a>

                <div class="hidden-xs suppliers_status">
                    <span class="producer_status-sign">status gold</span>
                </div>
                <div class="suppliers_statistic">
                    <div class="producer_goods-count">
                        <span class="show_md">Количество </span>товаров:
                        <strong>{{$company->products->count()}}</strong> {{--TODO withCount--}}
                    </div>
                </div>
                <div class="suppliers_details">
                    <a class="h3" href="#" target="_blank">Контакты</a>
                    <div id="popover-contact" class="producer_office">
                        <div class="popover-contact-wrapper">
                            <p class="producer_office-contacts"></p>
                            <p class="producer_office-address">
                                <span class="producer_office-address_country">{{$company->country}}</span>
                                <span class="producer_office-address_city">{{$company->city}}</span>
                                <span>
                                      <span class="producer_office-address_street">, {{$company->address}}</span>
                                      <span class="producer_office-address_building"></span>
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-9 col-md-9">
            <div class="card suppliers_info-wrapper">
                <div class="card__content">
                    <div class="suppliers_info">
                        <div class="media">
                            <div class="media-left">
                                <a href="/company/{{$company->slug}}" target="_blank">

                                </a>
                                <div class="btm-space visible-xs"></div>
                            </div>

                            <div class="media-body">

                                <div class="pull-right text-muted">
                                    <span class="pro_icon r-space hidden-xs" title="PRO Стандарт">PRO</span>
                                </div>


                                <h2 class="card__title-list">
                                    <a href="{{ route('suppliers.show', ['slug' => $company->slug]) }}"
                                       title="{{$company->name}} - {{$company->tagline}}"
                                       target="_blank">
                                        {{$company->name}}<br>
                                        <span class="text-md">{{$company->tagline}}</span>
                                    </a>
                                </h2>

                                <p class="text-md">{{$company->description}}</p>

                            </div>
                        </div>

                        <div class="suppliers_product_list">
                            <div class="product_list">
                                @foreach ($company->products as $product)

                                    @include('company.suppliers.product.product-item', $company->products)

                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <hr>
</div>