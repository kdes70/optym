@extends('layouts.app')

@section('content')
    <div class="company">

        @if($company)
            <div class="company__header">

                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">


                            <h1 class="company_name">
                                <a href="{{route('suppliers.show', ['slug' => $company->slug])}}">{{$company->name}}</a>
                            </h1>

                            <div class="company_links">
                                {{--                                TODO не работает получения категории в компании--}}
                                @if($company->category->isNotEmpty())
                                    <a class="company_links__item popover_open_link"
                                       @click="popover_open = !popover_open"
                                       :class="{active:popover_open}">
                                        Каталог
                                    </a>
                                    <transition name="fade">
                                        <div v-cloak v-show="popover_open" class="popover-list__wrapper">
                                            <div class="popover-list__content">
                                                <ul class="popover-list">
                                                    @foreach($company->category as $category)
                                                        <li>
                                                            <a href="{{route('suppliers.category', [$company->getSlug(), $category])}}"
                                                               class="active popover-list__item">
                                                                {{$category->getName()}}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </transition>
                                @endif
                                <a href="{{ route('suppliers.contact', $company)}}"
                                   class="producer_links__item">Контакты!</a>
                            </div>

                            {{--TODO вернуть когда будет чат между пользователями--}}
                            {{-- @if(isset($company->users[0]->id))
                                 <a href="{{route('new_conversation', ['recipient_id' => $company->users[0]->id])}}"
                                    class="btn btn-warning -align-right js-send_feedback" target="_blank">Отправить
                                     запрос</a>
                             @endif--}}


                        </div>
                    </div>

                    @if($company->category->isNotEmpty())
                        @include('company.suppliers._partition')
                    @endif
                </div>
            </div>



            <div class="company_info">
                <div class="card">
                    <div class="card-body">
                        {{$company->getDescription()}}
                    </div>
                </div>
            </div>

            <div class="company_info">
                <div class="card">
                    <div class="card-header">
                        <h3>О компании</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-20">
                                    <div>Тип компании</div>
                                    <div>{{$company->getCompanyType()}}</div>
                                    <div>{{$company->types_business}}</div>
                                </div>
                                <div class="mb-20">
                                    <div>Объем поставок</div>
                                    <div>{{$company->getSupply()}}</div>
                                </div>
                                <div class="mb-20">
                                    <div>Минимальная сумма заказа min_amount</div>
                                    <div>{{$company->min_amount}}</div>
                                </div>
                                <div class="mb-20">
                                    <div>Юр. лицо</div>
                                    <div>{{$company->min_amount}}</div>
                                    <div>{{$company->name_legal_entity}}</div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-20">
                                    <div>Варианты оплаты</div>
                                    <div>{{$company->delivery_options}}</div>
                                </div>
                                <div class="mb-20">
                                    <div>Год основания</div>
                                    <div>{{$company->reg_data}}</div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="company_info">
                <div class="card">
                    <div class="card-header">
                        <h3>Контакты</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-20">
                                    <div>Телефон</div>
                                    <div>{{$company->phone}}</div>

                                </div>
                                <div class="mb-20">
                                    <div>E-mail</div>
                                    <div>{{$company->email}}</div>
                                </div>
                                <div class="mb-20">
                                    <div>Официальный сайт</div>
                                    <div>{{$company->site_link}}</div>
                                </div>
                                <div class="mb-20">
                                    <div>Юр. лицо</div>
                                    <div>{{$company->min_amount}}</div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-20">
                                    <div>Адрес</div>
                                    <div>
                                        <p>
                                            <span>{{$company->country}},</span>
                                            <span>{{$company->region}},</span>
                                            <span>{{$company->city}}</span>
                                        </p>
                                        <p>{{$company->address}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endif
    </div>
@endsection
