@extends('layouts.app')

@yield('includes.nav_bar_top')

@section('content')


    {{--@include('layouts.partials.catalog_top')--}}

    {{--<div class="container">--}}
    {{--<div class="row justify-content-center">--}}
    {{--<div class="col-md-10">--}}
    {{--<h1>{{$categories->getName()}} - {{trans('pages.pages.wholesale.title')}}</h1>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}



    <div class="container wholesale">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-3">

                <small>Всего: {{count($companies)}}</small>

                {{--  @if($children->isNotEmpty())
                  <div class="left_catalog">
                      <p class="popover_open_link js-left_catalog-open">Уточнить категорию</p>
                      <div id="left_catalog_wrapper" class="hidden-xs">
                          <p class="left_catalog__item">{{$categories->getName()}}</p>
                          <div class="left_catalog__level">
                              <ul>
                                  @foreach($children as $category)
                                      <li>
                                          <a href="{{route('suppliers.show', ['category' => $category->getUrl()])}}"
                                             class="left_catalog__item">
                                              {{$category->getName()}}
                                          </a>
                                      </li>
                                  @endforeach
                              </ul>
                          </div>
                      </div>
                  </div>
                  @endif--}}
            </div>
            <div class="col-xs-12 col-sm-8 col-md-9 suppliersList">
                @each('company.suppliers.item', $companies, 'company')

                {{ $companies->links() }}
            </div>
        </div>
    </div>
@endsection
