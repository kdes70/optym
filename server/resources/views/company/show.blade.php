@extends('layouts.app')
{{--@section('title', $product->getTitle())--}}
@section('content')
    <div class="container company">

        <div class="company__header">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="company_name">
                            <a href="/producer/stile-n">ТМ "MARINA"</a>
                        </h1>

                        <div class="company_links">
                            <a class="js-show-focus-partitials active producer_links__item popover_open_link">
                                Каталог
                            </a>
                            <div class="hide focus-partition__wrapper">
                                <div>
                                    <ul class="popover-list">
                                        <li>
                                            <a class="active popover-list__item">
                                                Женская одежда
                                            </a>
                                        </li>

                                        <li>
                                            <a href="/producer/stile-n/domashnjaja-odezhda" class="popover-list__item">
                                                Домашняя одежда
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <a href="{{ route('company.contact', ['slug' => $company->getSlug()])}}" class="producer_links__item">Контакты</a>
                        </div>
                        @if(isset($company->users[0]->id))
                        <a href="{{route('new_conversation', ['recipient_id' => $company->users[0]->id])}}"
                           class="btn btn-warning -align-right js-send_feedback"  target="_blank">Отправить запрос</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="company__info">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <h2 class="company-header">{{$company->name}}</h2>
                    <div class="company_text">
                        {{$company->description}}
                    </div>
                </div>

            </div>
        </div>

        @include('company.goods.list', ['products' => $company->products])
    </div>
@endsection
