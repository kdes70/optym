<div class="card list-group">
    <div class="list-group-item list-group-item-gray">
        <div class="media btm-space-zero">
            <div class="media-body">
                <div class="dropdown">
                    <a href="javascript:void(0)"
                       class="dropdown-toggle"
                       data-toggle="dropdown"
                       aria-haspopup="true"
                       aria-expanded="false">
                        <b>{{ $company->name }}&nbsp;<span class="caret"></span></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ route('profiles.company.home') }}">Моя компания</a>
                        </li>
                        <li>
                            <a href="{{ route('profiles.company.edit') }}" class="">Редактировать информацию</a>
                        </li>
                        <li>
                            <a href="#/company/delete/id">Удалить компанию</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <a class="list-group-item {{ (Route::currentRouteName() == 'profiles.company.home') ? 'active' : '' }}" href="{{ route('profiles.company.home') }}">
        <i class="fa fa-address-card" aria-hidden="true"></i>{{__('Моя компания')}}
    </a>
    <a class="list-group-item {{ (Route::currentRouteName() == 'profiles.company.home') ? 'active' : '' }}" href="{{ route('profiles.company.edit') }}" class="">
        <i class="fa fa-fw fa-book"></i>{{__('Описание')}}
    </a>
    <a class="list-group-item" href="{{ route('profiles.company.edit') }}" class="">
        <i class="fa fa-fw fa-list-alt"></i>{{__('Сфера деятельности')}}
    </a>
    <a class="list-group-item" href="{{ route('profiles.company.edit') }}" class="">
        <i class="fa fa-fw fa-shopping-cart"></i>{{__('Способы оплаты и доставка')}}
    </a>
    <a class="list-group-item" href="{{ route('profiles.company.edit') }}" class="">
        <i class="fa fa-fw fa-list-alt"></i>{{__('Реквизиты')}}
    </a>

    <a class="list-group-item" href="#/order">Заказы</a>
    <a class="list-group-item" href="#/product">Товары</a>

</div>