<form method="POST" action="{{ route('profiles.company.update') }}">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label for="name" class="col-form-label">Общеизвестное название компании / бренда</label>
        <div class="text-help">Без кавычек. Например: Мир мебели</div>
        <input id="name"
               type="text"
               class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
               name="name"
               value="{{ old('name', $company->name) }}"
               autofocus>
        @if ($errors->has('name'))
            <span class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>
        @endif
    </div>

    <div class="form-group">
        <label for="tagline" class="col-form-label">Краткое описание</label>
        <div class="help-block">Например: мебель из массива дерева оптом</div>
        <input id="tagline"
               type="text"
               class="form-control{{ $errors->has('tagline') ? ' is-invalid' : '' }}"
               name="tagline"
               value="{{ old('tagline', $company->tagline) }}">
        @if ($errors->has('tagline'))
            <span class="invalid-feedback"><strong>{{ $errors->first('tagline') }}</strong></span>
        @endif
    </div>

    <div class="form-group">
        <label>Основная деятельность вашей компании</label>
            @foreach($list_type as $key => $type)
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input"
                           type="radio"
                           name="company_type"
                           id="company_type"
                           value="{{$key}}"
                           @if(old('company_type', $company->company_type) == $key) checked @endif>
                    {{$type}}
                </label>
            </div>
            @endforeach
    </div>

    <div class="form-group">
        <label>Описание </label>
        <small>ВНИМАНИЕ! Описание должно быть уникальным и подробным (иначе компания не попадет в поисковые системы Яндекс и Google). Не копируйте описание с других сайтов. Потратьте 5 минут на составление хорошего описания, это позволит увеличить посещаемость вашей страницы. Контактные данные и ссылки запрещены в этом поле.</small>
        <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}">
            {{old('description', $company->description)}}</textarea>
        @if ($errors->has('description'))
            <span class="invalid-feedback"><strong>{{ $errors->first('description') }}</strong></span>
        @endif
    </div>

    <div class="form-group">
        <label>Ключевые слова для поиска / Теги</label>
        <small>Введите через запятую ключевые слова и синонимы по которым будут искать вашу компанию.</small>
        <textarea class="form-control{{ $errors->has('tags') ? ' is-invalid' : '' }}">
            {{old('tags', $company->tags)}}</textarea>
        @if ($errors->has('tags'))
            <span class="invalid-feedback"><strong>{{ $errors->first('tags') }}</strong></span>
        @endif
    </div>

    <fieldset >
        <legend class="col-form-legend">Контактные данные</legend>
        <div class="form-group">
            <label for="country" class="col-form-label">Страна</label>
            <input id="country"
                   type="text"
                   class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}"
                   name="country"
                   value="{{ old('country', $company->country) }}">
            @if ($errors->has('country'))
                <span class="invalid-feedback"><strong>{{ $errors->first('country') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="region" class="col-form-label">Регион</label>
            <input id="region"
                   type="text"
                   class="form-control{{ $errors->has('region') ? ' is-invalid' : '' }}"
                   name="region"
                   value="{{ old('region', $company->region) }}">
            @if ($errors->has('region'))
                <span class="invalid-feedback"><strong>{{ $errors->first('region') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="city" class="col-form-label">Город</label>
            <input id="city"
                   type="text"
                   class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}"
                   name="city"
                   value="{{ old('city', $company->city) }}">
            @if ($errors->has('city'))
                <span class="invalid-feedback"><strong>{{ $errors->first('city') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="address" class="col-form-label">Адрес</label>
            <input id="address"
                   type="text"
                   class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}"
                   name="address"
                   value="{{ old('address', $company->address) }}">
            @if ($errors->has('address'))
                <span class="invalid-feedback"><strong>{{ $errors->first('address') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="zip" class="col-form-label">Индекс</label>
            <input id="zip"
                   type="text"
                   class="form-control{{ $errors->has('zip') ? ' is-invalid' : '' }}"
                   name="zip"
                   value="{{ old('zip', $company->zip) }}">
            @if ($errors->has('zip'))
                <span class="invalid-feedback"><strong>{{ $errors->first('zip') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="phone" class="col-form-label">Телефон</label>
            <input id="phone"
                   type="text"
                   class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                   name="phone"
                   value="{{ old('phone', $company->phone) }}">
            @if ($errors->has('phone'))
                <span class="invalid-feedback"><strong>{{ $errors->first('phone') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="email" class="col-form-label">Публичный адрес электронной почты</label>
            <input id="email"
                   type="email"
                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                   name="email"
                   value="{{ old('email', $company->email) }}">
            @if ($errors->has('email'))
                <span class="invalid-feedback"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="site_link" class="col-form-label">Адрес сайта</label>
            <input id="site_link"
                   type="text"
                   class="form-control{{ $errors->has('site_link') ? ' is-invalid' : '' }}"
                   name="site_link"
                   value="{{ old('site_link', $company->site_link) }}">
            @if ($errors->has('site_link'))
                <span class="invalid-feedback"><strong>{{ $errors->first('site_link') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="skype" class="col-form-label">Skype</label>
            <input id="skype"
                   type="skype"
                   class="form-control{{ $errors->has('skype') ? ' is-invalid' : '' }}"
                   name="skype"
                   value="{{ old('skype', $company->skype) }}">
            @if ($errors->has('skype'))
                <span class="invalid-feedback"><strong>{{ $errors->first('skype') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label>Другие контакты</label>
            <textarea name="other_contacts" class="form-control{{ $errors->has('other_contacts') ? ' is-invalid' : '' }}">
            {{old('other_contacts', $company->other_contacts)}}</textarea>
            @if ($errors->has('other_contacts'))
                <span class="invalid-feedback"><strong>{{ $errors->first('other_contacts') }}</strong></span>
            @endif
        </div>

    </fieldset>


    <fieldset >
        <legend class="col-form-legend">Категории</legend>

        {{print_r($company->category_id)}}
        <tree-select :old="{{ collect([2,4])->toJson() }}"></tree-select>
        @if ($errors->has('category_id'))
            <span class="invalid-feedback"><strong>{{ $errors->first('category_id') }}</strong></span>
        @endif

    </fieldset>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Save</button>
    </div>



</form>