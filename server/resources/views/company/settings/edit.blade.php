@extends('layouts.app')

@section('content')
    <main class="app-content py-3">
        <div class="container">
            {{--@section('breadcrumbs', Breadcrumbs::render())--}}
            {{--@yield('breadcrumbs')--}}
            @include('layouts.partials.flash')
            <section class="content">
                <div class="row ">
                    <div class="col-md-12">
                        <div class="row ">

                            {{-- left block--}}
                                @include('profiles.partials._left-block')

                            <div class="col-md-9">
                                <div class="card">
                                    <div class="card__head">
                                        <div class="card__title">
                                            {{--<a class="link-black" href="#/settings/user_list">--}}
                                            {{--<i class="fa fa-arrow-left" aria-hidden="true"></i>--}}
                                            {{--</a>--}}
                                            Редактирование профиля компании {{$company->getName()}}
                                        </div>
                                    </div>
                                    <div class="card__content">
                                        <div class="col-md-8">
                                            @include('profiles.company._form')
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

        </div>
    </main>
@endsection
