@extends('layouts.app')

@section('content')

    <main class="app-content py-3">
        <div class="container">
            {{--@section('breadcrumbs', Breadcrumbs::render())--}}
            {{--@yield('breadcrumbs')--}}
            @include('layouts.partials.flash')
            <section class="content">
                <div class="row ">
                    <div class="col-md-12">
                        <div class="row ">

                            {{-- left block--}}

                            @include('profiles.partials._left-block')

                            <div class="col-md-9">
                                <div class="card">
                                    <div class="card__content">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img class="company-logo" width="200" height="200" alt=""
                                                     src="/img/no_avatar.jpg">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <h1 class="first last">{{$company->name}}</h1>
                                                    {{--<div class="lead">{{$company->tagline}}</div>--}}
                                                </div>
                                                <div class="row">
                                                    <p>
                                                        <span class="badge badge-warning">Новая</span>
                                                        <span class="badge badge-info">На проверке</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="pull-right no-float-sm">
                                                    <a class="btn btn-default" href="{{ route('profiles.company.edit') }}">
                                                        <i class="material-icons icon--no-space">edit</i> Редактировать</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-container-flex">
                                    <div class="card card--flex-md">
                                        <div class="card__head">
                                            <div class="card__title">
                                                <a class="link-black" href="#/company/photo#top">
                                                    <i class="material-icons text-muted">photo_camera</i>Фотографии</a>
                                            </div>
                                        </div>
                                        <div class="card__content">
                                            <div>
                                                <a class="link-btn" href="#/company/photo#top">Добавить фотографии</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card card--flex-md">
                                        <div class="card__head">
                                            <div class="card__title">
                                                <a class="link-black"
                                                   href="#/company/news#top">
                                                    <i class="material-icons text-muted">import_contacts</i>
                                                    Статьи, информация</a>
                                            </div>
                                        </div>
                                        <div class="card__content">

                                            <div class="btm-space">
                                                <p class="text-muted">Добавьте статьи, новости или другую
                                                    информацию.</p>
                                                <ul class="list-unstyled">
                                                    <!-- $item in news -->
                                                </ul>
                                            </div>
                                            <a class="link-btn" href="#/company/news/add#top">Добавить</a>
                                            <a class="link-btn" href="#/company/news#top">Управление статьями</a>
                                        </div>
                                    </div>

                                </div>

                                <div class="card-container-flex ng-scope" ng-if="isSupplier()">
                                    <div class="card card--flex-md">
                                        <div class="card__head">
                                            <div class="card__title">
                                                <a class="link-black" href="#/product/all#top">
                                                    <i class="material-icons text-muted">shopping_cart</i>Товары</a>
                                            </div>
                                        </div>
                                        <div class="card__content">
                                            <div class="btm-space">
                                                <p class="text-danger ng-scope"><strong>0</strong> товаров</p>
                                                <p class="text-muted ng-scope">Добавьте или импортируйте товары своей
                                                    компании</p>
                                            </div>
                                            <a class="link-btn" href="#/product/all#top">Управление товарами</a>
                                        </div>
                                    </div>

                                    <div class="card card--flex-md">
                                        <div class="card__head">
                                            <div class="card__title">
                                                <a class="link-black" href="#/company/files#top"><i
                                                            class="material-icons text-muted">insert_drive_file</i>Файлы,
                                                    документы</a>
                                            </div>
                                        </div>
                                        <div class="card__content">
                                            <div class="btm-space">
                                                <p class="text-muted ng-scope">Загрузите презентации, буклеты,
                                                    сертификаты и&nbsp;другие файлы для вашей компании.</p>
                                                <!-- end ngIf: currentCompany.Files.length == 0 -->

                                            </div>
                                            <a class="link-btn" href="#/company/files#top">Управление файлами</a>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

        </div>
    </main>

@endsection