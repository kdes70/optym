@extends('layouts.app')

@section('title', 'Сервис для поиска оптовых поставщиков.')

@section('content')

    <section class="intro">
        <div class="row justify-content-center">

            <div class="row">
                <div class="col-12">

                    <div class="content">
                        <h1 class="text-center">ПЛОЩАДКА ДЛЯ БИЗНЕСА</h1>
                        <h2 class="text-center">Место встречи поставщиков и заказчиков по всей России</h2>

                        <div class="group_action">
                            <a class="btn btn-outline-primary btn-action" href="">Добавить компанию</a>
                            <a class="btn btn-outline-primary btn-action" href="">Найти поставщика</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    @isset($categories)
        @foreach($categories as $category)
            <section class="section">
                <div class="container">
                    <div class="section-category">
                        @if(!$category->parent)
                            <h2>{{$category->name}}</h2>
                        @endif

                        <div class="section-category__list row">
                            @if($category->children)

                                @foreach($category->children as $children)

                                    <div class="col-xs-6 col-sm-3 col-md-2 category_item-wrapper">
                                        <a href="{{route('category', $children)}}"
                                           class="category_item">
                                            <div class="card">
                                                <img class="card-img-top"
                                                     src="{{$children->getImagePath()}}"
                                                     alt="Card image cap">
                                                <div class="card-body">
                                                    <p class="card-text">{{$children->getName()}}</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                @endforeach
                            @endif

                        </div>
                    </div>
                </div>
            </section>
        @endforeach
    @endisset


    <section class="section"></section>



@endsection
