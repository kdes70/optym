@extends('layouts.profiles')

@section('content')

            <div class="card">
                <div class="card__head">
                    <div class="card__title">
                        {{--<a class="link-black" href="#/settings/user_list">--}}
                        {{--<i class="fa fa-arrow-left" aria-hidden="true"></i>--}}
                        {{--</a>--}}
                        Редактирование профиля
                    </div>
                </div>


                <div class="card__content">
                    <div class="row">
                        <div class="col-md-4">
                            @include('profiles.profile._block-info')
                        </div>
                        <div class="col-md-8">
                            @include('profiles.profile._form')
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection