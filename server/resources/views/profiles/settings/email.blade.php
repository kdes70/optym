@extends('layouts.profiles')

@section('content')

    <div class="card">
        <div class="card__head">
            <div class="card__title">
                {{--<a class="link-black" href="#/settings/user_list">--}}
                {{--<i class="fa fa-arrow-left" aria-hidden="true"></i>--}}
                {{--</a>--}}
                Редактирование профиля
            </div>
        </div>
        <div class="card__content">
            <div class="panel">
                <div class="alert alert-info">
                    <i class="fa fa-exclamation-triangle fa-lg"></i>
                    <span>Если Вы потеряли доступ к текущему адресу электронной почты, пожалуйста, напишите на</span>
                    &nbsp;
                    {{--TODO вынести почту в настройки--}}
                    <a class="alert-link" href="mailto:admin@admin.ru">
                        <span class="ng-scope">admin@admin.ru</span>
                    </a>
                </div>
                <form name="form" class="form">
                    <!-- ngRepeat: error in non_field_errors -->
                    <div class="balloon-wrap">
                        <p class="form-group">
                            <span>Ваш текущий адрес электронной почты:</span>
                            <b><span class="ng-binding">kdes70@mail.ru</span></b>
                        </p>
                        <div class="form-group">
                                <label for="email" class="col-form-label">{{__('Новый Email')}}</label>

                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email or old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif

                        </div>
                        <p class="form-group">
                            <span>После того, как вы нажмете кнопку "Сохранить", на</span>
                            &nbsp;<b><span>введённый вами адрес</span></b>&nbsp;
                            <span>придет письмо с подтверждением смены почты. Пожалуйста,</span>
                            &nbsp;<b><span>проверьте правильность</span></b>&nbsp;
                            <span>написания нового адреса, чтобы ваш аккаунт не попал в руки злоумышленников.</span>
                        </p>
                        <div class="balloon balloon--right block ng-isolate-scope" place="form-settings-email">
                            <!-- ngIf: block.text && !block.file --><!-- ngIf: block.file --></div>
                    </div>
                    <div class="actions-group">
                        <button type="submit" class="btn btn-success" disabled="disabled">
                            <i class="fa fa-fw fa-save"></i>
                            <span>Сохранить</span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    </section>
@endsection