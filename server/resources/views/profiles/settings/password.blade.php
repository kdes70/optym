@extends('layouts.profiles')

@section('content')

    <div class="card">
        <div class="card__head">
            <div class="card__title">
                {{--<a class="link-black" href="#/settings/user_list">--}}
                {{--<i class="fa fa-arrow-left" aria-hidden="true"></i>--}}
                {{--</a>--}}
                Редактирование профиля
            </div>
        </div>
        <div class="card__content">
            <div class="panel">
                <form method="POST" action="{{ route('password.request') }}">
                    @csrf

                    {{--TODO TOKEN --}}
                    {{--<input type="hidden" name="token" value="{{ $token }}">--}}
                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirm Password</label>
                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required>

                            @if ($errors->has('password_confirmation'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Reset Password
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    </section>
@endsection