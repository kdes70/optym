@extends('layouts.app')

@section('content')

    <section class="section-profile">

        @include('profiles.partials._profile-header')

        <div class="container">

            <div class="sub-menu">
                <span class="sub-menu__item active">Основное</span>
                <a class="sub-menu__item" href="/account/producer/feature">Условия</a>
                <a class="sub-menu__item" href="/account/producer/office/add">Контакты</a>
            </div>

            {{--content--}}
        </div>
    </section>

@endsection
