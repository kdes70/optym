<form method="POST" action="{{ route('profiles.settings.update') }}">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label for="username" class="col-form-label">Username</label>
        <input id="username"
               type="text"
               class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}"
               name="username"
               value="{{ old('username', $user->username) }}"
               required>
        @if ($errors->has('username'))
            <span class="invalid-feedback"><strong>{{ $errors->first('username') }}</strong></span>
        @endif
    </div>

    <div class="form-group">
        <label for="email" class="col-form-label">Email</label>
        <input id="email"
               type="email"
               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
               name="email"
               value="{{ old('email', $user->email) }}" required>
        @if ($errors->has('email'))
            <span class="invalid-feedback"><strong>{{ $errors->first('email') }}</strong></span>
        @endif
    </div>

    <div class="form-group">
        <label for="first_name" class="col-form-label">First Name</label>
        <input id="first_name"
               type="text"
               class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
               name="first_name"
               value="{{ old('first_name', $user->first_name) }}">
        @if ($errors->has('first_name'))
            <span class="invalid-feedback"><strong>{{ $errors->first('first_name') }}</strong></span>
        @endif
    </div>

    <div class="form-group">
        <label for="last_name" class="col-form-label">Last Name</label>
        <input id="last_name"
               type="text"
               class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
               name="last_name"
               value="{{ old('last_name', $user->last_name) }}">
        @if ($errors->has('last_name'))
            <span class="invalid-feedback"><strong>{{ $errors->first('last_name') }}</strong></span>
        @endif
    </div>

    <div class="form-group">
        <label for="phone" class="col-form-label">Phone</label>
        <input id="phone"
               type="tel"
               class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
               name="phone"
               value="{{ old('phone', $user->phone) }}"
               required>
        @if ($errors->has('phone'))
            <span class="invalid-feedback"><strong>{{ $errors->first('phone') }}</strong></span>
        @endif
    </div>

    <div class="form-group">
        <label for="country" class="col-form-label">Country</label>
        <input id="country"
               type="text"
               class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}"
               name="country"
               value="{{ old('country', $user->country) }}">
        @if ($errors->has('country'))
            <span class="invalid-feedback"><strong>{{ $errors->first('country') }}</strong></span>
        @endif
    </div>

    <div class="form-group">
        <label for="city" class="col-form-label">City</label>
        <input id="city"
               type="text"
               class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}"
               name="city"
               value="{{ old('city', $user->city) }}">
        @if ($errors->has('city'))
            <span class="invalid-feedback"><strong>{{ $errors->first('city') }}</strong></span>
        @endif
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
</form>