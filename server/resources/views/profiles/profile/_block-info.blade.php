<div class="text-center">
    <div class="text-center vert-space">

        <img class="avatar" v-if="imgDataUrl" :src="imgDataUrl" v-show="true" style="display: none;">

        <img class="avatar" v-else="" width="200" height="200" alt="" src="{{ $user->profile->getAvatarUrl() }}">


        {{--<file-input filename="files[]" fileid="avatar" classname="hidde-new-file"></file-input>--}}

        <div class="fileinput">
            <div class="fileinput__block">
                <div class="fileinput__button" @click="toggleShow">
                    <span><i class="fa fa-pencil" aria-hidden="true"></i>Изменить</span>
                </div>
            </div>
            <my-upload field="avatar"
                       @crop-success="cropSuccess"
                       @crop-upload-success="cropUploadSuccess"
                       @crop-upload-fail="cropUploadFail"
                       v-model="show_crop"
                       :width="200"
                       :height="200"
                       lang-type="ru"
                       url="/cabinet/profile/avatar"
                       :params="params"
                       :headers="headers"
                       img-format="png">

            </my-upload>
        </div>


        {{--<file-management :settings="{{ json_encode($props) }}"></file-management>--}}
        {{--<input type="file" id="img-uploader-btn-1">--}}
    </div>
</div>