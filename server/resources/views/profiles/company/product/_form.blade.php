<div class="section-product-add">
    <div class="container">
        <product-add :segment="{{ json_encode($segment) }}"
                     save_url="{{route('profiles.company.product.save')}}">
        </product-add>
    </div>
</div>