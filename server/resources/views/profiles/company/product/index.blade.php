@extends('layouts.app')


@section('title', 'Личный кабинет поставщика')

@section('content')

<section class="section-profile">

    @include('profiles.partials._profile-header')

    <div class="container">

        {{--content--}}

        <div class="section-profile-content">
            <div class="row">

                <div class="col-xs-12 col-sm-9 col-md-12">
                    <div class="sub-header">
                        <p class="sub-header__title">Добавить новый товар</p>
                    </div>
                </div>
            </div>


            @include('profiles.company.product._form')

        </div>
    </div>

</section>

@endsection
