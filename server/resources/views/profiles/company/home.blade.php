@extends('layouts.app')

@section('content')

    <section class="section-profile">

        @include('profiles.partials._profile-header')

        <div class="container">

            <div class="sub-menu">
                <span class="sub-menu__item active">Основное</span>
                <a class="sub-menu__item" href="/account/producer/feature">Условия</a>
                <a class="sub-menu__item" href="/account/producer/office/add">Контакты</a>
            </div>

            {{--content--}}

            <div class="section-profile-content">
                <div class="row">

                    <div class="col-xs-12 col-sm-9 col-md-12">
                        <div class="sub-header">
                            <p class="sub-header__title">Расскажите о своей компании</p>
                            <p class="sub-header__text">Расскажите так, чтобы это действительно заинтересовало
                                покупателей!</p>
                        </div>
                    </div>
                </div>

                <form id="company-info" action="{{route('profiles.company.edit', $user->getUsername())}}">

                    <div class="row">
                        <div class="col-xs-12 col-sm-3 company-info__photo-wrapper">

                            <img class="avatar" v-if="imgDataUrl" :src="imgDataUrl" v-show="true"
                                 style="display: none;">

                            <img class="avatar" v-else="" width="200" height="200" alt=""
                                 src="{{ $user->profile->getAvatarUrl() }}">

                            <div class="fileinput">
                                <div class="fileinput__block">
                                    <div class="fileinput__button" @click="toggleShow">
                                        <span><i class="fa fa-pencil" aria-hidden="true"></i>Изменить</span>
                                    </div>
                                </div>
                                <my-upload field="avatar"
                                           @crop-success="cropSuccess"
                                           @crop-upload-success="cropUploadSuccess"
                                           @crop-upload-fail="cropUploadFail"
                                           v-model="show_crop"
                                           :width="200"
                                           :height="200"
                                           lang-type="ru"
                                           url="/cabinet/profile/avatar"
                                           :params="params"
                                           :headers="headers"
                                           img-format="png">
                                </my-upload>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-9 col-md-6">

                            <div class="form-group">
                                <label for="name" class="col-form-label">Название</label>
                                <input id="name"
                                       type="text"
                                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                       name="name"
                                       value="{{ old('name', $company->getName()) }}"
                                       required>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="tagline" class="col-form-label">Краткий текст о компании</label>
                                <textarea id="tagline"
                                          name="tagline"
                                          rows="3"
                                          class="form-control{{ $errors->has('tagline') ? ' is-invalid' : '' }}">{{old('tagline', $company->tagline)}}
                               </textarea>
                                @if ($errors->has('tagline'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('tagline') }}</strong></span>
                                @endif
                            </div>


                            <div class="form-group">
                                <label for="description" class="col-form-label">Краткий текст о компании</label>
                                <textarea id="description" name="description" rows="4"
                                          class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}">{{old('description', $company->description)}}
                               </textarea>
                                @if ($errors->has('description'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('description') }}</strong></span>
                                @endif
                            </div>

                        </div>

                    </div>
                    <div class="row">


                        <div class="col-md-9 offset-md-0">
                            <div class="form-group">
                                <label for="text" class="col-form-label">Подробный текст о компании</label>
                                <textarea id="text" name="text" rows="6"
                                          class="form-control{{ $errors->has('text') ? ' is-invalid' : '' }}">{{old('text', $company->text)}}
                                    </textarea>
                                @if ($errors->has('text'))
                                    <span class="invalid-feedback"><strong>{{ $errors->first('text') }}</strong></span>
                                @endif
                            </div>
                        </div>


                        <div class="col-xs-12 col-md-9 offset-md-3">
                            <button class="btn btn-outline-success" type="submit">Сохранить</button>
                        </div>
                    </div>


                </form>
            </div>
        </div>

    </section>

@endsection
