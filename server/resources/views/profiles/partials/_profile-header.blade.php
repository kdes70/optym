<div class="section-profile__menu">

    <div class="section-profile__navigation">

        <div class="profile-header-wrapper">
            <div class="container">
                <div class="section-profile__header">
                    <h1>Личный кабинет поставщика</h1>
                </div>
            </div>
        </div>

        <div class="profile-menu-wrapper">
            <div class="container">

                @if(Auth::user()->isWholesale())
                    @include('profiles.partials._profile-company-menu')
                @else
                    @include('profiles.partials._profile-menu')
                @endif



            </div>
        </div>
    </div>

</div>