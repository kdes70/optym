<div id="profile-menu" class="profile-menu">
    {{--<div class="user-menu__header">
        <a href="#" class="js-toggle-navigation" data-navigation="user-navigation-menu"></a>
        <h3>Меню личного кабинета</h3>
    </div>--}}
    <ul class="profile-menu__list">
        <li>
            <a href="/account/customer/" class="btn btn-profile-header  active ">Мой профиль</a>
        </li>
        <li>
            <a href="/account/customer/cart" class="btn btn-profile-header ">Корзины</a>
        </li>
        <li>
            <a href="/account/customer/order" class="btn btn-profile-header ">Заказы</a>
        </li>
        <li>
            <a href="/account/customer/inquiry" class="btn btn-profile-header ">
                <span>Запросы</span>
            </a>
        </li>
        <li>
            <a href="{{route('profiles.company.product.add')}}" class="btn btn-profile-header ">Мои товары</a>
        </li>
        <!-- <li>
            <a href="/account/producer/settings" class="btn btn-user-header ">Настройки</a>
        </li> -->
    </ul>
</div>