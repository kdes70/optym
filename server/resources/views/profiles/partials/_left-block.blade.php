<div>
    <div class="card list-group">
        <div class="list-group-item list-group-item-gray">

            <div class="media btm-space-zero">
                <div class="media-left">
                    <a href="{{ route('profiles.home', ['username' => $user->getUsername()]) }}">
                        <div class="img_avatar">
                            <img class="img_avatar__circle"
                                 alt=""
                                 src="{{ $user->profile->getAvatarUrl() }}">
                        </div>
                    </a>
                </div>
                <div class="media-body">
                    <div class="dropdown">

                        <a href="javascript:void(0)"
                           class="dropdown-toggle"
                           data-toggle="dropdown"
                           aria-haspopup="true"
                           aria-expanded="false">
                            <b>{{ Auth::user()->getUsername() }}
                                &nbsp
                                <span class="caret"></span></b>
                        </a>

                        <ul class="dropdown-menu">
                            <li><a href="{{ route('profiles.settings.home') }}">Изменить профиль</a></li>
                            <li><a href="#/settings/user_pass">Изменить пароль</a></li>
                            <li><a href="/my/logout">Выйти из аккаунта</a></li>
                        </ul>
                    </div>
                    <p class="list-group-item-text">
                        <a class="link-black" href="#/money">300 руб.</a></p>
                </div>
            </div>
        </div>
        <a class="list-group-item active" href="{{ route('profiles.home', ['username' => $user->getUsername()]) }}">
            <i class="fa fa-fw fa-user"></i>{{__('Профиль')}}
        </a>
        <a class="list-group-item" href="{{ route('profiles.settings.email') }}">
            <i class="fa fa-fw fa-at"></i>{{__('Изменить Email')}}
        </a>
        <a class="list-group-item" href="{{ route('profiles.settings.password') }}">
            <i class="fa fa-fw fa-key"></i>{{__('Сменить пароль')}}
        </a>
        {{--<a class="list-group-item" href="{{route('messages')}}">Сообщения</a>--}}
        {{--<a class="list-group-item" href="#/money">Баланс</a>--}}
        {{--<a class="list-group-item" href="#/favorites">Избранное</a>--}}
        {{--<a class="list-group-item" href="#/settings">Настройки</a>--}}
        {{--<a class="list-group-item" href="#/message/chat/admin">Служба поддержки</a>--}}
    </div>

    {{--TODO переделать в вид компонент--}}
    {{--@if($company = $user->company()->first())--}}
        {{--@include('profiles.company._nav-block', compact('company'))--}}
    {{--@endif--}}


</div>
