@extends('layouts.non_navbar')
@section('body_class', 'body__register')
@section('content')

    <registration_form :user_type="{{json_encode($user_types)}}" :company_type="{{ json_encode($company_type) }}"></registration_form>

@endsection
