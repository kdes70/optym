@extends('auth.register')
@section('registration-form')
    {{--Регистрация оптового продавца--}}
    <div class="card-header">Регистрация оптового продавца</div>

    <div class="card-body">
        <div class="login-panel panel panel-default wholesaler-reg">
            <div class="panel-body">
                {!! Form::open(['url' => 'register', 'id' => 'wholesaler']) !!}
                <fieldset>
                    <input type="hidden" name="user_type" value="{{$input['user_type']}}">
                    <div class="form-group">
                        <label for="company_name">Название компании
                            <span class="label-tip">обязательно</span>
                        </label>
                        <input id="company_name"
                               type="text"
                               class="form-control{{ $errors->has('company_name') ? ' is-invalid' : '' }}"
                               placeholder="Название предприятия без кавычек. Например: Мир мебели"
                               name="company_name"
                               value="{{ old('company_name') }}"
                               autofocus
                        >
                        @if ($errors->has('company_name'))
                            <span class="invalid-feedback">
                    <strong>{{ $errors->first('company_name') }}</strong>
                </span>
                        @endif
                    </div>

                    @if($company_types)
                        <div class="form-group">
                            <label for="company_type">Вид деятельности
                                <span class="label-tip">обязательно</span>
                            </label>
                            <select id=company_type"
                                    class="form-control {{ $errors->has('company_type') ? ' is-invalid' : '' }}"
                                    name="company_type">
                                <option value="">Вид деятельности</option>
                                @foreach ($company_types as $key => $company_type)
                                    <option value="{{ $key }}"{{ $key == old('company_type')? ' selected' : '' }}>
                                        {{ $company_type }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="country">Страна
                                    <span class="label-tip">обязательно</span>
                                </label>
                                <input id="country"
                                       type="text"
                                       class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}"
                                       placeholder="Страна"
                                       name="country"
                                       value="{{ old('country') }}"
                                >
                                @if ($errors->has('country'))
                                    <span class="invalid-feedback">
                                <strong>{{ $errors->first('country') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="city">Город
                                    <span class="label-tip">обязательно</span>
                                </label>
                                <input id="city"
                                       type="text"
                                       class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}"
                                       placeholder="Город"
                                       name="city"
                                       value="{{ old('city') }}"
                                >
                                @if ($errors->has('city'))
                                    <span class="invalid-feedback">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                                @endif
                            </div>

                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="first_name">Ваше Имя
                                    <span class="label-tip">обязательно</span>
                                </label>
                                <input id="first_name"
                                       class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                       placeholder="Ваше имя"
                                       name="first_name"
                                       type="text"
                                       maxlength="30"
                                       value="{{ old('first_name') }}"
                                >
                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('company_name') }}</strong>
                      </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="last_name">Фамилия
                                    <span class="label-tip">обязательно</span>
                                </label>
                                <input id="last_name"
                                       class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                       placeholder="Ваше имя"
                                       name="last_name"
                                       type="text"
                                       maxlength="30"
                                       value="{{ old('last_name') }}"
                                >
                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group phone-cont">
                        <label for="phone">Телефон
                            <span class="label-tip">обязательно</span>
                        </label>
                        <input id="phone"
                               class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                               placeholder="+7(___)___-__-__"
                               name="phone"
                               type="tel"
                               autocomplete="off"
                               value="{{ old('phone') }}"
                        >
                        @if ($errors->has('phone'))
                            <span class="invalid-feedback">
                    <strong>{{ $errors->first('phone') }}</strong>
                </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="email">Email
                            <span class="label-tip">обязательно</span>
                        </label>
                        <input id="email"
                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                               placeholder="Email"
                               name="email"
                               type="email"
                               value="{{ old('email') }}"
                        >
                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="email">Придумайте пароль
                            <span class="label-tip">обязательно</span>
                        </label>
                        <input id="password"
                               class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                               placeholder="*******"
                               name="password"
                               type="password"
                        >
                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                        @endif
                    </div>
                </fieldset>
                <button class="btn btn-success registration" type="submit">Зарегистрироватся</button>
                <button class="btn btn-default registration" type="submit">Назад</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection