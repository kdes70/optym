@extends('auth.register')
@section('registration-form')
    {{--Регистрация оптового покупателя --}}
    <div class="card-header">Регистрация оптового покупателя</div>

    <div class="card-body">
        <div class="login-panel panel panel-default wholesale_buyer-reg">
            <div class="panel-body">
                {!! Form::open(['route' => 'register', 'id' => 'register_user']) !!}
                <input type="hidden" name="user_type" value="{{$input['user_type']}}">
                <fieldset>
                    <div class="form-group phone-cont">
                        <label for="phone">Телефон
                            <span class="label-tip">обязательно</span>
                        </label>
                        <input id="phone"
                               class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                               placeholder="+7(___)___-__-__"
                               name="phone"
                               type="tel"
                               autocomplete="off"
                               value="{{ old('phone') }}"
                        >
                        @if ($errors->has('phone'))
                            <span class="invalid-feedback">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="email">Email
                            <span class="label-tip">обязательно</span>
                        </label>
                        <input id="email"
                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                               placeholder="Email"
                               name="email"
                               type="email"
                               value="{{ old('email') }}"
                        >
                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="email">Придумайте пароль
                            <span class="label-tip">обязательно</span>
                        </label>
                        <input id="password"
                               class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                               placeholder="*******"
                               name="password"
                               type="password"
                        >
                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                        @endif
                    </div>
                </fieldset>
                <button class="btn btn-success registration" type="submit">Зарегистрироватся</button>
                <button class="btn btn-default registration" type="submit">Назад</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
