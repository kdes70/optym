@extends('layouts.non_navbar')
@section('body_class', 'body__register')
@section('content')
    <div class="container registration_form">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">{{__('Register')}}</div>

                    <div class="card-body">

                        <div class="registration-panel panel panel-default account-reg">
                            <div class="panel-body">
                                {!! Form::open([round('register.account_type'), 'id' => 'register_account_type']) !!}
                                <fieldset>
                                    <!--Radio group-->
                                    <div class="radios-as-buttons ">
                                        @if($user_types)
                                            @foreach($user_types as $key => $user_type)
                                                <div>
                                                    <input type="radio"
                                                           name="user_type"
                                                           value="{{ $key }}"
                                                           id="user_type_{{$key}}"
                                                           @if($key === 1) checked @endif/>
                                                    <label for="user_type_{{$key}}">
                                                        Регистрация как <b>{{$user_type}}</b>
                                                    </label>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <!--Radio group-->
                                </fieldset>
                                <br>
                                <br>
                                <button class="btn btn-success registration" type="submit">Зарегистрироватся</button>
                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
