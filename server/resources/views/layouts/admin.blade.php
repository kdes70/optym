<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>{{ config('app.name', 'Laravel') }} Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700italic,700,900,900italic"
          rel="stylesheet">
    <!--THEME STYLESHEETS -->
    <style type="text/css">
        [fuse-cloak],
        .fuse-cloak {
            display: none !important;
        }
    </style>

    <!-- Icons.css -->
    <link type="text/css" rel="stylesheet" href="{{asset_admin('icons/fuse-icon-font/style.css')}}">
    <!-- Animate.css -->
    <link type="text/css" rel="stylesheet" href="{{asset_admin('node_modules/animate.css/animate.min.css')}}">
    <!-- PNotify -->
    <link type="text/css" rel="stylesheet" href="{{asset_admin('node_modules/pnotify/dist/PNotifyBrightTheme.css')}}">
    <!-- Nvd3 - D3 Charts -->
    <link type="text/css" rel="stylesheet" href="{{asset_admin('node_modules/nvd3/build/nv.d3.min.css')}}"/>
    <!-- Perfect Scrollbar -->
    <link type="text/css" rel="stylesheet"
          href="{{asset_admin('node_modules/perfect-scrollbar/css/perfect-scrollbar.css')}}"/>

    <link type="text/css" rel="stylesheet" href="{{asset_admin('node_modules/switchery/dist/switchery.css')}}"/>
    <!-- Fuse Html -->
    <link type="text/css" rel="stylesheet" href="{{asset_admin('fuse-html/fuse-html.min.css')}}"/>
    <!-- Main CSS -->
    <link type="text/css" rel="stylesheet" href="{{asset_admin('css/main.css')}}">
    <!-- / THEME STYLESHEETS -->
    {!! style_ts(asset('css/admin/admin-theme.css')) !!}


</head>
<body class="layout layout-vertical layout-left-navigation layout-below-toolbar layout-below-footer">
<main id="app_admin">
    <div id="wrapper">

        @include('layouts.partials.admin.aside')

        <div class="content-wrapper">

            @include('layouts.partials.admin._top-toolbar')

            <div class="content custom-scrollbar">

                @include('layouts.partials.admin.breadcrumbs')

                @yield('content')
            </div>

        </div>
        <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar"
             data-fuse-bar-position="right">

        </div>
    </div>
</main>
<!-- THEME JAVASCRIPT -->
<!-- jQuery -->
<script type="text/javascript" src="{{asset_admin('node_modules/jquery/dist/jquery.min.js')}}"></script>
<!-- Mobile Detect -->
<script type="text/javascript" src="{{asset_admin('node_modules/mobile-detect/mobile-detect.min.js')}}"></script>
<!-- Perfect Scrollbar -->
<script type="text/javascript"
        src="{{asset_admin('node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js')}}"></script>
<!-- Popper.js -->
<script type="text/javascript" src="{{asset_admin('node_modules/popper.js/dist/umd/popper.min.js')}}"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="{{asset_admin('node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Nvd3 - D3 Charts -->
<script type="text/javascript" src="{{asset_admin('node_modules/d3/d3.min.js')}}"></script>
<script type="text/javascript" src="{{asset_admin('node_modules/nvd3/build/nv.d3.min.js')}}"></script>
<!-- Data tables -->
<script type="text/javascript" src="{{asset_admin('node_modules/datatables.net/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript"
        src="{{asset_admin('node_modules/datatables-responsive/js/dataTables.responsive.js')}}"></script>
<!-- PNotify -->
<script type="text/javascript" src="{{asset_admin('node_modules/pnotify/dist/iife/PNotify.js')}}"></script>
<script type="text/javascript" src="{{asset_admin('node_modules/pnotify/dist/iife/PNotifyStyleMaterial.js')}}"></script>
<script type="text/javascript" src="{{asset_admin('node_modules/pnotify/dist/iife/PNotifyButtons.js')}}"></script>
<script type="text/javascript" src="{{asset_admin('node_modules/pnotify/dist/iife/PNotifyCallbacks.js')}}"></script>
<script type="text/javascript" src="{{asset_admin('node_modules/pnotify/dist/iife/PNotifyMobile.js')}}"></script>
<script type="text/javascript" src="{{asset_admin('node_modules/pnotify/dist/iife/PNotifyHistory.js')}}"></script>
<script type="text/javascript" src="{{asset_admin('node_modules/pnotify/dist/iife/PNotifyDesktop.js')}}"></script>
<script type="text/javascript" src="{{asset_admin('node_modules/pnotify/dist/iife/PNotifyConfirm.js')}}"></script>
<script type="text/javascript" src="{{asset_admin('node_modules/pnotify/dist/iife/PNotifyReference.js')}}"></script>

<script type="text/javascript" src="{{asset_admin('node_modules/switchery/dist/switchery.js')}}"></script>


<!-- Fuse Html -->
<script type="text/javascript" src="{{asset_admin('fuse-html/fuse-html.min.js')}}"></script>
{{--<!-- Main JS -->--}}
<script type="text/javascript" src="{{asset_admin('js/main.js')}}"></script>

<!-- / THEME JAVASCRIPT -->
{!! script_ts(asset('js/admin.js')) !!}
@yield('scripts')
</body>
</html>
