@include('layouts.partials._header')

<body class="@yield('body_class', 'main')">

<div id="app">

    @include('layouts.partials.navigation')

    <main class="app-content">
        @include('layouts.partials.flash')
        @yield('content')
    </main>

    @include('layouts.partials._modals')

</div>


@include('layouts.partials._footer')
