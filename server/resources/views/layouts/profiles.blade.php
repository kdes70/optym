@include('layouts.partials._header')

<body class="@yield('body_class', 'cabinet')">
<div id="app">

    @include('layouts.partials.navigation')

    <main class="app-content py-3">
        <div class="container">
            <div class="row">
                {{-- left block--}}
                @section('cabinet-sidebar')
                    <div class="col-md-3">
                        @include('profiles.partials._left-block')
                    </div>
                @show

                <div class="col-md-9">
                    <section class="content">
                        @include('layouts.partials.flash')
                        @yield('content')
                    </section>
                </div>

            </div>

        </div>
    </main>

</div>

@include('layouts.partials._footer')
