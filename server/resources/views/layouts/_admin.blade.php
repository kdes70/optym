<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
</head>
<body class="@yield('body_class', 'admin')">

<div id="app-admin" class="wrapper">
    <!-- Sidebar Holder -->
        @include('layouts.partials.admin.sidebar')

    <!-- Page Content Holder -->
    <div id="content">

       @include('layouts.partials.admin.navbar')

        @include('layouts.partials.flash')

        @yield('content')

    </div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/admin.js') }}"></script>
@yield ('scripts')
</body>
</html>
