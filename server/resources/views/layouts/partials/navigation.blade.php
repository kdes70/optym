<nav class="navbar navbar-expand-md navbar-light optum__navbar">
    <div class="container">
        <figure class="logo">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{assetImg('logo-48x48.png')}}" alt="{{ config('app.name', 'Laravel') }}">
                {{ config('app.name', 'Laravel') }}
            </a>

        </figure>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            @if(isset($categories))
                <div class="catalog_nav_block">
                    <catalog-drop :categories="{{$categories}}"/>
                </div>
        @endif

        <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                <li><a class="nav-link" href="{{ route('suppliers.list') }}">Поставщики</a></li>
                <li><a class="nav-link" href="#">Покупатели</a></li>
                @can('manage-own-product')
                    <li><a class="nav-link" href="{{route('profiles.company.product.add')}}">Мои товары</a></li>
                @endcan
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    {{--<li><a class="nav-link" href="{{ route('login') }}">Login</a></li>--}}
                    <li><a href="" class="nav-link" data-toggle="modal" data-target="#elegantModalForm">Login</a>
                    </li>
                    <li><a class="nav-link" href="{{ route('register.form') }}">Register</a></li>
                @else

                    {{--<li class="nav-item Dropdown">--}}
                    {{--<a class="nav-link Dropdown-toggle" data-toggle="Dropdown" id="Preview" href="#" role="button" aria-haspopup="true" aria-expanded="false">--}}
                    {{--Dropdown--}}
                    {{--</a>--}}
                    {{--<div class="Dropdown-menu" aria-labelledby="Preview">--}}
                    {{--<a class="Dropdown-item" href="#">Dropdown Link 1</a>--}}
                    {{--<a class="Dropdown-item" href="#">Dropdown Link 2</a>--}}
                    {{--<a class="Dropdown-item" href="#">Dropdown Link 3</a>--}}
                    {{--</div>--}}
                    {{--</li>--}}


                    <li class="nav-item dropdown profile-menu">
                        <a id="profileDropdown"
                           class="nav-link dropdown-toggle user_nav_link"
                           href="#"
                           role="button"
                           data-toggle="dropdown"
                           aria-haspopup="true"
                           aria-expanded="false">
                            <div class="img_avatar">
                                <img class="img_avatar__circle x25"
                                     alt="{{ Auth::user()->getUsername() }}"
                                     src="{{ Auth::user()->profile->getAvatarUrl() }}">
                            </div>
                            {{ Auth::user()->getUsername() }}
                            <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="profileDropdown">

                            @if(Auth::user()->isWholesale())
                                <a class="dropdown-item"
                                   href="{{ route('profiles.company.home', ['username' =>  Auth::user()->getUsername()] ) }}">
                                    <i class="fa fa-lg fa-user"></i>
                                    {{__('Профиль')}} № {{auth()->id()}}
                                </a>
                            @else
                                <a class="dropdown-item"
                                   href="{{ route('profiles.settings.home', ['username' =>  Auth::user()->getUsername()] ) }}">
                                    <i class="fa fa-lg fa-user"></i>
                                    {{__('Профиль')}} № {{auth()->id()}}
                                </a>
                            @endif


                            <a class="dropdown-item" href="{{ route('profiles.settings.home') }}">
                                <i class="fa fa-lg fa-pencil"></i>
                                {{__('Редактировать')}}
                            </a>
                            @can ('admin-panel')
                                <a class="dropdown-item" href="{{ route('admin.dashboard') }}">Admin</a>
                            @endcan

                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-lg fa-sign-out"></i>
                                {{__('Выйти')}}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>

                @endguest
            </ul>
        </div>
    </div>
</nav>
