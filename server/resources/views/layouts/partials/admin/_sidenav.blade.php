<ul class="nav flex-column custom-scrollbar" id="sidenav" data-children=".nav-item">

    <li class="subheader">
        <span>APPS</span>
    </li>

    <li class="nav-item">
        <a class="nav-link ripple {{ Route::is('admin.dashboard') ? 'active' : '' }}"
           href="{{route('admin.dashboard')}}"
           data-url="{{route('admin.dashboard')}}">
            <i class="icon s-4 icon-tile-four"></i>
            <span>Dashboards</span>
        </a>
    </li>
    <li class="subheader">
        <span>USERS</span>
    </li>
    <li class="nav-item" role="tab" id="heading-users">

        <a class="nav-link ripple with-arrow collapsed"
           data-toggle="collapse"
           data-target="#collapse-users"
           href="#"
           aria-expanded="false"
           aria-controls="collapse-dashboards">

            <i class="icon s-4 icon-layers"></i>

            <span>{{__('users')}}</span>
        </a>
        <ul id="collapse-users"
            class='collapse '
            role="tabpanel"
            aria-labelledby="heading-users"
            data-children=".nav-item">

            <li class="nav-item">
                <a class="nav-link ripple {{ Route::is('admin.users.index') ? 'active' : '' }}"
                   href="{{ route('admin.users.index') }}"
                   data-url="{{ route('admin.users.index') }}">
                    <span>{{__('List')}}</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple {{ Route::is('admin.users.create') ? 'active' : '' }}"
                   href="{{ route('admin.users.create') }}"
                   data-url="{{ route('admin.users.create') }}">
                    <span>{{__('Add Users')}}</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="subheader">
        <span>PAGES</span>
    </li>

    <li class="nav-item" role="tab" id="heading-dashboards">

        <a class="nav-link ripple with-arrow collapsed"
           data-toggle="collapse"
           data-target="#collapse-dashboards"
           href="#"
           aria-expanded="false"
           aria-controls="collapse-dashboards">

            <i class="icon s-4 icon-layers"></i>

            <span>{{__('pages.navigation')}}</span>
        </a>
        <ul id="collapse-dashboards"
            class='collapse '
            role="tabpanel"
            aria-labelledby="heading-dashboards"
            data-children=".nav-item">

            <li class="nav-item">
                <a class="nav-link ripple {{ Route::is('admin.pages.index') ? 'active' : '' }}"
                   href="{{ route('admin.pages.index') }}"
                   data-url="{{ route('admin.pages.index') }}">
                    <span>{{__('List')}}</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple {{ Route::is('admin.pages.create') ? 'active' : '' }}"
                   href="{{ route('admin.pages.create') }}"
                   data-url="{{ route('admin.pages.create') }}">
                    <span>{{__('Add Pages')}}</span>
                </a>
            </li>
        </ul>
    </li>

    <li class="subheader">
        <span>CATALOG</span>
    </li>
    <li class="nav-item" role="tab" id="heading-catalog">

        <a class="nav-link ripple with-arrow collapsed"
           data-toggle="collapse"
           data-target="#collapse-catalog"
           href="#"
           aria-expanded="false"
           aria-controls="collapse-catalog">

            <i class="icon s-4 icon-layers"></i>

            <span>{{__('Категории')}}</span>
        </a>
        <ul id="collapse-catalog"
            class='collapse '
            role="tabpanel"
            aria-labelledby="heading-catalog"
            data-children=".nav-item">

            <li class="nav-item">
                <a class="nav-link ripple {{ Route::is('admin.categories.index') ? 'active' : '' }}"
                   href="{{ route('admin.categories.index') }}"
                   data-url="{{ route('admin.categories.index') }}">
                    <span>{{__('List')}}</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple {{ Route::is('admin.categories.create') ? 'active' : '' }}"
                   href="{{ route('admin.categories.create') }}"
                   data-url="{{ route('admin.categories.create') }}">
                    <span>{{__('Add')}}</span>
                </a>
            </li>
        </ul>
    </li>

</ul>
