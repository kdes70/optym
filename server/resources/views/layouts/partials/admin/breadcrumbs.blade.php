@isset($breadcrumbs_admin)
    <nav class="breadcrumb">
    @foreach($breadcrumbs_admin as $breadcrumb_admin)

        @if($breadcrumb_admin->isDisabled())
            <span class="breadcrumb-item active">{{$breadcrumb_admin->getTitle()}}</span>
       @else
            <a class="breadcrumb-item" href="{{$breadcrumb_admin->getLink()}}">{{$breadcrumb_admin->getTitle()}}</a>
        @endif
    @endforeach
    </nav>
@endisset