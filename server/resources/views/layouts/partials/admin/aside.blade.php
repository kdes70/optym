<aside id="aside" class="aside aside-left" data-fuse-bar="aside" data-fuse-bar-media-step="md" data-fuse-bar-position="left">
    <div class="aside-content bg-primary-700 text-auto">

        <div class="aside-toolbar">

            <div class="logo">
                <span class="logo-icon">
                     <img src="{{assetImg('logo-48x48.png')}}" alt="{{ config('app.name', 'Laravel') }}">
                </span>
                <span class="logo-text">{{ config('app.name', 'Laravel') }}</span>
            </div>

            <button id="toggle-fold-aside-button" type="button" class="btn btn-icon d-none d-lg-block" data-fuse-aside-toggle-fold>
                <i class="icon icon-backburger"></i>
            </button>

        </div>

        @include('layouts.partials.admin._sidenav')

    </div>

</aside>
