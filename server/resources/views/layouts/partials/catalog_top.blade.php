@include('layouts.partials.breadcrumbs')

<div class="container">
    <div class="row catalog-name-wrapper">
        <div class="col-md-8">
            <h1 class="catalog-name" data-cat-id="{{$category->id}}">{{$category->name}} оптом</h1>
        </div>
        <div class="col-sm-4 switch-btn__wrapper">

            @if( Route::is('catalog.suppliers'))
                <div class="switch-btn__sign">
                    <span>Поставщики</span>
                </div>
            @else
            <a href="{{route('catalog.suppliers', ['category' => $category->getUrl()])}} " class="{{ Route::is('catalog.suppliers') ? 'active' : '' }}">
                <div class="switch-btn__sign">
                    <span>Поставщики</span>
                </div>
            </a>
            @endif
            @

            @if( Route::is('category'))
                <div class="switch-btn__sign">
                    <span>Товары</span>
                </div>
            @else

            <a href="{{route('category', ['path' => $category->getUrl()])}}" class="{{ Route::is('category') ? 'active' : '' }}">
                <div class="switch-btn__sign">
                    <span>Товары</span>
                </div>
            </a>

            @endif
        </div>
    </div>
</div>

@include('catalog.siblings')