<footer>
    <div class="container">
        <div class="border-top pt-3">
            <p>&copy Copyright (c) <b>  {{ date('Y') }} - {{ config('app.name', 'Laravel') }} </b>
                Все права защищены. Все торговые марки и бренды, используемые на сайте, принадлежат их владельцам и
                исполняют информационные функции. Использование данного веб-сайта означает принятие условий заказа,
                торговые условия {{ config('app.name', 'Laravel') }}. Компания {{ config('app.name', 'Laravel') }} не несет ответственности за содержание предложений,
                представленных на этой странице.
            </p>

        </div>
    </div>
</footer>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
@yield ('scripts')
</body>
</html>