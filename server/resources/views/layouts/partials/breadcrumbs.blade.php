@if($breadcrumbs)

    <div class="container">

        <ul class="breadcrumb">

            {{--{{dd($breadcrumbs)}}--}}

            @foreach($breadcrumbs as $breadcrumb)

                {{--{{dd($breadcrumb)}}--}}
                <li>
                    @if($breadcrumb->isDisabled())
                        <span>{{$breadcrumb->getTitle()}}</span>
                    @else
                        <a href="{{$breadcrumb->getLink()}}">
                            <span>{{$breadcrumb->getTitle()}}</span>
                        </a>
                    @endif
                </li>
            @endforeach

        </ul>

    </div>
@endif