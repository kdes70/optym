<nav id="sidebar">
    <div class="sidebar-header">
        <h3>{{config('app.name', 'Laravel')}}</h3>
        <strong>OPT</strong>
    </div>

    <ul class="list-unstyled components">
        <li @if(Route::currentRouteName() == 'admin.home') class="active" @endif>
            <a href="{{ route('admin.home') }}">
                <i class="fa fa-tachometer" aria-hidden="true"></i>
                {{__('Dashboard')}}
            </a>
        </li>
        <li @if(Route::currentRouteName() == 'admin.pages.index') class="active" @endif>
            <a href="#pagesSubmenu" data-toggle="collapse" aria-expanded="false">
                <i class="fa fa-list-ul" aria-hidden="true"></i>
                {{__('pages.navigation')}}
            </a>
            <ul class="collapse list-unstyled" id="pagesSubmenu">
                <li><a href="{{ route('admin.pages.index') }}">{{__('List')}}</a></li>
                <li><a href="{{ route('admin.pages.create') }}">{{__('Add Pages')}}</a></li>
            </ul>
        </li>
        <li @if(Route::currentRouteName() == 'admin.activities') class="active" @endif>
            <a href="{{ route('admin.activities') }}">
                <i class="fa fa-universal-access" aria-hidden="true"></i>
                {{__('Activity Log')}}
            </a>
        </li>
        <li @if(Route::currentRouteName() == 'admin.categories.index') class="active" @endif>
            <a href="#categorySubmenu" data-toggle="collapse" aria-expanded="false">
                <i class="fa fa-list-ul" aria-hidden="true"></i>
                {{__('Categories')}}
            </a>
            <ul class="collapse list-unstyled" id="categorySubmenu">
                <li><a href="{{ route('admin.categories.index') }}">{{__('List')}}</a></li>
                <li><a href="{{ route('admin.categories.create') }}">{{__('Add Category')}}</a></li>
            </ul>
        </li>
        <li @if(Route::currentRouteName() == 'admin.regions.index') class="active" @endif>
            <a href="#regionSubmenu" data-toggle="collapse" aria-expanded="false">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                {{__('Regions')}}
            </a>
            <ul class="collapse list-unstyled" id="regionSubmenu">
                <li><a href="{{ route('admin.regions.index') }}">{{__('List')}}</a></li>
                <li><a href="{{ route('admin.regions.create') }}">{{__('oldRegion')}}</a></li>

                {{-- <li><a href="{{ route('admin.categories.create') }}">{{__('Add Category')}}</a></li>--}}
            </ul>
        </li>

        <li @if(Route::currentRouteName() == 'admin.users.index') class="active" @endif>
            <a href="#userSubmenu" data-toggle="collapse" aria-expanded="false">
                <i class="fa fa-users" aria-hidden="true"></i>
                {{__('Users')}}
            </a>
            <ul class="collapse list-unstyled" id="userSubmenu">
                <li><a href="{{ route('admin.users.index') }}">{{__('List')}}</a></li>
                <li><a href="{{ route('admin.roles.index') }}">{{__('Roles/Permission')}}</a></li>

                {{-- <li><a href="{{ route('admin.categories.create') }}">{{__('Add Category')}}</a></li>--}}
            </ul>
        </li>
        <li>
            <a href="#">
                <i class="glyphicon glyphicon-link"></i>
                Portfolio
            </a>
        </li>
        <li>
            <a href="#">
                <i class="glyphicon glyphicon-paperclip"></i>
                FAQ
            </a>
        </li>
        <li>
            <a href="#">
                <i class="glyphicon glyphicon-send"></i>
                Contact
            </a>
        </li>
    </ul>

    {{--  <ul class="list-unstyled CTAs">
          <li><a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a></li>
          <li><a href="https://bootstrapious.com/p/bootstrap-sidebar" class="article">Back to article</a></li>
      </ul>--}}
</nav>