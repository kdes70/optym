<nav class="navbar navbar-expand-md navbar-light">
    <div class="container">

        <div class="navbar-header">
            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
        </div>


        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->


                    <li class="nav-item dropdown messages-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <i
                                    class="livicon" data-name="message-flag" data-loop="true" data-color="#42aaca"
                                    data-hovercolor="#42aaca" data-size="28" id="livicon-1"
                                    style="width: 28px; height: 28px;">
                                @include('svg.messages')
                            </i>
                            <span class="label label-success">4</span>
                        </a>
                        <ul class="dropdown-menu dropdown-messages pull-right">
                            <li class="dropdown-title">4 New Messages</li>
                            {{--    <li class="unread message">
                                    <a href="javascript:;" class="message">
                                        <i class="pull-right"
                                           data-toggle="tooltip"
                                           data-placement="top" title=""
                                           data-original-title="Mark as Read">
                                            <span class="pull-right ol livicon" data-n="adjust" data-s="10" data-c="#287b0b"
                                                  id="livicon-2" style="width: 10px; height: 10px;">
                                                <svg height="10" version="1.1" width="10"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" id="canvas-for-livicon-2"
                                                     style="overflow: hidden; position: relative;"><desc
                                                            style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.2</desc><defs
                                                            style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path
                                                            fill="#287b0b" stroke="none"
                                                            d="M16,2C8.268,2,2,8.268,2,16S8.268,30,16,30S30,23.732,30,16S23.732,2,16,2ZM16,27C9.924,27,5,22.076,5,16S9.924,5,16,5C16,6.005,16,7.244,16,9C16,10.736,16,12.979,16,16C16,19.259,16,21.323,16,23C16,24.45,16,25.609,16,27Z"
                                                            stroke-width="0" transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                            style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                                </svg>
                                            </span>
                                        </i>
                                        --}}{{--<img src="http://demo.joshadmin.com/assets/img/authors/xavatar.jpg.pagespeed.ic.W3rL0HBlDK.webp"--}}{{--
                                             --}}{{--class="img-responsive message-image" alt="icon"--}}{{--
                                             --}}{{--data-pagespeed-url-hash="550509839">--}}{{--

                                        <div class="message-body">
                                            <strong>Riot Zeast</strong>
                                            <br>Hello, You there?
                                            <br>
                                            <small>8 minutes ago</small>
                                        </div>
                                    </a>
                                </li>
                                <li class="unread message">
                                    <a href="javascript:;" class="message">
                                        <i class="pull-right" data-toggle="tooltip" data-placement="top" title=""
                                           data-original-title="Mark as Read">
                                            <span class="pull-right ol livicon"
                                                  data-n="adjust" data-s="10"
                                                  data-c="#287b0b" id="livicon-3"
                                                  style="width: 10px; height: 10px;">
                                                <svg
                                                        height="10" version="1.1" width="10"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        xmlns:xlink="http://www.w3.org/1999/xlink" id="canvas-for-livicon-3"
                                                        style="overflow: hidden; position: relative;"><desc
                                                            style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.2</desc><defs
                                                            style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path
                                                            fill="#287b0b" stroke="none"
                                                            d="M16,2C8.268,2,2,8.268,2,16S8.268,30,16,30S30,23.732,30,16S23.732,2,16,2ZM16,27C9.924,27,5,22.076,5,16S9.924,5,16,5C16,6.005,16,7.244,16,9C16,10.736,16,12.979,16,16C16,19.259,16,21.323,16,23C16,24.45,16,25.609,16,27Z"
                                                            stroke-width="0" transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                            style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                                </svg>
                                            </span>
                                        </i>
                                       --}}{{-- <img src="http://demo.joshadmin.com/assets/img/authors/xavatar1.jpg.pagespeed.ic.AMbPn1UrT6.webp"
                                             class="img-responsive message-image" alt="icon"
                                             data-pagespeed-url-hash="4145824792">
    --}}{{--
                                        <div class="message-body">
                                            <strong>John Kerry</strong>
                                            <br>Can we Meet ?
                                            <br>
                                            <small>45 minutes ago</small>
                                        </div>
                                    </a>
                                </li>--}}

                            <li class="footer">
                                <a href="{{route('messages')}}">View all</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="livicon" data-name="bell" data-loop="true" data-color="#e9573f"
                               data-hovercolor="#e9573f" data-size="28" id="livicon-6"
                               style="width: 28px; height: 28px;">
                                <svg height="28" version="1.1" width="28" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                     style="overflow: hidden; position: relative; left: -0.515625px;"
                                     id="canvas-for-livicon-6">
                                    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël
                                        2.1.2
                                    </desc>
                                    <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
                                    <path fill="#e9573f" stroke="none"
                                          d="M17.5,3C17.5,3.828,16.828,4.5,16,4.5C15.17,4.5,14.5,3.828,14.5,3S15.17,1.5,16,1.5C16.828,1.5,17.5,2.172,17.5,3Z"
                                          stroke-width="0" transform="matrix(0.875,0,0,0.875,0,0)"
                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                    <path fill="#e9573f" stroke="none"
                                          d="M17,28.5C17,30.156,16.552,30.5,16,30.5S15,30.031,15,28.5C15,27,15.447,26.5,16,26.5S17,27,17,28.5Z"
                                          stroke-width="0" transform="matrix(0.875,0,0,0.875,0,0)"
                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                    <path fill="#e9573f" stroke="none"
                                          d="M5.5,26C5.5,26,5.16,23.94,5.5,23C5.943,21.775,7.481,21.194,8,20C9.605,16.303,7.729,11.621,9.5,8C10.018,6.941,10.911,5.9510000000000005,12,5.5C14.464,4.479,17.536,4.479,20,5.5C21.089,5.951,21.982,6.941,22.5,8C24.271,11.621,22.395,16.303,24,20C24.518,21.194,26.057,21.775,26.5,23C26.84,23.94,26.5,26,26.5,26H5.5Z"
                                          stroke-width="0" transform="matrix(0.875,0,0,0.875,0,0)"
                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                </svg>
                            </i>
                            <span class="label label-warning">7</span>
                        </a>
                        <ul class=" notifications dropdown-menu drop_notify">
                            <li class="dropdown-title">You have 7 notifications</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <div class="slimScrollDiv"
                                     style="position: relative; overflow: hidden; width: auto; height: 200px;">
                                    <ul class="menu" style="overflow: hidden; width: 100%; height: 200px;">
                                        <li>
                                            <i class="livicon danger" data-n="timer" data-s="20" data-c="white"
                                               data-hc="white" id="livicon-7" style="width: 20px; height: 20px;">
                                                <svg height="20" version="1.1" width="20"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                     id="canvas-for-livicon-7"
                                                     style="overflow: hidden; position: relative;">
                                                    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created
                                                        with Raphaël 2.1.2
                                                    </desc>
                                                    <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
                                                    <path fill="#ffffff" stroke="none"
                                                          d="M29.4,20.001H22.31C21.98,20.001,21.901999999999997,20.19,22.134,20.425L24.574,22.865000000000002C22.559,25.379,19.474,27,16,27C9.925,27,5,22.075,5,16S9.925,5,16,5C21.874,5,26.673000000000002,9.604,26.984,15.401C27.002,15.732,27.269,16,27.6,16H29.4C29.730999999999998,16,30.000999999999998,15.731,29.987,15.4C29.673,7.946,23.531,2,16,2C8.268,2,2,8.268,2,16S8.268,30,16,30C20.302,30,24.148,28.06,26.717,25.006999999999998L29.576,27.866C29.81,28.099,30,28.021,30,27.69V20.6C30,20.269,29.731,20.001,29.4,20.001Z"
                                                          transform="matrix(0.625,0,0,0.625,0,0)" stroke-width="0"
                                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                                    <path fill="#ffffff" stroke="none"
                                                          d="M22.632,12.287C22.908,12.766,16.979000000000003,16.59,16.5,16.866S15.41,16.979,15.134,16.5C14.858,16.021,15.022,15.411,15.5,15.134S22.355,11.809,22.632,12.287Z"
                                                          transform="matrix(0.625,0,0,0.625,0,0)" stroke-width="0"
                                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                                    <path fill="#ffffff" stroke="none"
                                                          d="M17.082,15.375C17.428,15.973,17.223,16.737000000000002,16.625,17.083C16.028,17.427999999999997,15.263,17.223,14.917,16.625S10.403,7.685,11,7.34C11.598,6.995,16.737,14.777,17.082,15.375Z"
                                                          stroke-width="0" transform="matrix(0.625,0,0,0.625,0,0)"
                                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                                </svg>
                                            </i>
                                            <a href="#">after a long time</a>
                                            <small class="pull-right">
                                                <span class="livicon paddingright_10" data-n="timer" data-s="10"
                                                      id="livicon-8" style="width: 10px; height: 10px;"><svg height="10"
                                                                                                             version="1.1"
                                                                                                             width="10"
                                                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                             id="canvas-for-livicon-8"
                                                                                                             style="overflow: hidden; position: relative;"><desc
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.2</desc><defs
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path
                                                                fill="#333333" stroke="none"
                                                                d="M29.4,20.001H22.31C21.98,20.001,21.901999999999997,20.19,22.134,20.425L24.574,22.865000000000002C22.559,25.379,19.474,27,16,27C9.925,27,5,22.075,5,16S9.925,5,16,5C21.874,5,26.673000000000002,9.604,26.984,15.401C27.002,15.732,27.269,16,27.6,16H29.4C29.730999999999998,16,30.000999999999998,15.731,29.987,15.4C29.673,7.946,23.531,2,16,2C8.268,2,2,8.268,2,16S8.268,30,16,30C20.302,30,24.148,28.06,26.717,25.006999999999998L29.576,27.866C29.81,28.099,30,28.021,30,27.69V20.6C30,20.269,29.731,20.001,29.4,20.001Z"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                stroke-width="0"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path
                                                                fill="#333333" stroke="none"
                                                                d="M22.632,12.287C22.908,12.766,16.979000000000003,16.59,16.5,16.866S15.41,16.979,15.134,16.5C14.858,16.021,15.022,15.411,15.5,15.134S22.355,11.809,22.632,12.287Z"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                stroke-width="0"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path
                                                                fill="#333333" stroke="none"
                                                                d="M17.082,15.375C17.428,15.973,17.223,16.737000000000002,16.625,17.083C16.028,17.427999999999997,15.263,17.223,14.917,16.625S10.403,7.685,11,7.34C11.598,6.995,16.737,14.777,17.082,15.375Z"
                                                                stroke-width="0"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path></svg></span>
                                                Just Now
                                            </small>
                                        </li>
                                        <li>
                                            <i class="livicon success" data-n="gift" data-s="20" data-c="white"
                                               data-hc="white" id="livicon-9" style="width: 20px; height: 20px;">
                                                <svg height="20" version="1.1" width="20"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                     id="canvas-for-livicon-9"
                                                     style="overflow: hidden; position: relative;">
                                                    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created
                                                        with Raphaël 2.1.2
                                                    </desc>
                                                    <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
                                                    <path fill="#ffffff" stroke="none"
                                                          d="M20,8.542C20,7.669,21,7.592,21,7.011C21,6.852,20.922,6.640000000000001,20.853,6.501L20.860000000000003,6.503L20.857,6.5C21.473,6.79,22,7.188,22,7.958C22,8.831,21,8.908,21,9.49C21,9.648,21.078,9.861,21.15,10L21.139999999999997,9.998L21.143,10C20.527,9.711,20,9.312,20,8.542ZM11.143,10L11.141,9.998L11.15,10C11.078,9.861,11,9.649,11,9.49C11,8.909,12,8.831,12,7.9590000000000005C12,7.189,11.473,6.790000000000001,10.857,6.501L10.859,6.503L10.853,6.5C10.922,6.639,11,6.851,11,7.01C11,7.592,10,7.669,10,8.541C10,9.312,10.527,9.71,11.143,10ZM16.143,9L16.141000000000002,8.998L16.15,9C16.078,8.861,16,8.649,16,8.49C16,7.909000000000001,17,7.831,17,6.9590000000000005C17,6.189,16.473,5.790000000000001,15.857,5.501L15.859,5.503L15.853,5.5C15.922,5.639,16,5.851,16,6.01C16,6.592,15,6.669,15,7.5409999999999995C15,8.312,15.527,8.71,16.143,9ZM27,17V24C27,26.209,22.075,28,16,28S5,26.209,5,24V17C5,15.597,6.992,14.364,10,13.65V11.438C10,11.196,10.224,11,10.5,11H11.5C11.776,11,12,11.196,12,11.438V13.277000000000001C12.945,13.143,13.95,13.053,15,13.019000000000002V10.438000000000002C15,10.196,15.224,10,15.5,10H16.5C16.775,10,17,10.196,17,10.438V13.019C18.051,13.053,19.056,13.143,20,13.277000000000001V11.438C20,11.196,20.224,11,20.501,11H21.5C21.777,11,22,11.196,22,11.438V13.65C25.008,14.364,27,15.597,27,17ZM25,17C25,16.26,23.837,15.598,22,15.14V17.562C22,17.805,21.777,18,21.5,18H20.501C20.224,18,20,17.805,20,17.563V14.762999999999998C19.076999999999998,14.635999999999997,18.069,14.548999999999998,17,14.515999999999998V16.563C17,16.805,16.775,17,16.5,17H15.5C15.224,17,15,16.805,15,16.563V14.515999999999998C13.931000000000001,14.548999999999998,12.922,14.634999999999998,12,14.762999999999998V17.563C12,17.805,11.776,18,11.5,18H10.5C10.224,18,10,17.805,10,17.562V15.14C8.163,15.598,7,16.26,7,17C7,18.381,11.029,19.5,16,19.5S25,18.381,25,17Z"
                                                          opacity="0" stroke-width="0"
                                                          transform="matrix(0.625,0,0,0.625,0,0)"
                                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path>
                                                    <path fill="#ffffff" stroke="none"
                                                          d="M5.199,30C4.537,30,4,29.418,4,28.701V17H15V30H5.199ZM28,28.701V17H17V30H26.801000000000002C27.463,30,28,29.418,28,28.701Z"
                                                          opacity="1" stroke-width="0"
                                                          transform="matrix(0.625,0,0,0.625,0,0)"
                                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path>
                                                    <path fill="#ffffff" stroke="none"
                                                          d="M11.508,2.353C10.597999999999999,1.9410000000000003,8.748,1.7460000000000002,7.507999999999999,2.353C6.187999999999999,3,5.124999999999999,4.625,5.507999999999999,6.353C6.115,9.083,10,10,10,10H3.199C2.537,10,2,10.537,2,11.2V16H15V10H16C16,10,14.688,3.79,11.508,2.353ZM9.908,8.263C9.103,8.099,7.702999999999999,7.743,7.359,6.193C7.142,5.212999999999999,7.744,4.2909999999999995,8.494,3.9229999999999996C9.197,3.5779999999999994,10.248,3.6899999999999995,10.765,3.9229999999999996C12.568000000000001,4.736999999999999,13.314,8.263,13.314,8.263S11.375,8.563,9.908,8.263ZM28.801,10H22C22,10,25.884999999999998,9.083,26.492,6.353C26.875,4.625,25.812,2.9999999999999996,24.492,2.3529999999999998C23.252000000000002,1.7459999999999998,21.402,1.9409999999999998,20.492,2.3529999999999998C17.313,3.79,16,10,16,10H17V16H30V11.2C30,10.537,29.463,10,28.801,10ZM18.686,8.263C18.686,8.263,19.432,4.7379999999999995,21.235,3.923C21.753,3.69,22.803,3.5780000000000003,23.506,3.923C24.256,4.29,24.858,5.212,24.641000000000002,6.193C24.297,7.744,22.897000000000002,8.099,22.092000000000002,8.263C20.625,8.563,18.686,8.263,18.686,8.263Z"
                                                          opacity="1" stroke-width="0"
                                                          transform="matrix(0.625,0,0,0.625,0,0)"
                                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path>
                                                </svg>
                                            </i>
                                            <a href="#">Jef's Birthday today</a>
                                            <small class="pull-right">
                                                <span class="livicon paddingright_10" data-n="timer" data-s="10"
                                                      id="livicon-10" style="width: 10px; height: 10px;"><svg
                                                            height="10" version="1.1" width="10"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                                            id="canvas-for-livicon-10"
                                                            style="overflow: hidden; position: relative;"><desc
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.2</desc><defs
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path
                                                                fill="#333333" stroke="none"
                                                                d="M29.4,20.001H22.31C21.98,20.001,21.901999999999997,20.19,22.134,20.425L24.574,22.865000000000002C22.559,25.379,19.474,27,16,27C9.925,27,5,22.075,5,16S9.925,5,16,5C21.874,5,26.673000000000002,9.604,26.984,15.401C27.002,15.732,27.269,16,27.6,16H29.4C29.730999999999998,16,30.000999999999998,15.731,29.987,15.4C29.673,7.946,23.531,2,16,2C8.268,2,2,8.268,2,16S8.268,30,16,30C20.302,30,24.148,28.06,26.717,25.006999999999998L29.576,27.866C29.81,28.099,30,28.021,30,27.69V20.6C30,20.269,29.731,20.001,29.4,20.001Z"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                stroke-width="0"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path
                                                                fill="#333333" stroke="none"
                                                                d="M22.632,12.287C22.908,12.766,16.979000000000003,16.59,16.5,16.866S15.41,16.979,15.134,16.5C14.858,16.021,15.022,15.411,15.5,15.134S22.355,11.809,22.632,12.287Z"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                stroke-width="0"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path
                                                                fill="#333333" stroke="none"
                                                                d="M17.082,15.375C17.428,15.973,17.223,16.737000000000002,16.625,17.083C16.028,17.427999999999997,15.263,17.223,14.917,16.625S10.403,7.685,11,7.34C11.598,6.995,16.737,14.777,17.082,15.375Z"
                                                                stroke-width="0"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path></svg></span>
                                                Few seconds ago
                                            </small>
                                        </li>
                                        <li>
                                            <i class="livicon warning" data-n="dashboard" data-s="20" data-c="white"
                                               data-hc="white" id="livicon-11" style="width: 20px; height: 20px;">
                                                <svg height="20" version="1.1" width="20"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                     id="canvas-for-livicon-11"
                                                     style="overflow: hidden; position: relative;">
                                                    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created
                                                        with Raphaël 2.1.2
                                                    </desc>
                                                    <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
                                                    <path fill="#ffffff" stroke="none"
                                                          d="M16,6C8.269,6,2,12.264,2,19.992C2,22.142,2.485,24.181,3.354,26H28.648C29.516,24.182,30,22.143,30,19.992C30,12.264,23.732,6,16,6ZM27.826,22H20.584C19.814,23.771,18.055,25,16,25S12.186,23.771,11.416,22H4.172C4.059,21.361,4,20.686,4,19.992C4,13.37,9.374,7.999,16,7.999C22.628999999999998,7.999,27.997999999999998,13.370000000000001,27.997999999999998,19.992C27.998,20.684,27.939,21.355,27.826,22Z"
                                                          stroke-width="0" transform="matrix(0.625,0,0,0.625,0,0)"
                                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                                    <path fill="#ffffff" stroke="none"
                                                          d="M9,20C9,18.921,9.244,17.899,9.68,16.987000000000002C10.806,14.629,13.213,13,16,13C19.866,13,23,16.134,23,20H26C26,14.478,21.522,10,16,10C12.265,10,9.008,12.048,7.291,15.083C6.469,16.535,6,18.213,6,20H9Z"
                                                          opacity="0.1" stroke-width="0"
                                                          transform="matrix(0.625,0,0,0.625,0,0)"
                                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0.1;"></path>
                                                    <path fill="#ffffff" stroke="none"
                                                          d="M7.165,15.313C6.804,16.006,6.53,16.672,6.338,17.362000000000002C6.111,18.18,6,19.031,6,20H9C9,19.463,9.053,18.956,9.16,18.47C9.268,17.98,9.431000000000001,17.511,9.649000000000001,17.052999999999997L7.165,15.313ZM17.404,18.576C18.196,19.355,18.189,20.632,17.414,21.414C16.635,22.203000000000003,15.376000000000001,22.197000000000003,14.595000000000002,21.424000000000003C14.325000000000003,21.160000000000004,13.564000000000002,18.970000000000002,13.172000000000002,18.586000000000002L8.594,14C8.199,13.611,8.341,12.834,8.591,12.583C8.84,12.333,9.607999999999999,12.189,10.001,12.578L14.587,17.172C14.979,17.561,17.143,18.314,17.404,18.576Z"
                                                          stroke-width="0" transform="matrix(0.625,0,0,0.625,0,0)"
                                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                                </svg>
                                            </i>
                                            <a href="#">out of space</a>
                                            <small class="pull-right">
                                                <span class="livicon paddingright_10" data-n="timer" data-s="10"
                                                      id="livicon-12" style="width: 10px; height: 10px;"><svg
                                                            height="10" version="1.1" width="10"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                                            id="canvas-for-livicon-12"
                                                            style="overflow: hidden; position: relative;"><desc
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.2</desc><defs
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path
                                                                fill="#333333" stroke="none"
                                                                d="M29.4,20.001H22.31C21.98,20.001,21.901999999999997,20.19,22.134,20.425L24.574,22.865000000000002C22.559,25.379,19.474,27,16,27C9.925,27,5,22.075,5,16S9.925,5,16,5C21.874,5,26.673000000000002,9.604,26.984,15.401C27.002,15.732,27.269,16,27.6,16H29.4C29.730999999999998,16,30.000999999999998,15.731,29.987,15.4C29.673,7.946,23.531,2,16,2C8.268,2,2,8.268,2,16S8.268,30,16,30C20.302,30,24.148,28.06,26.717,25.006999999999998L29.576,27.866C29.81,28.099,30,28.021,30,27.69V20.6C30,20.269,29.731,20.001,29.4,20.001Z"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                stroke-width="0"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path
                                                                fill="#333333" stroke="none"
                                                                d="M22.632,12.287C22.908,12.766,16.979000000000003,16.59,16.5,16.866S15.41,16.979,15.134,16.5C14.858,16.021,15.022,15.411,15.5,15.134S22.355,11.809,22.632,12.287Z"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                stroke-width="0"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path
                                                                fill="#333333" stroke="none"
                                                                d="M17.082,15.375C17.428,15.973,17.223,16.737000000000002,16.625,17.083C16.028,17.427999999999997,15.263,17.223,14.917,16.625S10.403,7.685,11,7.34C11.598,6.995,16.737,14.777,17.082,15.375Z"
                                                                stroke-width="0"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path></svg></span>
                                                8 minutes ago
                                            </small>
                                        </li>
                                        <li>
                                            <i class="livicon bg-aqua" data-n="hand-right" data-s="20" data-c="white"
                                               data-hc="white" id="livicon-13" style="width: 20px; height: 20px;">
                                                <svg height="20" version="1.1" width="20"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                     id="canvas-for-livicon-13"
                                                     style="overflow: hidden; position: relative;">
                                                    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created
                                                        with Raphaël 2.1.2
                                                    </desc>
                                                    <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
                                                    <path fill="#ffffff" stroke="none"
                                                          d="M16.361,25.192C16.82,25.188,20.1,25.202,20.055,22.422C20.801,21.902,21.355999999999998,20.516000000000002,20.881,19.348C21.799,18.501,22,17.001,21,16H26C27.104,16,28,15.058,28,14C28,12.941,27.104,11.998000000000001,26,12L17.537,12.025C18.172,10.756,18.746,9.395,18.587999999999997,8.192C18.463,7.233,18,6,16,6C16,6,12.64,12.038,9,12.89V12H5.2C4.537,12,4,12.537,4,13.2V23.000999999999998C4,23.665,4.537,24.000999999999998,5.2,24.000999999999998H9V23.136C12.398,23.663,14.548,25.2,16.361,25.192ZM7,23.001V13H8V23.000999999999998H7Z"
                                                          transform="matrix(0.625,0,0,0.625,0,0)" stroke-width="0"
                                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                                </svg>
                                            </i>
                                            <a href="#">New friend request</a>
                                            <small class="pull-right">
                                                <span class="livicon paddingright_10" data-n="timer" data-s="10"
                                                      id="livicon-14" style="width: 10px; height: 10px;"><svg
                                                            height="10" version="1.1" width="10"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                                            id="canvas-for-livicon-14"
                                                            style="overflow: hidden; position: relative;"><desc
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.2</desc><defs
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path
                                                                fill="#333333" stroke="none"
                                                                d="M29.4,20.001H22.31C21.98,20.001,21.901999999999997,20.19,22.134,20.425L24.574,22.865000000000002C22.559,25.379,19.474,27,16,27C9.925,27,5,22.075,5,16S9.925,5,16,5C21.874,5,26.673000000000002,9.604,26.984,15.401C27.002,15.732,27.269,16,27.6,16H29.4C29.730999999999998,16,30.000999999999998,15.731,29.987,15.4C29.673,7.946,23.531,2,16,2C8.268,2,2,8.268,2,16S8.268,30,16,30C20.302,30,24.148,28.06,26.717,25.006999999999998L29.576,27.866C29.81,28.099,30,28.021,30,27.69V20.6C30,20.269,29.731,20.001,29.4,20.001Z"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                stroke-width="0"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path
                                                                fill="#333333" stroke="none"
                                                                d="M22.632,12.287C22.908,12.766,16.979000000000003,16.59,16.5,16.866S15.41,16.979,15.134,16.5C14.858,16.021,15.022,15.411,15.5,15.134S22.355,11.809,22.632,12.287Z"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                stroke-width="0"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path
                                                                fill="#333333" stroke="none"
                                                                d="M17.082,15.375C17.428,15.973,17.223,16.737000000000002,16.625,17.083C16.028,17.427999999999997,15.263,17.223,14.917,16.625S10.403,7.685,11,7.34C11.598,6.995,16.737,14.777,17.082,15.375Z"
                                                                stroke-width="0"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path></svg></span>
                                                An hour ago
                                            </small>
                                        </li>
                                        <li>
                                            <i class="livicon danger" data-n="shopping-cart-in" data-s="20"
                                               data-c="white" data-hc="white" id="livicon-15"
                                               style="width: 20px; height: 20px;">
                                                <svg height="20" version="1.1" width="20"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                     id="canvas-for-livicon-15"
                                                     style="overflow: hidden; position: relative;">
                                                    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created
                                                        with Raphaël 2.1.2
                                                    </desc>
                                                    <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
                                                    <path fill="#ffffff" stroke="none"
                                                          d="M12.628,8C11.959,8,11.799,8.383,12.273,8.857L17.143,13.643999999999998C17.617,14.118999999999998,18.385,14.118999999999998,18.86,13.643999999999998L23.727,8.857C24.202,8.382,24.041,8,23.372,8H20V2.6C20,2.269,19.731,2,19.4,2H16.599999999999998C16.269,2,16,2.269,16,2.6V8H12.628Z"
                                                          stroke-width="0" transform="matrix(0.625,0,0,0.625,0,0)"
                                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                                    <path fill="#ffffff" stroke="none"
                                                          d="M14,27C14,28.105,13.104,29,12,29S10,28.105,10,27S10.896,25,12,25S14,25.895,14,27ZM24,25C22.895,25,22,25.895,22,27S22.895,29,24,29S26,28.105,26,27S25.105,25,24,25ZM29.4,8H26L25.333,10H27.604L26.493,15H8.447L7.5249999999999995,10H10.437999999999999L10,8H7.193L6.73,5.184C6.622,4.53,5.997,4,5.334,4H1.3339999999999996C0.7819999999999996,4,0.33399999999999963,4.448,0.33399999999999963,5S0.7819999999999996,6,1.3339999999999996,6H4.042C4.373,6,4.69,6.264,4.75,6.59L7.873,22.821C7.998,23.473,8.637,24,9.3,24H25.799C26.13,24,26.457,23.737,26.529,23.414L26.713,22.586000000000002C26.785,22.263,26.575,22,26.244,22H10.26C9.927999999999999,22,9.612,21.735,9.552999999999999,21.409L9.3,20H26.735C27.066,20,27.393,19.737,27.464,19.414L29.87,8.586C29.941,8.262,29.731,8,29.4,8ZM25.828,18H9L8.631,16H26.273L25.828,18Z"
                                                          stroke-width="0" transform="matrix(0.625,0,0,0.625,0,0)"
                                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                                </svg>
                                            </i>
                                            <a href="#">On sale 2 products</a>
                                            <small class="pull-right">
                                                <span class="livicon paddingright_10" data-n="timer" data-s="10"
                                                      id="livicon-16" style="width: 10px; height: 10px;"><svg
                                                            height="10" version="1.1" width="10"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                                            id="canvas-for-livicon-16"
                                                            style="overflow: hidden; position: relative;"><desc
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.2</desc><defs
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path
                                                                fill="#333333" stroke="none"
                                                                d="M29.4,20.001H22.31C21.98,20.001,21.901999999999997,20.19,22.134,20.425L24.574,22.865000000000002C22.559,25.379,19.474,27,16,27C9.925,27,5,22.075,5,16S9.925,5,16,5C21.874,5,26.673000000000002,9.604,26.984,15.401C27.002,15.732,27.269,16,27.6,16H29.4C29.730999999999998,16,30.000999999999998,15.731,29.987,15.4C29.673,7.946,23.531,2,16,2C8.268,2,2,8.268,2,16S8.268,30,16,30C20.302,30,24.148,28.06,26.717,25.006999999999998L29.576,27.866C29.81,28.099,30,28.021,30,27.69V20.6C30,20.269,29.731,20.001,29.4,20.001Z"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                stroke-width="0"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path
                                                                fill="#333333" stroke="none"
                                                                d="M22.632,12.287C22.908,12.766,16.979000000000003,16.59,16.5,16.866S15.41,16.979,15.134,16.5C14.858,16.021,15.022,15.411,15.5,15.134S22.355,11.809,22.632,12.287Z"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                stroke-width="0"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path
                                                                fill="#333333" stroke="none"
                                                                d="M17.082,15.375C17.428,15.973,17.223,16.737000000000002,16.625,17.083C16.028,17.427999999999997,15.263,17.223,14.917,16.625S10.403,7.685,11,7.34C11.598,6.995,16.737,14.777,17.082,15.375Z"
                                                                stroke-width="0"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path></svg></span>
                                                3 Hours ago
                                            </small>
                                        </li>
                                        <li>
                                            <i class="livicon success" data-n="image" data-s="20" data-c="white"
                                               data-hc="white" id="livicon-17" style="width: 20px; height: 20px;">
                                                <svg height="20" version="1.1" width="20"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                     id="canvas-for-livicon-17"
                                                     style="overflow: hidden; position: relative;">
                                                    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created
                                                        with Raphaël 2.1.2
                                                    </desc>
                                                    <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
                                                    <path fill="#ffffff" stroke="none"
                                                          d="M6,24L8,18L10,20L16,14L18,16L21.664,10.75L26,18V24H6ZM9,10C7.343,10,6,11.343,6,13S7.343,16,9,16S12,14.657,12,13S10.657,10,9,10Z"
                                                          opacity="1" stroke-width="0"
                                                          transform="matrix(0.625,0,0,0.625,0,0)"
                                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path>
                                                    <path fill="#ffffff" stroke="none"
                                                          d="M19.2,24C19.249,23.66,19.258,23.602,19.258,23.232C19.258,22.733,19.207,22.253999999999998,19.122,21.814C19.608,22.25,20.076999999999998,22.429,20.524,22.19C21.52,21.671000000000003,21.392,19.682000000000002,20.236,17.742C19.740000000000002,16.906000000000002,19.132,16.229,18.522000000000002,15.756C17.811000000000003,16.241,16.944000000000003,16.537,16.001,16.537C15.055000000000001,16.537,14.188,16.241,13.478000000000002,15.755999999999998C12.868000000000002,16.229,12.261000000000001,16.906,11.763000000000002,17.741999999999997C10.607000000000001,19.682,10.480000000000002,21.671999999999997,11.478000000000002,22.189999999999998C11.924000000000001,22.424999999999997,12.394000000000002,22.249999999999996,12.879000000000001,21.813999999999997C12.794,22.253999999999998,12.744000000000002,22.732999999999997,12.744000000000002,23.231999999999996C12.744000000000002,23.567999999999994,12.761000000000001,23.687999999999995,12.8,23.999999999999996H19.2ZM16,15.779C17.92,15.779,19.477,14.328,19.477,12.539C19.477,10.75,17.92,9.3,16,9.3C14.079,9.3,12.522,10.751000000000001,12.522,12.540000000000001S14.079,15.779,16,15.779ZM22.985,17.833C24.561,17.833,25.839,16.642999999999997,25.839,15.172999999999998C25.839,13.704999999999998,24.563,12.512999999999998,22.985,12.512999999999998S20.13,13.703999999999999,20.13,15.172999999999998C20.13,16.643,21.407,17.833,22.985,17.833ZM25.7,24C25.7,23.588,25.618,23.149,25.548,22.788C25.945999999999998,23.146,26.331999999999997,23.294,26.698999999999998,23.096C27.517999999999997,22.674,27.410999999999998,21.038,26.464,19.446C26.055999999999997,18.759,25.555999999999997,18.204,25.057,17.816000000000003C24.473,18.213,23.759999999999998,18.456000000000003,22.987,18.456000000000003C22.372999999999998,18.456000000000003,21.802,18.301000000000002,21.299,18.039C21.703,18.866,21.951999999999998,19.706000000000003,21.999,20.455000000000002C22.075,21.603,21.689,22.455000000000002,20.918999999999997,22.852C20.752999999999997,22.94,20.572999999999997,22.990000000000002,20.388999999999996,23.021C20.343,23.317,20.3,23.672,20.3,24H25.7ZM9.016,17.833C10.592,17.833,11.870000000000001,16.642999999999997,11.870000000000001,15.172999999999998C11.870000000000001,13.704999999999998,10.592,12.512999999999998,9.016000000000002,12.512999999999998C7.439000000000002,12.512999999999998,6.160000000000002,13.703999999999999,6.160000000000002,15.172999999999998C6.16,16.643,7.438,17.833,9.016,17.833ZM11.7,24C11.7,23.67,11.657,23.315,11.610999999999999,23.019C11.427,22.988999999999997,11.245999999999999,22.939999999999998,11.075,22.848C9.780999999999999,22.177999999999997,9.661,20.179,10.703,18.037C10.200999999999999,18.3,9.629,18.455,9.014999999999999,18.455C8.238,18.455,7.525999999999999,18.212999999999997,6.942999999999999,17.814999999999998C6.442999999999999,18.203,5.943999999999999,18.758,5.533999999999999,19.444999999999997C4.5859999999999985,21.037999999999997,4.481999999999999,22.673999999999996,5.300999999999999,23.094999999999995C5.667999999999999,23.288999999999994,6.052,23.143999999999995,6.449999999999999,22.786999999999995C6.381,23.149,6.4,23.588,6.4,24H11.7Z"
                                                          opacity="0" stroke-width="0"
                                                          transform="matrix(0.625,0,0,0.625,0,0)"
                                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path>
                                                    <path fill="#ffffff" stroke="none"
                                                          d="M5.5,18.791L7.356,17.936L9.975,19.219L13.033,17.188000000000002L7.954999999999999,14.782000000000002L9.758,13.017000000000001L19.067999999999998,14.434000000000001C19.067999999999998,14.434000000000001,22.424999999999997,12.001000000000001,24.773999999999997,12.001000000000001C27.557999999999996,12.001000000000001,26.136999999999997,13.337000000000002,26.136999999999997,13.337000000000002C19.312,20.023,9.759,22,9.759,22C7.467,20.797,5.5,18.791,5.5,18.791Z"
                                                          opacity="0" stroke-width="0"
                                                          transform="matrix(0.625,0,0,0.625,-3.75,0)"
                                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path>
                                                    <path fill="#ffffff" stroke="none"
                                                          d="M2,7.2V26.8C2,27.463,2.537,28,3.2,28H28.8C29.463,28,30,27.463,30,26.8V7.2C30,6.537,29.463,6,28.8,6H3.2C2.537,6,2,6.537,2,7.2ZM28,26H4V8H28V26Z"
                                                          stroke-width="0" transform="matrix(0.625,0,0,0.625,0,0)"
                                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                                </svg>
                                            </i>
                                            <a href="#">Lee Shared your photo</a>
                                            <small class="pull-right">
                                                <span class="livicon paddingright_10" data-n="timer" data-s="10"
                                                      id="livicon-18" style="width: 10px; height: 10px;"><svg
                                                            height="10" version="1.1" width="10"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                                            id="canvas-for-livicon-18"
                                                            style="overflow: hidden; position: relative;"><desc
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.2</desc><defs
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path
                                                                fill="#333333" stroke="none"
                                                                d="M29.4,20.001H22.31C21.98,20.001,21.901999999999997,20.19,22.134,20.425L24.574,22.865000000000002C22.559,25.379,19.474,27,16,27C9.925,27,5,22.075,5,16S9.925,5,16,5C21.874,5,26.673000000000002,9.604,26.984,15.401C27.002,15.732,27.269,16,27.6,16H29.4C29.730999999999998,16,30.000999999999998,15.731,29.987,15.4C29.673,7.946,23.531,2,16,2C8.268,2,2,8.268,2,16S8.268,30,16,30C20.302,30,24.148,28.06,26.717,25.006999999999998L29.576,27.866C29.81,28.099,30,28.021,30,27.69V20.6C30,20.269,29.731,20.001,29.4,20.001Z"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                stroke-width="0"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path
                                                                fill="#333333" stroke="none"
                                                                d="M22.632,12.287C22.908,12.766,16.979000000000003,16.59,16.5,16.866S15.41,16.979,15.134,16.5C14.858,16.021,15.022,15.411,15.5,15.134S22.355,11.809,22.632,12.287Z"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                stroke-width="0"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path
                                                                fill="#333333" stroke="none"
                                                                d="M17.082,15.375C17.428,15.973,17.223,16.737000000000002,16.625,17.083C16.028,17.427999999999997,15.263,17.223,14.917,16.625S10.403,7.685,11,7.34C11.598,6.995,16.737,14.777,17.082,15.375Z"
                                                                stroke-width="0"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path></svg></span>
                                                Yesterday
                                            </small>
                                        </li>
                                        <li>
                                            <i class="livicon warning" data-n="thumbs-up" data-s="20" data-c="white"
                                               data-hc="white" id="livicon-19" style="width: 20px; height: 20px;">
                                                <svg height="20" version="1.1" width="20"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                     id="canvas-for-livicon-19"
                                                     style="overflow: hidden; position: relative;">
                                                    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created
                                                        with Raphaël 2.1.2
                                                    </desc>
                                                    <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
                                                    <path fill="#ffffff" stroke="none"
                                                          d="M20.387,28.53C21.01,28.522000000000002,25.852,29.341,25.787,25.577C27.394,25.026000000000003,27.871,22.475,26.977999999999998,21.391000000000002C28.644,20.059,28.040999999999997,17.557000000000002,26.81,16.642000000000003L26.81,16.642000000000003C28.31,15.974000000000004,27.999,12.141000000000002,24.9,12.104000000000003L21.586,12.136000000000003C22.445,10.417000000000003,23.729,7.807000000000003,23.648999999999997,5.9570000000000025C23.589999999999996,4.619000000000002,23.060999999999996,2.6410000000000027,20.624,3.0550000000000024C20.624,3.0550000000000024,19.592,5.617000000000003,17.695999999999998,8.176000000000002C16.029999999999998,10.426000000000002,13.697999999999997,12.674000000000003,10.813999999999998,13.179000000000002V11.974000000000002H5.667C4.771,11.974000000000002,4.041,12.700000000000003,4.041,13.600000000000001V26.618000000000002C4.041,27.518,4.7700000000000005,27.975,5.667,27.975H10.812999999999999V26.803C15.42,27.519,17.93,28.544,20.387,28.53ZM8.105,26.618V13.327H9.659V26.618000000000002H8.105Z"
                                                          stroke-width="0" transform="matrix(0.625,0,0,0.625,0,0)"
                                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                                </svg>
                                            </i>
                                            <a href="#">David liked your photo</a>
                                            <small class="pull-right">
                                                <span class="livicon paddingright_10" data-n="timer" data-s="10"
                                                      id="livicon-20" style="width: 10px; height: 10px;"><svg
                                                            height="10" version="1.1" width="10"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                                            id="canvas-for-livicon-20"
                                                            style="overflow: hidden; position: relative;"><desc
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.2</desc><defs
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path
                                                                fill="#333333" stroke="none"
                                                                d="M29.4,20.001H22.31C21.98,20.001,21.901999999999997,20.19,22.134,20.425L24.574,22.865000000000002C22.559,25.379,19.474,27,16,27C9.925,27,5,22.075,5,16S9.925,5,16,5C21.874,5,26.673000000000002,9.604,26.984,15.401C27.002,15.732,27.269,16,27.6,16H29.4C29.730999999999998,16,30.000999999999998,15.731,29.987,15.4C29.673,7.946,23.531,2,16,2C8.268,2,2,8.268,2,16S8.268,30,16,30C20.302,30,24.148,28.06,26.717,25.006999999999998L29.576,27.866C29.81,28.099,30,28.021,30,27.69V20.6C30,20.269,29.731,20.001,29.4,20.001Z"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                stroke-width="0"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path
                                                                fill="#333333" stroke="none"
                                                                d="M22.632,12.287C22.908,12.766,16.979000000000003,16.59,16.5,16.866S15.41,16.979,15.134,16.5C14.858,16.021,15.022,15.411,15.5,15.134S22.355,11.809,22.632,12.287Z"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                stroke-width="0"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path
                                                                fill="#333333" stroke="none"
                                                                d="M17.082,15.375C17.428,15.973,17.223,16.737000000000002,16.625,17.083C16.028,17.427999999999997,15.263,17.223,14.917,16.625S10.403,7.685,11,7.34C11.598,6.995,16.737,14.777,17.082,15.375Z"
                                                                stroke-width="0"
                                                                transform="matrix(0.3125,0,0,0.3125,0,0)"
                                                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path></svg></span>
                                                2 July 2014
                                            </small>
                                        </li>
                                    </ul>
                                    <div class="slimScrollBar"
                                         style="background: rgb(0, 0, 0); width: 3px; position: absolute; top: 96px; opacity: 0.4; display: block; border-radius: 0px; z-index: 99; right: 1px; height: 103.896px;"></div>
                                    <div class="slimScrollRail"
                                         style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 0px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                                </div>
                            </li>
                            <li class="footer">
                                <a href="#">View all</a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle user_nav_link" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="img_avatar">
                                <img class="img_avatar__circle x25"
                                     alt=""
                                     src="{{ Auth::user()->profile->getAvatarUrl() }}">
                            </div>
                            {{ Auth::user()->getUsername() }}
                            <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            {{--<a class="Dropdown-item" href="{{ route('admin.home') }}">Admin</a>--}}
                            <a class="dropdown-item" href="{{ route('home') }}">SITE</a>
                            {{--<a class="Dropdown-item" href="{{ route('cabinet.home') }}">Cabinet</a>--}}
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>

            </ul>
        </div>
    </div>
</nav>