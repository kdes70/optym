@extends('layouts.app')

@section('title', $category->getName() . ' оптом')

{{--@yield('includes.nav_bar_top')--}}

@section('content')

    @include('layouts.partials.breadcrumbs')


    <div class="container">

        <div class="catalog_header" style="background-image: url({{$page->getBanner()}})">
            <div class="row catalog-name-wrapper">
                <div class="col-md-8">
                    <h1 class="catalog-name" data-cat-id="{{$category->id}}">{{$category->name}} оптом</h1>
                </div>
                <div class="col-sm-4 switch-btn__wrapper">
                </div>
            </div>
        </div>
    </div>

    <div class="container">

            @if($category->children->isNotEmpty())

                <div class="masonry-container pt-20">

                    @foreach($category->children as $children)

                        <div class="masonry-item catalog_children">
                            <div class="card">
                                <img
                                     src="{{$children->getImagePath()}}"
                                     height="180"
                                     alt="Card image cap">
                                <div class="card-body">

                                    <a class="catalog_title" href="{{route('category', ['path' => $children->getUrl()])}}">{{$children->getName()}}</a>

                                    @if($children->children->count() > 0)
                                        <ul>
                                            @foreach($children->children as $child)
                                                <li>
                                                    <a href="{{route('category', ['path' => $child->getUrl()])}}">{{$child->getName()}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif

                                </div>
                            </div>
                        </div>

                    @endforeach
                </div>


            @endif


    </div>

@endsection

@section('scripts')
{{--    <script type="text/javascript"></script>--}}
@endsection
