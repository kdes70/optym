@extends('layouts.app')

@section('title', $category->name. ' оптом')

@section('content')

    @include('layouts.partials.catalog_top')

    @include('company.goods.list')

@endsection

@section('scripts')
    <script type="text/javascript">

      // Initialize Masonry inside Foundation 5.5 Tab component
      // $(window).load(function () {
      //   $('.masonry-container').masonry({
      //     itemSelector: '.masonry-item'
      //   })
      // })

    </script>
@endsection
