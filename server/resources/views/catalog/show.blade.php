@extends('layouts.app')

@section('title', $page->getTitle())

@yield('includes.nav_bar_top')

@section('content')


    @include('layouts.partials.catalog_top')

    <div class="container">
        <div class="row">

            @if($children)

                @foreach($children as $category)

                    <div class="col-md-3">
                        <div class="card">
                            <img class="card-img-top"
                                 src="https://media.novosib.ru/CACHE/images/catalog/img/clothes_pqiBAmM/7bc294db7810b6340d3c5e896d8da9a5.png"
                                 alt="Card image cap">
                            <div class="card-body">
                                <a href="{{route('catalog.suppliers', ['category' => $category->getUrl()])}}">{{$category->getName()}}</a>
                                @if($category->children->count() > 0)
                                    <ul>
                                        @foreach($category->children as $child)
                                            <li>
                                                <a href="{{route('category', ['path' => $child->getUrl()])}}">{{$child->getName()}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif

                            </div>
                        </div>

                    </div>

                @endforeach

            @endif

        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">

      // Initialize Masonry inside Foundation 5.5 Tab component
      // $(window).load(function () {
      //   $('.masonry-container').masonry({
      //     itemSelector: '.masonry-item'
      //   })
      // })

    </script>
@endsection
