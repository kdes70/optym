<div class="partition-list__wrapper" style="max-height: 56px;">

    <div class="container partition-list">

        @isset($category)
        <a class="partition-list__item active"
           href="{{route('category', ['path' => $category->getUrl()])}}">
            <span>{{$category->name}}</span>
        </a>
        @endisset

        @if($siblings)
            @foreach($siblings as $sibling)
                <a class="partition-list__item"
                   href="{{route('category', ['path' => $sibling->getUrl()])}}">
                    <span>{{$sibling->getName()}}</span>
                </a>
            @endforeach
        @endif
    </div>
</div>