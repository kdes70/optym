@extends('layouts.app')

@section('content')
    @include('admin.nav')
    <activity :user="{{ auth()->user() }}"></activity>
@endsection