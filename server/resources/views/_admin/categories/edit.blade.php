@extends('layouts.admin')

@section('content')

    <form method="POST" action="{{ route('admin.categories.update', $category) }}">
        @csrf
        @method('PUT')


        <img class="avatar" v-if="imgDataUrl" :src="imgDataUrl" v-show="true" style="display: none;">

        {{--<img class="avatar" v-else="" width="200" height="200" alt="" src="{{ $user->profile->getAvatarUrl() }}">--}}


        {{--<file-input filename="files[]" fileid="avatar" classname="hidde-new-file"></file-input>--}}

        <div class="fileinput">
            <div class="fileinput__block">
                <div class="fileinput__button" @click="toggleShow">
                    <span><i class="fa fa-pencil" aria-hidden="true"></i>Изменить</span>
                </div>
            </div>
            <my-upload field="avatar"
                       @crop-success="cropSuccess"
                       @crop-upload-success="cropUploadSuccess"
                       @crop-upload-fail="cropUploadFail"
                       v-model="show_crop"
                       :width="200"
                       :height="200"
                       lang-type="ru"
                       url="/cabinet/profile/avatar"
                       :params="params"
                       :headers="headers"
                       img-format="png">

            </my-upload>
        </div>

        <div class="form-group">
            <label for="name" class="col-form-label">Name</label>
            <input id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $category->name) }}" required>
            @if ($errors->has('name'))
                <span class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="slug" class="col-form-label">Slug</label>
            <input id="slug" type="text" class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}" name="slug" value="{{ old('slug', $category->slug) }}" required>
            @if ($errors->has('slug'))
                <span class="invalid-feedback"><strong>{{ $errors->first('slug') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="parent" class="col-form-label">Parent</label>
            <select id="parent" class="form-control{{ $errors->has('parent') ? ' is-invalid' : '' }}" name="parent">
                <option value=""></option>
                @foreach ($parents as $parent)
                    <option value="{{ $parent->id }}"{{ $parent->id == old('parent', $category->parent_id) ? ' selected' : '' }}>
                        @for ($i = 0; $i < $parent->depth; $i++) &mdash; @endfor
                        {{ $parent->name }}
                    </option>
                @endforeach;
            </select>
            @if ($errors->has('parent'))
                <span class="invalid-feedback"><strong>{{ $errors->first('parent') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endsection