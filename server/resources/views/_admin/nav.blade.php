



<ul class="nav nav-tabs mb-3">
    <li class="nav-item"><a class="nav-link {{ (Route::currentRouteName() == 'admin.home') ? 'active' : ''  }}" href="{{ route('admin.home') }}">Dashboard</a></li>
    <li class="nav-item"><a class="nav-link  {{ (Route::currentRouteName() == 'admin.activities') ? 'active' : ''  }}" href="{{ route('admin.activities') }}">Activity log</a></li>
    {{--<li class="nav-item"><a class="nav-link" href="{{ route('admin.regions.index') }}">Regions</a></li>--}}
    <li class="nav-item"><a class="nav-link" href="{{ route('admin.categories.index') }}">Categories</a></li>
    <li class="nav-item"><a class="nav-link {{ (Route::currentRouteName() == 'admin.users.index') ? 'active' : ''  }}" href="{{ route('admin.users.index') }}">Users</a></li>
    <li class="nav-item"><a class="nav-link {{ (Route::currentRouteName() == 'admin.roles.index') ? 'active' : ''  }}" href="{{ route('admin.roles.index') }}">Users Roles</a></li>
</ul>