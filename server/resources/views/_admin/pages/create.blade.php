@extends('layouts.admin')

@section('content')

    <form method="POST" action="{{ route('admin.pages.store') }}" >
        @csrf

        <div class="form-group">
            <label for="title" class="col-form-label">Title</label>
            <input id="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required>
            @if ($errors->has('title'))
                <span class="invalid-feedback"><strong>{{ $errors->first('title') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="slug" class="col-form-label">Slug</label>
            <input id="slug" type="text" class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}" name="slug" value="{{ old('slug') }}" required>
            @if ($errors->has('slug'))
                <span class="invalid-feedback"><strong>{{ $errors->first('slug') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="category_id" class="col-form-label">Category</label>
            <select id="category_id" class="form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }}" name="category_id">
                <option value=""></option>
                @foreach ($categories as $category)
                    <optgroup label="{{ $category->name }}">
                        @foreach($category->children as $item)
                            <option value="{{ $item->id }}"{{ $item->id == old('category_id') ? ' selected' : '' }}>
                                {{ $item->name }}
                            </option>
                        @endforeach
                    </optgroup>
                @endforeach;
            </select>
            @if ($errors->has('category_id'))
                <span class="invalid-feedback"><strong>{{ $errors->first('category_id') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endsection