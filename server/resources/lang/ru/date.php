<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Date Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the date library. Each line can
    | have a singular and plural translation separated by a '|'.
    |
    */
    'ago'       => ':time назад',
    'from_now'  => 'через :time',
    'after'     => ':time после',
    'before'    => ':time до',
    'year'      => ':count год|:count года|:count лет',
    'month'     => ':count месяц|:count месяца|:count месяцев',
    'week'      => ':count неделю|:count недели|:count недель',
    'day'       => ':count день|:count дня|:count дней',
    'hour'      => ':count час|:count часа|:count часов',
    'minute'    => ':count минуту|:count минуты|:count минут',
    'second'    => ':count секунду|:count секунды|:count секунд',

    'january'   => 'января',
    'february'  => 'февраля',
    'march'     => 'марта',
    'april'     => 'апреля',
    'may'       => 'мая',
    'june'      => 'июня',
    'july'      => 'июля',
    'august'    => 'августа',
    'september' => 'сентября',
    'october'   => 'октября',
    'november'  => 'ноября',
    'december'  => 'декабря',

    'monday'    => 'понедельник',
    'tuesday'   => 'вторник',
    'wednesday' => 'среда',
    'thursday'  => 'четверг',
    'friday'    => 'пятница',
    'saturday'  => 'суббота',
    'sunday'    => 'воскресенье',
];
