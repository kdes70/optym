<?php

use App\Domain\Company\Enums\CompanyTypeEnum;
use App\Domain\User\Enums\UserStateEnum;
use App\Domain\User\Enums\UserTypeEnum;
use App\Enums\Domain\User\UserRoleEnum;

return [
    UserStateEnum::class   => [
        UserStateEnum::WAIT        => 'В ожидание',
        UserStateEnum::ACTIVE      => 'Активен',
        UserStateEnum::BANNED      => 'Забанен на время',
        UserStateEnum::INACTIVE    => 'Заблокирован',
        UserStateEnum::UNCONFIRMED => 'Не подтвержден',
    ],
    UserTypeEnum::class    => [
        UserTypeEnum::USER      => 'Пользователь',
        UserTypeEnum::WHOLESALE => 'Поставщик',
        UserTypeEnum::BUYER     => 'Покупатель',
    ],
    UserRoleEnum::class    => [
        UserRoleEnum::USER      => 'Пользователь',
        UserRoleEnum::ADMIN     => 'Администратор',
        UserRoleEnum::MODERATOR => 'Модератор',
        UserRoleEnum::WHOLESALE => 'Поставщик',
        UserRoleEnum::BUYER     => 'Покупатель',
    ],
    CompanyTypeEnum::class => [
        CompanyTypeEnum::RETAIL     => 'Розничная торговля',
        CompanyTypeEnum::WHOLESALE  => 'Оптовая торговля',
        CompanyTypeEnum::PRODUCTION => 'Производство товаров',
        CompanyTypeEnum::SERVICES   => 'Предоставление услуг',
    ]
];
