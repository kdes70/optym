<?php

use App\Domain\Company\Enums\CompanyTypeEnum;
use App\Domain\User\Enums\UserStateEnum;
use App\Domain\User\Enums\UserTypeEnum;

return [
    UserStateEnum::class   => [
        UserStateEnum::WAIT        => 'Wait',
        UserStateEnum::ACTIVE      => 'Active',
        UserStateEnum::BANNED      => 'Banned',
        UserStateEnum::INACTIVE    => 'Inactive',
        UserStateEnum::UNCONFIRMED => 'Unconfirmed',
    ],
    UserTypeEnum::class    => [
        UserTypeEnum::USER      => 'User',
        UserTypeEnum::WHOLESALE => 'Wholesale',
        UserTypeEnum::BUYER     => 'Buyer',
    ],
    CompanyTypeEnum::class => [
        CompanyTypeEnum::RETAIL     => 'Retail',
        CompanyTypeEnum::WHOLESALE  => 'Wholesale',
        CompanyTypeEnum::PRODUCTION => 'Production of goods',
        CompanyTypeEnum::SERVICES   => 'Provision of services',
    ]
];
