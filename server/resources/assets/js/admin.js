// require('./bootstrap')

window.Vue = require('vue')

import myUpload from 'vue-image-crop-upload'

const app_admin = new Vue({
  el: '#app_admin',
  data: () => ({
    show_crop: false,
    params: {
      token: window.token,
      name: 'avatar'
    },
    headers: {
      'X-XSRF-TOKEN': window.token
    },
    imgDataUrl: '', // the datebase64 url of created image

    pages: {
      title_selected: '',
      enabled: false
    },

    //type_attribyte: '',

  }),
  components: {
    'my-upload': myUpload
  },
  mounted () {
    // this.$nextTick(() => {
    //   this.sw = new Switchery(this.$refs.cb)
    // })
  },
  computed: {
    // геттер вычисляемого значения
    titleSelected: function () {
      // `this` указывает на экземпляр vm
      return (this.pages.title_selected.length) ? this.pages.title_selected.trim() + ' оптом' : ''
    }
  },
  methods: {

    // typeAttibute (event) {
    //   console.log(event.target.value)
    //   this.type_attribyte = event.target.value ? event.target.value : ''
    // },
    changeTitlePage (event) {

      this.pages.title_selected = event.target.options[event.target.options.selectedIndex].textContent.trim() // example: One
    },

    toggleShow () {
      this.show_crop = !this.show_crop
    },
    /**
     * crop success
     *
     * [param] imgDataUrl
     * [param] field
     */
    cropSuccess (imgDataUrl, field) {
      console.log('-------- crop success --------')
      this.imgDataUrl = imgDataUrl
    },
    /**
     * upload success
     *
     * [param] jsonData  server api return data, already json encode
     * [param] field
     */
    cropUploadSuccess (jsonData, field) {
      console.log('-------- upload success --------')
      console.log(jsonData)
      console.log('field: ' + field)
    },
    /**
     * upload fail
     *
     * [param] status    server api return error status, like 500
     * [param] field
     */
    cropUploadFail (status, field) {
      console.log('-------- upload fail --------')
      console.log(status)
      console.log('field: ' + field)
    }
  }
})

$(document).ready(function () {
  $('#sidebarCollapse').on('click', function () {
    $('#sidebar').toggleClass('active')
  })

  // var elem = document.querySelector('.js-switch')
  // var init = new Switchery(elem)
  //
  // init.element.checked = true; // or false or (item.active == 1)
  // init.setPosition();
})

