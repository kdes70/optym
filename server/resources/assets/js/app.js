require('./bootstrap')

window.Vue = require('vue')

import VueAxios from 'vue-axios';
import VueHead from 'vue-head'
import Alertify from './plugins/Alertify.js';
import ErrorsCatcher from './plugins/ErrorsCatcher.js';

Vue.use(VueAxios, axios);
Vue.use(VueHead)
Vue.use(Alertify);
Vue.use(ErrorsCatcher);

Vue.config.devtools = true
Vue.config.debug = true


window.apikey = process.env.MIX_YANDEX_API_KEY;
window.apikeyDadata = process.env.MIX_DADATA_API_KEY;
window.cookei_name = process.env.MIX_COOKIE_LOCATION;

// Vue.component('conversations', require('./components/chatter/Conversations'));
// Vue.component('chat-messages', require('./components/chatter/ChatMessages'));

// Vue.component('search-box', require('./components/SearchBox'));
// Vue.component('tree-select', require('./components/Treeselect'));

Vue.component('InvalidFeedback', require('./components/Utils/InvalidFeedback.vue').default);

Vue.component('catalog-drop', require('./components/Catalog/Catalog').default)

// Vue.component('RangeCostPrice', require('./components/Inputs/RangeCostPrice').default)
Vue.component('LoginForm', require('./components/Auth/LoginForm').default)
Vue.component('registration_form', require('./components/Auth/RegistrationForm').default)
Vue.component('ProductAdd', require('./pages/product/ProductAdd').default)
Vue.component('ProductList', require('./pages/product/ProductList').default)

import Masonry from 'masonry-layout'
import myUpload from 'vue-image-crop-upload'

//var token = readCookie('XSRF-TOKEN');

const app = new Vue({
    el: '#app',
    data: () => ({
        show_crop: false,
        params: {
            token: window.token,
            name: 'avatar'
        },
        headers: {
            'X-XSRF-TOKEN': window.token
        },
        imgDataUrl: '', // the datebase64 url of created image
        popover_open: false,
    }),
    components: {
        'my-upload': myUpload,
    },
    created: function () {
        window.addEventListener('click', this.close);

        var elem = document.querySelector('.masonry-container');

        if (elem) {
            var msnry = new Masonry(elem, {
                // options
                gutter: 10,
                itemSelector: '.masonry-item',
                columnWidth: 270,
                horizontalOrder: true,
                //percentPosition: true,
                fitWidth: true
            });
        }


    },
    beforeDestroy() {
        window.removeEventListener('click', this.close)
    },
    methods: {

        close(e) {
            if (!this.$el.contains(e.target)) {
                this.popover_open = false
            }
        },

        toggleShow() {
            this.show_crop = !this.show_crop
        },
        /**
         * crop success
         *
         * [param] imgDataUrl
         * [param] field
         */
        cropSuccess(imgDataUrl, field) {
            console.log('-------- crop success --------')
            this.imgDataUrl = imgDataUrl
        },
        /**
         * upload success
         *
         * [param] jsonData  server api return data, already json encode
         * [param] field
         */
        cropUploadSuccess(jsonData, field) {
            console.log('-------- upload success --------')
            console.log(jsonData)
            console.log('field: ' + field)
        },
        /**
         * upload fail
         *
         * [param] status    server api return error status, like 500
         * [param] field
         */
        cropUploadFail(status, field) {
            console.log('-------- upload fail --------')
            console.log(status)
            console.log('field: ' + field)
        }
    }
})

// import masonry from "masonry";

import Inputmask from 'inputmask'

Inputmask({'mask': '+7(999)999-99-99', showMaskOnFocus: true}).mask('#phone')

// $(".heading-compose").click(function () {
//     $(".side-two").css({
//         "left": "0"
//     });
// });
//
// $(".newMessage-back").click(function () {
//     $(".side-two").css({
//         "left": "-100%"
//     });
// });

//
// $(document).ready(function () {
//     $('.forgot-pass').click(function(event) {
//         $(".pr-wrap").toggleClass("show-pass-reset");
//     });
//
//     $('.pass-reset-submit').click(function(event) {
//         $(".pr-wrap").removeClass("show-pass-reset");
//     });
// });
