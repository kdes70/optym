
  export default {
    data: {
      pages: {
        title_selected: '',
        enabled: false
      }
    },
    // watch: {
    //   list(list) {
    //     axios.put('/api/lists', { list }, { headers: { 'X-CSRF-TOKEN': this.csrfToken } });
    //   }
    // },
    mounted () {
      this.$nextTick(() => {
        this.sw = new Switchery(this.$refs.cb)
      })
    },
    computed: {
      // геттер вычисляемого значения
      titleSelected: function () {
        // `this` указывает на экземпляр vm
        return this.pages.title_selected.trim() + ' оптом'
      }
    },
    methods: {
      changeTitlePage (event) {
        this.pages.title_selected = event.target.options[event.target.options.selectedIndex].textContent.trim() // example: One
      },
    }
  }
