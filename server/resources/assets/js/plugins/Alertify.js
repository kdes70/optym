import alertify from 'alertifyjs'

import 'alertifyjs/build/css/alertify.min.css'

const VueAlertify = {
    install (Vue) {
        window.alertify = Vue.alertify = alertify
        Object.defineProperty(Vue.prototype, '$alertify', {
            get () {
                return alertify
            },
        })
    },
}

export default VueAlertify
