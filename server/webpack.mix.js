let mix = require('laravel-mix')
const { VueLoaderPlugin } = require('vue-loader')

// mix.setPublicPath(path.join(__dirname, "/dist"))
mix.setPublicPath(path.normalize('public_html'))
// frontend
mix.js('resources/assets/js/app.js', 'js')
    .sass('resources/assets/sass/app.scss', 'css')
    .sass('resources/assets/sass/admin-theme.scss', 'css/admin')
    // admin thems vendor
    .js('resources/assets/js/admin.js', 'js')
    .autoload({
        jquery: ['$', 'window.jQuery', 'jQuery']
    })
    .sourceMaps()
    .version()

mix.copyDirectory('node_modules/font-awesome/fonts', 'public_html/fonts/font-awesome')

mix.webpackConfig({
    plugins: [
        new VueLoaderPlugin()
    ],
})
