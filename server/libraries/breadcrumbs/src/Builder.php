<?php

namespace Kdes70\Breadcrumbs;

use App\Domain\Catalog\Category\Category;
use Illuminate\Support\Collection;

class Builder
{
    /** @var array */
    protected $config = [];

    /** @var Collection */
    protected $segments;

    public function __construct(array $config)
    {
        $this->config = $config;
        $this->segments = new Collection();
    }

    public function append($title, $link, $disabled = false, array $options = [])
    {
        $this->segments->push(new Segment($title, $link, $disabled, $options));
    }

    public function prepend($title, $link, $disabled = false, array $options = [])
    {
        $this->segments->prepend(new Segment($title, $link, $disabled, $options));
    }

    public function addHierarchyOfNeeds($categories, $route)
    {
        $chains = [];

        /** @var Category $category */
        foreach ($categories as $category) {

            if ($category->isRoot() === false) {
                $chains[] = [
                    'self' => array_except($category->toArray(), ['pivot']),
                    'parents' => $this->getNotRoot($category),
                ];
            }

        }


        if (!empty($chains)) {
            usort($chains, function (array $a, array $b) {
                return count($b['parents']) <=> count($a['parents']);
            });

            $crumbs = array_shift($chains);

            $crumbs = array_merge($crumbs['parents'], [$crumbs['self']]);

            if (count($crumbs) > 3) {
                $crumbs = $crumbs[0];
            }

            foreach ($crumbs as $crumb) {

                $this->append($crumb['name'], route($route, $crumb['path']));
            }
        }
    }

    public function getNotRoot(Category $category)
    {
        $category_parents = [];

        foreach ($category->getAncestors() as $item) {
            if (!$item->isRoot()) {
                $category_parents[] = $item->toArray();
            }
        }

        return $category_parents;
    }

    public function clear()
    {
        $this->segments = new Collection();
    }

    /**
     * @return Segment[]
     */
    public function all()
    {
        return $this->segments->all();
    }
}
