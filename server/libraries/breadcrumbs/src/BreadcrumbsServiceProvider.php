<?php

namespace Kdes70\Breadcrumbs;

use Illuminate\Support\ServiceProvider;

class BreadcrumbsServiceProvider extends ServiceProvider
{
    /** @var \Illuminate\Foundation\Application */
    protected $app;

    public function register()
    {
        $this->app->singleton('app.breadcrumbs', function() {
            return new Builder([]);
        });

        $this->app->alias('app.breadcrumbs', Builder::class);
    }

    public function provides()
    {
        return ['app.breadcrumbs', Builder::class];
    }
}
