<?php

namespace Kdes70\Breadcrumbs;

use Illuminate\Support\Facades\Facade;

class BreadcrumbsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'app.breadcrumbs';
    }
}
