<?php

namespace Kdes70\Breadcrumbs;

class Segment
{
    protected $title;
    protected $link;
    protected $disabled;
    protected $options;

    public function __construct($title, $link, $disabled = false, array $options = [])
    {
        $this->title    = $title;
        $this->link     = $link;
        $this->disabled = $disabled;
        $this->options  = $options;
    }

    /**
     * @return boolean
     */
    public function isActive(): bool
    {
        return !($this->disabled || empty($this->link));
    }

    /**
     * @return boolean
     */
    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    /**
     * @return string
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @return int|string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }
}
