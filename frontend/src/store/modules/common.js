import { GEO_GET_LIST, REGISTER_FORM, REGISTER_FORM_FAIL, REGISTER_FORM_SUCCESS } from '../types/common'
import AuthService from '../../services/auth.service'

export default {
  state: {
    geo: {
      countries: [],
      regions: [],
      cities: [],
    },
    registration: {
      user_types: [],
      company_types: [],
    },
  },
  actions: {
    [GEO_GET_LIST]: ({ commit }) => {

    },
    [REGISTER_FORM]: ({ commit }) => {
      return AuthService.registerFormData()
      .then(
        response => {
          commit(REGISTER_FORM_SUCCESS, response.data)
          return Promise.resolve(response.data)
        },
        error => {
          commit(REGISTER_FORM_FAIL)
          return Promise.reject(error)
        }
      )
    },
  },
  mutations: {
    [GEO_GET_LIST]: (state, geo) => {
      state.geo.countries = geo.countries
      state.geo.regions = geo.regions
      state.geo.cities = geo.cities
    },
    [REGISTER_FORM_SUCCESS]: (state, data) => {
      state.registration.user_types = data.user_types
      state.registration.company_types = data.company_types
    },
    [REGISTER_FORM_FAIL]: (state) => {
      state.registration.user_types = []
      state.registration.company_types = []
    },
  }
}
