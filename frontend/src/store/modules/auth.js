import AuthService from '../../services/auth.service'
import {
  AUTH_LOGIN,
  AUTH_LOGIN_FAIL,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGOUT,
  AUTH_REGISTER,
  AUTH_REGISTER_FAIL, AUTH_REGISTER_FORM, AUTH_REGISTER_FORM_FAIL, AUTH_REGISTER_FORM_SUCCESS, AUTH_REGISTER_SUCCESS
} from '../types/auth'

const user = JSON.parse(localStorage.getItem('user'))
const initialState = user
  ? { status: { loggedIn: true }, user }
  : { status: { loggedIn: false }, user: null }

export default {
  state: initialState,
  getters: {},
  actions: {
    [AUTH_LOGIN]: ({ commit }, user) => {
      return AuthService.login(user).then(
        user => {
          commit(AUTH_LOGIN_SUCCESS, user)
          return Promise.resolve(user)
        },
        error => {
          commit(AUTH_LOGIN_FAIL)
          return Promise.reject(error)
        }
      )
    },
    [AUTH_LOGOUT]: ({ commit }) => {
      AuthService.logout()
      commit(AUTH_LOGOUT)
    },
    [AUTH_REGISTER]: ({ commit }, user) => {
      return AuthService.register(user).then(
        response => {
          commit(AUTH_REGISTER_SUCCESS)
          return Promise.resolve(response.data)
        },
        error => {
          commit(AUTH_REGISTER_FAIL)
          return Promise.reject(error)
        }
      )
    }
  },
  mutations: {
    [AUTH_LOGIN_SUCCESS]: (state, user) => {
      state.status.loggedIn = true
      state.user = user
    },
    [AUTH_LOGIN_FAIL]: (state) => {
      state.status.loggedIn = false
      state.user = null
    },
    [AUTH_LOGOUT]: (state) => {
      state.status.loggedIn = false
      state.user = null
    },
    [AUTH_REGISTER_SUCCESS]: (state) => {
      state.status.loggedIn = false
    },
    [AUTH_REGISTER_FAIL]: (state) => {
      state.status.loggedIn = false
    }
  }
}
