export default class Company {
  constructor (company_name, company_types, country, region, city) {
    this.company_name = company_name
    this.company_types = company_types
    this.country = country
    this.region = region
    this.city = city
  }
}
