export default class User {
  constructor (type, first_name, last_name, email, password) {
    this.type = type
    this.first_name = first_name
    this.last_name = last_name
    this.email = email
    this.password = password
  }
}
