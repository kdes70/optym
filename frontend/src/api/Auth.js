import request from '../plugins/request'

export function ApiLogin (username, password) {
  return request.post('auth/login', {
    username: username,
    password: password
  })
}

export function ApiRegisterFormData () {
  return request.get('auth/register')
}

export function ApiRegister (type, first_name, last_name, email, password) {
  return request.post('auth/register', {
    type: type,
    first_name: first_name,
    last_name: last_name,
    email: email,
    password: password
  })
}
