import axios from 'axios'
import { vm } from '@/main.js'

// create axios instance
const api = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
  headers: {
    Accept: `application/json, text/plain, */*`,
    'Content-type': `application/json;charset=utf-8`
  },
  withCredentials: true
})

// Add a response interceptor
// api.interceptors.response.use(
//   response => {
//
//     return response
//   },
//   error => {
//
//     if (error.response.status === 401) {
//
//       vm.$router.push({ name: 'auth.login' })
//
//     } else if (error.response.status === 422) {
//
//       if (error.response.data.errors) {
//
//         for (let key in error.response.data.errors) {
//
//           vm.$validator.errors.add({ field: key, msg: error.response.data.errors[key] })
//         }
//       }
//
//     } else {
//
//       console.error(error)
//     }
//
//     return Promise.reject(error)
//   })

// request interceptor
// api.interceptors.request.use(
//   config => {
//     const token = getToken()
//
//     if (token) {
//       // eslint-disable-next-line no-param-reassign
//       config.headers.authorization = makeTokenHeader(token)
//     }
//     return config
//   },
//   error => {
//     Promise.reject(error)
//   }
// )

// api.interceptors.response.use(response => {
//   return response
// }, error => {
//
//   // Pass all non 401s back to the caller.
//   if (error.response && error.response.status !== undefined && error.response.status === 401) {
//     // if you ever get an unauthorized, logout the user
//     // if (process.env.VUE_APP_API_URL !== 'http://localhost:8080') {
//     //   return store.dispatch(AUTH_LOGOUT)
//     // }
//     return store.dispatch(AUTH_CLEAR)
//   }
//
//   return new Promise((resolve, reject) => {
//     reject(error)
//   })
// })
//
export default api

