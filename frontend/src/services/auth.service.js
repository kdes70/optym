import { ApiLogin, ApiRegister, ApiRegisterFormData } from '../api/Auth'

// const API_URL = 'http://localhost:8080/api/auth/'

class AuthService {
  login (user) {
    return ApiLogin(user.username, user.password)
    .then(response => {
      if (response.data.accessToken) {
        localStorage.setItem('user', JSON.stringify(response.data))
      }

      return response.data
    })
  }

  logout () {
    localStorage.removeItem('user')
  }

  registerFormData (user) {
    return ApiRegisterFormData()
  }

  register (user) {
    return ApiRegister(user.type, user.first_name, user.last_name, user.email, user.password)
  }
}

export default new AuthService()
